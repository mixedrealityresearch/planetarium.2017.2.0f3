﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData[]
struct MeshDataU5BU5D_t2194650048;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t2778389198;
// HoloToolkit.Unity.SurfaceMeshesToPlanes
struct SurfaceMeshesToPlanes_t2358468568;
// HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_t1668648882;
// System.Collections.Generic.List`1<UnityEngine.MeshFilter>
struct List_1_t2396058581;
// System.Threading.Tasks.Task`1<HoloToolkit.Unity.BoundedPlane[]>
struct Task_1_t4230515705;
// HoloToolkit.Unity.BoundedPlane[]
struct BoundedPlaneU5BU5D_t815519402;
// System.Collections.Generic.List`1<HoloToolkit.Unity.PlaneFinding/MeshData>
struct List_1_t880011750;
// System.Comparison`1<UnityEngine.GameObject>
struct Comparison_1_t3018271998;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2022420531;
// System.Text.EncoderFallback
struct EncoderFallback_t1756452756;
// System.Text.DecoderFallback
struct DecoderFallback_t1715117820;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// HoloToolkit.Unity.RemoveSurfaceVertices
struct RemoveSurfaceVertices_t3740445364;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.EventArgs
struct EventArgs_t3289624707;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// HoloToolkit.Unity.HandsManager
struct HandsManager_t3572349227;
// HoloToolkit.Unity.GazeManager
struct GazeManager_t2256266604;
// HoloToolkit.Unity.GestureManager
struct GestureManager_t1645247838;
// HoloToolkit.Unity.SpatialMappingManager
struct SpatialMappingManager_t2439353561;
// InteractibleParameters
struct InteractibleParameters_t2389709274;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMappingSource/SurfaceObject>
struct List_1_t557165259;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// SpaceCollectionManager
struct SpaceCollectionManager_t455062415;
// SunScript
struct SunScript_t3500173621;
// InteractibleManager
struct InteractibleManager_t2692509793;
// PlaySpaceManager
struct PlaySpaceManager_t690741465;
// System.Collections.Generic.Dictionary`2<System.String,SunScript/PlanetaryData>
struct Dictionary_2_t4140758868;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1518803153;
// HoloToolkit.Unity.RemoveSurfaceVertices/EventHandler
struct EventHandler_t2773844868;
// System.Collections.Generic.Queue`1<UnityEngine.Bounds>
struct Queue_1_t2853020538;
// HoloToolkit.Unity.SpatialMappingObserver
struct SpatialMappingObserver_t3654215968;
// HoloToolkit.Unity.SpatialMappingSource
struct SpatialMappingSource_t2008115569;
// UnityEngine.XR.WSA.SurfaceObserver/SurfaceChangedDelegate
struct SurfaceChangedDelegate_t3858419612;
// UnityEngine.XR.WSA.SurfaceObserver/SurfaceDataReadyDelegate
struct SurfaceDataReadyDelegate_t686817981;
// UnityEngine.XR.WSA.SurfaceObserver
struct SurfaceObserver_t595079417;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t764358782;
// System.Collections.Generic.Queue`1<UnityEngine.GameObject>
struct Queue_1_t1576189982;
// System.Collections.Generic.Queue`1<UnityEngine.XR.WSA.SurfaceData>
struct Queue_1_t3691932036;
// HoloToolkit.Unity.SurfaceMeshesToPlanes/EventHandler
struct EventHandler_t1067160880;
// UnityEngine.XR.WSA.Input.GestureRecognizer
struct GestureRecognizer_t1861500035;

struct Vector3_t2243707580 ;



#ifndef U3CMODULEU3E_T3783534244_H
#define U3CMODULEU3E_T3783534244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534244 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534244_H
#ifndef U3CMODULEU3E_T3783534245_H
#define U3CMODULEU3E_T3783534245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534245 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534245_H
#ifndef U3CMODULEU3E_T3783534246_H
#define U3CMODULEU3E_T3783534246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534246 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534246_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DLLIMPORTS_T3544469217_H
#define DLLIMPORTS_T3544469217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PlaneFinding/DLLImports
struct  DLLImports_t3544469217  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DLLIMPORTS_T3544469217_H
#ifndef INTERACTIBLEPARAMETERS_T2389709274_H
#define INTERACTIBLEPARAMETERS_T2389709274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InteractibleParameters
struct  InteractibleParameters_t2389709274  : public RuntimeObject
{
public:
	// System.Boolean InteractibleParameters::Scrollable
	bool ___Scrollable_0;
	// System.Boolean InteractibleParameters::Placeable
	bool ___Placeable_1;

public:
	inline static int32_t get_offset_of_Scrollable_0() { return static_cast<int32_t>(offsetof(InteractibleParameters_t2389709274, ___Scrollable_0)); }
	inline bool get_Scrollable_0() const { return ___Scrollable_0; }
	inline bool* get_address_of_Scrollable_0() { return &___Scrollable_0; }
	inline void set_Scrollable_0(bool value)
	{
		___Scrollable_0 = value;
	}

	inline static int32_t get_offset_of_Placeable_1() { return static_cast<int32_t>(offsetof(InteractibleParameters_t2389709274, ___Placeable_1)); }
	inline bool get_Placeable_1() const { return ___Placeable_1; }
	inline bool* get_address_of_Placeable_1() { return &___Placeable_1; }
	inline void set_Placeable_1(bool value)
	{
		___Placeable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIBLEPARAMETERS_T2389709274_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef PLANEFINDING_T2534126505_H
#define PLANEFINDING_T2534126505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PlaneFinding
struct  PlaneFinding_t2534126505  : public RuntimeObject
{
public:

public:
};

struct PlaneFinding_t2534126505_StaticFields
{
public:
	// System.Boolean HoloToolkit.Unity.PlaneFinding::findPlanesRunning
	bool ___findPlanesRunning_0;
	// System.Object HoloToolkit.Unity.PlaneFinding::findPlanesLock
	RuntimeObject * ___findPlanesLock_1;
	// HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData[] HoloToolkit.Unity.PlaneFinding::reusedMeshesForMarshalling
	MeshDataU5BU5D_t2194650048* ___reusedMeshesForMarshalling_2;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> HoloToolkit.Unity.PlaneFinding::reusedPinnedMemoryHandles
	List_1_t2778389198 * ___reusedPinnedMemoryHandles_3;

public:
	inline static int32_t get_offset_of_findPlanesRunning_0() { return static_cast<int32_t>(offsetof(PlaneFinding_t2534126505_StaticFields, ___findPlanesRunning_0)); }
	inline bool get_findPlanesRunning_0() const { return ___findPlanesRunning_0; }
	inline bool* get_address_of_findPlanesRunning_0() { return &___findPlanesRunning_0; }
	inline void set_findPlanesRunning_0(bool value)
	{
		___findPlanesRunning_0 = value;
	}

	inline static int32_t get_offset_of_findPlanesLock_1() { return static_cast<int32_t>(offsetof(PlaneFinding_t2534126505_StaticFields, ___findPlanesLock_1)); }
	inline RuntimeObject * get_findPlanesLock_1() const { return ___findPlanesLock_1; }
	inline RuntimeObject ** get_address_of_findPlanesLock_1() { return &___findPlanesLock_1; }
	inline void set_findPlanesLock_1(RuntimeObject * value)
	{
		___findPlanesLock_1 = value;
		Il2CppCodeGenWriteBarrier((&___findPlanesLock_1), value);
	}

	inline static int32_t get_offset_of_reusedMeshesForMarshalling_2() { return static_cast<int32_t>(offsetof(PlaneFinding_t2534126505_StaticFields, ___reusedMeshesForMarshalling_2)); }
	inline MeshDataU5BU5D_t2194650048* get_reusedMeshesForMarshalling_2() const { return ___reusedMeshesForMarshalling_2; }
	inline MeshDataU5BU5D_t2194650048** get_address_of_reusedMeshesForMarshalling_2() { return &___reusedMeshesForMarshalling_2; }
	inline void set_reusedMeshesForMarshalling_2(MeshDataU5BU5D_t2194650048* value)
	{
		___reusedMeshesForMarshalling_2 = value;
		Il2CppCodeGenWriteBarrier((&___reusedMeshesForMarshalling_2), value);
	}

	inline static int32_t get_offset_of_reusedPinnedMemoryHandles_3() { return static_cast<int32_t>(offsetof(PlaneFinding_t2534126505_StaticFields, ___reusedPinnedMemoryHandles_3)); }
	inline List_1_t2778389198 * get_reusedPinnedMemoryHandles_3() const { return ___reusedPinnedMemoryHandles_3; }
	inline List_1_t2778389198 ** get_address_of_reusedPinnedMemoryHandles_3() { return &___reusedPinnedMemoryHandles_3; }
	inline void set_reusedPinnedMemoryHandles_3(List_1_t2778389198 * value)
	{
		___reusedPinnedMemoryHandles_3 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPinnedMemoryHandles_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEFINDING_T2534126505_H
#ifndef U3CMAKEPLANESROUTINEU3ED__24_T2308419383_H
#define U3CMAKEPLANESROUTINEU3ED__24_T2308419383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24
struct  U3CMakePlanesRoutineU3Ed__24_t2308419383  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>4__this
	SurfaceMeshesToPlanes_t2358468568 * ___U3CU3E4__this_2;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>8__1
	U3CU3Ec__DisplayClass24_0_t1668648882 * ___U3CU3E8__1_3;
	// System.Collections.Generic.List`1<UnityEngine.MeshFilter> HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<filters>5__2
	List_1_t2396058581 * ___U3CfiltersU3E5__2_4;
	// System.Int32 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<index>5__3
	int32_t ___U3CindexU3E5__3_5;
	// System.Threading.Tasks.Task`1<HoloToolkit.Unity.BoundedPlane[]> HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<planeTask>5__4
	Task_1_t4230515705 * ___U3CplaneTaskU3E5__4_6;
	// HoloToolkit.Unity.BoundedPlane[] HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<planes>5__5
	BoundedPlaneU5BU5D_t815519402* ___U3CplanesU3E5__5_7;
	// System.Int32 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<index>5__6
	int32_t ___U3CindexU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E4__this_2)); }
	inline SurfaceMeshesToPlanes_t2358468568 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SurfaceMeshesToPlanes_t2358468568 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SurfaceMeshesToPlanes_t2358468568 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass24_0_t1668648882 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass24_0_t1668648882 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass24_0_t1668648882 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_U3CfiltersU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CfiltersU3E5__2_4)); }
	inline List_1_t2396058581 * get_U3CfiltersU3E5__2_4() const { return ___U3CfiltersU3E5__2_4; }
	inline List_1_t2396058581 ** get_address_of_U3CfiltersU3E5__2_4() { return &___U3CfiltersU3E5__2_4; }
	inline void set_U3CfiltersU3E5__2_4(List_1_t2396058581 * value)
	{
		___U3CfiltersU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfiltersU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CindexU3E5__3_5)); }
	inline int32_t get_U3CindexU3E5__3_5() const { return ___U3CindexU3E5__3_5; }
	inline int32_t* get_address_of_U3CindexU3E5__3_5() { return &___U3CindexU3E5__3_5; }
	inline void set_U3CindexU3E5__3_5(int32_t value)
	{
		___U3CindexU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CplaneTaskU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CplaneTaskU3E5__4_6)); }
	inline Task_1_t4230515705 * get_U3CplaneTaskU3E5__4_6() const { return ___U3CplaneTaskU3E5__4_6; }
	inline Task_1_t4230515705 ** get_address_of_U3CplaneTaskU3E5__4_6() { return &___U3CplaneTaskU3E5__4_6; }
	inline void set_U3CplaneTaskU3E5__4_6(Task_1_t4230515705 * value)
	{
		___U3CplaneTaskU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplaneTaskU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CplanesU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CplanesU3E5__5_7)); }
	inline BoundedPlaneU5BU5D_t815519402* get_U3CplanesU3E5__5_7() const { return ___U3CplanesU3E5__5_7; }
	inline BoundedPlaneU5BU5D_t815519402** get_address_of_U3CplanesU3E5__5_7() { return &___U3CplanesU3E5__5_7; }
	inline void set_U3CplanesU3E5__5_7(BoundedPlaneU5BU5D_t815519402* value)
	{
		___U3CplanesU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplanesU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CindexU3E5__6_8)); }
	inline int32_t get_U3CindexU3E5__6_8() const { return ___U3CindexU3E5__6_8; }
	inline int32_t* get_address_of_U3CindexU3E5__6_8() { return &___U3CindexU3E5__6_8; }
	inline void set_U3CindexU3E5__6_8(int32_t value)
	{
		___U3CindexU3E5__6_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAKEPLANESROUTINEU3ED__24_T2308419383_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_T1668648882_H
#define U3CU3EC__DISPLAYCLASS24_0_T1668648882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_t1668648882  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.PlaneFinding/MeshData> HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0::meshData
	List_1_t880011750 * ___meshData_0;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0::<>4__this
	SurfaceMeshesToPlanes_t2358468568 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_meshData_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t1668648882, ___meshData_0)); }
	inline List_1_t880011750 * get_meshData_0() const { return ___meshData_0; }
	inline List_1_t880011750 ** get_address_of_meshData_0() { return &___meshData_0; }
	inline void set_meshData_0(List_1_t880011750 * value)
	{
		___meshData_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshData_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t1668648882, ___U3CU3E4__this_1)); }
	inline SurfaceMeshesToPlanes_t2358468568 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SurfaceMeshesToPlanes_t2358468568 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SurfaceMeshesToPlanes_t2358468568 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_T1668648882_H
#ifndef U3CU3EC_T466077745_H
#define U3CU3EC_T466077745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpaceCollectionManager/<>c
struct  U3CU3Ec_t466077745  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t466077745_StaticFields
{
public:
	// SpaceCollectionManager/<>c SpaceCollectionManager/<>c::<>9
	U3CU3Ec_t466077745 * ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.GameObject> SpaceCollectionManager/<>c::<>9__2_0
	Comparison_1_t3018271998 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t466077745_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t466077745 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t466077745 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t466077745 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t466077745_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Comparison_1_t3018271998 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Comparison_1_t3018271998 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Comparison_1_t3018271998 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T466077745_H
#ifndef CONSTS_T2407773021_H
#define CONSTS_T2407773021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Consts
struct  Consts_t2407773021  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTS_T2407773021_H
#ifndef ENCODING_T663144255_H
#define ENCODING_T663144255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t663144255  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2022420531 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1756452756 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t1715117820 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___dataItem_10)); }
	inline CodePageDataItem_t2022420531 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2022420531 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2022420531 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoderFallback_13)); }
	inline EncoderFallback_t1756452756 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t1756452756 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t1756452756 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___decoderFallback_14)); }
	inline DecoderFallback_t1715117820 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t1715117820 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t1715117820 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t663144255_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t663144255 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t663144255 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t663144255 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t663144255 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t663144255 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t663144255 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t663144255 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t663144255 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t909839986 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t663144255 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t663144255 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t663144255 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t663144255 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t663144255 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t663144255 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t663144255 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t663144255 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t663144255 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t663144255 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t663144255 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t663144255 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t663144255 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t663144255 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t663144255 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t663144255 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t663144255 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t663144255 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t663144255 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t663144255 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t663144255 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t663144255 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t663144255 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t663144255 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___encodings_8)); }
	inline Hashtable_t909839986 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t909839986 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t909839986 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T663144255_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_T2067141549_H
#define __STATICARRAYINITTYPESIZEU3D512_T2067141549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512
struct  __StaticArrayInitTypeSizeU3D512_t2067141549 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t2067141549__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_T2067141549_H
#ifndef PLANETARYDATA_T2225979606_H
#define PLANETARYDATA_T2225979606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SunScript/PlanetaryData
struct  PlanetaryData_t2225979606 
{
public:
	// System.Single SunScript/PlanetaryData::<yearInEarthYears>k__BackingField
	float ___U3CyearInEarthYearsU3Ek__BackingField_0;
	// System.Single SunScript/PlanetaryData::<dayInEarthYears>k__BackingField
	float ___U3CdayInEarthYearsU3Ek__BackingField_1;
	// System.Single SunScript/PlanetaryData::<distanceFromSun>k__BackingField
	float ___U3CdistanceFromSunU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CyearInEarthYearsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlanetaryData_t2225979606, ___U3CyearInEarthYearsU3Ek__BackingField_0)); }
	inline float get_U3CyearInEarthYearsU3Ek__BackingField_0() const { return ___U3CyearInEarthYearsU3Ek__BackingField_0; }
	inline float* get_address_of_U3CyearInEarthYearsU3Ek__BackingField_0() { return &___U3CyearInEarthYearsU3Ek__BackingField_0; }
	inline void set_U3CyearInEarthYearsU3Ek__BackingField_0(float value)
	{
		___U3CyearInEarthYearsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CdayInEarthYearsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PlanetaryData_t2225979606, ___U3CdayInEarthYearsU3Ek__BackingField_1)); }
	inline float get_U3CdayInEarthYearsU3Ek__BackingField_1() const { return ___U3CdayInEarthYearsU3Ek__BackingField_1; }
	inline float* get_address_of_U3CdayInEarthYearsU3Ek__BackingField_1() { return &___U3CdayInEarthYearsU3Ek__BackingField_1; }
	inline void set_U3CdayInEarthYearsU3Ek__BackingField_1(float value)
	{
		___U3CdayInEarthYearsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceFromSunU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlanetaryData_t2225979606, ___U3CdistanceFromSunU3Ek__BackingField_2)); }
	inline float get_U3CdistanceFromSunU3Ek__BackingField_2() const { return ___U3CdistanceFromSunU3Ek__BackingField_2; }
	inline float* get_address_of_U3CdistanceFromSunU3Ek__BackingField_2() { return &___U3CdistanceFromSunU3Ek__BackingField_2; }
	inline void set_U3CdistanceFromSunU3Ek__BackingField_2(float value)
	{
		___U3CdistanceFromSunU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETARYDATA_T2225979606_H
#ifndef SURFACEOBJECT_T1188044127_H
#define SURFACEOBJECT_T1188044127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMappingSource/SurfaceObject
struct  SurfaceObject_t1188044127 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::ID
	int32_t ___ID_0;
	// System.Int32 HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::UpdateID
	int32_t ___UpdateID_1;
	// UnityEngine.GameObject HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::Object
	GameObject_t1756533147 * ___Object_2;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::Renderer
	MeshRenderer_t1268241104 * ___Renderer_3;
	// UnityEngine.MeshFilter HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::Filter
	MeshFilter_t3026937449 * ___Filter_4;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___ID_0)); }
	inline int32_t get_ID_0() const { return ___ID_0; }
	inline int32_t* get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(int32_t value)
	{
		___ID_0 = value;
	}

	inline static int32_t get_offset_of_UpdateID_1() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___UpdateID_1)); }
	inline int32_t get_UpdateID_1() const { return ___UpdateID_1; }
	inline int32_t* get_address_of_UpdateID_1() { return &___UpdateID_1; }
	inline void set_UpdateID_1(int32_t value)
	{
		___UpdateID_1 = value;
	}

	inline static int32_t get_offset_of_Object_2() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___Object_2)); }
	inline GameObject_t1756533147 * get_Object_2() const { return ___Object_2; }
	inline GameObject_t1756533147 ** get_address_of_Object_2() { return &___Object_2; }
	inline void set_Object_2(GameObject_t1756533147 * value)
	{
		___Object_2 = value;
		Il2CppCodeGenWriteBarrier((&___Object_2), value);
	}

	inline static int32_t get_offset_of_Renderer_3() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___Renderer_3)); }
	inline MeshRenderer_t1268241104 * get_Renderer_3() const { return ___Renderer_3; }
	inline MeshRenderer_t1268241104 ** get_address_of_Renderer_3() { return &___Renderer_3; }
	inline void set_Renderer_3(MeshRenderer_t1268241104 * value)
	{
		___Renderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___Renderer_3), value);
	}

	inline static int32_t get_offset_of_Filter_4() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___Filter_4)); }
	inline MeshFilter_t3026937449 * get_Filter_4() const { return ___Filter_4; }
	inline MeshFilter_t3026937449 ** get_address_of_Filter_4() { return &___Filter_4; }
	inline void set_Filter_4(MeshFilter_t3026937449 * value)
	{
		___Filter_4 = value;
		Il2CppCodeGenWriteBarrier((&___Filter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialMappingSource/SurfaceObject
struct SurfaceObject_t1188044127_marshaled_pinvoke
{
	int32_t ___ID_0;
	int32_t ___UpdateID_1;
	GameObject_t1756533147 * ___Object_2;
	MeshRenderer_t1268241104 * ___Renderer_3;
	MeshFilter_t3026937449 * ___Filter_4;
};
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialMappingSource/SurfaceObject
struct SurfaceObject_t1188044127_marshaled_com
{
	int32_t ___ID_0;
	int32_t ___UpdateID_1;
	GameObject_t1756533147 * ___Object_2;
	MeshRenderer_t1268241104 * ___Renderer_3;
	MeshFilter_t3026937449 * ___Filter_4;
};
#endif // SURFACEOBJECT_T1188044127_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_T2067141550_H
#define __STATICARRAYINITTYPESIZEU3D512_T2067141550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512
struct  __StaticArrayInitTypeSizeU3D512_t2067141550 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t2067141550__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_T2067141550_H
#ifndef MONOENCODING_T2723633290_H
#define MONOENCODING_T2723633290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoEncoding
struct  MonoEncoding_t2723633290  : public Encoding_t663144255
{
public:
	// System.Int32 I18N.Common.MonoEncoding::win_code_page
	int32_t ___win_code_page_16;

public:
	inline static int32_t get_offset_of_win_code_page_16() { return static_cast<int32_t>(offsetof(MonoEncoding_t2723633290, ___win_code_page_16)); }
	inline int32_t get_win_code_page_16() const { return ___win_code_page_16; }
	inline int32_t* get_address_of_win_code_page_16() { return &___win_code_page_16; }
	inline void set_win_code_page_16(int32_t value)
	{
		___win_code_page_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOENCODING_T2723633290_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1841601450__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef ENUMERATOR_T1930788255_H
#define ENUMERATOR_T1930788255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshFilter>
struct  Enumerator_t1930788255 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t2396058581 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	MeshFilter_t3026937449 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1930788255, ___list_0)); }
	inline List_1_t2396058581 * get_list_0() const { return ___list_0; }
	inline List_1_t2396058581 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2396058581 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1930788255, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1930788255, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1930788255, ___current_3)); }
	inline MeshFilter_t3026937449 * get_current_3() const { return ___current_3; }
	inline MeshFilter_t3026937449 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MeshFilter_t3026937449 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1930788255_H
#ifndef ORIENTEDBOUNDINGBOX_T566734849_H
#define ORIENTEDBOUNDINGBOX_T566734849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.OrientedBoundingBox
struct  OrientedBoundingBox_t566734849 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.OrientedBoundingBox::Center
	Vector3_t2243707580  ___Center_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.OrientedBoundingBox::Extents
	Vector3_t2243707580  ___Extents_1;
	// UnityEngine.Quaternion HoloToolkit.Unity.OrientedBoundingBox::Rotation
	Quaternion_t4030073918  ___Rotation_2;

public:
	inline static int32_t get_offset_of_Center_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t566734849, ___Center_0)); }
	inline Vector3_t2243707580  get_Center_0() const { return ___Center_0; }
	inline Vector3_t2243707580 * get_address_of_Center_0() { return &___Center_0; }
	inline void set_Center_0(Vector3_t2243707580  value)
	{
		___Center_0 = value;
	}

	inline static int32_t get_offset_of_Extents_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t566734849, ___Extents_1)); }
	inline Vector3_t2243707580  get_Extents_1() const { return ___Extents_1; }
	inline Vector3_t2243707580 * get_address_of_Extents_1() { return &___Extents_1; }
	inline void set_Extents_1(Vector3_t2243707580  value)
	{
		___Extents_1 = value;
	}

	inline static int32_t get_offset_of_Rotation_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t566734849, ___Rotation_2)); }
	inline Quaternion_t4030073918  get_Rotation_2() const { return ___Rotation_2; }
	inline Quaternion_t4030073918 * get_address_of_Rotation_2() { return &___Rotation_2; }
	inline void set_Rotation_2(Quaternion_t4030073918  value)
	{
		___Rotation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX_T566734849_H
#ifndef MESHDATA_T1510890618_H
#define MESHDATA_T1510890618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PlaneFinding/MeshData
struct  MeshData_t1510890618 
{
public:
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.PlaneFinding/MeshData::Transform
	Matrix4x4_t2933234003  ___Transform_0;
	// UnityEngine.Vector3[] HoloToolkit.Unity.PlaneFinding/MeshData::Verts
	Vector3U5BU5D_t1172311765* ___Verts_1;
	// UnityEngine.Vector3[] HoloToolkit.Unity.PlaneFinding/MeshData::Normals
	Vector3U5BU5D_t1172311765* ___Normals_2;
	// System.Int32[] HoloToolkit.Unity.PlaneFinding/MeshData::Indices
	Int32U5BU5D_t3030399641* ___Indices_3;

public:
	inline static int32_t get_offset_of_Transform_0() { return static_cast<int32_t>(offsetof(MeshData_t1510890618, ___Transform_0)); }
	inline Matrix4x4_t2933234003  get_Transform_0() const { return ___Transform_0; }
	inline Matrix4x4_t2933234003 * get_address_of_Transform_0() { return &___Transform_0; }
	inline void set_Transform_0(Matrix4x4_t2933234003  value)
	{
		___Transform_0 = value;
	}

	inline static int32_t get_offset_of_Verts_1() { return static_cast<int32_t>(offsetof(MeshData_t1510890618, ___Verts_1)); }
	inline Vector3U5BU5D_t1172311765* get_Verts_1() const { return ___Verts_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_Verts_1() { return &___Verts_1; }
	inline void set_Verts_1(Vector3U5BU5D_t1172311765* value)
	{
		___Verts_1 = value;
		Il2CppCodeGenWriteBarrier((&___Verts_1), value);
	}

	inline static int32_t get_offset_of_Normals_2() { return static_cast<int32_t>(offsetof(MeshData_t1510890618, ___Normals_2)); }
	inline Vector3U5BU5D_t1172311765* get_Normals_2() const { return ___Normals_2; }
	inline Vector3U5BU5D_t1172311765** get_address_of_Normals_2() { return &___Normals_2; }
	inline void set_Normals_2(Vector3U5BU5D_t1172311765* value)
	{
		___Normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_2), value);
	}

	inline static int32_t get_offset_of_Indices_3() { return static_cast<int32_t>(offsetof(MeshData_t1510890618, ___Indices_3)); }
	inline Int32U5BU5D_t3030399641* get_Indices_3() const { return ___Indices_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_Indices_3() { return &___Indices_3; }
	inline void set_Indices_3(Int32U5BU5D_t3030399641* value)
	{
		___Indices_3 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.PlaneFinding/MeshData
struct MeshData_t1510890618_marshaled_pinvoke
{
	Matrix4x4_t2933234003  ___Transform_0;
	Vector3_t2243707580 * ___Verts_1;
	Vector3_t2243707580 * ___Normals_2;
	int32_t* ___Indices_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.PlaneFinding/MeshData
struct MeshData_t1510890618_marshaled_com
{
	Matrix4x4_t2933234003  ___Transform_0;
	Vector3_t2243707580 * ___Verts_1;
	Vector3_t2243707580 * ___Normals_2;
	int32_t* ___Indices_3;
};
#endif // MESHDATA_T1510890618_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305147_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305147  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::05C64B887A4D766EEB5842345D6B609A0E3A91C9
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::148F346330E294B4157ED412259A7E7F373E26A8
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___148F346330E294B4157ED412259A7E7F373E26A8_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::1F867F0E96DB3A56943B8CB2662D1DA624023FCD
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::3220DE2BE9769737429E0DE2C6D837CB7C5A02AD
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::356CE585046545CD2D0B109FF8A85DB88EE29074
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___356CE585046545CD2D0B109FF8A85DB88EE29074_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::4FEA2A6324C0192B29C90830F5D578051A4B1B16
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::598D9433A53523A59D462896B803E416AB0B1901
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___598D9433A53523A59D462896B803E416AB0B1901_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::6E6F169B075CACDE575F1FEA42B1376D31D5A7E5
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7089F9820A6F9CC830909BCD1FAF2EF0A540F348
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D5DB24A984219B0001B4B86CDE1E0DF54572D83A
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::E11338F644FF140C0E23D8B7526DB4D2D73FC3F7
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4
	__StaticArrayInitTypeSizeU3D512_t2067141550  ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14;

public:
	inline static int32_t get_offset_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() const { return ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() { return &___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0; }
	inline void set_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0 = value;
	}

	inline static int32_t get_offset_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___148F346330E294B4157ED412259A7E7F373E26A8_1)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U3148F346330E294B4157ED412259A7E7F373E26A8_1() const { return ___148F346330E294B4157ED412259A7E7F373E26A8_1; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1() { return &___148F346330E294B4157ED412259A7E7F373E26A8_1; }
	inline void set_U3148F346330E294B4157ED412259A7E7F373E26A8_1(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___148F346330E294B4157ED412259A7E7F373E26A8_1 = value;
	}

	inline static int32_t get_offset_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() const { return ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() { return &___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2; }
	inline void set_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2 = value;
	}

	inline static int32_t get_offset_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() const { return ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() { return &___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3; }
	inline void set_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3 = value;
	}

	inline static int32_t get_offset_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___356CE585046545CD2D0B109FF8A85DB88EE29074_4)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() const { return ___356CE585046545CD2D0B109FF8A85DB88EE29074_4; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() { return &___356CE585046545CD2D0B109FF8A85DB88EE29074_4; }
	inline void set_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___356CE585046545CD2D0B109FF8A85DB88EE29074_4 = value;
	}

	inline static int32_t get_offset_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() const { return ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() { return &___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5; }
	inline void set_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5 = value;
	}

	inline static int32_t get_offset_of_U3598D9433A53523A59D462896B803E416AB0B1901_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___598D9433A53523A59D462896B803E416AB0B1901_6)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U3598D9433A53523A59D462896B803E416AB0B1901_6() const { return ___598D9433A53523A59D462896B803E416AB0B1901_6; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U3598D9433A53523A59D462896B803E416AB0B1901_6() { return &___598D9433A53523A59D462896B803E416AB0B1901_6; }
	inline void set_U3598D9433A53523A59D462896B803E416AB0B1901_6(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___598D9433A53523A59D462896B803E416AB0B1901_6 = value;
	}

	inline static int32_t get_offset_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() const { return ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() { return &___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7; }
	inline void set_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7 = value;
	}

	inline static int32_t get_offset_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() const { return ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() { return &___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8; }
	inline void set_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8 = value;
	}

	inline static int32_t get_offset_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() const { return ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() { return &___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9; }
	inline void set_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9 = value;
	}

	inline static int32_t get_offset_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() const { return ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() { return &___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10; }
	inline void set_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10 = value;
	}

	inline static int32_t get_offset_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() const { return ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() { return &___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11; }
	inline void set_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11 = value;
	}

	inline static int32_t get_offset_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() const { return ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() { return &___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12; }
	inline void set_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12 = value;
	}

	inline static int32_t get_offset_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() const { return ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() { return &___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13; }
	inline void set_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13 = value;
	}

	inline static int32_t get_offset_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields, ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550  get_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() const { return ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141550 * get_address_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() { return &___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14; }
	inline void set_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14(__StaticArrayInitTypeSizeU3D512_t2067141550  value)
	{
		___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305147_H
#ifndef PLACEMENTSURFACES_T532268329_H
#define PLACEMENTSURFACES_T532268329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlacementSurfaces
struct  PlacementSurfaces_t532268329 
{
public:
	// System.Int32 PlacementSurfaces::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlacementSurfaces_t532268329, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTSURFACES_T532268329_H
#ifndef MESHDATA_T1583775213_H
#define MESHDATA_T1583775213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData
struct  MeshData_t1583775213 
{
public:
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData::transform
	Matrix4x4_t2933234003  ___transform_0;
	// System.Int32 HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData::vertCount
	int32_t ___vertCount_1;
	// System.Int32 HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData::indexCount
	int32_t ___indexCount_2;
	// System.IntPtr HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData::verts
	intptr_t ___verts_3;
	// System.IntPtr HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData::normals
	intptr_t ___normals_4;
	// System.IntPtr HoloToolkit.Unity.PlaneFinding/DLLImports/MeshData::indices
	intptr_t ___indices_5;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(MeshData_t1583775213, ___transform_0)); }
	inline Matrix4x4_t2933234003  get_transform_0() const { return ___transform_0; }
	inline Matrix4x4_t2933234003 * get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Matrix4x4_t2933234003  value)
	{
		___transform_0 = value;
	}

	inline static int32_t get_offset_of_vertCount_1() { return static_cast<int32_t>(offsetof(MeshData_t1583775213, ___vertCount_1)); }
	inline int32_t get_vertCount_1() const { return ___vertCount_1; }
	inline int32_t* get_address_of_vertCount_1() { return &___vertCount_1; }
	inline void set_vertCount_1(int32_t value)
	{
		___vertCount_1 = value;
	}

	inline static int32_t get_offset_of_indexCount_2() { return static_cast<int32_t>(offsetof(MeshData_t1583775213, ___indexCount_2)); }
	inline int32_t get_indexCount_2() const { return ___indexCount_2; }
	inline int32_t* get_address_of_indexCount_2() { return &___indexCount_2; }
	inline void set_indexCount_2(int32_t value)
	{
		___indexCount_2 = value;
	}

	inline static int32_t get_offset_of_verts_3() { return static_cast<int32_t>(offsetof(MeshData_t1583775213, ___verts_3)); }
	inline intptr_t get_verts_3() const { return ___verts_3; }
	inline intptr_t* get_address_of_verts_3() { return &___verts_3; }
	inline void set_verts_3(intptr_t value)
	{
		___verts_3 = value;
	}

	inline static int32_t get_offset_of_normals_4() { return static_cast<int32_t>(offsetof(MeshData_t1583775213, ___normals_4)); }
	inline intptr_t get_normals_4() const { return ___normals_4; }
	inline intptr_t* get_address_of_normals_4() { return &___normals_4; }
	inline void set_normals_4(intptr_t value)
	{
		___normals_4 = value;
	}

	inline static int32_t get_offset_of_indices_5() { return static_cast<int32_t>(offsetof(MeshData_t1583775213, ___indices_5)); }
	inline intptr_t get_indices_5() const { return ___indices_5; }
	inline intptr_t* get_address_of_indices_5() { return &___indices_5; }
	inline void set_indices_5(intptr_t value)
	{
		___indices_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T1583775213_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305146_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305146  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::05B1E1067273E188A50BA4342B4BC09ABEE669CD
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::09885C8B0840E863B3A14261999895D896677D5A
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___09885C8B0840E863B3A14261999895D896677D5A_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::271EF3D1C1F8136E08DBCAB443CE02538A4156F0
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::2E1C78D5F4666324672DFACDC9307935CF14E98F
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::3551F16F8A11003B8289B76B9E3D97969B68E7D9
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::36A0A1CE2E522AB1D22668B38F6126D2F6E17D44
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::37C03949C69BDDA042205E1A573B349E75F693B8
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___37C03949C69BDDA042205E1A573B349E75F693B8_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::47DCC01E928630EBCC018C5E0285145985F92532
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___47DCC01E928630EBCC018C5E0285145985F92532_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::52623ED7F33E8C1C541F1C3101FA981B173D795E
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::659C24C855D0E50CBD366B2774AF841E29202E70
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___659C24C855D0E50CBD366B2774AF841E29202E70_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::82273BF74F02F3FDFABEED76BCA4B82DDB2C2761
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::8E958C1847104F390F9C9B64F0F39C98ED4AC63F
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::94F89F72CC0508D891A0C71DD7B57B703869A3FB
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9EF46C677634CB82C0BD475E372C71C5F68C981A
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::A5A5BE77BD3D095389FABC21D18BB648787E6618
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B1D1CEE08985760776A35065014EAF3D4D3CDE8D
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B2574B82126B684C0CB3CF93BF7473F0FBB34400
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B6BD9A82204938ABFE97CF3FAB5A47824080EAA0
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::BC1CEF8131E7F0D8206029373157806126E21026
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___BC1CEF8131E7F0D8206029373157806126E21026_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C14817C33562352073848F1A8EA633C19CF1A4F5
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___C14817C33562352073848F1A8EA633C19CF1A4F5_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C392E993B9E622A36C30E0BB997A9F41293CD9EF
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::CFB85A64A0D1FAB523C3DE56096F8803CDFEA674
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D257291FBF3DA6F5C38B3275534902BC9EDE92EA
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D4795500A3D0F00D8EE85626648C3FBAFA4F70C3
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::DF4C38A5E59685BBEC109623762B6914BE00E866
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___DF4C38A5E59685BBEC109623762B6914BE00E866_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::F632BE2E03B27F265F779A5D3D217E5C14AB6CB5
	__StaticArrayInitTypeSizeU3D512_t2067141549  ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36;

public:
	inline static int32_t get_offset_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() const { return ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() { return &___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0; }
	inline void set_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0 = value;
	}

	inline static int32_t get_offset_of_U309885C8B0840E863B3A14261999895D896677D5A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___09885C8B0840E863B3A14261999895D896677D5A_1)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U309885C8B0840E863B3A14261999895D896677D5A_1() const { return ___09885C8B0840E863B3A14261999895D896677D5A_1; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U309885C8B0840E863B3A14261999895D896677D5A_1() { return &___09885C8B0840E863B3A14261999895D896677D5A_1; }
	inline void set_U309885C8B0840E863B3A14261999895D896677D5A_1(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___09885C8B0840E863B3A14261999895D896677D5A_1 = value;
	}

	inline static int32_t get_offset_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() const { return ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() { return &___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2; }
	inline void set_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2 = value;
	}

	inline static int32_t get_offset_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() const { return ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() { return &___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3; }
	inline void set_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3 = value;
	}

	inline static int32_t get_offset_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() const { return ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() { return &___2E1C78D5F4666324672DFACDC9307935CF14E98F_4; }
	inline void set_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___2E1C78D5F4666324672DFACDC9307935CF14E98F_4 = value;
	}

	inline static int32_t get_offset_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() const { return ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() { return &___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5; }
	inline void set_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5 = value;
	}

	inline static int32_t get_offset_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() const { return ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() { return &___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6; }
	inline void set_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6 = value;
	}

	inline static int32_t get_offset_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___37C03949C69BDDA042205E1A573B349E75F693B8_7)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U337C03949C69BDDA042205E1A573B349E75F693B8_7() const { return ___37C03949C69BDDA042205E1A573B349E75F693B8_7; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7() { return &___37C03949C69BDDA042205E1A573B349E75F693B8_7; }
	inline void set_U337C03949C69BDDA042205E1A573B349E75F693B8_7(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___37C03949C69BDDA042205E1A573B349E75F693B8_7 = value;
	}

	inline static int32_t get_offset_of_U347DCC01E928630EBCC018C5E0285145985F92532_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___47DCC01E928630EBCC018C5E0285145985F92532_8)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U347DCC01E928630EBCC018C5E0285145985F92532_8() const { return ___47DCC01E928630EBCC018C5E0285145985F92532_8; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U347DCC01E928630EBCC018C5E0285145985F92532_8() { return &___47DCC01E928630EBCC018C5E0285145985F92532_8; }
	inline void set_U347DCC01E928630EBCC018C5E0285145985F92532_8(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___47DCC01E928630EBCC018C5E0285145985F92532_8 = value;
	}

	inline static int32_t get_offset_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() const { return ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() { return &___52623ED7F33E8C1C541F1C3101FA981B173D795E_9; }
	inline void set_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___52623ED7F33E8C1C541F1C3101FA981B173D795E_9 = value;
	}

	inline static int32_t get_offset_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() const { return ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() { return &___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10; }
	inline void set_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10 = value;
	}

	inline static int32_t get_offset_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___659C24C855D0E50CBD366B2774AF841E29202E70_11)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() const { return ___659C24C855D0E50CBD366B2774AF841E29202E70_11; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() { return &___659C24C855D0E50CBD366B2774AF841E29202E70_11; }
	inline void set_U3659C24C855D0E50CBD366B2774AF841E29202E70_11(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___659C24C855D0E50CBD366B2774AF841E29202E70_11 = value;
	}

	inline static int32_t get_offset_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() const { return ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() { return &___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12; }
	inline void set_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12 = value;
	}

	inline static int32_t get_offset_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() const { return ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() { return &___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13; }
	inline void set_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13 = value;
	}

	inline static int32_t get_offset_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() const { return ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() { return &___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14; }
	inline void set_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14 = value;
	}

	inline static int32_t get_offset_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() const { return ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() { return &___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15; }
	inline void set_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15 = value;
	}

	inline static int32_t get_offset_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() const { return ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() { return &___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16; }
	inline void set_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16 = value;
	}

	inline static int32_t get_offset_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() const { return ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() { return &___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17; }
	inline void set_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17 = value;
	}

	inline static int32_t get_offset_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() const { return ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() { return &___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18; }
	inline void set_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18 = value;
	}

	inline static int32_t get_offset_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() const { return ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() { return &___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19; }
	inline void set_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19 = value;
	}

	inline static int32_t get_offset_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() const { return ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() { return &___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20; }
	inline void set_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20 = value;
	}

	inline static int32_t get_offset_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() const { return ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() { return &___9EF46C677634CB82C0BD475E372C71C5F68C981A_21; }
	inline void set_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___9EF46C677634CB82C0BD475E372C71C5F68C981A_21 = value;
	}

	inline static int32_t get_offset_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() const { return ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() { return &___A5A5BE77BD3D095389FABC21D18BB648787E6618_22; }
	inline void set_A5A5BE77BD3D095389FABC21D18BB648787E6618_22(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___A5A5BE77BD3D095389FABC21D18BB648787E6618_22 = value;
	}

	inline static int32_t get_offset_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() const { return ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() { return &___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23; }
	inline void set_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23 = value;
	}

	inline static int32_t get_offset_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() const { return ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() { return &___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24; }
	inline void set_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24 = value;
	}

	inline static int32_t get_offset_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() const { return ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() { return &___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25; }
	inline void set_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25 = value;
	}

	inline static int32_t get_offset_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() const { return ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() { return &___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26; }
	inline void set_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26 = value;
	}

	inline static int32_t get_offset_of_BC1CEF8131E7F0D8206029373157806126E21026_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___BC1CEF8131E7F0D8206029373157806126E21026_27)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_BC1CEF8131E7F0D8206029373157806126E21026_27() const { return ___BC1CEF8131E7F0D8206029373157806126E21026_27; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_BC1CEF8131E7F0D8206029373157806126E21026_27() { return &___BC1CEF8131E7F0D8206029373157806126E21026_27; }
	inline void set_BC1CEF8131E7F0D8206029373157806126E21026_27(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___BC1CEF8131E7F0D8206029373157806126E21026_27 = value;
	}

	inline static int32_t get_offset_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___C14817C33562352073848F1A8EA633C19CF1A4F5_28)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_C14817C33562352073848F1A8EA633C19CF1A4F5_28() const { return ___C14817C33562352073848F1A8EA633C19CF1A4F5_28; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28() { return &___C14817C33562352073848F1A8EA633C19CF1A4F5_28; }
	inline void set_C14817C33562352073848F1A8EA633C19CF1A4F5_28(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___C14817C33562352073848F1A8EA633C19CF1A4F5_28 = value;
	}

	inline static int32_t get_offset_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() const { return ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() { return &___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29; }
	inline void set_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29 = value;
	}

	inline static int32_t get_offset_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() const { return ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() { return &___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30; }
	inline void set_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30 = value;
	}

	inline static int32_t get_offset_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() const { return ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() { return &___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31; }
	inline void set_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31 = value;
	}

	inline static int32_t get_offset_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() const { return ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() { return &___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32; }
	inline void set_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32 = value;
	}

	inline static int32_t get_offset_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() const { return ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() { return &___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33; }
	inline void set_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33 = value;
	}

	inline static int32_t get_offset_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() const { return ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() { return &___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34; }
	inline void set_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34 = value;
	}

	inline static int32_t get_offset_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___DF4C38A5E59685BBEC109623762B6914BE00E866_35)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_DF4C38A5E59685BBEC109623762B6914BE00E866_35() const { return ___DF4C38A5E59685BBEC109623762B6914BE00E866_35; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35() { return &___DF4C38A5E59685BBEC109623762B6914BE00E866_35; }
	inline void set_DF4C38A5E59685BBEC109623762B6914BE00E866_35(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___DF4C38A5E59685BBEC109623762B6914BE00E866_35 = value;
	}

	inline static int32_t get_offset_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields, ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36)); }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549  get_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() const { return ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36; }
	inline __StaticArrayInitTypeSizeU3D512_t2067141549 * get_address_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() { return &___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36; }
	inline void set_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36(__StaticArrayInitTypeSizeU3D512_t2067141549  value)
	{
		___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305146_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef PLANE_T3727654732_H
#define PLANE_T3727654732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t3727654732 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t2243707580  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t3727654732, ___m_Normal_0)); }
	inline Vector3_t2243707580  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t2243707580  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t3727654732, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T3727654732_H
#ifndef PLANETYPES_T512441349_H
#define PLANETYPES_T512441349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PlaneTypes
struct  PlaneTypes_t512441349 
{
public:
	// System.Int32 HoloToolkit.Unity.PlaneTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneTypes_t512441349, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETYPES_T512441349_H
#ifndef OBSERVERSTATES_T4003756946_H
#define OBSERVERSTATES_T4003756946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ObserverStates
struct  ObserverStates_t4003756946 
{
public:
	// System.Int32 HoloToolkit.Unity.ObserverStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObserverStates_t4003756946, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVERSTATES_T4003756946_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef BYTEENCODING_T1810358777_H
#define BYTEENCODING_T1810358777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.ByteEncoding
struct  ByteEncoding_t1810358777  : public MonoEncoding_t2723633290
{
public:
	// System.Char[] I18N.Common.ByteEncoding::toChars
	CharU5BU5D_t1328083999* ___toChars_17;
	// System.String I18N.Common.ByteEncoding::encodingName
	String_t* ___encodingName_18;
	// System.String I18N.Common.ByteEncoding::bodyName
	String_t* ___bodyName_19;
	// System.String I18N.Common.ByteEncoding::headerName
	String_t* ___headerName_20;
	// System.String I18N.Common.ByteEncoding::webName
	String_t* ___webName_21;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserDisplay
	bool ___isBrowserDisplay_22;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserSave
	bool ___isBrowserSave_23;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsDisplay
	bool ___isMailNewsDisplay_24;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsSave
	bool ___isMailNewsSave_25;
	// System.Int32 I18N.Common.ByteEncoding::windowsCodePage
	int32_t ___windowsCodePage_26;

public:
	inline static int32_t get_offset_of_toChars_17() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___toChars_17)); }
	inline CharU5BU5D_t1328083999* get_toChars_17() const { return ___toChars_17; }
	inline CharU5BU5D_t1328083999** get_address_of_toChars_17() { return &___toChars_17; }
	inline void set_toChars_17(CharU5BU5D_t1328083999* value)
	{
		___toChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___toChars_17), value);
	}

	inline static int32_t get_offset_of_encodingName_18() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___encodingName_18)); }
	inline String_t* get_encodingName_18() const { return ___encodingName_18; }
	inline String_t** get_address_of_encodingName_18() { return &___encodingName_18; }
	inline void set_encodingName_18(String_t* value)
	{
		___encodingName_18 = value;
		Il2CppCodeGenWriteBarrier((&___encodingName_18), value);
	}

	inline static int32_t get_offset_of_bodyName_19() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___bodyName_19)); }
	inline String_t* get_bodyName_19() const { return ___bodyName_19; }
	inline String_t** get_address_of_bodyName_19() { return &___bodyName_19; }
	inline void set_bodyName_19(String_t* value)
	{
		___bodyName_19 = value;
		Il2CppCodeGenWriteBarrier((&___bodyName_19), value);
	}

	inline static int32_t get_offset_of_headerName_20() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___headerName_20)); }
	inline String_t* get_headerName_20() const { return ___headerName_20; }
	inline String_t** get_address_of_headerName_20() { return &___headerName_20; }
	inline void set_headerName_20(String_t* value)
	{
		___headerName_20 = value;
		Il2CppCodeGenWriteBarrier((&___headerName_20), value);
	}

	inline static int32_t get_offset_of_webName_21() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___webName_21)); }
	inline String_t* get_webName_21() const { return ___webName_21; }
	inline String_t** get_address_of_webName_21() { return &___webName_21; }
	inline void set_webName_21(String_t* value)
	{
		___webName_21 = value;
		Il2CppCodeGenWriteBarrier((&___webName_21), value);
	}

	inline static int32_t get_offset_of_isBrowserDisplay_22() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___isBrowserDisplay_22)); }
	inline bool get_isBrowserDisplay_22() const { return ___isBrowserDisplay_22; }
	inline bool* get_address_of_isBrowserDisplay_22() { return &___isBrowserDisplay_22; }
	inline void set_isBrowserDisplay_22(bool value)
	{
		___isBrowserDisplay_22 = value;
	}

	inline static int32_t get_offset_of_isBrowserSave_23() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___isBrowserSave_23)); }
	inline bool get_isBrowserSave_23() const { return ___isBrowserSave_23; }
	inline bool* get_address_of_isBrowserSave_23() { return &___isBrowserSave_23; }
	inline void set_isBrowserSave_23(bool value)
	{
		___isBrowserSave_23 = value;
	}

	inline static int32_t get_offset_of_isMailNewsDisplay_24() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___isMailNewsDisplay_24)); }
	inline bool get_isMailNewsDisplay_24() const { return ___isMailNewsDisplay_24; }
	inline bool* get_address_of_isMailNewsDisplay_24() { return &___isMailNewsDisplay_24; }
	inline void set_isMailNewsDisplay_24(bool value)
	{
		___isMailNewsDisplay_24 = value;
	}

	inline static int32_t get_offset_of_isMailNewsSave_25() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___isMailNewsSave_25)); }
	inline bool get_isMailNewsSave_25() const { return ___isMailNewsSave_25; }
	inline bool* get_address_of_isMailNewsSave_25() { return &___isMailNewsSave_25; }
	inline void set_isMailNewsSave_25(bool value)
	{
		___isMailNewsSave_25 = value;
	}

	inline static int32_t get_offset_of_windowsCodePage_26() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777, ___windowsCodePage_26)); }
	inline int32_t get_windowsCodePage_26() const { return ___windowsCodePage_26; }
	inline int32_t* get_address_of_windowsCodePage_26() { return &___windowsCodePage_26; }
	inline void set_windowsCodePage_26(int32_t value)
	{
		___windowsCodePage_26 = value;
	}
};

struct ByteEncoding_t1810358777_StaticFields
{
public:
	// System.Byte[] I18N.Common.ByteEncoding::isNormalized
	ByteU5BU5D_t3397334013* ___isNormalized_27;
	// System.Byte[] I18N.Common.ByteEncoding::isNormalizedComputed
	ByteU5BU5D_t3397334013* ___isNormalizedComputed_28;
	// System.Byte[] I18N.Common.ByteEncoding::normalization_bytes
	ByteU5BU5D_t3397334013* ___normalization_bytes_29;

public:
	inline static int32_t get_offset_of_isNormalized_27() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777_StaticFields, ___isNormalized_27)); }
	inline ByteU5BU5D_t3397334013* get_isNormalized_27() const { return ___isNormalized_27; }
	inline ByteU5BU5D_t3397334013** get_address_of_isNormalized_27() { return &___isNormalized_27; }
	inline void set_isNormalized_27(ByteU5BU5D_t3397334013* value)
	{
		___isNormalized_27 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalized_27), value);
	}

	inline static int32_t get_offset_of_isNormalizedComputed_28() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777_StaticFields, ___isNormalizedComputed_28)); }
	inline ByteU5BU5D_t3397334013* get_isNormalizedComputed_28() const { return ___isNormalizedComputed_28; }
	inline ByteU5BU5D_t3397334013** get_address_of_isNormalizedComputed_28() { return &___isNormalizedComputed_28; }
	inline void set_isNormalizedComputed_28(ByteU5BU5D_t3397334013* value)
	{
		___isNormalizedComputed_28 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalizedComputed_28), value);
	}

	inline static int32_t get_offset_of_normalization_bytes_29() { return static_cast<int32_t>(offsetof(ByteEncoding_t1810358777_StaticFields, ___normalization_bytes_29)); }
	inline ByteU5BU5D_t3397334013* get_normalization_bytes_29() const { return ___normalization_bytes_29; }
	inline ByteU5BU5D_t3397334013** get_address_of_normalization_bytes_29() { return &___normalization_bytes_29; }
	inline void set_normalization_bytes_29(ByteU5BU5D_t3397334013* value)
	{
		___normalization_bytes_29 = value;
		Il2CppCodeGenWriteBarrier((&___normalization_bytes_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEENCODING_T1810358777_H
#ifndef CP863_T3178766156_H
#define CP863_T3178766156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP863
struct  CP863_t3178766156  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP863_t3178766156_StaticFields
{
public:
	// System.Char[] I18N.West.CP863::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP863_t3178766156_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP863_T3178766156_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef CP865_T3178766158_H
#define CP865_T3178766158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP865
struct  CP865_t3178766158  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP865_t3178766158_StaticFields
{
public:
	// System.Char[] I18N.West.CP865::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP865_t3178766158_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP865_T3178766158_H
#ifndef BOUNDEDPLANE_T3629713867_H
#define BOUNDEDPLANE_T3629713867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.BoundedPlane
struct  BoundedPlane_t3629713867 
{
public:
	// UnityEngine.Plane HoloToolkit.Unity.BoundedPlane::Plane
	Plane_t3727654732  ___Plane_0;
	// HoloToolkit.Unity.OrientedBoundingBox HoloToolkit.Unity.BoundedPlane::Bounds
	OrientedBoundingBox_t566734849  ___Bounds_1;
	// System.Single HoloToolkit.Unity.BoundedPlane::Area
	float ___Area_2;

public:
	inline static int32_t get_offset_of_Plane_0() { return static_cast<int32_t>(offsetof(BoundedPlane_t3629713867, ___Plane_0)); }
	inline Plane_t3727654732  get_Plane_0() const { return ___Plane_0; }
	inline Plane_t3727654732 * get_address_of_Plane_0() { return &___Plane_0; }
	inline void set_Plane_0(Plane_t3727654732  value)
	{
		___Plane_0 = value;
	}

	inline static int32_t get_offset_of_Bounds_1() { return static_cast<int32_t>(offsetof(BoundedPlane_t3629713867, ___Bounds_1)); }
	inline OrientedBoundingBox_t566734849  get_Bounds_1() const { return ___Bounds_1; }
	inline OrientedBoundingBox_t566734849 * get_address_of_Bounds_1() { return &___Bounds_1; }
	inline void set_Bounds_1(OrientedBoundingBox_t566734849  value)
	{
		___Bounds_1 = value;
	}

	inline static int32_t get_offset_of_Area_2() { return static_cast<int32_t>(offsetof(BoundedPlane_t3629713867, ___Area_2)); }
	inline float get_Area_2() const { return ___Area_2; }
	inline float* get_address_of_Area_2() { return &___Area_2; }
	inline void set_Area_2(float value)
	{
		___Area_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDEDPLANE_T3629713867_H
#ifndef U3CREMOVESURFACEVERTICESWITHINBOUNDSROUTINEU3ED__11_T2587033248_H
#define U3CREMOVESURFACEVERTICESWITHINBOUNDSROUTINEU3ED__11_T2587033248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11
struct  U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.RemoveSurfaceVertices HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<>4__this
	RemoveSurfaceVertices_t3740445364 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<UnityEngine.MeshFilter> HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<meshFilters>5__1
	List_1_t2396058581 * ___U3CmeshFiltersU3E5__1_3;
	// UnityEngine.Bounds HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<bounds>5__2
	Bounds_t3033363703  ___U3CboundsU3E5__2_4;
	// UnityEngine.MeshFilter HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<filter>5__3
	MeshFilter_t3026937449 * ___U3CfilterU3E5__3_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<verts>5__4
	Vector3U5BU5D_t1172311765* ___U3CvertsU3E5__4_6;
	// System.Collections.Generic.List`1<System.Int32> HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<vertsToRemove>5__5
	List_1_t1440998580 * ___U3CvertsToRemoveU3E5__5_7;
	// System.Int32 HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<i>5__6
	int32_t ___U3CiU3E5__6_8;
	// UnityEngine.Mesh HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<mesh>5__7
	Mesh_t1356156583 * ___U3CmeshU3E5__7_9;
	// System.Int32[] HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<indices>5__8
	Int32U5BU5D_t3030399641* ___U3CindicesU3E5__8_10;
	// System.Collections.Generic.List`1<System.Int32> HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<updatedIndices>5__9
	List_1_t1440998580 * ___U3CupdatedIndicesU3E5__9_11;
	// System.Int32 HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<index>5__10
	int32_t ___U3CindexU3E5__10_12;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshFilter> HoloToolkit.Unity.RemoveSurfaceVertices/<RemoveSurfaceVerticesWithinBoundsRoutine>d__11::<>7__wrap1
	Enumerator_t1930788255  ___U3CU3E7__wrap1_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CU3E4__this_2)); }
	inline RemoveSurfaceVertices_t3740445364 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RemoveSurfaceVertices_t3740445364 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RemoveSurfaceVertices_t3740445364 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmeshFiltersU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CmeshFiltersU3E5__1_3)); }
	inline List_1_t2396058581 * get_U3CmeshFiltersU3E5__1_3() const { return ___U3CmeshFiltersU3E5__1_3; }
	inline List_1_t2396058581 ** get_address_of_U3CmeshFiltersU3E5__1_3() { return &___U3CmeshFiltersU3E5__1_3; }
	inline void set_U3CmeshFiltersU3E5__1_3(List_1_t2396058581 * value)
	{
		___U3CmeshFiltersU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshFiltersU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_U3CboundsU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CboundsU3E5__2_4)); }
	inline Bounds_t3033363703  get_U3CboundsU3E5__2_4() const { return ___U3CboundsU3E5__2_4; }
	inline Bounds_t3033363703 * get_address_of_U3CboundsU3E5__2_4() { return &___U3CboundsU3E5__2_4; }
	inline void set_U3CboundsU3E5__2_4(Bounds_t3033363703  value)
	{
		___U3CboundsU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CfilterU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CfilterU3E5__3_5)); }
	inline MeshFilter_t3026937449 * get_U3CfilterU3E5__3_5() const { return ___U3CfilterU3E5__3_5; }
	inline MeshFilter_t3026937449 ** get_address_of_U3CfilterU3E5__3_5() { return &___U3CfilterU3E5__3_5; }
	inline void set_U3CfilterU3E5__3_5(MeshFilter_t3026937449 * value)
	{
		___U3CfilterU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfilterU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3CvertsU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CvertsU3E5__4_6)); }
	inline Vector3U5BU5D_t1172311765* get_U3CvertsU3E5__4_6() const { return ___U3CvertsU3E5__4_6; }
	inline Vector3U5BU5D_t1172311765** get_address_of_U3CvertsU3E5__4_6() { return &___U3CvertsU3E5__4_6; }
	inline void set_U3CvertsU3E5__4_6(Vector3U5BU5D_t1172311765* value)
	{
		___U3CvertsU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvertsU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CvertsToRemoveU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CvertsToRemoveU3E5__5_7)); }
	inline List_1_t1440998580 * get_U3CvertsToRemoveU3E5__5_7() const { return ___U3CvertsToRemoveU3E5__5_7; }
	inline List_1_t1440998580 ** get_address_of_U3CvertsToRemoveU3E5__5_7() { return &___U3CvertsToRemoveU3E5__5_7; }
	inline void set_U3CvertsToRemoveU3E5__5_7(List_1_t1440998580 * value)
	{
		___U3CvertsToRemoveU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvertsToRemoveU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CiU3E5__6_8)); }
	inline int32_t get_U3CiU3E5__6_8() const { return ___U3CiU3E5__6_8; }
	inline int32_t* get_address_of_U3CiU3E5__6_8() { return &___U3CiU3E5__6_8; }
	inline void set_U3CiU3E5__6_8(int32_t value)
	{
		___U3CiU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CmeshU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CmeshU3E5__7_9)); }
	inline Mesh_t1356156583 * get_U3CmeshU3E5__7_9() const { return ___U3CmeshU3E5__7_9; }
	inline Mesh_t1356156583 ** get_address_of_U3CmeshU3E5__7_9() { return &___U3CmeshU3E5__7_9; }
	inline void set_U3CmeshU3E5__7_9(Mesh_t1356156583 * value)
	{
		___U3CmeshU3E5__7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshU3E5__7_9), value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CindicesU3E5__8_10)); }
	inline Int32U5BU5D_t3030399641* get_U3CindicesU3E5__8_10() const { return ___U3CindicesU3E5__8_10; }
	inline Int32U5BU5D_t3030399641** get_address_of_U3CindicesU3E5__8_10() { return &___U3CindicesU3E5__8_10; }
	inline void set_U3CindicesU3E5__8_10(Int32U5BU5D_t3030399641* value)
	{
		___U3CindicesU3E5__8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CindicesU3E5__8_10), value);
	}

	inline static int32_t get_offset_of_U3CupdatedIndicesU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CupdatedIndicesU3E5__9_11)); }
	inline List_1_t1440998580 * get_U3CupdatedIndicesU3E5__9_11() const { return ___U3CupdatedIndicesU3E5__9_11; }
	inline List_1_t1440998580 ** get_address_of_U3CupdatedIndicesU3E5__9_11() { return &___U3CupdatedIndicesU3E5__9_11; }
	inline void set_U3CupdatedIndicesU3E5__9_11(List_1_t1440998580 * value)
	{
		___U3CupdatedIndicesU3E5__9_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedIndicesU3E5__9_11), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CindexU3E5__10_12)); }
	inline int32_t get_U3CindexU3E5__10_12() const { return ___U3CindexU3E5__10_12; }
	inline int32_t* get_address_of_U3CindexU3E5__10_12() { return &___U3CindexU3E5__10_12; }
	inline void set_U3CindexU3E5__10_12(int32_t value)
	{
		___U3CindexU3E5__10_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_13() { return static_cast<int32_t>(offsetof(U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248, ___U3CU3E7__wrap1_13)); }
	inline Enumerator_t1930788255  get_U3CU3E7__wrap1_13() const { return ___U3CU3E7__wrap1_13; }
	inline Enumerator_t1930788255 * get_address_of_U3CU3E7__wrap1_13() { return &___U3CU3E7__wrap1_13; }
	inline void set_U3CU3E7__wrap1_13(Enumerator_t1930788255  value)
	{
		___U3CU3E7__wrap1_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVESURFACEVERTICESWITHINBOUNDSROUTINEU3ED__11_T2587033248_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef CP21025_T3771795245_H
#define CP21025_T3771795245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP21025
struct  CP21025_t3771795245  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP21025_t3771795245_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP21025::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP21025_t3771795245_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP21025_T3771795245_H
#ifndef CP861_T3178766154_H
#define CP861_T3178766154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP861
struct  CP861_t3178766154  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP861_t3178766154_StaticFields
{
public:
	// System.Char[] I18N.West.CP861::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP861_t3178766154_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP861_T3178766154_H
#ifndef CP28593_T2233549564_H
#define CP28593_T2233549564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28593
struct  CP28593_t2233549564  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP28593_t2233549564_StaticFields
{
public:
	// System.Char[] I18N.West.CP28593::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28593_t2233549564_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28593_T2233549564_H
#ifndef CP500_T2188172604_H
#define CP500_T2188172604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP500
struct  CP500_t2188172604  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP500_t2188172604_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP500::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP500_t2188172604_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP500_T2188172604_H
#ifndef CP10079_T1554584814_H
#define CP10079_T1554584814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP10079
struct  CP10079_t1554584814  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP10079_t1554584814_StaticFields
{
public:
	// System.Char[] I18N.West.CP10079::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP10079_t1554584814_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP10079_T1554584814_H
#ifndef CP28605_T311235100_H
#define CP28605_T311235100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28605
struct  CP28605_t311235100  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP28605_t311235100_StaticFields
{
public:
	// System.Char[] I18N.West.CP28605::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28605_t311235100_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28605_T311235100_H
#ifndef CP37_T661932205_H
#define CP37_T661932205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP37
struct  CP37_t661932205  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP37_t661932205_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP37::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP37_t661932205_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP37_T661932205_H
#ifndef CP1252_T1816577383_H
#define CP1252_T1816577383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1252
struct  CP1252_t1816577383  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP1252_t1816577383_StaticFields
{
public:
	// System.Char[] I18N.West.CP1252::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1252_t1816577383_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1252_T1816577383_H
#ifndef CP852_T1784887916_H
#define CP852_T1784887916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP852
struct  CP852_t1784887916  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP852_t1784887916_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP852::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP852_t1784887916_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP852_T1784887916_H
#ifndef CP855_T1784887909_H
#define CP855_T1784887909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP855
struct  CP855_t1784887909  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP855_t1784887909_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP855::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP855_t1784887909_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP855_T1784887909_H
#ifndef CP857_T1784887911_H
#define CP857_T1784887911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP857
struct  CP857_t1784887911  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP857_t1784887911_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP857::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP857_t1784887911_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP857_T1784887911_H
#ifndef CP1250_T653777969_H
#define CP1250_T653777969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1250
struct  CP1250_t653777969  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP1250_t653777969_StaticFields
{
public:
	// System.Char[] I18N.West.CP1250::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1250_t653777969_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1250_T653777969_H
#ifndef CP28597_T2233549560_H
#define CP28597_T2233549560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28597
struct  CP28597_t2233549560  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP28597_t2233549560_StaticFields
{
public:
	// System.Char[] I18N.West.CP28597::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28597_t2233549560_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28597_T2233549560_H
#ifndef CP708_T2188172662_H
#define CP708_T2188172662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP708
struct  CP708_t2188172662  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP708_t2188172662_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP708::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP708_t2188172662_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP708_T2188172662_H
#ifndef CP860_T3178766153_H
#define CP860_T3178766153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP860
struct  CP860_t3178766153  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP860_t3178766153_StaticFields
{
public:
	// System.Char[] I18N.West.CP860::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP860_t3178766153_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP860_T3178766153_H
#ifndef CP1253_T3382661324_H
#define CP1253_T3382661324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1253
struct  CP1253_t3382661324  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP1253_t3382661324_StaticFields
{
public:
	// System.Char[] I18N.West.CP1253::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1253_t3382661324_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1253_T3382661324_H
#ifndef CP858_T1784887922_H
#define CP858_T1784887922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP858
struct  CP858_t1784887922  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP858_t1784887922_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP858::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP858_t1784887922_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP858_T1784887922_H
#ifndef CP862_T3350971857_H
#define CP862_T3350971857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP862
struct  CP862_t3350971857  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP862_t3350971857_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP862::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP862_t3350971857_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP862_T3350971857_H
#ifndef CP850_T1612682212_H
#define CP850_T1612682212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP850
struct  CP850_t1612682212  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP850_t1612682212_StaticFields
{
public:
	// System.Char[] I18N.West.CP850::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP850_t1612682212_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP850_T1612682212_H
#ifndef CP10000_T3476899124_H
#define CP10000_T3476899124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP10000
struct  CP10000_t3476899124  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP10000_t3476899124_StaticFields
{
public:
	// System.Char[] I18N.West.CP10000::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP10000_t3476899124_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP10000_T3476899124_H
#ifndef CP28592_T2233549565_H
#define CP28592_T2233549565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28592
struct  CP28592_t2233549565  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP28592_t2233549565_StaticFields
{
public:
	// System.Char[] I18N.West.CP28592::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28592_t2233549565_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28592_T2233549565_H
#ifndef CP437_T2775482021_H
#define CP437_T2775482021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP437
struct  CP437_t2775482021  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP437_t2775482021_StaticFields
{
public:
	// System.Char[] I18N.West.CP437::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP437_t2775482021_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP437_T2775482021_H
#ifndef CP864_T3350971851_H
#define CP864_T3350971851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP864
struct  CP864_t3350971851  : public ByteEncoding_t1810358777
{
public:

public:
};

struct CP864_t3350971851_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP864::ToChars
	CharU5BU5D_t1328083999* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP864_t3350971851_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t1328083999* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t1328083999** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t1328083999* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP864_T3350971851_H
#ifndef EVENTHANDLER_T2773844868_H
#define EVENTHANDLER_T2773844868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RemoveSurfaceVertices/EventHandler
struct  EventHandler_t2773844868  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T2773844868_H
#ifndef ENCWINDOWS_1252_T336475774_H
#define ENCWINDOWS_1252_T336475774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1252
struct  ENCwindows_1252_t336475774  : public CP1252_t1816577383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1252_T336475774_H
#ifndef ENCIBM00858_T197894961_H
#define ENCIBM00858_T197894961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm00858
struct  ENCibm00858_t197894961  : public CP858_t1784887922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM00858_T197894961_H
#ifndef ENCIBM864_T2859551230_H
#define ENCIBM864_T2859551230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm864
struct  ENCibm864_t2859551230  : public CP864_t3350971851
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM864_T2859551230_H
#ifndef ENCMACINTOSH_T2108151194_H
#define ENCMACINTOSH_T2108151194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCmacintosh
struct  ENCmacintosh_t2108151194  : public CP10000_t3476899124
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCMACINTOSH_T2108151194_H
#ifndef ENCIBM862_T2859551224_H
#define ENCIBM862_T2859551224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm862
struct  ENCibm862_t2859551224  : public CP862_t3350971857
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM862_T2859551224_H
#ifndef ENCIBM857_T3262835756_H
#define ENCIBM857_T3262835756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm857
struct  ENCibm857_t3262835756  : public CP857_t1784887911
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM857_T3262835756_H
#ifndef ENCIBM855_T3262835758_H
#define ENCIBM855_T3262835758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm855
struct  ENCibm855_t3262835758  : public CP855_t1784887909
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM855_T3262835758_H
#ifndef ENCWINDOWS_1250_T336475776_H
#define ENCWINDOWS_1250_T336475776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1250
struct  ENCwindows_1250_t336475776  : public CP1250_t653777969
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1250_T336475776_H
#ifndef ENCX_MAC_ICELANDIC_T62238089_H
#define ENCX_MAC_ICELANDIC_T62238089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCx_mac_icelandic
struct  ENCx_mac_icelandic_t62238089  : public CP10079_t1554584814
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCX_MAC_ICELANDIC_T62238089_H
#ifndef EVENTHANDLER_T1067160880_H
#define EVENTHANDLER_T1067160880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes/EventHandler
struct  EventHandler_t1067160880  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T1067160880_H
#ifndef ENCISO_8859_15_T1092500295_H
#define ENCISO_8859_15_T1092500295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_15
struct  ENCiso_8859_15_t1092500295  : public CP28605_t311235100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_15_T1092500295_H
#ifndef ENCIBM437_T3242889364_H
#define ENCIBM437_T3242889364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm437
struct  ENCibm437_t3242889364  : public CP437_t2775482021
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM437_T3242889364_H
#ifndef ENCIBM1025_T1780724032_H
#define ENCIBM1025_T1780724032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm1025
struct  ENCibm1025_t1780724032  : public CP21025_t3771795245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM1025_T1780724032_H
#ifndef ENCISO_8859_7_T2126773860_H
#define ENCISO_8859_7_T2126773860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_7
struct  ENCiso_8859_7_t2126773860  : public CP28597_t2233549560
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_7_T2126773860_H
#ifndef ENCIBM037_T2100036606_H
#define ENCIBM037_T2100036606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm037
struct  ENCibm037_t2100036606  : public CP37_t661932205
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM037_T2100036606_H
#ifndef ENCIBM860_T3646173758_H
#define ENCIBM860_T3646173758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm860
struct  ENCibm860_t3646173758  : public CP860_t3178766153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM860_T3646173758_H
#ifndef ENCIBM861_T3646173757_H
#define ENCIBM861_T3646173757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm861
struct  ENCibm861_t3646173757  : public CP861_t3178766154
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM861_T3646173757_H
#ifndef ENCIBM863_T3646173755_H
#define ENCIBM863_T3646173755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm863
struct  ENCibm863_t3646173755  : public CP863_t3178766156
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM863_T3646173755_H
#ifndef ENCIBM850_T4049458285_H
#define ENCIBM850_T4049458285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm850
struct  ENCibm850_t4049458285  : public CP850_t1612682212
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM850_T4049458285_H
#ifndef ENCIBM865_T3646173753_H
#define ENCIBM865_T3646173753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm865
struct  ENCibm865_t3646173753  : public CP865_t3178766158
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM865_T3646173753_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ENCISO_8859_3_T2126773856_H
#define ENCISO_8859_3_T2126773856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_3
struct  ENCiso_8859_3_t2126773856  : public CP28593_t2233549564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_3_T2126773856_H
#ifndef ENCIBM852_T3262835751_H
#define ENCIBM852_T3262835751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm852
struct  ENCibm852_t3262835751  : public CP852_t1784887916
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM852_T3262835751_H
#ifndef ENCASMO_708_T1618404456_H
#define ENCASMO_708_T1618404456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCasmo_708
struct  ENCasmo_708_t1618404456  : public CP708_t2188172662
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCASMO_708_T1618404456_H
#ifndef ENCISO_8859_2_T2126773855_H
#define ENCISO_8859_2_T2126773855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_2
struct  ENCiso_8859_2_t2126773855  : public CP28592_t2233549565
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_2_T2126773855_H
#ifndef ENCWINDOWS_1253_T336475775_H
#define ENCWINDOWS_1253_T336475775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1253
struct  ENCwindows_1253_t336475775  : public CP1253_t3382661324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1253_T336475775_H
#ifndef ENCIBM500_T3666120773_H
#define ENCIBM500_T3666120773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm500
struct  ENCibm500_t3666120773  : public CP500_t2188172604
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM500_T3666120773_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SINGLETON_1_T4294043717_H
#define SINGLETON_1_T4294043717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.RemoveSurfaceVertices>
struct  Singleton_1_t4294043717  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t4294043717_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	RemoveSurfaceVertices_t3740445364 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t4294043717_StaticFields, ____Instance_2)); }
	inline RemoveSurfaceVertices_t3740445364 * get__Instance_2() const { return ____Instance_2; }
	inline RemoveSurfaceVertices_t3740445364 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(RemoveSurfaceVertices_t3740445364 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4294043717_H
#ifndef SINGLETON_1_T4125947580_H
#define SINGLETON_1_T4125947580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.HandsManager>
struct  Singleton_1_t4125947580  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t4125947580_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	HandsManager_t3572349227 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t4125947580_StaticFields, ____Instance_2)); }
	inline HandsManager_t3572349227 * get__Instance_2() const { return ____Instance_2; }
	inline HandsManager_t3572349227 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(HandsManager_t3572349227 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4125947580_H
#ifndef SINGLETON_1_T2809864957_H
#define SINGLETON_1_T2809864957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.GazeManager>
struct  Singleton_1_t2809864957  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t2809864957_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	GazeManager_t2256266604 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2809864957_StaticFields, ____Instance_2)); }
	inline GazeManager_t2256266604 * get__Instance_2() const { return ____Instance_2; }
	inline GazeManager_t2256266604 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(GazeManager_t2256266604 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2809864957_H
#ifndef SINGLETON_1_T2198846191_H
#define SINGLETON_1_T2198846191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.GestureManager>
struct  Singleton_1_t2198846191  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t2198846191_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	GestureManager_t1645247838 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2198846191_StaticFields, ____Instance_2)); }
	inline GestureManager_t1645247838 * get__Instance_2() const { return ____Instance_2; }
	inline GestureManager_t1645247838 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(GestureManager_t1645247838 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2198846191_H
#ifndef SINGLETON_1_T2912066921_H
#define SINGLETON_1_T2912066921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SurfaceMeshesToPlanes>
struct  Singleton_1_t2912066921  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t2912066921_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	SurfaceMeshesToPlanes_t2358468568 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2912066921_StaticFields, ____Instance_2)); }
	inline SurfaceMeshesToPlanes_t2358468568 * get__Instance_2() const { return ____Instance_2; }
	inline SurfaceMeshesToPlanes_t2358468568 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(SurfaceMeshesToPlanes_t2358468568 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2912066921_H
#ifndef SINGLETON_1_T2992951914_H
#define SINGLETON_1_T2992951914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SpatialMappingManager>
struct  Singleton_1_t2992951914  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t2992951914_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	SpatialMappingManager_t2439353561 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2992951914_StaticFields, ____Instance_2)); }
	inline SpatialMappingManager_t2439353561 * get__Instance_2() const { return ____Instance_2; }
	inline SpatialMappingManager_t2439353561 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(SpatialMappingManager_t2439353561 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2992951914_H
#ifndef PLANETOCCLUSION_T2352355769_H
#define PLANETOCCLUSION_T2352355769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlanetOcclusion
struct  PlanetOcclusion_t2352355769  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PlanetOcclusion::occlusionObject
	GameObject_t1756533147 * ___occlusionObject_2;
	// UnityEngine.Vector3[] PlanetOcclusion::checkPoints
	Vector3U5BU5D_t1172311765* ___checkPoints_3;

public:
	inline static int32_t get_offset_of_occlusionObject_2() { return static_cast<int32_t>(offsetof(PlanetOcclusion_t2352355769, ___occlusionObject_2)); }
	inline GameObject_t1756533147 * get_occlusionObject_2() const { return ___occlusionObject_2; }
	inline GameObject_t1756533147 ** get_address_of_occlusionObject_2() { return &___occlusionObject_2; }
	inline void set_occlusionObject_2(GameObject_t1756533147 * value)
	{
		___occlusionObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___occlusionObject_2), value);
	}

	inline static int32_t get_offset_of_checkPoints_3() { return static_cast<int32_t>(offsetof(PlanetOcclusion_t2352355769, ___checkPoints_3)); }
	inline Vector3U5BU5D_t1172311765* get_checkPoints_3() const { return ___checkPoints_3; }
	inline Vector3U5BU5D_t1172311765** get_address_of_checkPoints_3() { return &___checkPoints_3; }
	inline void set_checkPoints_3(Vector3U5BU5D_t1172311765* value)
	{
		___checkPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___checkPoints_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETOCCLUSION_T2352355769_H
#ifndef BASICCURSOR_T2131533874_H
#define BASICCURSOR_T2131533874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.BasicCursor
struct  BasicCursor_t2131533874  : public MonoBehaviour_t1158329972
{
public:
	// System.Single HoloToolkit.Unity.BasicCursor::DistanceFromCollision
	float ___DistanceFromCollision_2;
	// UnityEngine.Quaternion HoloToolkit.Unity.BasicCursor::cursorDefaultRotation
	Quaternion_t4030073918  ___cursorDefaultRotation_3;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.BasicCursor::meshRenderer
	MeshRenderer_t1268241104 * ___meshRenderer_4;

public:
	inline static int32_t get_offset_of_DistanceFromCollision_2() { return static_cast<int32_t>(offsetof(BasicCursor_t2131533874, ___DistanceFromCollision_2)); }
	inline float get_DistanceFromCollision_2() const { return ___DistanceFromCollision_2; }
	inline float* get_address_of_DistanceFromCollision_2() { return &___DistanceFromCollision_2; }
	inline void set_DistanceFromCollision_2(float value)
	{
		___DistanceFromCollision_2 = value;
	}

	inline static int32_t get_offset_of_cursorDefaultRotation_3() { return static_cast<int32_t>(offsetof(BasicCursor_t2131533874, ___cursorDefaultRotation_3)); }
	inline Quaternion_t4030073918  get_cursorDefaultRotation_3() const { return ___cursorDefaultRotation_3; }
	inline Quaternion_t4030073918 * get_address_of_cursorDefaultRotation_3() { return &___cursorDefaultRotation_3; }
	inline void set_cursorDefaultRotation_3(Quaternion_t4030073918  value)
	{
		___cursorDefaultRotation_3 = value;
	}

	inline static int32_t get_offset_of_meshRenderer_4() { return static_cast<int32_t>(offsetof(BasicCursor_t2131533874, ___meshRenderer_4)); }
	inline MeshRenderer_t1268241104 * get_meshRenderer_4() const { return ___meshRenderer_4; }
	inline MeshRenderer_t1268241104 ** get_address_of_meshRenderer_4() { return &___meshRenderer_4; }
	inline void set_meshRenderer_4(MeshRenderer_t1268241104 * value)
	{
		___meshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCURSOR_T2131533874_H
#ifndef INTERACTIBLE_T1795579682_H
#define INTERACTIBLE_T1795579682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interactible
struct  Interactible_t1795579682  : public MonoBehaviour_t1158329972
{
public:
	// InteractibleParameters Interactible::InteractibleParameters
	InteractibleParameters_t2389709274 * ___InteractibleParameters_2;
	// UnityEngine.AudioClip Interactible::TargetFeedbackSound
	AudioClip_t1932558630 * ___TargetFeedbackSound_3;
	// UnityEngine.AudioSource Interactible::audioSource
	AudioSource_t1135106623 * ___audioSource_4;

public:
	inline static int32_t get_offset_of_InteractibleParameters_2() { return static_cast<int32_t>(offsetof(Interactible_t1795579682, ___InteractibleParameters_2)); }
	inline InteractibleParameters_t2389709274 * get_InteractibleParameters_2() const { return ___InteractibleParameters_2; }
	inline InteractibleParameters_t2389709274 ** get_address_of_InteractibleParameters_2() { return &___InteractibleParameters_2; }
	inline void set_InteractibleParameters_2(InteractibleParameters_t2389709274 * value)
	{
		___InteractibleParameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___InteractibleParameters_2), value);
	}

	inline static int32_t get_offset_of_TargetFeedbackSound_3() { return static_cast<int32_t>(offsetof(Interactible_t1795579682, ___TargetFeedbackSound_3)); }
	inline AudioClip_t1932558630 * get_TargetFeedbackSound_3() const { return ___TargetFeedbackSound_3; }
	inline AudioClip_t1932558630 ** get_address_of_TargetFeedbackSound_3() { return &___TargetFeedbackSound_3; }
	inline void set_TargetFeedbackSound_3(AudioClip_t1932558630 * value)
	{
		___TargetFeedbackSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetFeedbackSound_3), value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(Interactible_t1795579682, ___audioSource_4)); }
	inline AudioSource_t1135106623 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t1135106623 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIBLE_T1795579682_H
#ifndef SPATIALMAPPINGSOURCE_T2008115569_H
#define SPATIALMAPPINGSOURCE_T2008115569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMappingSource
struct  SpatialMappingSource_t2008115569  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMappingSource/SurfaceObject> HoloToolkit.Unity.SpatialMappingSource::<SurfaceObjects>k__BackingField
	List_1_t557165259 * ___U3CSurfaceObjectsU3Ek__BackingField_2;
	// System.Type[] HoloToolkit.Unity.SpatialMappingSource::componentsRequiredForSurfaceMesh
	TypeU5BU5D_t1664964607* ___componentsRequiredForSurfaceMesh_3;

public:
	inline static int32_t get_offset_of_U3CSurfaceObjectsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t2008115569, ___U3CSurfaceObjectsU3Ek__BackingField_2)); }
	inline List_1_t557165259 * get_U3CSurfaceObjectsU3Ek__BackingField_2() const { return ___U3CSurfaceObjectsU3Ek__BackingField_2; }
	inline List_1_t557165259 ** get_address_of_U3CSurfaceObjectsU3Ek__BackingField_2() { return &___U3CSurfaceObjectsU3Ek__BackingField_2; }
	inline void set_U3CSurfaceObjectsU3Ek__BackingField_2(List_1_t557165259 * value)
	{
		___U3CSurfaceObjectsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSurfaceObjectsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_componentsRequiredForSurfaceMesh_3() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t2008115569, ___componentsRequiredForSurfaceMesh_3)); }
	inline TypeU5BU5D_t1664964607* get_componentsRequiredForSurfaceMesh_3() const { return ___componentsRequiredForSurfaceMesh_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_componentsRequiredForSurfaceMesh_3() { return &___componentsRequiredForSurfaceMesh_3; }
	inline void set_componentsRequiredForSurfaceMesh_3(TypeU5BU5D_t1664964607* value)
	{
		___componentsRequiredForSurfaceMesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___componentsRequiredForSurfaceMesh_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALMAPPINGSOURCE_T2008115569_H
#ifndef PLACEABLE_T582649283_H
#define PLACEABLE_T582649283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Placeable
struct  Placeable_t582649283  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material Placeable::PlaceableBoundsMaterial
	Material_t193706927 * ___PlaceableBoundsMaterial_2;
	// UnityEngine.Material Placeable::NotPlaceableBoundsMaterial
	Material_t193706927 * ___NotPlaceableBoundsMaterial_3;
	// UnityEngine.Material Placeable::PlaceableShadowMaterial
	Material_t193706927 * ___PlaceableShadowMaterial_4;
	// UnityEngine.Material Placeable::NotPlaceableShadowMaterial
	Material_t193706927 * ___NotPlaceableShadowMaterial_5;
	// PlacementSurfaces Placeable::PlacementSurface
	int32_t ___PlacementSurface_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Placeable::ChildrenToHide
	List_1_t1125654279 * ___ChildrenToHide_7;
	// System.Boolean Placeable::<IsPlacing>k__BackingField
	bool ___U3CIsPlacingU3Ek__BackingField_8;
	// System.Single Placeable::lastDistance
	float ___lastDistance_9;
	// System.Single Placeable::hoverDistance
	float ___hoverDistance_10;
	// System.Single Placeable::distanceThreshold
	float ___distanceThreshold_11;
	// System.Single Placeable::upNormalThreshold
	float ___upNormalThreshold_12;
	// System.Single Placeable::maximumPlacementDistance
	float ___maximumPlacementDistance_13;
	// System.Single Placeable::placementVelocity
	float ___placementVelocity_14;
	// System.Boolean Placeable::managingBoxCollider
	bool ___managingBoxCollider_15;
	// UnityEngine.BoxCollider Placeable::boxCollider
	BoxCollider_t22920061 * ___boxCollider_16;
	// UnityEngine.GameObject Placeable::boundsAsset
	GameObject_t1756533147 * ___boundsAsset_17;
	// UnityEngine.GameObject Placeable::shadowAsset
	GameObject_t1756533147 * ___shadowAsset_18;
	// UnityEngine.Vector3 Placeable::targetPosition
	Vector3_t2243707580  ___targetPosition_19;

public:
	inline static int32_t get_offset_of_PlaceableBoundsMaterial_2() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___PlaceableBoundsMaterial_2)); }
	inline Material_t193706927 * get_PlaceableBoundsMaterial_2() const { return ___PlaceableBoundsMaterial_2; }
	inline Material_t193706927 ** get_address_of_PlaceableBoundsMaterial_2() { return &___PlaceableBoundsMaterial_2; }
	inline void set_PlaceableBoundsMaterial_2(Material_t193706927 * value)
	{
		___PlaceableBoundsMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlaceableBoundsMaterial_2), value);
	}

	inline static int32_t get_offset_of_NotPlaceableBoundsMaterial_3() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___NotPlaceableBoundsMaterial_3)); }
	inline Material_t193706927 * get_NotPlaceableBoundsMaterial_3() const { return ___NotPlaceableBoundsMaterial_3; }
	inline Material_t193706927 ** get_address_of_NotPlaceableBoundsMaterial_3() { return &___NotPlaceableBoundsMaterial_3; }
	inline void set_NotPlaceableBoundsMaterial_3(Material_t193706927 * value)
	{
		___NotPlaceableBoundsMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___NotPlaceableBoundsMaterial_3), value);
	}

	inline static int32_t get_offset_of_PlaceableShadowMaterial_4() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___PlaceableShadowMaterial_4)); }
	inline Material_t193706927 * get_PlaceableShadowMaterial_4() const { return ___PlaceableShadowMaterial_4; }
	inline Material_t193706927 ** get_address_of_PlaceableShadowMaterial_4() { return &___PlaceableShadowMaterial_4; }
	inline void set_PlaceableShadowMaterial_4(Material_t193706927 * value)
	{
		___PlaceableShadowMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___PlaceableShadowMaterial_4), value);
	}

	inline static int32_t get_offset_of_NotPlaceableShadowMaterial_5() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___NotPlaceableShadowMaterial_5)); }
	inline Material_t193706927 * get_NotPlaceableShadowMaterial_5() const { return ___NotPlaceableShadowMaterial_5; }
	inline Material_t193706927 ** get_address_of_NotPlaceableShadowMaterial_5() { return &___NotPlaceableShadowMaterial_5; }
	inline void set_NotPlaceableShadowMaterial_5(Material_t193706927 * value)
	{
		___NotPlaceableShadowMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___NotPlaceableShadowMaterial_5), value);
	}

	inline static int32_t get_offset_of_PlacementSurface_6() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___PlacementSurface_6)); }
	inline int32_t get_PlacementSurface_6() const { return ___PlacementSurface_6; }
	inline int32_t* get_address_of_PlacementSurface_6() { return &___PlacementSurface_6; }
	inline void set_PlacementSurface_6(int32_t value)
	{
		___PlacementSurface_6 = value;
	}

	inline static int32_t get_offset_of_ChildrenToHide_7() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___ChildrenToHide_7)); }
	inline List_1_t1125654279 * get_ChildrenToHide_7() const { return ___ChildrenToHide_7; }
	inline List_1_t1125654279 ** get_address_of_ChildrenToHide_7() { return &___ChildrenToHide_7; }
	inline void set_ChildrenToHide_7(List_1_t1125654279 * value)
	{
		___ChildrenToHide_7 = value;
		Il2CppCodeGenWriteBarrier((&___ChildrenToHide_7), value);
	}

	inline static int32_t get_offset_of_U3CIsPlacingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___U3CIsPlacingU3Ek__BackingField_8)); }
	inline bool get_U3CIsPlacingU3Ek__BackingField_8() const { return ___U3CIsPlacingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsPlacingU3Ek__BackingField_8() { return &___U3CIsPlacingU3Ek__BackingField_8; }
	inline void set_U3CIsPlacingU3Ek__BackingField_8(bool value)
	{
		___U3CIsPlacingU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_lastDistance_9() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___lastDistance_9)); }
	inline float get_lastDistance_9() const { return ___lastDistance_9; }
	inline float* get_address_of_lastDistance_9() { return &___lastDistance_9; }
	inline void set_lastDistance_9(float value)
	{
		___lastDistance_9 = value;
	}

	inline static int32_t get_offset_of_hoverDistance_10() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___hoverDistance_10)); }
	inline float get_hoverDistance_10() const { return ___hoverDistance_10; }
	inline float* get_address_of_hoverDistance_10() { return &___hoverDistance_10; }
	inline void set_hoverDistance_10(float value)
	{
		___hoverDistance_10 = value;
	}

	inline static int32_t get_offset_of_distanceThreshold_11() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___distanceThreshold_11)); }
	inline float get_distanceThreshold_11() const { return ___distanceThreshold_11; }
	inline float* get_address_of_distanceThreshold_11() { return &___distanceThreshold_11; }
	inline void set_distanceThreshold_11(float value)
	{
		___distanceThreshold_11 = value;
	}

	inline static int32_t get_offset_of_upNormalThreshold_12() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___upNormalThreshold_12)); }
	inline float get_upNormalThreshold_12() const { return ___upNormalThreshold_12; }
	inline float* get_address_of_upNormalThreshold_12() { return &___upNormalThreshold_12; }
	inline void set_upNormalThreshold_12(float value)
	{
		___upNormalThreshold_12 = value;
	}

	inline static int32_t get_offset_of_maximumPlacementDistance_13() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___maximumPlacementDistance_13)); }
	inline float get_maximumPlacementDistance_13() const { return ___maximumPlacementDistance_13; }
	inline float* get_address_of_maximumPlacementDistance_13() { return &___maximumPlacementDistance_13; }
	inline void set_maximumPlacementDistance_13(float value)
	{
		___maximumPlacementDistance_13 = value;
	}

	inline static int32_t get_offset_of_placementVelocity_14() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___placementVelocity_14)); }
	inline float get_placementVelocity_14() const { return ___placementVelocity_14; }
	inline float* get_address_of_placementVelocity_14() { return &___placementVelocity_14; }
	inline void set_placementVelocity_14(float value)
	{
		___placementVelocity_14 = value;
	}

	inline static int32_t get_offset_of_managingBoxCollider_15() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___managingBoxCollider_15)); }
	inline bool get_managingBoxCollider_15() const { return ___managingBoxCollider_15; }
	inline bool* get_address_of_managingBoxCollider_15() { return &___managingBoxCollider_15; }
	inline void set_managingBoxCollider_15(bool value)
	{
		___managingBoxCollider_15 = value;
	}

	inline static int32_t get_offset_of_boxCollider_16() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___boxCollider_16)); }
	inline BoxCollider_t22920061 * get_boxCollider_16() const { return ___boxCollider_16; }
	inline BoxCollider_t22920061 ** get_address_of_boxCollider_16() { return &___boxCollider_16; }
	inline void set_boxCollider_16(BoxCollider_t22920061 * value)
	{
		___boxCollider_16 = value;
		Il2CppCodeGenWriteBarrier((&___boxCollider_16), value);
	}

	inline static int32_t get_offset_of_boundsAsset_17() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___boundsAsset_17)); }
	inline GameObject_t1756533147 * get_boundsAsset_17() const { return ___boundsAsset_17; }
	inline GameObject_t1756533147 ** get_address_of_boundsAsset_17() { return &___boundsAsset_17; }
	inline void set_boundsAsset_17(GameObject_t1756533147 * value)
	{
		___boundsAsset_17 = value;
		Il2CppCodeGenWriteBarrier((&___boundsAsset_17), value);
	}

	inline static int32_t get_offset_of_shadowAsset_18() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___shadowAsset_18)); }
	inline GameObject_t1756533147 * get_shadowAsset_18() const { return ___shadowAsset_18; }
	inline GameObject_t1756533147 ** get_address_of_shadowAsset_18() { return &___shadowAsset_18; }
	inline void set_shadowAsset_18(GameObject_t1756533147 * value)
	{
		___shadowAsset_18 = value;
		Il2CppCodeGenWriteBarrier((&___shadowAsset_18), value);
	}

	inline static int32_t get_offset_of_targetPosition_19() { return static_cast<int32_t>(offsetof(Placeable_t582649283, ___targetPosition_19)); }
	inline Vector3_t2243707580  get_targetPosition_19() const { return ___targetPosition_19; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_19() { return &___targetPosition_19; }
	inline void set_targetPosition_19(Vector3_t2243707580  value)
	{
		___targetPosition_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEABLE_T582649283_H
#ifndef PLANETORBIT_T2107838810_H
#define PLANETORBIT_T2107838810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlanetOrbit
struct  PlanetOrbit_t2107838810  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlanetOrbit::orbitPeriod
	float ___orbitPeriod_2;
	// System.Single PlanetOrbit::rotatePeriod
	float ___rotatePeriod_3;
	// System.Single PlanetOrbit::distance
	float ___distance_4;
	// UnityEngine.GameObject PlanetOrbit::sun
	GameObject_t1756533147 * ___sun_5;

public:
	inline static int32_t get_offset_of_orbitPeriod_2() { return static_cast<int32_t>(offsetof(PlanetOrbit_t2107838810, ___orbitPeriod_2)); }
	inline float get_orbitPeriod_2() const { return ___orbitPeriod_2; }
	inline float* get_address_of_orbitPeriod_2() { return &___orbitPeriod_2; }
	inline void set_orbitPeriod_2(float value)
	{
		___orbitPeriod_2 = value;
	}

	inline static int32_t get_offset_of_rotatePeriod_3() { return static_cast<int32_t>(offsetof(PlanetOrbit_t2107838810, ___rotatePeriod_3)); }
	inline float get_rotatePeriod_3() const { return ___rotatePeriod_3; }
	inline float* get_address_of_rotatePeriod_3() { return &___rotatePeriod_3; }
	inline void set_rotatePeriod_3(float value)
	{
		___rotatePeriod_3 = value;
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(PlanetOrbit_t2107838810, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_sun_5() { return static_cast<int32_t>(offsetof(PlanetOrbit_t2107838810, ___sun_5)); }
	inline GameObject_t1756533147 * get_sun_5() const { return ___sun_5; }
	inline GameObject_t1756533147 ** get_address_of_sun_5() { return &___sun_5; }
	inline void set_sun_5(GameObject_t1756533147 * value)
	{
		___sun_5 = value;
		Il2CppCodeGenWriteBarrier((&___sun_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETORBIT_T2107838810_H
#ifndef ROTATEASTEROIDS_T2371484563_H
#define ROTATEASTEROIDS_T2371484563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateAsteroids
struct  RotateAsteroids_t2371484563  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RotateAsteroids::period
	float ___period_2;

public:
	inline static int32_t get_offset_of_period_2() { return static_cast<int32_t>(offsetof(RotateAsteroids_t2371484563, ___period_2)); }
	inline float get_period_2() const { return ___period_2; }
	inline float* get_address_of_period_2() { return &___period_2; }
	inline void set_period_2(float value)
	{
		___period_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEASTEROIDS_T2371484563_H
#ifndef SINGLETON_1_T1008660768_H
#define SINGLETON_1_T1008660768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<SpaceCollectionManager>
struct  Singleton_1_t1008660768  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t1008660768_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	SpaceCollectionManager_t455062415 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1008660768_StaticFields, ____Instance_2)); }
	inline SpaceCollectionManager_t455062415 * get__Instance_2() const { return ____Instance_2; }
	inline SpaceCollectionManager_t455062415 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(SpaceCollectionManager_t455062415 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1008660768_H
#ifndef SINGLETON_1_T4053771974_H
#define SINGLETON_1_T4053771974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<SunScript>
struct  Singleton_1_t4053771974  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t4053771974_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	SunScript_t3500173621 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t4053771974_StaticFields, ____Instance_2)); }
	inline SunScript_t3500173621 * get__Instance_2() const { return ____Instance_2; }
	inline SunScript_t3500173621 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(SunScript_t3500173621 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4053771974_H
#ifndef SURFACEPLANE_T191565817_H
#define SURFACEPLANE_T191565817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfacePlane
struct  SurfacePlane_t191565817  : public MonoBehaviour_t1158329972
{
public:
	// System.Single HoloToolkit.Unity.SurfacePlane::PlaneThickness
	float ___PlaneThickness_2;
	// System.Single HoloToolkit.Unity.SurfacePlane::UpNormalThreshold
	float ___UpNormalThreshold_3;
	// System.Single HoloToolkit.Unity.SurfacePlane::FloorBuffer
	float ___FloorBuffer_4;
	// System.Single HoloToolkit.Unity.SurfacePlane::CeilingBuffer
	float ___CeilingBuffer_5;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::WallMaterial
	Material_t193706927 * ___WallMaterial_6;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::FloorMaterial
	Material_t193706927 * ___FloorMaterial_7;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::CeilingMaterial
	Material_t193706927 * ___CeilingMaterial_8;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::TableMaterial
	Material_t193706927 * ___TableMaterial_9;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::UnknownMaterial
	Material_t193706927 * ___UnknownMaterial_10;
	// HoloToolkit.Unity.PlaneTypes HoloToolkit.Unity.SurfacePlane::PlaneType
	int32_t ___PlaneType_11;
	// HoloToolkit.Unity.BoundedPlane HoloToolkit.Unity.SurfacePlane::plane
	BoundedPlane_t3629713867  ___plane_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.SurfacePlane::<SurfaceNormal>k__BackingField
	Vector3_t2243707580  ___U3CSurfaceNormalU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_PlaneThickness_2() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___PlaneThickness_2)); }
	inline float get_PlaneThickness_2() const { return ___PlaneThickness_2; }
	inline float* get_address_of_PlaneThickness_2() { return &___PlaneThickness_2; }
	inline void set_PlaneThickness_2(float value)
	{
		___PlaneThickness_2 = value;
	}

	inline static int32_t get_offset_of_UpNormalThreshold_3() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___UpNormalThreshold_3)); }
	inline float get_UpNormalThreshold_3() const { return ___UpNormalThreshold_3; }
	inline float* get_address_of_UpNormalThreshold_3() { return &___UpNormalThreshold_3; }
	inline void set_UpNormalThreshold_3(float value)
	{
		___UpNormalThreshold_3 = value;
	}

	inline static int32_t get_offset_of_FloorBuffer_4() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___FloorBuffer_4)); }
	inline float get_FloorBuffer_4() const { return ___FloorBuffer_4; }
	inline float* get_address_of_FloorBuffer_4() { return &___FloorBuffer_4; }
	inline void set_FloorBuffer_4(float value)
	{
		___FloorBuffer_4 = value;
	}

	inline static int32_t get_offset_of_CeilingBuffer_5() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___CeilingBuffer_5)); }
	inline float get_CeilingBuffer_5() const { return ___CeilingBuffer_5; }
	inline float* get_address_of_CeilingBuffer_5() { return &___CeilingBuffer_5; }
	inline void set_CeilingBuffer_5(float value)
	{
		___CeilingBuffer_5 = value;
	}

	inline static int32_t get_offset_of_WallMaterial_6() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___WallMaterial_6)); }
	inline Material_t193706927 * get_WallMaterial_6() const { return ___WallMaterial_6; }
	inline Material_t193706927 ** get_address_of_WallMaterial_6() { return &___WallMaterial_6; }
	inline void set_WallMaterial_6(Material_t193706927 * value)
	{
		___WallMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___WallMaterial_6), value);
	}

	inline static int32_t get_offset_of_FloorMaterial_7() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___FloorMaterial_7)); }
	inline Material_t193706927 * get_FloorMaterial_7() const { return ___FloorMaterial_7; }
	inline Material_t193706927 ** get_address_of_FloorMaterial_7() { return &___FloorMaterial_7; }
	inline void set_FloorMaterial_7(Material_t193706927 * value)
	{
		___FloorMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___FloorMaterial_7), value);
	}

	inline static int32_t get_offset_of_CeilingMaterial_8() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___CeilingMaterial_8)); }
	inline Material_t193706927 * get_CeilingMaterial_8() const { return ___CeilingMaterial_8; }
	inline Material_t193706927 ** get_address_of_CeilingMaterial_8() { return &___CeilingMaterial_8; }
	inline void set_CeilingMaterial_8(Material_t193706927 * value)
	{
		___CeilingMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___CeilingMaterial_8), value);
	}

	inline static int32_t get_offset_of_TableMaterial_9() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___TableMaterial_9)); }
	inline Material_t193706927 * get_TableMaterial_9() const { return ___TableMaterial_9; }
	inline Material_t193706927 ** get_address_of_TableMaterial_9() { return &___TableMaterial_9; }
	inline void set_TableMaterial_9(Material_t193706927 * value)
	{
		___TableMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___TableMaterial_9), value);
	}

	inline static int32_t get_offset_of_UnknownMaterial_10() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___UnknownMaterial_10)); }
	inline Material_t193706927 * get_UnknownMaterial_10() const { return ___UnknownMaterial_10; }
	inline Material_t193706927 ** get_address_of_UnknownMaterial_10() { return &___UnknownMaterial_10; }
	inline void set_UnknownMaterial_10(Material_t193706927 * value)
	{
		___UnknownMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___UnknownMaterial_10), value);
	}

	inline static int32_t get_offset_of_PlaneType_11() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___PlaneType_11)); }
	inline int32_t get_PlaneType_11() const { return ___PlaneType_11; }
	inline int32_t* get_address_of_PlaneType_11() { return &___PlaneType_11; }
	inline void set_PlaneType_11(int32_t value)
	{
		___PlaneType_11 = value;
	}

	inline static int32_t get_offset_of_plane_12() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___plane_12)); }
	inline BoundedPlane_t3629713867  get_plane_12() const { return ___plane_12; }
	inline BoundedPlane_t3629713867 * get_address_of_plane_12() { return &___plane_12; }
	inline void set_plane_12(BoundedPlane_t3629713867  value)
	{
		___plane_12 = value;
	}

	inline static int32_t get_offset_of_U3CSurfaceNormalU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___U3CSurfaceNormalU3Ek__BackingField_13)); }
	inline Vector3_t2243707580  get_U3CSurfaceNormalU3Ek__BackingField_13() const { return ___U3CSurfaceNormalU3Ek__BackingField_13; }
	inline Vector3_t2243707580 * get_address_of_U3CSurfaceNormalU3Ek__BackingField_13() { return &___U3CSurfaceNormalU3Ek__BackingField_13; }
	inline void set_U3CSurfaceNormalU3Ek__BackingField_13(Vector3_t2243707580  value)
	{
		___U3CSurfaceNormalU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEPLANE_T191565817_H
#ifndef SINGLETON_1_T3246108146_H
#define SINGLETON_1_T3246108146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<InteractibleManager>
struct  Singleton_1_t3246108146  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t3246108146_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	InteractibleManager_t2692509793 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3246108146_StaticFields, ____Instance_2)); }
	inline InteractibleManager_t2692509793 * get__Instance_2() const { return ____Instance_2; }
	inline InteractibleManager_t2692509793 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(InteractibleManager_t2692509793 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3246108146_H
#ifndef SINGLETON_1_T1244339818_H
#define SINGLETON_1_T1244339818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<PlaySpaceManager>
struct  Singleton_1_t1244339818  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t1244339818_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	PlaySpaceManager_t690741465 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1244339818_StaticFields, ____Instance_2)); }
	inline PlaySpaceManager_t690741465 * get__Instance_2() const { return ____Instance_2; }
	inline PlaySpaceManager_t690741465 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(PlaySpaceManager_t690741465 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1244339818_H
#ifndef SUNSCRIPT_T3500173621_H
#define SUNSCRIPT_T3500173621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SunScript
struct  SunScript_t3500173621  : public Singleton_1_t4053771974
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SunScript/PlanetaryData> SunScript::PlanetData
	Dictionary_2_t4140758868 * ___PlanetData_5;
	// System.Single SunScript::EarthYear
	float ___EarthYear_6;
	// System.Single SunScript::OneAUInMeters
	float ___OneAUInMeters_7;

public:
	inline static int32_t get_offset_of_PlanetData_5() { return static_cast<int32_t>(offsetof(SunScript_t3500173621, ___PlanetData_5)); }
	inline Dictionary_2_t4140758868 * get_PlanetData_5() const { return ___PlanetData_5; }
	inline Dictionary_2_t4140758868 ** get_address_of_PlanetData_5() { return &___PlanetData_5; }
	inline void set_PlanetData_5(Dictionary_2_t4140758868 * value)
	{
		___PlanetData_5 = value;
		Il2CppCodeGenWriteBarrier((&___PlanetData_5), value);
	}

	inline static int32_t get_offset_of_EarthYear_6() { return static_cast<int32_t>(offsetof(SunScript_t3500173621, ___EarthYear_6)); }
	inline float get_EarthYear_6() const { return ___EarthYear_6; }
	inline float* get_address_of_EarthYear_6() { return &___EarthYear_6; }
	inline void set_EarthYear_6(float value)
	{
		___EarthYear_6 = value;
	}

	inline static int32_t get_offset_of_OneAUInMeters_7() { return static_cast<int32_t>(offsetof(SunScript_t3500173621, ___OneAUInMeters_7)); }
	inline float get_OneAUInMeters_7() const { return ___OneAUInMeters_7; }
	inline float* get_address_of_OneAUInMeters_7() { return &___OneAUInMeters_7; }
	inline void set_OneAUInMeters_7(float value)
	{
		___OneAUInMeters_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUNSCRIPT_T3500173621_H
#ifndef HANDSMANAGER_T3572349227_H
#define HANDSMANAGER_T3572349227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HandsManager
struct  HandsManager_t3572349227  : public Singleton_1_t4125947580
{
public:
	// System.Collections.Generic.List`1<System.UInt32> HoloToolkit.Unity.HandsManager::trackedHands
	List_1_t1518803153 * ___trackedHands_3;

public:
	inline static int32_t get_offset_of_trackedHands_3() { return static_cast<int32_t>(offsetof(HandsManager_t3572349227, ___trackedHands_3)); }
	inline List_1_t1518803153 * get_trackedHands_3() const { return ___trackedHands_3; }
	inline List_1_t1518803153 ** get_address_of_trackedHands_3() { return &___trackedHands_3; }
	inline void set_trackedHands_3(List_1_t1518803153 * value)
	{
		___trackedHands_3 = value;
		Il2CppCodeGenWriteBarrier((&___trackedHands_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSMANAGER_T3572349227_H
#ifndef INTERACTIBLEMANAGER_T2692509793_H
#define INTERACTIBLEMANAGER_T2692509793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InteractibleManager
struct  InteractibleManager_t2692509793  : public Singleton_1_t3246108146
{
public:
	// UnityEngine.GameObject InteractibleManager::<FocusedGameObject>k__BackingField
	GameObject_t1756533147 * ___U3CFocusedGameObjectU3Ek__BackingField_3;
	// UnityEngine.GameObject InteractibleManager::oldFocusedGameObject
	GameObject_t1756533147 * ___oldFocusedGameObject_4;

public:
	inline static int32_t get_offset_of_U3CFocusedGameObjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InteractibleManager_t2692509793, ___U3CFocusedGameObjectU3Ek__BackingField_3)); }
	inline GameObject_t1756533147 * get_U3CFocusedGameObjectU3Ek__BackingField_3() const { return ___U3CFocusedGameObjectU3Ek__BackingField_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CFocusedGameObjectU3Ek__BackingField_3() { return &___U3CFocusedGameObjectU3Ek__BackingField_3; }
	inline void set_U3CFocusedGameObjectU3Ek__BackingField_3(GameObject_t1756533147 * value)
	{
		___U3CFocusedGameObjectU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFocusedGameObjectU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_oldFocusedGameObject_4() { return static_cast<int32_t>(offsetof(InteractibleManager_t2692509793, ___oldFocusedGameObject_4)); }
	inline GameObject_t1756533147 * get_oldFocusedGameObject_4() const { return ___oldFocusedGameObject_4; }
	inline GameObject_t1756533147 ** get_address_of_oldFocusedGameObject_4() { return &___oldFocusedGameObject_4; }
	inline void set_oldFocusedGameObject_4(GameObject_t1756533147 * value)
	{
		___oldFocusedGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldFocusedGameObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIBLEMANAGER_T2692509793_H
#ifndef PLAYSPACEMANAGER_T690741465_H
#define PLAYSPACEMANAGER_T690741465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaySpaceManager
struct  PlaySpaceManager_t690741465  : public Singleton_1_t1244339818
{
public:
	// System.Boolean PlaySpaceManager::limitScanningByTime
	bool ___limitScanningByTime_3;
	// System.Single PlaySpaceManager::scanTime
	float ___scanTime_4;
	// UnityEngine.Material PlaySpaceManager::defaultMaterial
	Material_t193706927 * ___defaultMaterial_5;
	// UnityEngine.Material PlaySpaceManager::secondaryMaterial
	Material_t193706927 * ___secondaryMaterial_6;
	// System.UInt32 PlaySpaceManager::minimumFloors
	uint32_t ___minimumFloors_7;
	// System.UInt32 PlaySpaceManager::minimumWalls
	uint32_t ___minimumWalls_8;
	// System.Boolean PlaySpaceManager::meshesProcessed
	bool ___meshesProcessed_9;

public:
	inline static int32_t get_offset_of_limitScanningByTime_3() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___limitScanningByTime_3)); }
	inline bool get_limitScanningByTime_3() const { return ___limitScanningByTime_3; }
	inline bool* get_address_of_limitScanningByTime_3() { return &___limitScanningByTime_3; }
	inline void set_limitScanningByTime_3(bool value)
	{
		___limitScanningByTime_3 = value;
	}

	inline static int32_t get_offset_of_scanTime_4() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___scanTime_4)); }
	inline float get_scanTime_4() const { return ___scanTime_4; }
	inline float* get_address_of_scanTime_4() { return &___scanTime_4; }
	inline void set_scanTime_4(float value)
	{
		___scanTime_4 = value;
	}

	inline static int32_t get_offset_of_defaultMaterial_5() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___defaultMaterial_5)); }
	inline Material_t193706927 * get_defaultMaterial_5() const { return ___defaultMaterial_5; }
	inline Material_t193706927 ** get_address_of_defaultMaterial_5() { return &___defaultMaterial_5; }
	inline void set_defaultMaterial_5(Material_t193706927 * value)
	{
		___defaultMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultMaterial_5), value);
	}

	inline static int32_t get_offset_of_secondaryMaterial_6() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___secondaryMaterial_6)); }
	inline Material_t193706927 * get_secondaryMaterial_6() const { return ___secondaryMaterial_6; }
	inline Material_t193706927 ** get_address_of_secondaryMaterial_6() { return &___secondaryMaterial_6; }
	inline void set_secondaryMaterial_6(Material_t193706927 * value)
	{
		___secondaryMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___secondaryMaterial_6), value);
	}

	inline static int32_t get_offset_of_minimumFloors_7() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___minimumFloors_7)); }
	inline uint32_t get_minimumFloors_7() const { return ___minimumFloors_7; }
	inline uint32_t* get_address_of_minimumFloors_7() { return &___minimumFloors_7; }
	inline void set_minimumFloors_7(uint32_t value)
	{
		___minimumFloors_7 = value;
	}

	inline static int32_t get_offset_of_minimumWalls_8() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___minimumWalls_8)); }
	inline uint32_t get_minimumWalls_8() const { return ___minimumWalls_8; }
	inline uint32_t* get_address_of_minimumWalls_8() { return &___minimumWalls_8; }
	inline void set_minimumWalls_8(uint32_t value)
	{
		___minimumWalls_8 = value;
	}

	inline static int32_t get_offset_of_meshesProcessed_9() { return static_cast<int32_t>(offsetof(PlaySpaceManager_t690741465, ___meshesProcessed_9)); }
	inline bool get_meshesProcessed_9() const { return ___meshesProcessed_9; }
	inline bool* get_address_of_meshesProcessed_9() { return &___meshesProcessed_9; }
	inline void set_meshesProcessed_9(bool value)
	{
		___meshesProcessed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYSPACEMANAGER_T690741465_H
#ifndef SPACECOLLECTIONMANAGER_T455062415_H
#define SPACECOLLECTIONMANAGER_T455062415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpaceCollectionManager
struct  SpaceCollectionManager_t455062415  : public Singleton_1_t1008660768
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SpaceCollectionManager::spaceObjectPrefabs
	List_1_t1125654279 * ___spaceObjectPrefabs_3;

public:
	inline static int32_t get_offset_of_spaceObjectPrefabs_3() { return static_cast<int32_t>(offsetof(SpaceCollectionManager_t455062415, ___spaceObjectPrefabs_3)); }
	inline List_1_t1125654279 * get_spaceObjectPrefabs_3() const { return ___spaceObjectPrefabs_3; }
	inline List_1_t1125654279 ** get_address_of_spaceObjectPrefabs_3() { return &___spaceObjectPrefabs_3; }
	inline void set_spaceObjectPrefabs_3(List_1_t1125654279 * value)
	{
		___spaceObjectPrefabs_3 = value;
		Il2CppCodeGenWriteBarrier((&___spaceObjectPrefabs_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACECOLLECTIONMANAGER_T455062415_H
#ifndef REMOVESURFACEVERTICES_T3740445364_H
#define REMOVESURFACEVERTICES_T3740445364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RemoveSurfaceVertices
struct  RemoveSurfaceVertices_t3740445364  : public Singleton_1_t4294043717
{
public:
	// System.Single HoloToolkit.Unity.RemoveSurfaceVertices::BoundsExpansion
	float ___BoundsExpansion_3;
	// HoloToolkit.Unity.RemoveSurfaceVertices/EventHandler HoloToolkit.Unity.RemoveSurfaceVertices::RemoveVerticesComplete
	EventHandler_t2773844868 * ___RemoveVerticesComplete_4;
	// System.Boolean HoloToolkit.Unity.RemoveSurfaceVertices::removingVerts
	bool ___removingVerts_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Bounds> HoloToolkit.Unity.RemoveSurfaceVertices::boundingObjectsQueue
	Queue_1_t2853020538 * ___boundingObjectsQueue_6;

public:
	inline static int32_t get_offset_of_BoundsExpansion_3() { return static_cast<int32_t>(offsetof(RemoveSurfaceVertices_t3740445364, ___BoundsExpansion_3)); }
	inline float get_BoundsExpansion_3() const { return ___BoundsExpansion_3; }
	inline float* get_address_of_BoundsExpansion_3() { return &___BoundsExpansion_3; }
	inline void set_BoundsExpansion_3(float value)
	{
		___BoundsExpansion_3 = value;
	}

	inline static int32_t get_offset_of_RemoveVerticesComplete_4() { return static_cast<int32_t>(offsetof(RemoveSurfaceVertices_t3740445364, ___RemoveVerticesComplete_4)); }
	inline EventHandler_t2773844868 * get_RemoveVerticesComplete_4() const { return ___RemoveVerticesComplete_4; }
	inline EventHandler_t2773844868 ** get_address_of_RemoveVerticesComplete_4() { return &___RemoveVerticesComplete_4; }
	inline void set_RemoveVerticesComplete_4(EventHandler_t2773844868 * value)
	{
		___RemoveVerticesComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveVerticesComplete_4), value);
	}

	inline static int32_t get_offset_of_removingVerts_5() { return static_cast<int32_t>(offsetof(RemoveSurfaceVertices_t3740445364, ___removingVerts_5)); }
	inline bool get_removingVerts_5() const { return ___removingVerts_5; }
	inline bool* get_address_of_removingVerts_5() { return &___removingVerts_5; }
	inline void set_removingVerts_5(bool value)
	{
		___removingVerts_5 = value;
	}

	inline static int32_t get_offset_of_boundingObjectsQueue_6() { return static_cast<int32_t>(offsetof(RemoveSurfaceVertices_t3740445364, ___boundingObjectsQueue_6)); }
	inline Queue_1_t2853020538 * get_boundingObjectsQueue_6() const { return ___boundingObjectsQueue_6; }
	inline Queue_1_t2853020538 ** get_address_of_boundingObjectsQueue_6() { return &___boundingObjectsQueue_6; }
	inline void set_boundingObjectsQueue_6(Queue_1_t2853020538 * value)
	{
		___boundingObjectsQueue_6 = value;
		Il2CppCodeGenWriteBarrier((&___boundingObjectsQueue_6), value);
	}
};

struct RemoveSurfaceVertices_t3740445364_StaticFields
{
public:
	// System.Single HoloToolkit.Unity.RemoveSurfaceVertices::FrameTime
	float ___FrameTime_7;

public:
	inline static int32_t get_offset_of_FrameTime_7() { return static_cast<int32_t>(offsetof(RemoveSurfaceVertices_t3740445364_StaticFields, ___FrameTime_7)); }
	inline float get_FrameTime_7() const { return ___FrameTime_7; }
	inline float* get_address_of_FrameTime_7() { return &___FrameTime_7; }
	inline void set_FrameTime_7(float value)
	{
		___FrameTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVESURFACEVERTICES_T3740445364_H
#ifndef SPATIALMAPPINGMANAGER_T2439353561_H
#define SPATIALMAPPINGMANAGER_T2439353561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMappingManager
struct  SpatialMappingManager_t2439353561  : public Singleton_1_t2992951914
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialMappingManager::PhysicsLayer
	int32_t ___PhysicsLayer_3;
	// UnityEngine.Material HoloToolkit.Unity.SpatialMappingManager::surfaceMaterial
	Material_t193706927 * ___surfaceMaterial_4;
	// System.Boolean HoloToolkit.Unity.SpatialMappingManager::drawVisualMeshes
	bool ___drawVisualMeshes_5;
	// System.Boolean HoloToolkit.Unity.SpatialMappingManager::castShadows
	bool ___castShadows_6;
	// System.Boolean HoloToolkit.Unity.SpatialMappingManager::autoStartObserver
	bool ___autoStartObserver_7;
	// HoloToolkit.Unity.SpatialMappingObserver HoloToolkit.Unity.SpatialMappingManager::surfaceObserver
	SpatialMappingObserver_t3654215968 * ___surfaceObserver_8;
	// System.Single HoloToolkit.Unity.SpatialMappingManager::<StartTime>k__BackingField
	float ___U3CStartTimeU3Ek__BackingField_9;
	// HoloToolkit.Unity.SpatialMappingSource HoloToolkit.Unity.SpatialMappingManager::<Source>k__BackingField
	SpatialMappingSource_t2008115569 * ___U3CSourceU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_PhysicsLayer_3() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___PhysicsLayer_3)); }
	inline int32_t get_PhysicsLayer_3() const { return ___PhysicsLayer_3; }
	inline int32_t* get_address_of_PhysicsLayer_3() { return &___PhysicsLayer_3; }
	inline void set_PhysicsLayer_3(int32_t value)
	{
		___PhysicsLayer_3 = value;
	}

	inline static int32_t get_offset_of_surfaceMaterial_4() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___surfaceMaterial_4)); }
	inline Material_t193706927 * get_surfaceMaterial_4() const { return ___surfaceMaterial_4; }
	inline Material_t193706927 ** get_address_of_surfaceMaterial_4() { return &___surfaceMaterial_4; }
	inline void set_surfaceMaterial_4(Material_t193706927 * value)
	{
		___surfaceMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceMaterial_4), value);
	}

	inline static int32_t get_offset_of_drawVisualMeshes_5() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___drawVisualMeshes_5)); }
	inline bool get_drawVisualMeshes_5() const { return ___drawVisualMeshes_5; }
	inline bool* get_address_of_drawVisualMeshes_5() { return &___drawVisualMeshes_5; }
	inline void set_drawVisualMeshes_5(bool value)
	{
		___drawVisualMeshes_5 = value;
	}

	inline static int32_t get_offset_of_castShadows_6() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___castShadows_6)); }
	inline bool get_castShadows_6() const { return ___castShadows_6; }
	inline bool* get_address_of_castShadows_6() { return &___castShadows_6; }
	inline void set_castShadows_6(bool value)
	{
		___castShadows_6 = value;
	}

	inline static int32_t get_offset_of_autoStartObserver_7() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___autoStartObserver_7)); }
	inline bool get_autoStartObserver_7() const { return ___autoStartObserver_7; }
	inline bool* get_address_of_autoStartObserver_7() { return &___autoStartObserver_7; }
	inline void set_autoStartObserver_7(bool value)
	{
		___autoStartObserver_7 = value;
	}

	inline static int32_t get_offset_of_surfaceObserver_8() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___surfaceObserver_8)); }
	inline SpatialMappingObserver_t3654215968 * get_surfaceObserver_8() const { return ___surfaceObserver_8; }
	inline SpatialMappingObserver_t3654215968 ** get_address_of_surfaceObserver_8() { return &___surfaceObserver_8; }
	inline void set_surfaceObserver_8(SpatialMappingObserver_t3654215968 * value)
	{
		___surfaceObserver_8 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceObserver_8), value);
	}

	inline static int32_t get_offset_of_U3CStartTimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___U3CStartTimeU3Ek__BackingField_9)); }
	inline float get_U3CStartTimeU3Ek__BackingField_9() const { return ___U3CStartTimeU3Ek__BackingField_9; }
	inline float* get_address_of_U3CStartTimeU3Ek__BackingField_9() { return &___U3CStartTimeU3Ek__BackingField_9; }
	inline void set_U3CStartTimeU3Ek__BackingField_9(float value)
	{
		___U3CStartTimeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SpatialMappingManager_t2439353561, ___U3CSourceU3Ek__BackingField_10)); }
	inline SpatialMappingSource_t2008115569 * get_U3CSourceU3Ek__BackingField_10() const { return ___U3CSourceU3Ek__BackingField_10; }
	inline SpatialMappingSource_t2008115569 ** get_address_of_U3CSourceU3Ek__BackingField_10() { return &___U3CSourceU3Ek__BackingField_10; }
	inline void set_U3CSourceU3Ek__BackingField_10(SpatialMappingSource_t2008115569 * value)
	{
		___U3CSourceU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourceU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALMAPPINGMANAGER_T2439353561_H
#ifndef SPATIALMAPPINGOBSERVER_T3654215968_H
#define SPATIALMAPPINGOBSERVER_T3654215968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMappingObserver
struct  SpatialMappingObserver_t3654215968  : public SpatialMappingSource_t2008115569
{
public:
	// System.Single HoloToolkit.Unity.SpatialMappingObserver::TrianglesPerCubicMeter
	float ___TrianglesPerCubicMeter_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialMappingObserver::Extents
	Vector3_t2243707580  ___Extents_5;
	// System.Single HoloToolkit.Unity.SpatialMappingObserver::TimeBetweenUpdates
	float ___TimeBetweenUpdates_6;
	// System.Boolean HoloToolkit.Unity.SpatialMappingObserver::RecalculateNormals
	bool ___RecalculateNormals_7;
	// UnityEngine.XR.WSA.SurfaceObserver/SurfaceChangedDelegate HoloToolkit.Unity.SpatialMappingObserver::SurfaceChanged
	SurfaceChangedDelegate_t3858419612 * ___SurfaceChanged_8;
	// UnityEngine.XR.WSA.SurfaceObserver/SurfaceDataReadyDelegate HoloToolkit.Unity.SpatialMappingObserver::DataReady
	SurfaceDataReadyDelegate_t686817981 * ___DataReady_9;
	// UnityEngine.XR.WSA.SurfaceObserver HoloToolkit.Unity.SpatialMappingObserver::observer
	SurfaceObserver_t595079417 * ___observer_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> HoloToolkit.Unity.SpatialMappingObserver::surfaces
	Dictionary_2_t764358782 * ___surfaces_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> HoloToolkit.Unity.SpatialMappingObserver::pendingCleanup
	Dictionary_2_t764358782 * ___pendingCleanup_12;
	// System.Collections.Generic.Queue`1<UnityEngine.GameObject> HoloToolkit.Unity.SpatialMappingObserver::availableSurfaces
	Queue_1_t1576189982 * ___availableSurfaces_13;
	// System.Collections.Generic.Queue`1<UnityEngine.XR.WSA.SurfaceData> HoloToolkit.Unity.SpatialMappingObserver::surfaceWorkQueue
	Queue_1_t3691932036 * ___surfaceWorkQueue_14;
	// System.Boolean HoloToolkit.Unity.SpatialMappingObserver::surfaceWorkOutstanding
	bool ___surfaceWorkOutstanding_15;
	// System.Single HoloToolkit.Unity.SpatialMappingObserver::updateTime
	float ___updateTime_16;
	// HoloToolkit.Unity.ObserverStates HoloToolkit.Unity.SpatialMappingObserver::<ObserverState>k__BackingField
	int32_t ___U3CObserverStateU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_TrianglesPerCubicMeter_4() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___TrianglesPerCubicMeter_4)); }
	inline float get_TrianglesPerCubicMeter_4() const { return ___TrianglesPerCubicMeter_4; }
	inline float* get_address_of_TrianglesPerCubicMeter_4() { return &___TrianglesPerCubicMeter_4; }
	inline void set_TrianglesPerCubicMeter_4(float value)
	{
		___TrianglesPerCubicMeter_4 = value;
	}

	inline static int32_t get_offset_of_Extents_5() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___Extents_5)); }
	inline Vector3_t2243707580  get_Extents_5() const { return ___Extents_5; }
	inline Vector3_t2243707580 * get_address_of_Extents_5() { return &___Extents_5; }
	inline void set_Extents_5(Vector3_t2243707580  value)
	{
		___Extents_5 = value;
	}

	inline static int32_t get_offset_of_TimeBetweenUpdates_6() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___TimeBetweenUpdates_6)); }
	inline float get_TimeBetweenUpdates_6() const { return ___TimeBetweenUpdates_6; }
	inline float* get_address_of_TimeBetweenUpdates_6() { return &___TimeBetweenUpdates_6; }
	inline void set_TimeBetweenUpdates_6(float value)
	{
		___TimeBetweenUpdates_6 = value;
	}

	inline static int32_t get_offset_of_RecalculateNormals_7() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___RecalculateNormals_7)); }
	inline bool get_RecalculateNormals_7() const { return ___RecalculateNormals_7; }
	inline bool* get_address_of_RecalculateNormals_7() { return &___RecalculateNormals_7; }
	inline void set_RecalculateNormals_7(bool value)
	{
		___RecalculateNormals_7 = value;
	}

	inline static int32_t get_offset_of_SurfaceChanged_8() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___SurfaceChanged_8)); }
	inline SurfaceChangedDelegate_t3858419612 * get_SurfaceChanged_8() const { return ___SurfaceChanged_8; }
	inline SurfaceChangedDelegate_t3858419612 ** get_address_of_SurfaceChanged_8() { return &___SurfaceChanged_8; }
	inline void set_SurfaceChanged_8(SurfaceChangedDelegate_t3858419612 * value)
	{
		___SurfaceChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceChanged_8), value);
	}

	inline static int32_t get_offset_of_DataReady_9() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___DataReady_9)); }
	inline SurfaceDataReadyDelegate_t686817981 * get_DataReady_9() const { return ___DataReady_9; }
	inline SurfaceDataReadyDelegate_t686817981 ** get_address_of_DataReady_9() { return &___DataReady_9; }
	inline void set_DataReady_9(SurfaceDataReadyDelegate_t686817981 * value)
	{
		___DataReady_9 = value;
		Il2CppCodeGenWriteBarrier((&___DataReady_9), value);
	}

	inline static int32_t get_offset_of_observer_10() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___observer_10)); }
	inline SurfaceObserver_t595079417 * get_observer_10() const { return ___observer_10; }
	inline SurfaceObserver_t595079417 ** get_address_of_observer_10() { return &___observer_10; }
	inline void set_observer_10(SurfaceObserver_t595079417 * value)
	{
		___observer_10 = value;
		Il2CppCodeGenWriteBarrier((&___observer_10), value);
	}

	inline static int32_t get_offset_of_surfaces_11() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___surfaces_11)); }
	inline Dictionary_2_t764358782 * get_surfaces_11() const { return ___surfaces_11; }
	inline Dictionary_2_t764358782 ** get_address_of_surfaces_11() { return &___surfaces_11; }
	inline void set_surfaces_11(Dictionary_2_t764358782 * value)
	{
		___surfaces_11 = value;
		Il2CppCodeGenWriteBarrier((&___surfaces_11), value);
	}

	inline static int32_t get_offset_of_pendingCleanup_12() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___pendingCleanup_12)); }
	inline Dictionary_2_t764358782 * get_pendingCleanup_12() const { return ___pendingCleanup_12; }
	inline Dictionary_2_t764358782 ** get_address_of_pendingCleanup_12() { return &___pendingCleanup_12; }
	inline void set_pendingCleanup_12(Dictionary_2_t764358782 * value)
	{
		___pendingCleanup_12 = value;
		Il2CppCodeGenWriteBarrier((&___pendingCleanup_12), value);
	}

	inline static int32_t get_offset_of_availableSurfaces_13() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___availableSurfaces_13)); }
	inline Queue_1_t1576189982 * get_availableSurfaces_13() const { return ___availableSurfaces_13; }
	inline Queue_1_t1576189982 ** get_address_of_availableSurfaces_13() { return &___availableSurfaces_13; }
	inline void set_availableSurfaces_13(Queue_1_t1576189982 * value)
	{
		___availableSurfaces_13 = value;
		Il2CppCodeGenWriteBarrier((&___availableSurfaces_13), value);
	}

	inline static int32_t get_offset_of_surfaceWorkQueue_14() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___surfaceWorkQueue_14)); }
	inline Queue_1_t3691932036 * get_surfaceWorkQueue_14() const { return ___surfaceWorkQueue_14; }
	inline Queue_1_t3691932036 ** get_address_of_surfaceWorkQueue_14() { return &___surfaceWorkQueue_14; }
	inline void set_surfaceWorkQueue_14(Queue_1_t3691932036 * value)
	{
		___surfaceWorkQueue_14 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceWorkQueue_14), value);
	}

	inline static int32_t get_offset_of_surfaceWorkOutstanding_15() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___surfaceWorkOutstanding_15)); }
	inline bool get_surfaceWorkOutstanding_15() const { return ___surfaceWorkOutstanding_15; }
	inline bool* get_address_of_surfaceWorkOutstanding_15() { return &___surfaceWorkOutstanding_15; }
	inline void set_surfaceWorkOutstanding_15(bool value)
	{
		___surfaceWorkOutstanding_15 = value;
	}

	inline static int32_t get_offset_of_updateTime_16() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___updateTime_16)); }
	inline float get_updateTime_16() const { return ___updateTime_16; }
	inline float* get_address_of_updateTime_16() { return &___updateTime_16; }
	inline void set_updateTime_16(float value)
	{
		___updateTime_16 = value;
	}

	inline static int32_t get_offset_of_U3CObserverStateU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(SpatialMappingObserver_t3654215968, ___U3CObserverStateU3Ek__BackingField_17)); }
	inline int32_t get_U3CObserverStateU3Ek__BackingField_17() const { return ___U3CObserverStateU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CObserverStateU3Ek__BackingField_17() { return &___U3CObserverStateU3Ek__BackingField_17; }
	inline void set_U3CObserverStateU3Ek__BackingField_17(int32_t value)
	{
		___U3CObserverStateU3Ek__BackingField_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALMAPPINGOBSERVER_T3654215968_H
#ifndef OBJECTSURFACEOBSERVER_T4112124206_H
#define OBJECTSURFACEOBSERVER_T4112124206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ObjectSurfaceObserver
struct  ObjectSurfaceObserver_t4112124206  : public SpatialMappingSource_t2008115569
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.ObjectSurfaceObserver::roomModel
	GameObject_t1756533147 * ___roomModel_4;

public:
	inline static int32_t get_offset_of_roomModel_4() { return static_cast<int32_t>(offsetof(ObjectSurfaceObserver_t4112124206, ___roomModel_4)); }
	inline GameObject_t1756533147 * get_roomModel_4() const { return ___roomModel_4; }
	inline GameObject_t1756533147 ** get_address_of_roomModel_4() { return &___roomModel_4; }
	inline void set_roomModel_4(GameObject_t1756533147 * value)
	{
		___roomModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___roomModel_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSURFACEOBSERVER_T4112124206_H
#ifndef SURFACEMESHESTOPLANES_T2358468568_H
#define SURFACEMESHESTOPLANES_T2358468568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes
struct  SurfaceMeshesToPlanes_t2358468568  : public Singleton_1_t2912066921
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.SurfaceMeshesToPlanes::ActivePlanes
	List_1_t1125654279 * ___ActivePlanes_3;
	// UnityEngine.GameObject HoloToolkit.Unity.SurfaceMeshesToPlanes::SurfacePlanePrefab
	GameObject_t1756533147 * ___SurfacePlanePrefab_4;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::MinArea
	float ___MinArea_5;
	// HoloToolkit.Unity.PlaneTypes HoloToolkit.Unity.SurfaceMeshesToPlanes::drawPlanesMask
	int32_t ___drawPlanesMask_6;
	// HoloToolkit.Unity.PlaneTypes HoloToolkit.Unity.SurfaceMeshesToPlanes::destroyPlanesMask
	int32_t ___destroyPlanesMask_7;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::<FloorYPosition>k__BackingField
	float ___U3CFloorYPositionU3Ek__BackingField_8;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::<CeilingYPosition>k__BackingField
	float ___U3CCeilingYPositionU3Ek__BackingField_9;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes/EventHandler HoloToolkit.Unity.SurfaceMeshesToPlanes::MakePlanesComplete
	EventHandler_t1067160880 * ___MakePlanesComplete_10;
	// UnityEngine.GameObject HoloToolkit.Unity.SurfaceMeshesToPlanes::planesParent
	GameObject_t1756533147 * ___planesParent_11;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::snapToGravityThreshold
	float ___snapToGravityThreshold_12;
	// System.Boolean HoloToolkit.Unity.SurfaceMeshesToPlanes::makingPlanes
	bool ___makingPlanes_13;

public:
	inline static int32_t get_offset_of_ActivePlanes_3() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___ActivePlanes_3)); }
	inline List_1_t1125654279 * get_ActivePlanes_3() const { return ___ActivePlanes_3; }
	inline List_1_t1125654279 ** get_address_of_ActivePlanes_3() { return &___ActivePlanes_3; }
	inline void set_ActivePlanes_3(List_1_t1125654279 * value)
	{
		___ActivePlanes_3 = value;
		Il2CppCodeGenWriteBarrier((&___ActivePlanes_3), value);
	}

	inline static int32_t get_offset_of_SurfacePlanePrefab_4() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___SurfacePlanePrefab_4)); }
	inline GameObject_t1756533147 * get_SurfacePlanePrefab_4() const { return ___SurfacePlanePrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_SurfacePlanePrefab_4() { return &___SurfacePlanePrefab_4; }
	inline void set_SurfacePlanePrefab_4(GameObject_t1756533147 * value)
	{
		___SurfacePlanePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___SurfacePlanePrefab_4), value);
	}

	inline static int32_t get_offset_of_MinArea_5() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___MinArea_5)); }
	inline float get_MinArea_5() const { return ___MinArea_5; }
	inline float* get_address_of_MinArea_5() { return &___MinArea_5; }
	inline void set_MinArea_5(float value)
	{
		___MinArea_5 = value;
	}

	inline static int32_t get_offset_of_drawPlanesMask_6() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___drawPlanesMask_6)); }
	inline int32_t get_drawPlanesMask_6() const { return ___drawPlanesMask_6; }
	inline int32_t* get_address_of_drawPlanesMask_6() { return &___drawPlanesMask_6; }
	inline void set_drawPlanesMask_6(int32_t value)
	{
		___drawPlanesMask_6 = value;
	}

	inline static int32_t get_offset_of_destroyPlanesMask_7() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___destroyPlanesMask_7)); }
	inline int32_t get_destroyPlanesMask_7() const { return ___destroyPlanesMask_7; }
	inline int32_t* get_address_of_destroyPlanesMask_7() { return &___destroyPlanesMask_7; }
	inline void set_destroyPlanesMask_7(int32_t value)
	{
		___destroyPlanesMask_7 = value;
	}

	inline static int32_t get_offset_of_U3CFloorYPositionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___U3CFloorYPositionU3Ek__BackingField_8)); }
	inline float get_U3CFloorYPositionU3Ek__BackingField_8() const { return ___U3CFloorYPositionU3Ek__BackingField_8; }
	inline float* get_address_of_U3CFloorYPositionU3Ek__BackingField_8() { return &___U3CFloorYPositionU3Ek__BackingField_8; }
	inline void set_U3CFloorYPositionU3Ek__BackingField_8(float value)
	{
		___U3CFloorYPositionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCeilingYPositionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___U3CCeilingYPositionU3Ek__BackingField_9)); }
	inline float get_U3CCeilingYPositionU3Ek__BackingField_9() const { return ___U3CCeilingYPositionU3Ek__BackingField_9; }
	inline float* get_address_of_U3CCeilingYPositionU3Ek__BackingField_9() { return &___U3CCeilingYPositionU3Ek__BackingField_9; }
	inline void set_U3CCeilingYPositionU3Ek__BackingField_9(float value)
	{
		___U3CCeilingYPositionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_MakePlanesComplete_10() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___MakePlanesComplete_10)); }
	inline EventHandler_t1067160880 * get_MakePlanesComplete_10() const { return ___MakePlanesComplete_10; }
	inline EventHandler_t1067160880 ** get_address_of_MakePlanesComplete_10() { return &___MakePlanesComplete_10; }
	inline void set_MakePlanesComplete_10(EventHandler_t1067160880 * value)
	{
		___MakePlanesComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___MakePlanesComplete_10), value);
	}

	inline static int32_t get_offset_of_planesParent_11() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___planesParent_11)); }
	inline GameObject_t1756533147 * get_planesParent_11() const { return ___planesParent_11; }
	inline GameObject_t1756533147 ** get_address_of_planesParent_11() { return &___planesParent_11; }
	inline void set_planesParent_11(GameObject_t1756533147 * value)
	{
		___planesParent_11 = value;
		Il2CppCodeGenWriteBarrier((&___planesParent_11), value);
	}

	inline static int32_t get_offset_of_snapToGravityThreshold_12() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___snapToGravityThreshold_12)); }
	inline float get_snapToGravityThreshold_12() const { return ___snapToGravityThreshold_12; }
	inline float* get_address_of_snapToGravityThreshold_12() { return &___snapToGravityThreshold_12; }
	inline void set_snapToGravityThreshold_12(float value)
	{
		___snapToGravityThreshold_12 = value;
	}

	inline static int32_t get_offset_of_makingPlanes_13() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___makingPlanes_13)); }
	inline bool get_makingPlanes_13() const { return ___makingPlanes_13; }
	inline bool* get_address_of_makingPlanes_13() { return &___makingPlanes_13; }
	inline void set_makingPlanes_13(bool value)
	{
		___makingPlanes_13 = value;
	}
};

struct SurfaceMeshesToPlanes_t2358468568_StaticFields
{
public:
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::FrameTime
	float ___FrameTime_14;

public:
	inline static int32_t get_offset_of_FrameTime_14() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568_StaticFields, ___FrameTime_14)); }
	inline float get_FrameTime_14() const { return ___FrameTime_14; }
	inline float* get_address_of_FrameTime_14() { return &___FrameTime_14; }
	inline void set_FrameTime_14(float value)
	{
		___FrameTime_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEMESHESTOPLANES_T2358468568_H
#ifndef GAZEMANAGER_T2256266604_H
#define GAZEMANAGER_T2256266604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GazeManager
struct  GazeManager_t2256266604  : public Singleton_1_t2809864957
{
public:
	// System.Single HoloToolkit.Unity.GazeManager::MaxGazeDistance
	float ___MaxGazeDistance_3;
	// UnityEngine.LayerMask HoloToolkit.Unity.GazeManager::RaycastLayerMask
	LayerMask_t3188175821  ___RaycastLayerMask_4;
	// System.Boolean HoloToolkit.Unity.GazeManager::<Hit>k__BackingField
	bool ___U3CHitU3Ek__BackingField_5;
	// UnityEngine.RaycastHit HoloToolkit.Unity.GazeManager::<HitInfo>k__BackingField
	RaycastHit_t87180320  ___U3CHitInfoU3Ek__BackingField_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.GazeManager::<Position>k__BackingField
	Vector3_t2243707580  ___U3CPositionU3Ek__BackingField_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.GazeManager::<Normal>k__BackingField
	Vector3_t2243707580  ___U3CNormalU3Ek__BackingField_8;
	// System.Boolean HoloToolkit.Unity.GazeManager::SetStabilizationPlane
	bool ___SetStabilizationPlane_9;
	// System.Single HoloToolkit.Unity.GazeManager::LerpStabilizationPlanePowerCloser
	float ___LerpStabilizationPlanePowerCloser_10;
	// System.Single HoloToolkit.Unity.GazeManager::LerpStabilizationPlanePowerFarther
	float ___LerpStabilizationPlanePowerFarther_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.GazeManager::gazeOrigin
	Vector3_t2243707580  ___gazeOrigin_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.GazeManager::gazeDirection
	Vector3_t2243707580  ___gazeDirection_13;
	// System.Single HoloToolkit.Unity.GazeManager::lastHitDistance
	float ___lastHitDistance_14;
	// UnityEngine.GameObject HoloToolkit.Unity.GazeManager::focusedObject
	GameObject_t1756533147 * ___focusedObject_15;

public:
	inline static int32_t get_offset_of_MaxGazeDistance_3() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___MaxGazeDistance_3)); }
	inline float get_MaxGazeDistance_3() const { return ___MaxGazeDistance_3; }
	inline float* get_address_of_MaxGazeDistance_3() { return &___MaxGazeDistance_3; }
	inline void set_MaxGazeDistance_3(float value)
	{
		___MaxGazeDistance_3 = value;
	}

	inline static int32_t get_offset_of_RaycastLayerMask_4() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___RaycastLayerMask_4)); }
	inline LayerMask_t3188175821  get_RaycastLayerMask_4() const { return ___RaycastLayerMask_4; }
	inline LayerMask_t3188175821 * get_address_of_RaycastLayerMask_4() { return &___RaycastLayerMask_4; }
	inline void set_RaycastLayerMask_4(LayerMask_t3188175821  value)
	{
		___RaycastLayerMask_4 = value;
	}

	inline static int32_t get_offset_of_U3CHitU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___U3CHitU3Ek__BackingField_5)); }
	inline bool get_U3CHitU3Ek__BackingField_5() const { return ___U3CHitU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CHitU3Ek__BackingField_5() { return &___U3CHitU3Ek__BackingField_5; }
	inline void set_U3CHitU3Ek__BackingField_5(bool value)
	{
		___U3CHitU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CHitInfoU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___U3CHitInfoU3Ek__BackingField_6)); }
	inline RaycastHit_t87180320  get_U3CHitInfoU3Ek__BackingField_6() const { return ___U3CHitInfoU3Ek__BackingField_6; }
	inline RaycastHit_t87180320 * get_address_of_U3CHitInfoU3Ek__BackingField_6() { return &___U3CHitInfoU3Ek__BackingField_6; }
	inline void set_U3CHitInfoU3Ek__BackingField_6(RaycastHit_t87180320  value)
	{
		___U3CHitInfoU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___U3CPositionU3Ek__BackingField_7)); }
	inline Vector3_t2243707580  get_U3CPositionU3Ek__BackingField_7() const { return ___U3CPositionU3Ek__BackingField_7; }
	inline Vector3_t2243707580 * get_address_of_U3CPositionU3Ek__BackingField_7() { return &___U3CPositionU3Ek__BackingField_7; }
	inline void set_U3CPositionU3Ek__BackingField_7(Vector3_t2243707580  value)
	{
		___U3CPositionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CNormalU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___U3CNormalU3Ek__BackingField_8)); }
	inline Vector3_t2243707580  get_U3CNormalU3Ek__BackingField_8() const { return ___U3CNormalU3Ek__BackingField_8; }
	inline Vector3_t2243707580 * get_address_of_U3CNormalU3Ek__BackingField_8() { return &___U3CNormalU3Ek__BackingField_8; }
	inline void set_U3CNormalU3Ek__BackingField_8(Vector3_t2243707580  value)
	{
		___U3CNormalU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_SetStabilizationPlane_9() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___SetStabilizationPlane_9)); }
	inline bool get_SetStabilizationPlane_9() const { return ___SetStabilizationPlane_9; }
	inline bool* get_address_of_SetStabilizationPlane_9() { return &___SetStabilizationPlane_9; }
	inline void set_SetStabilizationPlane_9(bool value)
	{
		___SetStabilizationPlane_9 = value;
	}

	inline static int32_t get_offset_of_LerpStabilizationPlanePowerCloser_10() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___LerpStabilizationPlanePowerCloser_10)); }
	inline float get_LerpStabilizationPlanePowerCloser_10() const { return ___LerpStabilizationPlanePowerCloser_10; }
	inline float* get_address_of_LerpStabilizationPlanePowerCloser_10() { return &___LerpStabilizationPlanePowerCloser_10; }
	inline void set_LerpStabilizationPlanePowerCloser_10(float value)
	{
		___LerpStabilizationPlanePowerCloser_10 = value;
	}

	inline static int32_t get_offset_of_LerpStabilizationPlanePowerFarther_11() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___LerpStabilizationPlanePowerFarther_11)); }
	inline float get_LerpStabilizationPlanePowerFarther_11() const { return ___LerpStabilizationPlanePowerFarther_11; }
	inline float* get_address_of_LerpStabilizationPlanePowerFarther_11() { return &___LerpStabilizationPlanePowerFarther_11; }
	inline void set_LerpStabilizationPlanePowerFarther_11(float value)
	{
		___LerpStabilizationPlanePowerFarther_11 = value;
	}

	inline static int32_t get_offset_of_gazeOrigin_12() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___gazeOrigin_12)); }
	inline Vector3_t2243707580  get_gazeOrigin_12() const { return ___gazeOrigin_12; }
	inline Vector3_t2243707580 * get_address_of_gazeOrigin_12() { return &___gazeOrigin_12; }
	inline void set_gazeOrigin_12(Vector3_t2243707580  value)
	{
		___gazeOrigin_12 = value;
	}

	inline static int32_t get_offset_of_gazeDirection_13() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___gazeDirection_13)); }
	inline Vector3_t2243707580  get_gazeDirection_13() const { return ___gazeDirection_13; }
	inline Vector3_t2243707580 * get_address_of_gazeDirection_13() { return &___gazeDirection_13; }
	inline void set_gazeDirection_13(Vector3_t2243707580  value)
	{
		___gazeDirection_13 = value;
	}

	inline static int32_t get_offset_of_lastHitDistance_14() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___lastHitDistance_14)); }
	inline float get_lastHitDistance_14() const { return ___lastHitDistance_14; }
	inline float* get_address_of_lastHitDistance_14() { return &___lastHitDistance_14; }
	inline void set_lastHitDistance_14(float value)
	{
		___lastHitDistance_14 = value;
	}

	inline static int32_t get_offset_of_focusedObject_15() { return static_cast<int32_t>(offsetof(GazeManager_t2256266604, ___focusedObject_15)); }
	inline GameObject_t1756533147 * get_focusedObject_15() const { return ___focusedObject_15; }
	inline GameObject_t1756533147 ** get_address_of_focusedObject_15() { return &___focusedObject_15; }
	inline void set_focusedObject_15(GameObject_t1756533147 * value)
	{
		___focusedObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___focusedObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAZEMANAGER_T2256266604_H
#ifndef GESTUREMANAGER_T1645247838_H
#define GESTUREMANAGER_T1645247838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GestureManager
struct  GestureManager_t1645247838  : public Singleton_1_t2198846191
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.GestureManager::<OverrideFocusedObject>k__BackingField
	GameObject_t1756533147 * ___U3COverrideFocusedObjectU3Ek__BackingField_3;
	// UnityEngine.XR.WSA.Input.GestureRecognizer HoloToolkit.Unity.GestureManager::gestureRecognizer
	GestureRecognizer_t1861500035 * ___gestureRecognizer_4;
	// UnityEngine.GameObject HoloToolkit.Unity.GestureManager::focusedObject
	GameObject_t1756533147 * ___focusedObject_5;

public:
	inline static int32_t get_offset_of_U3COverrideFocusedObjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GestureManager_t1645247838, ___U3COverrideFocusedObjectU3Ek__BackingField_3)); }
	inline GameObject_t1756533147 * get_U3COverrideFocusedObjectU3Ek__BackingField_3() const { return ___U3COverrideFocusedObjectU3Ek__BackingField_3; }
	inline GameObject_t1756533147 ** get_address_of_U3COverrideFocusedObjectU3Ek__BackingField_3() { return &___U3COverrideFocusedObjectU3Ek__BackingField_3; }
	inline void set_U3COverrideFocusedObjectU3Ek__BackingField_3(GameObject_t1756533147 * value)
	{
		___U3COverrideFocusedObjectU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COverrideFocusedObjectU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_gestureRecognizer_4() { return static_cast<int32_t>(offsetof(GestureManager_t1645247838, ___gestureRecognizer_4)); }
	inline GestureRecognizer_t1861500035 * get_gestureRecognizer_4() const { return ___gestureRecognizer_4; }
	inline GestureRecognizer_t1861500035 ** get_address_of_gestureRecognizer_4() { return &___gestureRecognizer_4; }
	inline void set_gestureRecognizer_4(GestureRecognizer_t1861500035 * value)
	{
		___gestureRecognizer_4 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizer_4), value);
	}

	inline static int32_t get_offset_of_focusedObject_5() { return static_cast<int32_t>(offsetof(GestureManager_t1645247838, ___focusedObject_5)); }
	inline GameObject_t1756533147 * get_focusedObject_5() const { return ___focusedObject_5; }
	inline GameObject_t1756533147 ** get_address_of_focusedObject_5() { return &___focusedObject_5; }
	inline void set_focusedObject_5(GameObject_t1756533147 * value)
	{
		___focusedObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___focusedObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREMANAGER_T1645247838_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (CP21025_t3771795245), -1, sizeof(CP21025_t3771795245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3600[1] = 
{
	CP21025_t3771795245_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (ENCibm1025_t1780724032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (CP37_t661932205), -1, sizeof(CP37_t661932205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3602[1] = 
{
	CP37_t661932205_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (ENCibm037_t2100036606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (CP500_t2188172604), -1, sizeof(CP500_t2188172604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3604[1] = 
{
	CP500_t2188172604_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (ENCibm500_t3666120773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (CP708_t2188172662), -1, sizeof(CP708_t2188172662_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3606[1] = 
{
	CP708_t2188172662_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (ENCasmo_708_t1618404456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (CP852_t1784887916), -1, sizeof(CP852_t1784887916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3608[1] = 
{
	CP852_t1784887916_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (ENCibm852_t3262835751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (CP855_t1784887909), -1, sizeof(CP855_t1784887909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3610[1] = 
{
	CP855_t1784887909_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (ENCibm855_t3262835758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (CP857_t1784887911), -1, sizeof(CP857_t1784887911_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3612[1] = 
{
	CP857_t1784887911_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (ENCibm857_t3262835756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (CP858_t1784887922), -1, sizeof(CP858_t1784887922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3614[1] = 
{
	CP858_t1784887922_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (ENCibm00858_t197894961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (CP862_t3350971857), -1, sizeof(CP862_t3350971857_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3616[1] = 
{
	CP862_t3350971857_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (ENCibm862_t2859551224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (CP864_t3350971851), -1, sizeof(CP864_t3350971851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3618[1] = 
{
	CP864_t3350971851_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (ENCibm864_t2859551230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305146), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3620[37] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U309885C8B0840E863B3A14261999895D896677D5A_1(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U347DCC01E928630EBCC018C5E0285145985F92532_8(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_BC1CEF8131E7F0D8206029373157806126E21026_27(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35(),
	U3CPrivateImplementationDetailsU3E_t1486305146_StaticFields::get_offset_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (__StaticArrayInitTypeSizeU3D512_t2067141549)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t2067141549 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (U3CModuleU3E_t3783534244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (Consts_t2407773021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (CP10000_t3476899124), -1, sizeof(CP10000_t3476899124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3624[1] = 
{
	CP10000_t3476899124_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (ENCmacintosh_t2108151194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (CP10079_t1554584814), -1, sizeof(CP10079_t1554584814_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3626[1] = 
{
	CP10079_t1554584814_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (ENCx_mac_icelandic_t62238089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (CP1250_t653777969), -1, sizeof(CP1250_t653777969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3628[1] = 
{
	CP1250_t653777969_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (ENCwindows_1250_t336475776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (CP1252_t1816577383), -1, sizeof(CP1252_t1816577383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3630[1] = 
{
	CP1252_t1816577383_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (ENCwindows_1252_t336475774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (CP1253_t3382661324), -1, sizeof(CP1253_t3382661324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3632[1] = 
{
	CP1253_t3382661324_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (ENCwindows_1253_t336475775), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (CP28592_t2233549565), -1, sizeof(CP28592_t2233549565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3634[1] = 
{
	CP28592_t2233549565_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (ENCiso_8859_2_t2126773855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (CP28593_t2233549564), -1, sizeof(CP28593_t2233549564_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3636[1] = 
{
	CP28593_t2233549564_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (ENCiso_8859_3_t2126773856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (CP28597_t2233549560), -1, sizeof(CP28597_t2233549560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3638[1] = 
{
	CP28597_t2233549560_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (ENCiso_8859_7_t2126773860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (CP28605_t311235100), -1, sizeof(CP28605_t311235100_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3640[1] = 
{
	CP28605_t311235100_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (ENCiso_8859_15_t1092500295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (CP437_t2775482021), -1, sizeof(CP437_t2775482021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3642[1] = 
{
	CP437_t2775482021_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (ENCibm437_t3242889364), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (CP850_t1612682212), -1, sizeof(CP850_t1612682212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3644[1] = 
{
	CP850_t1612682212_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (ENCibm850_t4049458285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (CP860_t3178766153), -1, sizeof(CP860_t3178766153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3646[1] = 
{
	CP860_t3178766153_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (ENCibm860_t3646173758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (CP861_t3178766154), -1, sizeof(CP861_t3178766154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3648[1] = 
{
	CP861_t3178766154_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (ENCibm861_t3646173757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (CP863_t3178766156), -1, sizeof(CP863_t3178766156_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3650[1] = 
{
	CP863_t3178766156_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (ENCibm863_t3646173755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (CP865_t3178766158), -1, sizeof(CP865_t3178766158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3652[1] = 
{
	CP865_t3178766158_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (ENCibm865_t3646173753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305147), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3654[15] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U3598D9433A53523A59D462896B803E416AB0B1901_6(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (__StaticArrayInitTypeSizeU3D512_t2067141550)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t2067141550 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (U3CModuleU3E_t3783534245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (U3CModuleU3E_t3783534246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (InteractibleParameters_t2389709274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[2] = 
{
	InteractibleParameters_t2389709274::get_offset_of_Scrollable_0(),
	InteractibleParameters_t2389709274::get_offset_of_Placeable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (Interactible_t1795579682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[3] = 
{
	Interactible_t1795579682::get_offset_of_InteractibleParameters_2(),
	Interactible_t1795579682::get_offset_of_TargetFeedbackSound_3(),
	Interactible_t1795579682::get_offset_of_audioSource_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (InteractibleManager_t2692509793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[2] = 
{
	InteractibleManager_t2692509793::get_offset_of_U3CFocusedGameObjectU3Ek__BackingField_3(),
	InteractibleManager_t2692509793::get_offset_of_oldFocusedGameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (PlacementSurfaces_t532268329)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3661[3] = 
{
	PlacementSurfaces_t532268329::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (Placeable_t582649283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[18] = 
{
	Placeable_t582649283::get_offset_of_PlaceableBoundsMaterial_2(),
	Placeable_t582649283::get_offset_of_NotPlaceableBoundsMaterial_3(),
	Placeable_t582649283::get_offset_of_PlaceableShadowMaterial_4(),
	Placeable_t582649283::get_offset_of_NotPlaceableShadowMaterial_5(),
	Placeable_t582649283::get_offset_of_PlacementSurface_6(),
	Placeable_t582649283::get_offset_of_ChildrenToHide_7(),
	Placeable_t582649283::get_offset_of_U3CIsPlacingU3Ek__BackingField_8(),
	Placeable_t582649283::get_offset_of_lastDistance_9(),
	Placeable_t582649283::get_offset_of_hoverDistance_10(),
	Placeable_t582649283::get_offset_of_distanceThreshold_11(),
	Placeable_t582649283::get_offset_of_upNormalThreshold_12(),
	Placeable_t582649283::get_offset_of_maximumPlacementDistance_13(),
	Placeable_t582649283::get_offset_of_placementVelocity_14(),
	Placeable_t582649283::get_offset_of_managingBoxCollider_15(),
	Placeable_t582649283::get_offset_of_boxCollider_16(),
	Placeable_t582649283::get_offset_of_boundsAsset_17(),
	Placeable_t582649283::get_offset_of_shadowAsset_18(),
	Placeable_t582649283::get_offset_of_targetPosition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (PlaySpaceManager_t690741465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3663[7] = 
{
	PlaySpaceManager_t690741465::get_offset_of_limitScanningByTime_3(),
	PlaySpaceManager_t690741465::get_offset_of_scanTime_4(),
	PlaySpaceManager_t690741465::get_offset_of_defaultMaterial_5(),
	PlaySpaceManager_t690741465::get_offset_of_secondaryMaterial_6(),
	PlaySpaceManager_t690741465::get_offset_of_minimumFloors_7(),
	PlaySpaceManager_t690741465::get_offset_of_minimumWalls_8(),
	PlaySpaceManager_t690741465::get_offset_of_meshesProcessed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (PlanetOcclusion_t2352355769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[2] = 
{
	PlanetOcclusion_t2352355769::get_offset_of_occlusionObject_2(),
	PlanetOcclusion_t2352355769::get_offset_of_checkPoints_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (PlanetOrbit_t2107838810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[4] = 
{
	PlanetOrbit_t2107838810::get_offset_of_orbitPeriod_2(),
	PlanetOrbit_t2107838810::get_offset_of_rotatePeriod_3(),
	PlanetOrbit_t2107838810::get_offset_of_distance_4(),
	PlanetOrbit_t2107838810::get_offset_of_sun_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (RotateAsteroids_t2371484563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[1] = 
{
	RotateAsteroids_t2371484563::get_offset_of_period_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (SunScript_t3500173621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[5] = 
{
	0,
	0,
	SunScript_t3500173621::get_offset_of_PlanetData_5(),
	SunScript_t3500173621::get_offset_of_EarthYear_6(),
	SunScript_t3500173621::get_offset_of_OneAUInMeters_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (PlanetaryData_t2225979606)+ sizeof (RuntimeObject), sizeof(PlanetaryData_t2225979606 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3668[3] = 
{
	PlanetaryData_t2225979606::get_offset_of_U3CyearInEarthYearsU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlanetaryData_t2225979606::get_offset_of_U3CdayInEarthYearsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlanetaryData_t2225979606::get_offset_of_U3CdistanceFromSunU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (SpaceCollectionManager_t455062415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[1] = 
{
	SpaceCollectionManager_t455062415::get_offset_of_spaceObjectPrefabs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (U3CU3Ec_t466077745), -1, sizeof(U3CU3Ec_t466077745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3670[2] = 
{
	U3CU3Ec_t466077745_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t466077745_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (BasicCursor_t2131533874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[3] = 
{
	BasicCursor_t2131533874::get_offset_of_DistanceFromCollision_2(),
	BasicCursor_t2131533874::get_offset_of_cursorDefaultRotation_3(),
	BasicCursor_t2131533874::get_offset_of_meshRenderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (GazeManager_t2256266604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[13] = 
{
	GazeManager_t2256266604::get_offset_of_MaxGazeDistance_3(),
	GazeManager_t2256266604::get_offset_of_RaycastLayerMask_4(),
	GazeManager_t2256266604::get_offset_of_U3CHitU3Ek__BackingField_5(),
	GazeManager_t2256266604::get_offset_of_U3CHitInfoU3Ek__BackingField_6(),
	GazeManager_t2256266604::get_offset_of_U3CPositionU3Ek__BackingField_7(),
	GazeManager_t2256266604::get_offset_of_U3CNormalU3Ek__BackingField_8(),
	GazeManager_t2256266604::get_offset_of_SetStabilizationPlane_9(),
	GazeManager_t2256266604::get_offset_of_LerpStabilizationPlanePowerCloser_10(),
	GazeManager_t2256266604::get_offset_of_LerpStabilizationPlanePowerFarther_11(),
	GazeManager_t2256266604::get_offset_of_gazeOrigin_12(),
	GazeManager_t2256266604::get_offset_of_gazeDirection_13(),
	GazeManager_t2256266604::get_offset_of_lastHitDistance_14(),
	GazeManager_t2256266604::get_offset_of_focusedObject_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (GestureManager_t1645247838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[3] = 
{
	GestureManager_t1645247838::get_offset_of_U3COverrideFocusedObjectU3Ek__BackingField_3(),
	GestureManager_t1645247838::get_offset_of_gestureRecognizer_4(),
	GestureManager_t1645247838::get_offset_of_focusedObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (HandsManager_t3572349227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[1] = 
{
	HandsManager_t3572349227::get_offset_of_trackedHands_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (ObjectSurfaceObserver_t4112124206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[1] = 
{
	ObjectSurfaceObserver_t4112124206::get_offset_of_roomModel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (OrientedBoundingBox_t566734849)+ sizeof (RuntimeObject), sizeof(OrientedBoundingBox_t566734849 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3676[3] = 
{
	OrientedBoundingBox_t566734849::get_offset_of_Center_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox_t566734849::get_offset_of_Extents_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox_t566734849::get_offset_of_Rotation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (BoundedPlane_t3629713867)+ sizeof (RuntimeObject), sizeof(BoundedPlane_t3629713867 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3677[3] = 
{
	BoundedPlane_t3629713867::get_offset_of_Plane_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_t3629713867::get_offset_of_Bounds_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_t3629713867::get_offset_of_Area_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (PlaneFinding_t2534126505), -1, sizeof(PlaneFinding_t2534126505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3678[4] = 
{
	PlaneFinding_t2534126505_StaticFields::get_offset_of_findPlanesRunning_0(),
	PlaneFinding_t2534126505_StaticFields::get_offset_of_findPlanesLock_1(),
	PlaneFinding_t2534126505_StaticFields::get_offset_of_reusedMeshesForMarshalling_2(),
	PlaneFinding_t2534126505_StaticFields::get_offset_of_reusedPinnedMemoryHandles_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (MeshData_t1510890618)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[4] = 
{
	MeshData_t1510890618::get_offset_of_Transform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1510890618::get_offset_of_Verts_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1510890618::get_offset_of_Normals_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1510890618::get_offset_of_Indices_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (DLLImports_t3544469217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (MeshData_t1583775213)+ sizeof (RuntimeObject), sizeof(MeshData_t1583775213 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3681[6] = 
{
	MeshData_t1583775213::get_offset_of_transform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1583775213::get_offset_of_vertCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1583775213::get_offset_of_indexCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1583775213::get_offset_of_verts_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1583775213::get_offset_of_normals_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t1583775213::get_offset_of_indices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (RemoveSurfaceVertices_t3740445364), -1, sizeof(RemoveSurfaceVertices_t3740445364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3682[5] = 
{
	RemoveSurfaceVertices_t3740445364::get_offset_of_BoundsExpansion_3(),
	RemoveSurfaceVertices_t3740445364::get_offset_of_RemoveVerticesComplete_4(),
	RemoveSurfaceVertices_t3740445364::get_offset_of_removingVerts_5(),
	RemoveSurfaceVertices_t3740445364::get_offset_of_boundingObjectsQueue_6(),
	RemoveSurfaceVertices_t3740445364_StaticFields::get_offset_of_FrameTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (EventHandler_t2773844868), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3684[14] = 
{
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CU3E1__state_0(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CU3E2__current_1(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CU3E4__this_2(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CmeshFiltersU3E5__1_3(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CboundsU3E5__2_4(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CfilterU3E5__3_5(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CvertsU3E5__4_6(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CvertsToRemoveU3E5__5_7(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CiU3E5__6_8(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CmeshU3E5__7_9(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CindicesU3E5__8_10(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CupdatedIndicesU3E5__9_11(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CindexU3E5__10_12(),
	U3CRemoveSurfaceVerticesWithinBoundsRoutineU3Ed__11_t2587033248::get_offset_of_U3CU3E7__wrap1_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (SpatialMappingManager_t2439353561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[8] = 
{
	SpatialMappingManager_t2439353561::get_offset_of_PhysicsLayer_3(),
	SpatialMappingManager_t2439353561::get_offset_of_surfaceMaterial_4(),
	SpatialMappingManager_t2439353561::get_offset_of_drawVisualMeshes_5(),
	SpatialMappingManager_t2439353561::get_offset_of_castShadows_6(),
	SpatialMappingManager_t2439353561::get_offset_of_autoStartObserver_7(),
	SpatialMappingManager_t2439353561::get_offset_of_surfaceObserver_8(),
	SpatialMappingManager_t2439353561::get_offset_of_U3CStartTimeU3Ek__BackingField_9(),
	SpatialMappingManager_t2439353561::get_offset_of_U3CSourceU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (ObserverStates_t4003756946)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3686[3] = 
{
	ObserverStates_t4003756946::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (SpatialMappingObserver_t3654215968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[14] = 
{
	SpatialMappingObserver_t3654215968::get_offset_of_TrianglesPerCubicMeter_4(),
	SpatialMappingObserver_t3654215968::get_offset_of_Extents_5(),
	SpatialMappingObserver_t3654215968::get_offset_of_TimeBetweenUpdates_6(),
	SpatialMappingObserver_t3654215968::get_offset_of_RecalculateNormals_7(),
	SpatialMappingObserver_t3654215968::get_offset_of_SurfaceChanged_8(),
	SpatialMappingObserver_t3654215968::get_offset_of_DataReady_9(),
	SpatialMappingObserver_t3654215968::get_offset_of_observer_10(),
	SpatialMappingObserver_t3654215968::get_offset_of_surfaces_11(),
	SpatialMappingObserver_t3654215968::get_offset_of_pendingCleanup_12(),
	SpatialMappingObserver_t3654215968::get_offset_of_availableSurfaces_13(),
	SpatialMappingObserver_t3654215968::get_offset_of_surfaceWorkQueue_14(),
	SpatialMappingObserver_t3654215968::get_offset_of_surfaceWorkOutstanding_15(),
	SpatialMappingObserver_t3654215968::get_offset_of_updateTime_16(),
	SpatialMappingObserver_t3654215968::get_offset_of_U3CObserverStateU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (SpatialMappingSource_t2008115569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[2] = 
{
	SpatialMappingSource_t2008115569::get_offset_of_U3CSurfaceObjectsU3Ek__BackingField_2(),
	SpatialMappingSource_t2008115569::get_offset_of_componentsRequiredForSurfaceMesh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (SurfaceObject_t1188044127)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[5] = 
{
	SurfaceObject_t1188044127::get_offset_of_ID_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_UpdateID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_Object_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_Renderer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_Filter_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (SurfaceMeshesToPlanes_t2358468568), -1, sizeof(SurfaceMeshesToPlanes_t2358468568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3690[12] = 
{
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_ActivePlanes_3(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_SurfacePlanePrefab_4(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_MinArea_5(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_drawPlanesMask_6(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_destroyPlanesMask_7(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_U3CFloorYPositionU3Ek__BackingField_8(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_U3CCeilingYPositionU3Ek__BackingField_9(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_MakePlanesComplete_10(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_planesParent_11(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_snapToGravityThreshold_12(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_makingPlanes_13(),
	SurfaceMeshesToPlanes_t2358468568_StaticFields::get_offset_of_FrameTime_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (EventHandler_t1067160880), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (U3CU3Ec__DisplayClass24_0_t1668648882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[2] = 
{
	U3CU3Ec__DisplayClass24_0_t1668648882::get_offset_of_meshData_0(),
	U3CU3Ec__DisplayClass24_0_t1668648882::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (U3CMakePlanesRoutineU3Ed__24_t2308419383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3693[9] = 
{
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E1__state_0(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E2__current_1(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E4__this_2(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E8__1_3(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CfiltersU3E5__2_4(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CindexU3E5__3_5(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CplaneTaskU3E5__4_6(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CplanesU3E5__5_7(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CindexU3E5__6_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (PlaneTypes_t512441349)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3694[6] = 
{
	PlaneTypes_t512441349::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (SurfacePlane_t191565817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[12] = 
{
	SurfacePlane_t191565817::get_offset_of_PlaneThickness_2(),
	SurfacePlane_t191565817::get_offset_of_UpNormalThreshold_3(),
	SurfacePlane_t191565817::get_offset_of_FloorBuffer_4(),
	SurfacePlane_t191565817::get_offset_of_CeilingBuffer_5(),
	SurfacePlane_t191565817::get_offset_of_WallMaterial_6(),
	SurfacePlane_t191565817::get_offset_of_FloorMaterial_7(),
	SurfacePlane_t191565817::get_offset_of_CeilingMaterial_8(),
	SurfacePlane_t191565817::get_offset_of_TableMaterial_9(),
	SurfacePlane_t191565817::get_offset_of_UnknownMaterial_10(),
	SurfacePlane_t191565817::get_offset_of_PlaneType_11(),
	SurfacePlane_t191565817::get_offset_of_plane_12(),
	SurfacePlane_t191565817::get_offset_of_U3CSurfaceNormalU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
