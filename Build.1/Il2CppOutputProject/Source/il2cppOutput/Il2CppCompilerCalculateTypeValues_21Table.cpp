﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.UInt16[]
struct UInt16U5BU5D_t2527266722;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t1975884510;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t2217612696;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t3518500204;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_t482152337;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Uri
struct Uri_t19570940;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t1507412803;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.ValidationState
struct ValidationState_t3143048826;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1443605388;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Mono.Security.Protocol.Tls.CipherSuite[]
struct CipherSuiteU5BU5D_t3295586142;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Exception
struct Exception_t1927440687;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct HandshakeMessage_t3938752374;
// System.IO.Stream
struct Stream_t3255436806;
// Mono.Security.Protocol.Tls.Context
struct Context_t4285182719;
// System.Configuration.ElementMap
struct ElementMap_t997038224;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t1911180302;
// System.Configuration.ElementInformation
struct ElementInformation_t3165583784;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Configuration.ConfigurationLockCollection
struct ConfigurationLockCollection_t1011762925;
// System.Configuration.ConfigurationElement/SaveContext
struct SaveContext_t3996373180;
// Mono.Security.Protocol.Tls.CipherSuite
struct CipherSuite_t491456551;
// Mono.Security.ASN1
struct ASN1_t924533536;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1656058977;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// Mono.Security.Cryptography.RSAManaged
struct RSAManaged_t3034748748;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct CertificateSelectionCallback_t3721235490;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct PrivateKeySelectionCallback_t1663566523;
// System.Configuration.SectionInformation
struct SectionInformation_t2754609709;
// System.Configuration.IConfigurationSectionHandler
struct IConfigurationSectionHandler_t4214479838;
// Mono.Security.Protocol.Tls.Alert
struct Alert_t3405955216;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2624936259;
// System.Security.Cryptography.RSA
struct RSA_t3719518354;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t2745753060;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t461808439;
// System.Security.SecurityElement
struct SecurityElement_t2325568386;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Void
struct Void_t1841601450;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// Mono.Security.Protocol.Tls.RecordProtocol
struct RecordProtocol_t3166895267;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3592472866;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct ClientCertificateTypeU5BU5D_t2397899623;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.Xml.HWStack
struct HWStack_t738999989;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t224554150;
// System.Xml.Schema.Parser
struct Parser_t1940171737;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1108166522;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;
// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1374150027;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct CertificateValidationCallback_t989458295;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct CertificateValidationCallback2_t3318447433;
// Mono.Security.Protocol.Tls.TlsServerSettings
struct TlsServerSettings_t403340211;
// Mono.Security.Protocol.Tls.TlsClientSettings
struct TlsClientSettings_t2311449551;
// Mono.Security.Protocol.Tls.SecurityParameters
struct SecurityParameters_t2290372928;
// Mono.Security.Protocol.Tls.CipherSuiteCollection
struct CipherSuiteCollection_t2431504453;
// Mono.Security.Protocol.Tls.TlsStream
struct TlsStream_t4089752859;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Mono.Security.Interface.ValidationResult
struct ValidationResult_t2452252148;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t784058677;
// Mono.Security.Protocol.Tls.SslClientStream
struct SslClientStream_t3918817353;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;
// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t1929047274;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t4080334132;
// Mono.Security.Interface.CipherSuiteCode[]
struct CipherSuiteCodeU5BU5D_t4141109067;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t3067886638;

struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534216_H
#define U3CMODULEU3E_T3783534216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534216 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534216_H
#ifndef MINIPARSER_T185565106_H
#define MINIPARSER_T185565106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser
struct  MiniParser_t185565106  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.MiniParser::line
	int32_t ___line_3;
	// System.Int32 Mono.Xml.MiniParser::col
	int32_t ___col_4;
	// System.Int32[] Mono.Xml.MiniParser::twoCharBuff
	Int32U5BU5D_t3030399641* ___twoCharBuff_5;
	// System.Boolean Mono.Xml.MiniParser::splitCData
	bool ___splitCData_6;

public:
	inline static int32_t get_offset_of_line_3() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___line_3)); }
	inline int32_t get_line_3() const { return ___line_3; }
	inline int32_t* get_address_of_line_3() { return &___line_3; }
	inline void set_line_3(int32_t value)
	{
		___line_3 = value;
	}

	inline static int32_t get_offset_of_col_4() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___col_4)); }
	inline int32_t get_col_4() const { return ___col_4; }
	inline int32_t* get_address_of_col_4() { return &___col_4; }
	inline void set_col_4(int32_t value)
	{
		___col_4 = value;
	}

	inline static int32_t get_offset_of_twoCharBuff_5() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___twoCharBuff_5)); }
	inline Int32U5BU5D_t3030399641* get_twoCharBuff_5() const { return ___twoCharBuff_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_twoCharBuff_5() { return &___twoCharBuff_5; }
	inline void set_twoCharBuff_5(Int32U5BU5D_t3030399641* value)
	{
		___twoCharBuff_5 = value;
		Il2CppCodeGenWriteBarrier((&___twoCharBuff_5), value);
	}

	inline static int32_t get_offset_of_splitCData_6() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___splitCData_6)); }
	inline bool get_splitCData_6() const { return ___splitCData_6; }
	inline bool* get_address_of_splitCData_6() { return &___splitCData_6; }
	inline void set_splitCData_6(bool value)
	{
		___splitCData_6 = value;
	}
};

struct MiniParser_t185565106_StaticFields
{
public:
	// System.Int32 Mono.Xml.MiniParser::INPUT_RANGE
	int32_t ___INPUT_RANGE_0;
	// System.UInt16[] Mono.Xml.MiniParser::tbl
	UInt16U5BU5D_t2527266722* ___tbl_1;
	// System.String[] Mono.Xml.MiniParser::errors
	StringU5BU5D_t1642385972* ___errors_2;

public:
	inline static int32_t get_offset_of_INPUT_RANGE_0() { return static_cast<int32_t>(offsetof(MiniParser_t185565106_StaticFields, ___INPUT_RANGE_0)); }
	inline int32_t get_INPUT_RANGE_0() const { return ___INPUT_RANGE_0; }
	inline int32_t* get_address_of_INPUT_RANGE_0() { return &___INPUT_RANGE_0; }
	inline void set_INPUT_RANGE_0(int32_t value)
	{
		___INPUT_RANGE_0 = value;
	}

	inline static int32_t get_offset_of_tbl_1() { return static_cast<int32_t>(offsetof(MiniParser_t185565106_StaticFields, ___tbl_1)); }
	inline UInt16U5BU5D_t2527266722* get_tbl_1() const { return ___tbl_1; }
	inline UInt16U5BU5D_t2527266722** get_address_of_tbl_1() { return &___tbl_1; }
	inline void set_tbl_1(UInt16U5BU5D_t2527266722* value)
	{
		___tbl_1 = value;
		Il2CppCodeGenWriteBarrier((&___tbl_1), value);
	}

	inline static int32_t get_offset_of_errors_2() { return static_cast<int32_t>(offsetof(MiniParser_t185565106_StaticFields, ___errors_2)); }
	inline StringU5BU5D_t1642385972* get_errors_2() const { return ___errors_2; }
	inline StringU5BU5D_t1642385972** get_address_of_errors_2() { return &___errors_2; }
	inline void set_errors_2(StringU5BU5D_t1642385972* value)
	{
		___errors_2 = value;
		Il2CppCodeGenWriteBarrier((&___errors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIPARSER_T185565106_H
#ifndef HANDLERADAPTER_T3228504856_H
#define HANDLERADAPTER_T3228504856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser/HandlerAdapter
struct  HandlerAdapter_t3228504856  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLERADAPTER_T3228504856_H
#ifndef ATTRLISTIMPL_T383345538_H
#define ATTRLISTIMPL_T383345538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser/AttrListImpl
struct  AttrListImpl_t383345538  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Xml.MiniParser/AttrListImpl::names
	ArrayList_t4252133567 * ___names_0;
	// System.Collections.ArrayList Mono.Xml.MiniParser/AttrListImpl::values
	ArrayList_t4252133567 * ___values_1;

public:
	inline static int32_t get_offset_of_names_0() { return static_cast<int32_t>(offsetof(AttrListImpl_t383345538, ___names_0)); }
	inline ArrayList_t4252133567 * get_names_0() const { return ___names_0; }
	inline ArrayList_t4252133567 ** get_address_of_names_0() { return &___names_0; }
	inline void set_names_0(ArrayList_t4252133567 * value)
	{
		___names_0 = value;
		Il2CppCodeGenWriteBarrier((&___names_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(AttrListImpl_t383345538, ___values_1)); }
	inline ArrayList_t4252133567 * get_values_1() const { return ___values_1; }
	inline ArrayList_t4252133567 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t4252133567 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRLISTIMPL_T383345538_H
#ifndef LOCALE_T4255929015_H
#define LOCALE_T4255929015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Locale
struct  Locale_t4255929015  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALE_T4255929015_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1927440687 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t169632028* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____innerException_4)); }
	inline Exception_t1927440687 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1927440687 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1927440687 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t1975884510 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t1975884510 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t1975884510 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t2217612696* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t2217612696** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t2217612696* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t169632028* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t169632028** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t169632028* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1927440687_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1927440687_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1927440687_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1927440687_H
#ifndef MONOTLSPROVIDER_T823784021_H
#define MONOTLSPROVIDER_T823784021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsProvider
struct  MonoTlsProvider_t823784021  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDER_T823784021_H
#ifndef BASEVALIDATOR_T3557140249_H
#define BASEVALIDATOR_T3557140249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_t3557140249  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_t3518500204 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t19570940 * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t1507412803 * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_t1944712516 * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t3143048826 * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t1221177846 * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaCollection_0)); }
	inline XmlSchemaCollection_t3518500204 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_t3518500204 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_t3518500204 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaNames_3)); }
	inline SchemaNames_t1619962557 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t1619962557 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___positionInfo_4)); }
	inline PositionInfo_t3273236083 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_t3273236083 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___xmlResolver_5)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___baseUri_6)); }
	inline Uri_t19570940 * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t19570940 ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t19570940 * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaInfo_7)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___reader_8)); }
	inline XmlValidatingReaderImpl_t1507412803 * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t1507412803 ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t1507412803 * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___elementName_9)); }
	inline XmlQualifiedName_t1944712516 * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_t1944712516 * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___context_10)); }
	inline ValidationState_t3143048826 * get_context_10() const { return ___context_10; }
	inline ValidationState_t3143048826 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t3143048826 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textValue_11)); }
	inline StringBuilder_t1221177846 * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t1221177846 ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t1221177846 * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_T3557140249_H
#ifndef ENCRYPTEDDATA_T2656813773_H
#define ENCRYPTEDDATA_T2656813773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.PKCS7/EncryptedData
struct  EncryptedData_t2656813773  : public RuntimeObject
{
public:
	// System.Byte Mono.Security.PKCS7/EncryptedData::_version
	uint8_t ____version_0;
	// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::_content
	ContentInfo_t1443605388 * ____content_1;
	// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::_encryptionAlgorithm
	ContentInfo_t1443605388 * ____encryptionAlgorithm_2;
	// System.Byte[] Mono.Security.PKCS7/EncryptedData::_encrypted
	ByteU5BU5D_t3397334013* ____encrypted_3;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(EncryptedData_t2656813773, ____version_0)); }
	inline uint8_t get__version_0() const { return ____version_0; }
	inline uint8_t* get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(uint8_t value)
	{
		____version_0 = value;
	}

	inline static int32_t get_offset_of__content_1() { return static_cast<int32_t>(offsetof(EncryptedData_t2656813773, ____content_1)); }
	inline ContentInfo_t1443605388 * get__content_1() const { return ____content_1; }
	inline ContentInfo_t1443605388 ** get_address_of__content_1() { return &____content_1; }
	inline void set__content_1(ContentInfo_t1443605388 * value)
	{
		____content_1 = value;
		Il2CppCodeGenWriteBarrier((&____content_1), value);
	}

	inline static int32_t get_offset_of__encryptionAlgorithm_2() { return static_cast<int32_t>(offsetof(EncryptedData_t2656813773, ____encryptionAlgorithm_2)); }
	inline ContentInfo_t1443605388 * get__encryptionAlgorithm_2() const { return ____encryptionAlgorithm_2; }
	inline ContentInfo_t1443605388 ** get_address_of__encryptionAlgorithm_2() { return &____encryptionAlgorithm_2; }
	inline void set__encryptionAlgorithm_2(ContentInfo_t1443605388 * value)
	{
		____encryptionAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&____encryptionAlgorithm_2), value);
	}

	inline static int32_t get_offset_of__encrypted_3() { return static_cast<int32_t>(offsetof(EncryptedData_t2656813773, ____encrypted_3)); }
	inline ByteU5BU5D_t3397334013* get__encrypted_3() const { return ____encrypted_3; }
	inline ByteU5BU5D_t3397334013** get_address_of__encrypted_3() { return &____encrypted_3; }
	inline void set__encrypted_3(ByteU5BU5D_t3397334013* value)
	{
		____encrypted_3 = value;
		Il2CppCodeGenWriteBarrier((&____encrypted_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDDATA_T2656813773_H
#ifndef ASYMMETRICSIGNATUREFORMATTER_T4058014248_H
#define ASYMMETRICSIGNATUREFORMATTER_T4058014248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricSignatureFormatter
struct  AsymmetricSignatureFormatter_t4058014248  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICSIGNATUREFORMATTER_T4058014248_H
#ifndef CERTIFICATEVALIDATIONHELPER_T1569212914_H
#define CERTIFICATEVALIDATIONHELPER_T1569212914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.CertificateValidationHelper
struct  CertificateValidationHelper_t1569212914  : public RuntimeObject
{
public:

public:
};

struct CertificateValidationHelper_t1569212914_StaticFields
{
public:
	// System.Boolean Mono.Security.Interface.CertificateValidationHelper::noX509Chain
	bool ___noX509Chain_0;
	// System.Boolean Mono.Security.Interface.CertificateValidationHelper::supportsTrustAnchors
	bool ___supportsTrustAnchors_1;

public:
	inline static int32_t get_offset_of_noX509Chain_0() { return static_cast<int32_t>(offsetof(CertificateValidationHelper_t1569212914_StaticFields, ___noX509Chain_0)); }
	inline bool get_noX509Chain_0() const { return ___noX509Chain_0; }
	inline bool* get_address_of_noX509Chain_0() { return &___noX509Chain_0; }
	inline void set_noX509Chain_0(bool value)
	{
		___noX509Chain_0 = value;
	}

	inline static int32_t get_offset_of_supportsTrustAnchors_1() { return static_cast<int32_t>(offsetof(CertificateValidationHelper_t1569212914_StaticFields, ___supportsTrustAnchors_1)); }
	inline bool get_supportsTrustAnchors_1() const { return ___supportsTrustAnchors_1; }
	inline bool* get_address_of_supportsTrustAnchors_1() { return &___supportsTrustAnchors_1; }
	inline void set_supportsTrustAnchors_1(bool value)
	{
		___supportsTrustAnchors_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONHELPER_T1569212914_H
#ifndef HASHALGORITHM_T2624936259_H
#define HASHALGORITHM_T2624936259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t2624936259  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t3397334013* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___HashValue_1)); }
	inline ByteU5BU5D_t3397334013* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_t3397334013* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_1), value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T2624936259_H
#ifndef PKCS7_T3223261923_H
#define PKCS7_T3223261923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.PKCS7
struct  PKCS7_t3223261923  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS7_T3223261923_H
#ifndef ASN1_T924533536_H
#define ASN1_T924533536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.ASN1
struct  ASN1_t924533536  : public RuntimeObject
{
public:
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_t3397334013* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t4252133567 * ___elist_2;

public:
	inline static int32_t get_offset_of_m_nTag_0() { return static_cast<int32_t>(offsetof(ASN1_t924533536, ___m_nTag_0)); }
	inline uint8_t get_m_nTag_0() const { return ___m_nTag_0; }
	inline uint8_t* get_address_of_m_nTag_0() { return &___m_nTag_0; }
	inline void set_m_nTag_0(uint8_t value)
	{
		___m_nTag_0 = value;
	}

	inline static int32_t get_offset_of_m_aValue_1() { return static_cast<int32_t>(offsetof(ASN1_t924533536, ___m_aValue_1)); }
	inline ByteU5BU5D_t3397334013* get_m_aValue_1() const { return ___m_aValue_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_m_aValue_1() { return &___m_aValue_1; }
	inline void set_m_aValue_1(ByteU5BU5D_t3397334013* value)
	{
		___m_aValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_aValue_1), value);
	}

	inline static int32_t get_offset_of_elist_2() { return static_cast<int32_t>(offsetof(ASN1_t924533536, ___elist_2)); }
	inline ArrayList_t4252133567 * get_elist_2() const { return ___elist_2; }
	inline ArrayList_t4252133567 ** get_address_of_elist_2() { return &___elist_2; }
	inline void set_elist_2(ArrayList_t4252133567 * value)
	{
		___elist_2 = value;
		Il2CppCodeGenWriteBarrier((&___elist_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1_T924533536_H
#ifndef LIST_1_T4155544979_H
#define LIST_1_T4155544979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Mono.Security.Protocol.Tls.CipherSuite>
struct  List_1_t4155544979  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CipherSuiteU5BU5D_t3295586142* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4155544979, ____items_1)); }
	inline CipherSuiteU5BU5D_t3295586142* get__items_1() const { return ____items_1; }
	inline CipherSuiteU5BU5D_t3295586142** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CipherSuiteU5BU5D_t3295586142* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4155544979, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4155544979, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4155544979, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4155544979_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CipherSuiteU5BU5D_t3295586142* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4155544979_StaticFields, ____emptyArray_5)); }
	inline CipherSuiteU5BU5D_t3295586142* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CipherSuiteU5BU5D_t3295586142** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CipherSuiteU5BU5D_t3295586142* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4155544979_H
#ifndef BITCONVERTERLE_T2825370261_H
#define BITCONVERTERLE_T2825370261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.BitConverterLE
struct  BitConverterLE_t2825370261  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTERLE_T2825370261_H
#ifndef ASN1CONVERT_T3301846397_H
#define ASN1CONVERT_T3301846397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.ASN1Convert
struct  ASN1Convert_t3301846397  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1CONVERT_T3301846397_H
#ifndef CIPHERSUITEFACTORY_T3273693255_H
#define CIPHERSUITEFACTORY_T3273693255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherSuiteFactory
struct  CipherSuiteFactory_t3273693255  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITEFACTORY_T3273693255_H
#ifndef SENDRECORDASYNCRESULT_T173216930_H
#define SENDRECORDASYNCRESULT_T173216930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult
struct  SendRecordAsyncResult_t173216930  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::_userCallback
	AsyncCallback_t163412349 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::_asyncException
	Exception_t1927440687 * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::handle
	ManualResetEvent_t926074657 * ___handle_4;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::_message
	HandshakeMessage_t3938752374 * ____message_5;
	// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult::completed
	bool ___completed_6;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ____userCallback_1)); }
	inline AsyncCallback_t163412349 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t163412349 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t163412349 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ____asyncException_3)); }
	inline Exception_t1927440687 * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t1927440687 ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t1927440687 * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ___handle_4)); }
	inline ManualResetEvent_t926074657 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_t926074657 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of__message_5() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ____message_5)); }
	inline HandshakeMessage_t3938752374 * get__message_5() const { return ____message_5; }
	inline HandshakeMessage_t3938752374 ** get_address_of__message_5() { return &____message_5; }
	inline void set__message_5(HandshakeMessage_t3938752374 * value)
	{
		____message_5 = value;
		Il2CppCodeGenWriteBarrier((&____message_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(SendRecordAsyncResult_t173216930, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDRECORDASYNCRESULT_T173216930_H
#ifndef XMLCONFIGURATIONSTRING_T3722747970_H
#define XMLCONFIGURATIONSTRING_T3722747970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConfiguration.XmlConfigurationString
struct  XmlConfigurationString_t3722747970  : public RuntimeObject
{
public:

public:
};

struct XmlConfigurationString_t3722747970_StaticFields
{
public:
	// System.String System.Xml.XmlConfiguration.XmlConfigurationString::XmlReaderSectionPath
	String_t* ___XmlReaderSectionPath_0;
	// System.String System.Xml.XmlConfiguration.XmlConfigurationString::XsltSectionPath
	String_t* ___XsltSectionPath_1;

public:
	inline static int32_t get_offset_of_XmlReaderSectionPath_0() { return static_cast<int32_t>(offsetof(XmlConfigurationString_t3722747970_StaticFields, ___XmlReaderSectionPath_0)); }
	inline String_t* get_XmlReaderSectionPath_0() const { return ___XmlReaderSectionPath_0; }
	inline String_t** get_address_of_XmlReaderSectionPath_0() { return &___XmlReaderSectionPath_0; }
	inline void set_XmlReaderSectionPath_0(String_t* value)
	{
		___XmlReaderSectionPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___XmlReaderSectionPath_0), value);
	}

	inline static int32_t get_offset_of_XsltSectionPath_1() { return static_cast<int32_t>(offsetof(XmlConfigurationString_t3722747970_StaticFields, ___XsltSectionPath_1)); }
	inline String_t* get_XsltSectionPath_1() const { return ___XsltSectionPath_1; }
	inline String_t** get_address_of_XsltSectionPath_1() { return &___XsltSectionPath_1; }
	inline void set_XsltSectionPath_1(String_t* value)
	{
		___XsltSectionPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___XsltSectionPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONFIGURATIONSTRING_T3722747970_H
#ifndef RECORDPROTOCOL_T3166895267_H
#define RECORDPROTOCOL_T3166895267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RecordProtocol
struct  RecordProtocol_t3166895267  : public RuntimeObject
{
public:
	// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol::innerStream
	Stream_t3255436806 * ___innerStream_1;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.RecordProtocol::context
	Context_t4285182719 * ___context_2;

public:
	inline static int32_t get_offset_of_innerStream_1() { return static_cast<int32_t>(offsetof(RecordProtocol_t3166895267, ___innerStream_1)); }
	inline Stream_t3255436806 * get_innerStream_1() const { return ___innerStream_1; }
	inline Stream_t3255436806 ** get_address_of_innerStream_1() { return &___innerStream_1; }
	inline void set_innerStream_1(Stream_t3255436806 * value)
	{
		___innerStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___innerStream_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(RecordProtocol_t3166895267, ___context_2)); }
	inline Context_t4285182719 * get_context_2() const { return ___context_2; }
	inline Context_t4285182719 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(Context_t4285182719 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}
};

struct RecordProtocol_t3166895267_StaticFields
{
public:
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.RecordProtocol::record_processing
	ManualResetEvent_t926074657 * ___record_processing_0;

public:
	inline static int32_t get_offset_of_record_processing_0() { return static_cast<int32_t>(offsetof(RecordProtocol_t3166895267_StaticFields, ___record_processing_0)); }
	inline ManualResetEvent_t926074657 * get_record_processing_0() const { return ___record_processing_0; }
	inline ManualResetEvent_t926074657 ** get_address_of_record_processing_0() { return &___record_processing_0; }
	inline void set_record_processing_0(ManualResetEvent_t926074657 * value)
	{
		___record_processing_0 = value;
		Il2CppCodeGenWriteBarrier((&___record_processing_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDPROTOCOL_T3166895267_H
#ifndef RECEIVERECORDASYNCRESULT_T1946181211_H
#define RECEIVERECORDASYNCRESULT_T1946181211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct  ReceiveRecordAsyncResult_t1946181211  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::_userCallback
	AsyncCallback_t163412349 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::_asyncException
	Exception_t1927440687 * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::handle
	ManualResetEvent_t926074657 * ___handle_4;
	// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::_resultingBuffer
	ByteU5BU5D_t3397334013* ____resultingBuffer_5;
	// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::_record
	Stream_t3255436806 * ____record_6;
	// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::completed
	bool ___completed_7;
	// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::_initialBuffer
	ByteU5BU5D_t3397334013* ____initialBuffer_8;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ____userCallback_1)); }
	inline AsyncCallback_t163412349 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t163412349 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t163412349 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ____asyncException_3)); }
	inline Exception_t1927440687 * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t1927440687 ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t1927440687 * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ___handle_4)); }
	inline ManualResetEvent_t926074657 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_t926074657 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of__resultingBuffer_5() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ____resultingBuffer_5)); }
	inline ByteU5BU5D_t3397334013* get__resultingBuffer_5() const { return ____resultingBuffer_5; }
	inline ByteU5BU5D_t3397334013** get_address_of__resultingBuffer_5() { return &____resultingBuffer_5; }
	inline void set__resultingBuffer_5(ByteU5BU5D_t3397334013* value)
	{
		____resultingBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____resultingBuffer_5), value);
	}

	inline static int32_t get_offset_of__record_6() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ____record_6)); }
	inline Stream_t3255436806 * get__record_6() const { return ____record_6; }
	inline Stream_t3255436806 ** get_address_of__record_6() { return &____record_6; }
	inline void set__record_6(Stream_t3255436806 * value)
	{
		____record_6 = value;
		Il2CppCodeGenWriteBarrier((&____record_6), value);
	}

	inline static int32_t get_offset_of_completed_7() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ___completed_7)); }
	inline bool get_completed_7() const { return ___completed_7; }
	inline bool* get_address_of_completed_7() { return &___completed_7; }
	inline void set_completed_7(bool value)
	{
		___completed_7 = value;
	}

	inline static int32_t get_offset_of__initialBuffer_8() { return static_cast<int32_t>(offsetof(ReceiveRecordAsyncResult_t1946181211, ____initialBuffer_8)); }
	inline ByteU5BU5D_t3397334013* get__initialBuffer_8() const { return ____initialBuffer_8; }
	inline ByteU5BU5D_t3397334013** get_address_of__initialBuffer_8() { return &____initialBuffer_8; }
	inline void set__initialBuffer_8(ByteU5BU5D_t3397334013* value)
	{
		____initialBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&____initialBuffer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVERECORDASYNCRESULT_T1946181211_H
#ifndef CONFIGURATIONELEMENT_T1776195828_H
#define CONFIGURATIONELEMENT_T1776195828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_t1776195828  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationElement::rawXml
	String_t* ___rawXml_0;
	// System.Boolean System.Configuration.ConfigurationElement::modified
	bool ___modified_1;
	// System.Configuration.ElementMap System.Configuration.ConfigurationElement::map
	ElementMap_t997038224 * ___map_2;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::keyProps
	ConfigurationPropertyCollection_t3473514151 * ___keyProps_3;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElement::defaultCollection
	ConfigurationElementCollection_t1911180302 * ___defaultCollection_4;
	// System.Boolean System.Configuration.ConfigurationElement::readOnly
	bool ___readOnly_5;
	// System.Configuration.ElementInformation System.Configuration.ConfigurationElement::elementInfo
	ElementInformation_t3165583784 * ___elementInfo_6;
	// System.Configuration.Configuration System.Configuration.ConfigurationElement::_configuration
	Configuration_t3335372970 * ____configuration_7;
	// System.Boolean System.Configuration.ConfigurationElement::elementPresent
	bool ___elementPresent_8;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllAttributesExcept
	ConfigurationLockCollection_t1011762925 * ___lockAllAttributesExcept_9;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllElementsExcept
	ConfigurationLockCollection_t1011762925 * ___lockAllElementsExcept_10;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAttributes
	ConfigurationLockCollection_t1011762925 * ___lockAttributes_11;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockElements
	ConfigurationLockCollection_t1011762925 * ___lockElements_12;
	// System.Boolean System.Configuration.ConfigurationElement::lockItem
	bool ___lockItem_13;
	// System.Configuration.ConfigurationElement/SaveContext System.Configuration.ConfigurationElement::saveContext
	SaveContext_t3996373180 * ___saveContext_14;

public:
	inline static int32_t get_offset_of_rawXml_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___rawXml_0)); }
	inline String_t* get_rawXml_0() const { return ___rawXml_0; }
	inline String_t** get_address_of_rawXml_0() { return &___rawXml_0; }
	inline void set_rawXml_0(String_t* value)
	{
		___rawXml_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawXml_0), value);
	}

	inline static int32_t get_offset_of_modified_1() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___modified_1)); }
	inline bool get_modified_1() const { return ___modified_1; }
	inline bool* get_address_of_modified_1() { return &___modified_1; }
	inline void set_modified_1(bool value)
	{
		___modified_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___map_2)); }
	inline ElementMap_t997038224 * get_map_2() const { return ___map_2; }
	inline ElementMap_t997038224 ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(ElementMap_t997038224 * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_keyProps_3() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___keyProps_3)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_keyProps_3() const { return ___keyProps_3; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_keyProps_3() { return &___keyProps_3; }
	inline void set_keyProps_3(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___keyProps_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyProps_3), value);
	}

	inline static int32_t get_offset_of_defaultCollection_4() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___defaultCollection_4)); }
	inline ConfigurationElementCollection_t1911180302 * get_defaultCollection_4() const { return ___defaultCollection_4; }
	inline ConfigurationElementCollection_t1911180302 ** get_address_of_defaultCollection_4() { return &___defaultCollection_4; }
	inline void set_defaultCollection_4(ConfigurationElementCollection_t1911180302 * value)
	{
		___defaultCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollection_4), value);
	}

	inline static int32_t get_offset_of_readOnly_5() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___readOnly_5)); }
	inline bool get_readOnly_5() const { return ___readOnly_5; }
	inline bool* get_address_of_readOnly_5() { return &___readOnly_5; }
	inline void set_readOnly_5(bool value)
	{
		___readOnly_5 = value;
	}

	inline static int32_t get_offset_of_elementInfo_6() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___elementInfo_6)); }
	inline ElementInformation_t3165583784 * get_elementInfo_6() const { return ___elementInfo_6; }
	inline ElementInformation_t3165583784 ** get_address_of_elementInfo_6() { return &___elementInfo_6; }
	inline void set_elementInfo_6(ElementInformation_t3165583784 * value)
	{
		___elementInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___elementInfo_6), value);
	}

	inline static int32_t get_offset_of__configuration_7() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ____configuration_7)); }
	inline Configuration_t3335372970 * get__configuration_7() const { return ____configuration_7; }
	inline Configuration_t3335372970 ** get_address_of__configuration_7() { return &____configuration_7; }
	inline void set__configuration_7(Configuration_t3335372970 * value)
	{
		____configuration_7 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_7), value);
	}

	inline static int32_t get_offset_of_elementPresent_8() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___elementPresent_8)); }
	inline bool get_elementPresent_8() const { return ___elementPresent_8; }
	inline bool* get_address_of_elementPresent_8() { return &___elementPresent_8; }
	inline void set_elementPresent_8(bool value)
	{
		___elementPresent_8 = value;
	}

	inline static int32_t get_offset_of_lockAllAttributesExcept_9() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockAllAttributesExcept_9)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockAllAttributesExcept_9() const { return ___lockAllAttributesExcept_9; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockAllAttributesExcept_9() { return &___lockAllAttributesExcept_9; }
	inline void set_lockAllAttributesExcept_9(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockAllAttributesExcept_9 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllAttributesExcept_9), value);
	}

	inline static int32_t get_offset_of_lockAllElementsExcept_10() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockAllElementsExcept_10)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockAllElementsExcept_10() const { return ___lockAllElementsExcept_10; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockAllElementsExcept_10() { return &___lockAllElementsExcept_10; }
	inline void set_lockAllElementsExcept_10(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockAllElementsExcept_10 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllElementsExcept_10), value);
	}

	inline static int32_t get_offset_of_lockAttributes_11() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockAttributes_11)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockAttributes_11() const { return ___lockAttributes_11; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockAttributes_11() { return &___lockAttributes_11; }
	inline void set_lockAttributes_11(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockAttributes_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockAttributes_11), value);
	}

	inline static int32_t get_offset_of_lockElements_12() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockElements_12)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockElements_12() const { return ___lockElements_12; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockElements_12() { return &___lockElements_12; }
	inline void set_lockElements_12(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockElements_12 = value;
		Il2CppCodeGenWriteBarrier((&___lockElements_12), value);
	}

	inline static int32_t get_offset_of_lockItem_13() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockItem_13)); }
	inline bool get_lockItem_13() const { return ___lockItem_13; }
	inline bool* get_address_of_lockItem_13() { return &___lockItem_13; }
	inline void set_lockItem_13(bool value)
	{
		___lockItem_13 = value;
	}

	inline static int32_t get_offset_of_saveContext_14() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___saveContext_14)); }
	inline SaveContext_t3996373180 * get_saveContext_14() const { return ___saveContext_14; }
	inline SaveContext_t3996373180 ** get_address_of_saveContext_14() { return &___saveContext_14; }
	inline void set_saveContext_14(SaveContext_t3996373180 * value)
	{
		___saveContext_14 = value;
		Il2CppCodeGenWriteBarrier((&___saveContext_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T1776195828_H
#ifndef SECURITYPARAMETERS_T2290372928_H
#define SECURITYPARAMETERS_T2290372928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityParameters
struct  SecurityParameters_t2290372928  : public RuntimeObject
{
public:
	// Mono.Security.Protocol.Tls.CipherSuite Mono.Security.Protocol.Tls.SecurityParameters::cipher
	CipherSuite_t491456551 * ___cipher_0;
	// System.Byte[] Mono.Security.Protocol.Tls.SecurityParameters::clientWriteMAC
	ByteU5BU5D_t3397334013* ___clientWriteMAC_1;
	// System.Byte[] Mono.Security.Protocol.Tls.SecurityParameters::serverWriteMAC
	ByteU5BU5D_t3397334013* ___serverWriteMAC_2;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(SecurityParameters_t2290372928, ___cipher_0)); }
	inline CipherSuite_t491456551 * get_cipher_0() const { return ___cipher_0; }
	inline CipherSuite_t491456551 ** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(CipherSuite_t491456551 * value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_clientWriteMAC_1() { return static_cast<int32_t>(offsetof(SecurityParameters_t2290372928, ___clientWriteMAC_1)); }
	inline ByteU5BU5D_t3397334013* get_clientWriteMAC_1() const { return ___clientWriteMAC_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_clientWriteMAC_1() { return &___clientWriteMAC_1; }
	inline void set_clientWriteMAC_1(ByteU5BU5D_t3397334013* value)
	{
		___clientWriteMAC_1 = value;
		Il2CppCodeGenWriteBarrier((&___clientWriteMAC_1), value);
	}

	inline static int32_t get_offset_of_serverWriteMAC_2() { return static_cast<int32_t>(offsetof(SecurityParameters_t2290372928, ___serverWriteMAC_2)); }
	inline ByteU5BU5D_t3397334013* get_serverWriteMAC_2() const { return ___serverWriteMAC_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_serverWriteMAC_2() { return &___serverWriteMAC_2; }
	inline void set_serverWriteMAC_2(ByteU5BU5D_t3397334013* value)
	{
		___serverWriteMAC_2 = value;
		Il2CppCodeGenWriteBarrier((&___serverWriteMAC_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPARAMETERS_T2290372928_H
#ifndef CONTENTINFO_T1443605388_H
#define CONTENTINFO_T1443605388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.PKCS7/ContentInfo
struct  ContentInfo_t1443605388  : public RuntimeObject
{
public:
	// System.String Mono.Security.PKCS7/ContentInfo::contentType
	String_t* ___contentType_0;
	// Mono.Security.ASN1 Mono.Security.PKCS7/ContentInfo::content
	ASN1_t924533536 * ___content_1;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentInfo_t1443605388, ___contentType_0)); }
	inline String_t* get_contentType_0() const { return ___contentType_0; }
	inline String_t** get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(String_t* value)
	{
		___contentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(ContentInfo_t1443605388, ___content_1)); }
	inline ASN1_t924533536 * get_content_1() const { return ___content_1; }
	inline ASN1_t924533536 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(ASN1_t924533536 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTINFO_T1443605388_H
#ifndef INTERNALASYNCRESULT_T1610391122_H
#define INTERNALASYNCRESULT_T1610391122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
struct  InternalAsyncResult_t1610391122  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_userCallback
	AsyncCallback_t163412349 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_asyncException
	Exception_t1927440687 * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::handle
	ManualResetEvent_t926074657 * ___handle_4;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::completed
	bool ___completed_5;
	// System.Int32 Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_bytesRead
	int32_t ____bytesRead_6;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_fromWrite
	bool ____fromWrite_7;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_proceedAfterHandshake
	bool ____proceedAfterHandshake_8;
	// System.Byte[] Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_buffer
	ByteU5BU5D_t3397334013* ____buffer_9;
	// System.Int32 Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_offset
	int32_t ____offset_10;
	// System.Int32 Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult::_count
	int32_t ____count_11;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____userCallback_1)); }
	inline AsyncCallback_t163412349 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t163412349 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t163412349 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____asyncException_3)); }
	inline Exception_t1927440687 * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t1927440687 ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t1927440687 * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ___handle_4)); }
	inline ManualResetEvent_t926074657 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_t926074657 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of_completed_5() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ___completed_5)); }
	inline bool get_completed_5() const { return ___completed_5; }
	inline bool* get_address_of_completed_5() { return &___completed_5; }
	inline void set_completed_5(bool value)
	{
		___completed_5 = value;
	}

	inline static int32_t get_offset_of__bytesRead_6() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____bytesRead_6)); }
	inline int32_t get__bytesRead_6() const { return ____bytesRead_6; }
	inline int32_t* get_address_of__bytesRead_6() { return &____bytesRead_6; }
	inline void set__bytesRead_6(int32_t value)
	{
		____bytesRead_6 = value;
	}

	inline static int32_t get_offset_of__fromWrite_7() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____fromWrite_7)); }
	inline bool get__fromWrite_7() const { return ____fromWrite_7; }
	inline bool* get_address_of__fromWrite_7() { return &____fromWrite_7; }
	inline void set__fromWrite_7(bool value)
	{
		____fromWrite_7 = value;
	}

	inline static int32_t get_offset_of__proceedAfterHandshake_8() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____proceedAfterHandshake_8)); }
	inline bool get__proceedAfterHandshake_8() const { return ____proceedAfterHandshake_8; }
	inline bool* get_address_of__proceedAfterHandshake_8() { return &____proceedAfterHandshake_8; }
	inline void set__proceedAfterHandshake_8(bool value)
	{
		____proceedAfterHandshake_8 = value;
	}

	inline static int32_t get_offset_of__buffer_9() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____buffer_9)); }
	inline ByteU5BU5D_t3397334013* get__buffer_9() const { return ____buffer_9; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_9() { return &____buffer_9; }
	inline void set__buffer_9(ByteU5BU5D_t3397334013* value)
	{
		____buffer_9 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_9), value);
	}

	inline static int32_t get_offset_of__offset_10() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____offset_10)); }
	inline int32_t get__offset_10() const { return ____offset_10; }
	inline int32_t* get_address_of__offset_10() { return &____offset_10; }
	inline void set__offset_10(int32_t value)
	{
		____offset_10 = value;
	}

	inline static int32_t get_offset_of__count_11() { return static_cast<int32_t>(offsetof(InternalAsyncResult_t1610391122, ____count_11)); }
	inline int32_t get__count_11() const { return ____count_11; }
	inline int32_t* get_address_of__count_11() { return &____count_11; }
	inline void set__count_11(int32_t value)
	{
		____count_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALASYNCRESULT_T1610391122_H
#ifndef MARSHALBYREFOBJECT_T1285298191_H
#define MARSHALBYREFOBJECT_T1285298191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1285298191  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1656058977 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1285298191, ____identity_0)); }
	inline ServerIdentity_t1656058977 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1656058977 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1656058977 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_pinvoke
{
	ServerIdentity_t1656058977 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_com
{
	ServerIdentity_t1656058977 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T1285298191_H
#ifndef CLIENTSESSIONCACHE_T3595945587_H
#define CLIENTSESSIONCACHE_T3595945587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ClientSessionCache
struct  ClientSessionCache_t3595945587  : public RuntimeObject
{
public:

public:
};

struct ClientSessionCache_t3595945587_StaticFields
{
public:
	// System.Collections.Hashtable Mono.Security.Protocol.Tls.ClientSessionCache::cache
	Hashtable_t909839986 * ___cache_0;
	// System.Object Mono.Security.Protocol.Tls.ClientSessionCache::locker
	RuntimeObject * ___locker_1;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(ClientSessionCache_t3595945587_StaticFields, ___cache_0)); }
	inline Hashtable_t909839986 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t909839986 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t909839986 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}

	inline static int32_t get_offset_of_locker_1() { return static_cast<int32_t>(offsetof(ClientSessionCache_t3595945587_StaticFields, ___locker_1)); }
	inline RuntimeObject * get_locker_1() const { return ___locker_1; }
	inline RuntimeObject ** get_address_of_locker_1() { return &___locker_1; }
	inline void set_locker_1(RuntimeObject * value)
	{
		___locker_1 = value;
		Il2CppCodeGenWriteBarrier((&___locker_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSESSIONCACHE_T3595945587_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef TLSCLIENTSETTINGS_T2311449551_H
#define TLSCLIENTSETTINGS_T2311449551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsClientSettings
struct  TlsClientSettings_t2311449551  : public RuntimeObject
{
public:
	// System.String Mono.Security.Protocol.Tls.TlsClientSettings::targetHost
	String_t* ___targetHost_0;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Security.Protocol.Tls.TlsClientSettings::certificates
	X509CertificateCollection_t1197680765 * ___certificates_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.TlsClientSettings::clientCertificate
	X509Certificate_t283079845 * ___clientCertificate_2;
	// Mono.Security.Cryptography.RSAManaged Mono.Security.Protocol.Tls.TlsClientSettings::certificateRSA
	RSAManaged_t3034748748 * ___certificateRSA_3;

public:
	inline static int32_t get_offset_of_targetHost_0() { return static_cast<int32_t>(offsetof(TlsClientSettings_t2311449551, ___targetHost_0)); }
	inline String_t* get_targetHost_0() const { return ___targetHost_0; }
	inline String_t** get_address_of_targetHost_0() { return &___targetHost_0; }
	inline void set_targetHost_0(String_t* value)
	{
		___targetHost_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetHost_0), value);
	}

	inline static int32_t get_offset_of_certificates_1() { return static_cast<int32_t>(offsetof(TlsClientSettings_t2311449551, ___certificates_1)); }
	inline X509CertificateCollection_t1197680765 * get_certificates_1() const { return ___certificates_1; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_certificates_1() { return &___certificates_1; }
	inline void set_certificates_1(X509CertificateCollection_t1197680765 * value)
	{
		___certificates_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_1), value);
	}

	inline static int32_t get_offset_of_clientCertificate_2() { return static_cast<int32_t>(offsetof(TlsClientSettings_t2311449551, ___clientCertificate_2)); }
	inline X509Certificate_t283079845 * get_clientCertificate_2() const { return ___clientCertificate_2; }
	inline X509Certificate_t283079845 ** get_address_of_clientCertificate_2() { return &___clientCertificate_2; }
	inline void set_clientCertificate_2(X509Certificate_t283079845 * value)
	{
		___clientCertificate_2 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificate_2), value);
	}

	inline static int32_t get_offset_of_certificateRSA_3() { return static_cast<int32_t>(offsetof(TlsClientSettings_t2311449551, ___certificateRSA_3)); }
	inline RSAManaged_t3034748748 * get_certificateRSA_3() const { return ___certificateRSA_3; }
	inline RSAManaged_t3034748748 ** get_address_of_certificateRSA_3() { return &___certificateRSA_3; }
	inline void set_certificateRSA_3(RSAManaged_t3034748748 * value)
	{
		___certificateRSA_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificateRSA_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTSETTINGS_T2311449551_H
#ifndef U3CU3EC_T1881663454_H
#define U3CU3EC_T1881663454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HttpsClientStream/<>c
struct  U3CU3Ec_t1881663454  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1881663454_StaticFields
{
public:
	// Mono.Security.Protocol.Tls.HttpsClientStream/<>c Mono.Security.Protocol.Tls.HttpsClientStream/<>c::<>9
	U3CU3Ec_t1881663454 * ___U3CU3E9_0;
	// Mono.Security.Protocol.Tls.CertificateSelectionCallback Mono.Security.Protocol.Tls.HttpsClientStream/<>c::<>9__2_0
	CertificateSelectionCallback_t3721235490 * ___U3CU3E9__2_0_1;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.HttpsClientStream/<>c::<>9__2_1
	PrivateKeySelectionCallback_t1663566523 * ___U3CU3E9__2_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1881663454_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1881663454 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1881663454 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1881663454 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1881663454_StaticFields, ___U3CU3E9__2_0_1)); }
	inline CertificateSelectionCallback_t3721235490 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline CertificateSelectionCallback_t3721235490 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(CertificateSelectionCallback_t3721235490 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1881663454_StaticFields, ___U3CU3E9__2_1_2)); }
	inline PrivateKeySelectionCallback_t1663566523 * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline PrivateKeySelectionCallback_t1663566523 ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(PrivateKeySelectionCallback_t1663566523 * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1881663454_H
#ifndef CONFIGURATIONSECTION_T2600766927_H
#define CONFIGURATIONSECTION_T2600766927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSection
struct  ConfigurationSection_t2600766927  : public ConfigurationElement_t1776195828
{
public:
	// System.Configuration.SectionInformation System.Configuration.ConfigurationSection::sectionInformation
	SectionInformation_t2754609709 * ___sectionInformation_15;
	// System.Configuration.IConfigurationSectionHandler System.Configuration.ConfigurationSection::section_handler
	RuntimeObject* ___section_handler_16;
	// System.String System.Configuration.ConfigurationSection::externalDataXml
	String_t* ___externalDataXml_17;
	// System.Object System.Configuration.ConfigurationSection::_configContext
	RuntimeObject * ____configContext_18;

public:
	inline static int32_t get_offset_of_sectionInformation_15() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ___sectionInformation_15)); }
	inline SectionInformation_t2754609709 * get_sectionInformation_15() const { return ___sectionInformation_15; }
	inline SectionInformation_t2754609709 ** get_address_of_sectionInformation_15() { return &___sectionInformation_15; }
	inline void set_sectionInformation_15(SectionInformation_t2754609709 * value)
	{
		___sectionInformation_15 = value;
		Il2CppCodeGenWriteBarrier((&___sectionInformation_15), value);
	}

	inline static int32_t get_offset_of_section_handler_16() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ___section_handler_16)); }
	inline RuntimeObject* get_section_handler_16() const { return ___section_handler_16; }
	inline RuntimeObject** get_address_of_section_handler_16() { return &___section_handler_16; }
	inline void set_section_handler_16(RuntimeObject* value)
	{
		___section_handler_16 = value;
		Il2CppCodeGenWriteBarrier((&___section_handler_16), value);
	}

	inline static int32_t get_offset_of_externalDataXml_17() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ___externalDataXml_17)); }
	inline String_t* get_externalDataXml_17() const { return ___externalDataXml_17; }
	inline String_t** get_address_of_externalDataXml_17() { return &___externalDataXml_17; }
	inline void set_externalDataXml_17(String_t* value)
	{
		___externalDataXml_17 = value;
		Il2CppCodeGenWriteBarrier((&___externalDataXml_17), value);
	}

	inline static int32_t get_offset_of__configContext_18() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ____configContext_18)); }
	inline RuntimeObject * get__configContext_18() const { return ____configContext_18; }
	inline RuntimeObject ** get_address_of__configContext_18() { return &____configContext_18; }
	inline void set__configContext_18(RuntimeObject * value)
	{
		____configContext_18 = value;
		Il2CppCodeGenWriteBarrier((&____configContext_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTION_T2600766927_H
#ifndef CLIENTRECORDPROTOCOL_T2694504884_H
#define CLIENTRECORDPROTOCOL_T2694504884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ClientRecordProtocol
struct  ClientRecordProtocol_t2694504884  : public RecordProtocol_t3166895267
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTRECORDPROTOCOL_T2694504884_H
#ifndef TLSEXCEPTION_T583514812_H
#define TLSEXCEPTION_T583514812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsException
struct  TlsException_t583514812  : public Exception_t1927440687
{
public:
	// Mono.Security.Protocol.Tls.Alert Mono.Security.Protocol.Tls.TlsException::alert
	Alert_t3405955216 * ___alert_16;

public:
	inline static int32_t get_offset_of_alert_16() { return static_cast<int32_t>(offsetof(TlsException_t583514812, ___alert_16)); }
	inline Alert_t3405955216 * get_alert_16() const { return ___alert_16; }
	inline Alert_t3405955216 ** get_address_of_alert_16() { return &___alert_16; }
	inline void set_alert_16(Alert_t3405955216 * value)
	{
		___alert_16 = value;
		Il2CppCodeGenWriteBarrier((&___alert_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSEXCEPTION_T583514812_H
#ifndef SSLHANDSHAKEHASH_T3044322977_H
#define SSLHANDSHAKEHASH_T3044322977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslHandshakeHash
struct  SslHandshakeHash_t3044322977  : public HashAlgorithm_t2624936259
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.SslHandshakeHash::md5
	HashAlgorithm_t2624936259 * ___md5_4;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.SslHandshakeHash::sha
	HashAlgorithm_t2624936259 * ___sha_5;
	// System.Boolean Mono.Security.Protocol.Tls.SslHandshakeHash::hashing
	bool ___hashing_6;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::secret
	ByteU5BU5D_t3397334013* ___secret_7;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::innerPadMD5
	ByteU5BU5D_t3397334013* ___innerPadMD5_8;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::outerPadMD5
	ByteU5BU5D_t3397334013* ___outerPadMD5_9;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::innerPadSHA
	ByteU5BU5D_t3397334013* ___innerPadSHA_10;
	// System.Byte[] Mono.Security.Protocol.Tls.SslHandshakeHash::outerPadSHA
	ByteU5BU5D_t3397334013* ___outerPadSHA_11;

public:
	inline static int32_t get_offset_of_md5_4() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___md5_4)); }
	inline HashAlgorithm_t2624936259 * get_md5_4() const { return ___md5_4; }
	inline HashAlgorithm_t2624936259 ** get_address_of_md5_4() { return &___md5_4; }
	inline void set_md5_4(HashAlgorithm_t2624936259 * value)
	{
		___md5_4 = value;
		Il2CppCodeGenWriteBarrier((&___md5_4), value);
	}

	inline static int32_t get_offset_of_sha_5() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___sha_5)); }
	inline HashAlgorithm_t2624936259 * get_sha_5() const { return ___sha_5; }
	inline HashAlgorithm_t2624936259 ** get_address_of_sha_5() { return &___sha_5; }
	inline void set_sha_5(HashAlgorithm_t2624936259 * value)
	{
		___sha_5 = value;
		Il2CppCodeGenWriteBarrier((&___sha_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}

	inline static int32_t get_offset_of_secret_7() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___secret_7)); }
	inline ByteU5BU5D_t3397334013* get_secret_7() const { return ___secret_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_secret_7() { return &___secret_7; }
	inline void set_secret_7(ByteU5BU5D_t3397334013* value)
	{
		___secret_7 = value;
		Il2CppCodeGenWriteBarrier((&___secret_7), value);
	}

	inline static int32_t get_offset_of_innerPadMD5_8() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___innerPadMD5_8)); }
	inline ByteU5BU5D_t3397334013* get_innerPadMD5_8() const { return ___innerPadMD5_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_innerPadMD5_8() { return &___innerPadMD5_8; }
	inline void set_innerPadMD5_8(ByteU5BU5D_t3397334013* value)
	{
		___innerPadMD5_8 = value;
		Il2CppCodeGenWriteBarrier((&___innerPadMD5_8), value);
	}

	inline static int32_t get_offset_of_outerPadMD5_9() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___outerPadMD5_9)); }
	inline ByteU5BU5D_t3397334013* get_outerPadMD5_9() const { return ___outerPadMD5_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_outerPadMD5_9() { return &___outerPadMD5_9; }
	inline void set_outerPadMD5_9(ByteU5BU5D_t3397334013* value)
	{
		___outerPadMD5_9 = value;
		Il2CppCodeGenWriteBarrier((&___outerPadMD5_9), value);
	}

	inline static int32_t get_offset_of_innerPadSHA_10() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___innerPadSHA_10)); }
	inline ByteU5BU5D_t3397334013* get_innerPadSHA_10() const { return ___innerPadSHA_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_innerPadSHA_10() { return &___innerPadSHA_10; }
	inline void set_innerPadSHA_10(ByteU5BU5D_t3397334013* value)
	{
		___innerPadSHA_10 = value;
		Il2CppCodeGenWriteBarrier((&___innerPadSHA_10), value);
	}

	inline static int32_t get_offset_of_outerPadSHA_11() { return static_cast<int32_t>(offsetof(SslHandshakeHash_t3044322977, ___outerPadSHA_11)); }
	inline ByteU5BU5D_t3397334013* get_outerPadSHA_11() const { return ___outerPadSHA_11; }
	inline ByteU5BU5D_t3397334013** get_address_of_outerPadSHA_11() { return &___outerPadSHA_11; }
	inline void set_outerPadSHA_11(ByteU5BU5D_t3397334013* value)
	{
		___outerPadSHA_11 = value;
		Il2CppCodeGenWriteBarrier((&___outerPadSHA_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLHANDSHAKEHASH_T3044322977_H
#ifndef RSASSLSIGNATUREFORMATTER_T1282301050_H
#define RSASSLSIGNATUREFORMATTER_T1282301050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.RSASslSignatureFormatter
struct  RSASslSignatureFormatter_t1282301050  : public AsymmetricSignatureFormatter_t4058014248
{
public:
	// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.RSASslSignatureFormatter::key
	RSA_t3719518354 * ___key_0;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Protocol.Tls.RSASslSignatureFormatter::hash
	HashAlgorithm_t2624936259 * ___hash_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(RSASslSignatureFormatter_t1282301050, ___key_0)); }
	inline RSA_t3719518354 * get_key_0() const { return ___key_0; }
	inline RSA_t3719518354 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RSA_t3719518354 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(RSASslSignatureFormatter_t1282301050, ___hash_1)); }
	inline HashAlgorithm_t2624936259 * get_hash_1() const { return ___hash_1; }
	inline HashAlgorithm_t2624936259 ** get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(HashAlgorithm_t2624936259 * value)
	{
		___hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___hash_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSASSLSIGNATUREFORMATTER_T1282301050_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_4;

public:
	inline static int32_t get_offset_of_dateData_4() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___dateData_4)); }
	inline uint64_t get_dateData_4() const { return ___dateData_4; }
	inline uint64_t* get_address_of_dateData_4() { return &___dateData_4; }
	inline void set_dateData_4(uint64_t value)
	{
		___dateData_4 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t3030399641* ___DaysToMonth365_0;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t3030399641* ___DaysToMonth366_1;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_2;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_3;

public:
	inline static int32_t get_offset_of_DaysToMonth365_0() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DaysToMonth365_0)); }
	inline Int32U5BU5D_t3030399641* get_DaysToMonth365_0() const { return ___DaysToMonth365_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_DaysToMonth365_0() { return &___DaysToMonth365_0; }
	inline void set_DaysToMonth365_0(Int32U5BU5D_t3030399641* value)
	{
		___DaysToMonth365_0 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_0), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_1() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DaysToMonth366_1)); }
	inline Int32U5BU5D_t3030399641* get_DaysToMonth366_1() const { return ___DaysToMonth366_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_DaysToMonth366_1() { return &___DaysToMonth366_1; }
	inline void set_DaysToMonth366_1(Int32U5BU5D_t3030399641* value)
	{
		___DaysToMonth366_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_1), value);
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_2)); }
	inline DateTime_t693205669  get_MinValue_2() const { return ___MinValue_2; }
	inline DateTime_t693205669 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(DateTime_t693205669  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of_MaxValue_3() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_3)); }
	inline DateTime_t693205669  get_MaxValue_3() const { return ___MaxValue_3; }
	inline DateTime_t693205669 * get_address_of_MaxValue_3() { return &___MaxValue_3; }
	inline void set_MaxValue_3(DateTime_t693205669  value)
	{
		___MaxValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef NULLABLE_1_T2088641033_H
#define NULLABLE_1_T2088641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t2088641033 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2088641033_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T2544559955_H
#define __STATICARRAYINITTYPESIZEU3D32_T2544559955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t2544559955 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t2544559955__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T2544559955_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T978476019_H
#define __STATICARRAYINITTYPESIZEU3D28_T978476019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_t978476019 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t978476019__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T978476019_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T978476008_H
#define __STATICARRAYINITTYPESIZEU3D24_T978476008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t978476008 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t978476008__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T978476008_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_T2141275422_H
#define __STATICARRAYINITTYPESIZEU3D44_T2141275422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44
struct  __StaticArrayInitTypeSizeU3D44_t2141275422 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t2141275422__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_T2141275422_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_T2141275426_H
#define __STATICARRAYINITTYPESIZEU3D40_T2141275426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct  __StaticArrayInitTypeSizeU3D40_t2141275426 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_t2141275426__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_T2141275426_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T2544559951_H
#define __STATICARRAYINITTYPESIZEU3D36_T2544559951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36
struct  __StaticArrayInitTypeSizeU3D36_t2544559951 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t2544559951__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T2544559951_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T978476012_H
#define __STATICARRAYINITTYPESIZEU3D20_T978476012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t978476012 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t978476012__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T978476012_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef RSAPARAMETERS_T1462703416_H
#define RSAPARAMETERS_T1462703416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAParameters
struct  RSAParameters_t1462703416 
{
public:
	// System.Byte[] System.Security.Cryptography.RSAParameters::Exponent
	ByteU5BU5D_t3397334013* ___Exponent_0;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Modulus
	ByteU5BU5D_t3397334013* ___Modulus_1;
	// System.Byte[] System.Security.Cryptography.RSAParameters::P
	ByteU5BU5D_t3397334013* ___P_2;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Q
	ByteU5BU5D_t3397334013* ___Q_3;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DP
	ByteU5BU5D_t3397334013* ___DP_4;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DQ
	ByteU5BU5D_t3397334013* ___DQ_5;
	// System.Byte[] System.Security.Cryptography.RSAParameters::InverseQ
	ByteU5BU5D_t3397334013* ___InverseQ_6;
	// System.Byte[] System.Security.Cryptography.RSAParameters::D
	ByteU5BU5D_t3397334013* ___D_7;

public:
	inline static int32_t get_offset_of_Exponent_0() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Exponent_0)); }
	inline ByteU5BU5D_t3397334013* get_Exponent_0() const { return ___Exponent_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_Exponent_0() { return &___Exponent_0; }
	inline void set_Exponent_0(ByteU5BU5D_t3397334013* value)
	{
		___Exponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___Exponent_0), value);
	}

	inline static int32_t get_offset_of_Modulus_1() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Modulus_1)); }
	inline ByteU5BU5D_t3397334013* get_Modulus_1() const { return ___Modulus_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_Modulus_1() { return &___Modulus_1; }
	inline void set_Modulus_1(ByteU5BU5D_t3397334013* value)
	{
		___Modulus_1 = value;
		Il2CppCodeGenWriteBarrier((&___Modulus_1), value);
	}

	inline static int32_t get_offset_of_P_2() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___P_2)); }
	inline ByteU5BU5D_t3397334013* get_P_2() const { return ___P_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_P_2() { return &___P_2; }
	inline void set_P_2(ByteU5BU5D_t3397334013* value)
	{
		___P_2 = value;
		Il2CppCodeGenWriteBarrier((&___P_2), value);
	}

	inline static int32_t get_offset_of_Q_3() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Q_3)); }
	inline ByteU5BU5D_t3397334013* get_Q_3() const { return ___Q_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_Q_3() { return &___Q_3; }
	inline void set_Q_3(ByteU5BU5D_t3397334013* value)
	{
		___Q_3 = value;
		Il2CppCodeGenWriteBarrier((&___Q_3), value);
	}

	inline static int32_t get_offset_of_DP_4() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___DP_4)); }
	inline ByteU5BU5D_t3397334013* get_DP_4() const { return ___DP_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_DP_4() { return &___DP_4; }
	inline void set_DP_4(ByteU5BU5D_t3397334013* value)
	{
		___DP_4 = value;
		Il2CppCodeGenWriteBarrier((&___DP_4), value);
	}

	inline static int32_t get_offset_of_DQ_5() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___DQ_5)); }
	inline ByteU5BU5D_t3397334013* get_DQ_5() const { return ___DQ_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_DQ_5() { return &___DQ_5; }
	inline void set_DQ_5(ByteU5BU5D_t3397334013* value)
	{
		___DQ_5 = value;
		Il2CppCodeGenWriteBarrier((&___DQ_5), value);
	}

	inline static int32_t get_offset_of_InverseQ_6() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___InverseQ_6)); }
	inline ByteU5BU5D_t3397334013* get_InverseQ_6() const { return ___InverseQ_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_InverseQ_6() { return &___InverseQ_6; }
	inline void set_InverseQ_6(ByteU5BU5D_t3397334013* value)
	{
		___InverseQ_6 = value;
		Il2CppCodeGenWriteBarrier((&___InverseQ_6), value);
	}

	inline static int32_t get_offset_of_D_7() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___D_7)); }
	inline ByteU5BU5D_t3397334013* get_D_7() const { return ___D_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_D_7() { return &___D_7; }
	inline void set_D_7(ByteU5BU5D_t3397334013* value)
	{
		___D_7 = value;
		Il2CppCodeGenWriteBarrier((&___D_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t1462703416_marshaled_pinvoke
{
	uint8_t* ___Exponent_0;
	uint8_t* ___Modulus_1;
	uint8_t* ___P_2;
	uint8_t* ___Q_3;
	uint8_t* ___DP_4;
	uint8_t* ___DQ_5;
	uint8_t* ___InverseQ_6;
	uint8_t* ___D_7;
};
// Native definition for COM marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t1462703416_marshaled_com
{
	uint8_t* ___Exponent_0;
	uint8_t* ___Modulus_1;
	uint8_t* ___P_2;
	uint8_t* ___Q_3;
	uint8_t* ___DP_4;
	uint8_t* ___DQ_5;
	uint8_t* ___InverseQ_6;
	uint8_t* ___D_7;
};
#endif // RSAPARAMETERS_T1462703416_H
#ifndef XSDDURATION_T2624036407_H
#define XSDDURATION_T2624036407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration
struct  XsdDuration_t2624036407 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration::years
	int32_t ___years_0;
	// System.Int32 System.Xml.Schema.XsdDuration::months
	int32_t ___months_1;
	// System.Int32 System.Xml.Schema.XsdDuration::days
	int32_t ___days_2;
	// System.Int32 System.Xml.Schema.XsdDuration::hours
	int32_t ___hours_3;
	// System.Int32 System.Xml.Schema.XsdDuration::minutes
	int32_t ___minutes_4;
	// System.Int32 System.Xml.Schema.XsdDuration::seconds
	int32_t ___seconds_5;
	// System.UInt32 System.Xml.Schema.XsdDuration::nanoseconds
	uint32_t ___nanoseconds_6;

public:
	inline static int32_t get_offset_of_years_0() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___years_0)); }
	inline int32_t get_years_0() const { return ___years_0; }
	inline int32_t* get_address_of_years_0() { return &___years_0; }
	inline void set_years_0(int32_t value)
	{
		___years_0 = value;
	}

	inline static int32_t get_offset_of_months_1() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___months_1)); }
	inline int32_t get_months_1() const { return ___months_1; }
	inline int32_t* get_address_of_months_1() { return &___months_1; }
	inline void set_months_1(int32_t value)
	{
		___months_1 = value;
	}

	inline static int32_t get_offset_of_days_2() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___days_2)); }
	inline int32_t get_days_2() const { return ___days_2; }
	inline int32_t* get_address_of_days_2() { return &___days_2; }
	inline void set_days_2(int32_t value)
	{
		___days_2 = value;
	}

	inline static int32_t get_offset_of_hours_3() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___hours_3)); }
	inline int32_t get_hours_3() const { return ___hours_3; }
	inline int32_t* get_address_of_hours_3() { return &___hours_3; }
	inline void set_hours_3(int32_t value)
	{
		___hours_3 = value;
	}

	inline static int32_t get_offset_of_minutes_4() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___minutes_4)); }
	inline int32_t get_minutes_4() const { return ___minutes_4; }
	inline int32_t* get_address_of_minutes_4() { return &___minutes_4; }
	inline void set_minutes_4(int32_t value)
	{
		___minutes_4 = value;
	}

	inline static int32_t get_offset_of_seconds_5() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___seconds_5)); }
	inline int32_t get_seconds_5() const { return ___seconds_5; }
	inline int32_t* get_address_of_seconds_5() { return &___seconds_5; }
	inline void set_seconds_5(int32_t value)
	{
		___seconds_5 = value;
	}

	inline static int32_t get_offset_of_nanoseconds_6() { return static_cast<int32_t>(offsetof(XsdDuration_t2624036407, ___nanoseconds_6)); }
	inline uint32_t get_nanoseconds_6() const { return ___nanoseconds_6; }
	inline uint32_t* get_address_of_nanoseconds_6() { return &___nanoseconds_6; }
	inline void set_nanoseconds_6(uint32_t value)
	{
		___nanoseconds_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_T2624036407_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_T1381760537_H
#define __STATICARRAYINITTYPESIZEU3D16_T1381760537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_t1381760537 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t1381760537__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_T1381760537_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T1381760541_H
#define __STATICARRAYINITTYPESIZEU3D12_T1381760541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t1381760541 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t1381760541__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T1381760541_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T2497549768_H
#define __STATICARRAYINITTYPESIZEU3D6_T2497549768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_t2497549768 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t2497549768__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T2497549768_H
#ifndef __STATICARRAYINITTYPESIZEU3D112_T2631791551_H
#define __STATICARRAYINITTYPESIZEU3D112_T2631791551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112
struct  __StaticArrayInitTypeSizeU3D112_t2631791551 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D112_t2631791551__padding[112];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D112_T2631791551_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public MarshalByRefObject_t1285298191
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t2745753060 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t461808439 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t2745753060 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t2745753060 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t2745753060 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t461808439 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t461808439 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t461808439 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_1)); }
	inline Stream_t3255436806 * get_Null_1() const { return ___Null_1; }
	inline Stream_t3255436806 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t3255436806 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef XMLERROR_T1680455686_H
#define XMLERROR_T1680455686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser/XMLError
struct  XMLError_t1680455686  : public Exception_t1927440687
{
public:
	// System.String Mono.Xml.MiniParser/XMLError::descr
	String_t* ___descr_16;
	// System.Int32 Mono.Xml.MiniParser/XMLError::line
	int32_t ___line_17;
	// System.Int32 Mono.Xml.MiniParser/XMLError::column
	int32_t ___column_18;

public:
	inline static int32_t get_offset_of_descr_16() { return static_cast<int32_t>(offsetof(XMLError_t1680455686, ___descr_16)); }
	inline String_t* get_descr_16() const { return ___descr_16; }
	inline String_t** get_address_of_descr_16() { return &___descr_16; }
	inline void set_descr_16(String_t* value)
	{
		___descr_16 = value;
		Il2CppCodeGenWriteBarrier((&___descr_16), value);
	}

	inline static int32_t get_offset_of_line_17() { return static_cast<int32_t>(offsetof(XMLError_t1680455686, ___line_17)); }
	inline int32_t get_line_17() const { return ___line_17; }
	inline int32_t* get_address_of_line_17() { return &___line_17; }
	inline void set_line_17(int32_t value)
	{
		___line_17 = value;
	}

	inline static int32_t get_offset_of_column_18() { return static_cast<int32_t>(offsetof(XMLError_t1680455686, ___column_18)); }
	inline int32_t get_column_18() const { return ___column_18; }
	inline int32_t* get_address_of_column_18() { return &___column_18; }
	inline void set_column_18(int32_t value)
	{
		___column_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLERROR_T1680455686_H
#ifndef __STATICARRAYINITTYPESIZEU3D416_T1045504634_H
#define __STATICARRAYINITTYPESIZEU3D416_T1045504634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=416
struct  __StaticArrayInitTypeSizeU3D416_t1045504634 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D416_t1045504634__padding[416];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D416_T1045504634_H
#ifndef SECURITYPARSER_T30730986_H
#define SECURITYPARSER_T30730986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.SecurityParser
struct  SecurityParser_t30730986  : public MiniParser_t185565106
{
public:
	// System.Security.SecurityElement Mono.Xml.SecurityParser::root
	SecurityElement_t2325568386 * ___root_7;
	// System.String Mono.Xml.SecurityParser::xmldoc
	String_t* ___xmldoc_8;
	// System.Int32 Mono.Xml.SecurityParser::pos
	int32_t ___pos_9;
	// System.Security.SecurityElement Mono.Xml.SecurityParser::current
	SecurityElement_t2325568386 * ___current_10;
	// System.Collections.Stack Mono.Xml.SecurityParser::stack
	Stack_t1043988394 * ___stack_11;

public:
	inline static int32_t get_offset_of_root_7() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___root_7)); }
	inline SecurityElement_t2325568386 * get_root_7() const { return ___root_7; }
	inline SecurityElement_t2325568386 ** get_address_of_root_7() { return &___root_7; }
	inline void set_root_7(SecurityElement_t2325568386 * value)
	{
		___root_7 = value;
		Il2CppCodeGenWriteBarrier((&___root_7), value);
	}

	inline static int32_t get_offset_of_xmldoc_8() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___xmldoc_8)); }
	inline String_t* get_xmldoc_8() const { return ___xmldoc_8; }
	inline String_t** get_address_of_xmldoc_8() { return &___xmldoc_8; }
	inline void set_xmldoc_8(String_t* value)
	{
		___xmldoc_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmldoc_8), value);
	}

	inline static int32_t get_offset_of_pos_9() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___pos_9)); }
	inline int32_t get_pos_9() const { return ___pos_9; }
	inline int32_t* get_address_of_pos_9() { return &___pos_9; }
	inline void set_pos_9(int32_t value)
	{
		___pos_9 = value;
	}

	inline static int32_t get_offset_of_current_10() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___current_10)); }
	inline SecurityElement_t2325568386 * get_current_10() const { return ___current_10; }
	inline SecurityElement_t2325568386 ** get_address_of_current_10() { return &___current_10; }
	inline void set_current_10(SecurityElement_t2325568386 * value)
	{
		___current_10 = value;
		Il2CppCodeGenWriteBarrier((&___current_10), value);
	}

	inline static int32_t get_offset_of_stack_11() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___stack_11)); }
	inline Stack_t1043988394 * get_stack_11() const { return ___stack_11; }
	inline Stack_t1043988394 ** get_address_of_stack_11() { return &___stack_11; }
	inline void set_stack_11(Stack_t1043988394 * value)
	{
		___stack_11 = value;
		Il2CppCodeGenWriteBarrier((&___stack_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPARSER_T30730986_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef __STATICARRAYINITTYPESIZEU3D56_T3707359365_H
#define __STATICARRAYINITTYPESIZEU3D56_T3707359365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56
struct  __StaticArrayInitTypeSizeU3D56_t3707359365 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D56_t3707359365__padding[56];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D56_T3707359365_H
#ifndef __STATICARRAYINITTYPESIZEU3D144_T3794590962_H
#define __STATICARRAYINITTYPESIZEU3D144_T3794590962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144
struct  __StaticArrayInitTypeSizeU3D144_t3794590962 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D144_t3794590962__padding[144];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D144_T3794590962_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_T3304074836_H
#define __STATICARRAYINITTYPESIZEU3D64_T3304074836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64
struct  __StaticArrayInitTypeSizeU3D64_t3304074836 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t3304074836__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_T3304074836_H
#ifndef __STATICARRAYINITTYPESIZEU3D68_T3304074847_H
#define __STATICARRAYINITTYPESIZEU3D68_T3304074847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68
struct  __StaticArrayInitTypeSizeU3D68_t3304074847 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D68_t3304074847__padding[68];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D68_T3304074847_H
#ifndef SSLSTREAMBASE_T934199321_H
#define SSLSTREAMBASE_T934199321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslStreamBase
struct  SslStreamBase_t934199321  : public Stream_t3255436806
{
public:
	// System.IO.Stream Mono.Security.Protocol.Tls.SslStreamBase::innerStream
	Stream_t3255436806 * ___innerStream_5;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.SslStreamBase::inputBuffer
	MemoryStream_t743994179 * ___inputBuffer_6;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.SslStreamBase::context
	Context_t4285182719 * ___context_7;
	// Mono.Security.Protocol.Tls.RecordProtocol Mono.Security.Protocol.Tls.SslStreamBase::protocol
	RecordProtocol_t3166895267 * ___protocol_8;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase::ownsStream
	bool ___ownsStream_9;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Mono.Security.Protocol.Tls.SslStreamBase::disposed
	bool ___disposed_10;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_11;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::negotiate
	RuntimeObject * ___negotiate_12;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::read
	RuntimeObject * ___read_13;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::write
	RuntimeObject * ___write_14;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase::negotiationComplete
	ManualResetEvent_t926074657 * ___negotiationComplete_15;
	// System.Byte[] Mono.Security.Protocol.Tls.SslStreamBase::recbuf
	ByteU5BU5D_t3397334013* ___recbuf_16;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.SslStreamBase::recordStream
	MemoryStream_t743994179 * ___recordStream_17;

public:
	inline static int32_t get_offset_of_innerStream_5() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___innerStream_5)); }
	inline Stream_t3255436806 * get_innerStream_5() const { return ___innerStream_5; }
	inline Stream_t3255436806 ** get_address_of_innerStream_5() { return &___innerStream_5; }
	inline void set_innerStream_5(Stream_t3255436806 * value)
	{
		___innerStream_5 = value;
		Il2CppCodeGenWriteBarrier((&___innerStream_5), value);
	}

	inline static int32_t get_offset_of_inputBuffer_6() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___inputBuffer_6)); }
	inline MemoryStream_t743994179 * get_inputBuffer_6() const { return ___inputBuffer_6; }
	inline MemoryStream_t743994179 ** get_address_of_inputBuffer_6() { return &___inputBuffer_6; }
	inline void set_inputBuffer_6(MemoryStream_t743994179 * value)
	{
		___inputBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputBuffer_6), value);
	}

	inline static int32_t get_offset_of_context_7() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___context_7)); }
	inline Context_t4285182719 * get_context_7() const { return ___context_7; }
	inline Context_t4285182719 ** get_address_of_context_7() { return &___context_7; }
	inline void set_context_7(Context_t4285182719 * value)
	{
		___context_7 = value;
		Il2CppCodeGenWriteBarrier((&___context_7), value);
	}

	inline static int32_t get_offset_of_protocol_8() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___protocol_8)); }
	inline RecordProtocol_t3166895267 * get_protocol_8() const { return ___protocol_8; }
	inline RecordProtocol_t3166895267 ** get_address_of_protocol_8() { return &___protocol_8; }
	inline void set_protocol_8(RecordProtocol_t3166895267 * value)
	{
		___protocol_8 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_8), value);
	}

	inline static int32_t get_offset_of_ownsStream_9() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___ownsStream_9)); }
	inline bool get_ownsStream_9() const { return ___ownsStream_9; }
	inline bool* get_address_of_ownsStream_9() { return &___ownsStream_9; }
	inline void set_ownsStream_9(bool value)
	{
		___ownsStream_9 = value;
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_11() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___checkCertRevocationStatus_11)); }
	inline bool get_checkCertRevocationStatus_11() const { return ___checkCertRevocationStatus_11; }
	inline bool* get_address_of_checkCertRevocationStatus_11() { return &___checkCertRevocationStatus_11; }
	inline void set_checkCertRevocationStatus_11(bool value)
	{
		___checkCertRevocationStatus_11 = value;
	}

	inline static int32_t get_offset_of_negotiate_12() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___negotiate_12)); }
	inline RuntimeObject * get_negotiate_12() const { return ___negotiate_12; }
	inline RuntimeObject ** get_address_of_negotiate_12() { return &___negotiate_12; }
	inline void set_negotiate_12(RuntimeObject * value)
	{
		___negotiate_12 = value;
		Il2CppCodeGenWriteBarrier((&___negotiate_12), value);
	}

	inline static int32_t get_offset_of_read_13() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___read_13)); }
	inline RuntimeObject * get_read_13() const { return ___read_13; }
	inline RuntimeObject ** get_address_of_read_13() { return &___read_13; }
	inline void set_read_13(RuntimeObject * value)
	{
		___read_13 = value;
		Il2CppCodeGenWriteBarrier((&___read_13), value);
	}

	inline static int32_t get_offset_of_write_14() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___write_14)); }
	inline RuntimeObject * get_write_14() const { return ___write_14; }
	inline RuntimeObject ** get_address_of_write_14() { return &___write_14; }
	inline void set_write_14(RuntimeObject * value)
	{
		___write_14 = value;
		Il2CppCodeGenWriteBarrier((&___write_14), value);
	}

	inline static int32_t get_offset_of_negotiationComplete_15() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___negotiationComplete_15)); }
	inline ManualResetEvent_t926074657 * get_negotiationComplete_15() const { return ___negotiationComplete_15; }
	inline ManualResetEvent_t926074657 ** get_address_of_negotiationComplete_15() { return &___negotiationComplete_15; }
	inline void set_negotiationComplete_15(ManualResetEvent_t926074657 * value)
	{
		___negotiationComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___negotiationComplete_15), value);
	}

	inline static int32_t get_offset_of_recbuf_16() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___recbuf_16)); }
	inline ByteU5BU5D_t3397334013* get_recbuf_16() const { return ___recbuf_16; }
	inline ByteU5BU5D_t3397334013** get_address_of_recbuf_16() { return &___recbuf_16; }
	inline void set_recbuf_16(ByteU5BU5D_t3397334013* value)
	{
		___recbuf_16 = value;
		Il2CppCodeGenWriteBarrier((&___recbuf_16), value);
	}

	inline static int32_t get_offset_of_recordStream_17() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___recordStream_17)); }
	inline MemoryStream_t743994179 * get_recordStream_17() const { return ___recordStream_17; }
	inline MemoryStream_t743994179 ** get_address_of_recordStream_17() { return &___recordStream_17; }
	inline void set_recordStream_17(MemoryStream_t743994179 * value)
	{
		___recordStream_17 = value;
		Il2CppCodeGenWriteBarrier((&___recordStream_17), value);
	}
};

struct SslStreamBase_t934199321_StaticFields
{
public:
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase::record_processing
	ManualResetEvent_t926074657 * ___record_processing_4;

public:
	inline static int32_t get_offset_of_record_processing_4() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321_StaticFields, ___record_processing_4)); }
	inline ManualResetEvent_t926074657 * get_record_processing_4() const { return ___record_processing_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_record_processing_4() { return &___record_processing_4; }
	inline void set_record_processing_4(ManualResetEvent_t926074657 * value)
	{
		___record_processing_4 = value;
		Il2CppCodeGenWriteBarrier((&___record_processing_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSTREAMBASE_T934199321_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef XSDDATETIMEKIND_T2077819616_H
#define XSDDATETIMEKIND_T2077819616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/XsdDateTimeKind
struct  XsdDateTimeKind_t2077819616 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTime/XsdDateTimeKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XsdDateTimeKind_t2077819616, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIMEKIND_T2077819616_H
#ifndef NEGOTIATESTATE_T62894417_H
#define NEGOTIATESTATE_T62894417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream/NegotiateState
struct  NegotiateState_t62894417 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SslClientStream/NegotiateState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NegotiateState_t62894417, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGOTIATESTATE_T62894417_H
#ifndef CLIENTCERTIFICATETYPE_T4001384466_H
#define CLIENTCERTIFICATETYPE_T4001384466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
struct  ClientCertificateType_t4001384466 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.Handshake.ClientCertificateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientCertificateType_t4001384466, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCERTIFICATETYPE_T4001384466_H
#ifndef HANDSHAKETYPE_T2540099417_H
#define HANDSHAKETYPE_T2540099417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeType
struct  HandshakeType_t2540099417 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.Handshake.HandshakeType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandshakeType_t2540099417, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKETYPE_T2540099417_H
#ifndef XMLSCHEMACONTENTPROCESSING_T74226324_H
#define XMLSCHEMACONTENTPROCESSING_T74226324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t74226324 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t74226324, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T74226324_H
#ifndef TLSSTREAM_T4089752859_H
#define TLSSTREAM_T4089752859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsStream
struct  TlsStream_t4089752859  : public Stream_t3255436806
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canRead
	bool ___canRead_4;
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canWrite
	bool ___canWrite_5;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.TlsStream::buffer
	MemoryStream_t743994179 * ___buffer_6;
	// System.Byte[] Mono.Security.Protocol.Tls.TlsStream::temp
	ByteU5BU5D_t3397334013* ___temp_7;

public:
	inline static int32_t get_offset_of_canRead_4() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___canRead_4)); }
	inline bool get_canRead_4() const { return ___canRead_4; }
	inline bool* get_address_of_canRead_4() { return &___canRead_4; }
	inline void set_canRead_4(bool value)
	{
		___canRead_4 = value;
	}

	inline static int32_t get_offset_of_canWrite_5() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___canWrite_5)); }
	inline bool get_canWrite_5() const { return ___canWrite_5; }
	inline bool* get_address_of_canWrite_5() { return &___canWrite_5; }
	inline void set_canWrite_5(bool value)
	{
		___canWrite_5 = value;
	}

	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___buffer_6)); }
	inline MemoryStream_t743994179 * get_buffer_6() const { return ___buffer_6; }
	inline MemoryStream_t743994179 ** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(MemoryStream_t743994179 * value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_temp_7() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___temp_7)); }
	inline ByteU5BU5D_t3397334013* get_temp_7() const { return ___temp_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_temp_7() { return &___temp_7; }
	inline void set_temp_7(ByteU5BU5D_t3397334013* value)
	{
		___temp_7 = value;
		Il2CppCodeGenWriteBarrier((&___temp_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAM_T4089752859_H
#ifndef CIPHERMODE_T162592484_H
#define CIPHERMODE_T162592484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t162592484 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t162592484, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T162592484_H
#ifndef DATETIMETYPECODE_T563167880_H
#define DATETIMETYPECODE_T563167880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/DateTimeTypeCode
struct  DateTimeTypeCode_t563167880 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTime/DateTimeTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeTypeCode_t563167880, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMETYPECODE_T563167880_H
#ifndef TLSSERVERSETTINGS_T403340211_H
#define TLSSERVERSETTINGS_T403340211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsServerSettings
struct  TlsServerSettings_t403340211  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.Protocol.Tls.TlsServerSettings::certificates
	X509CertificateCollection_t3592472866 * ___certificates_0;
	// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.TlsServerSettings::certificateRSA
	RSA_t3719518354 * ___certificateRSA_1;
	// System.Security.Cryptography.RSAParameters Mono.Security.Protocol.Tls.TlsServerSettings::rsaParameters
	RSAParameters_t1462703416  ___rsaParameters_2;
	// System.String[] Mono.Security.Protocol.Tls.TlsServerSettings::distinguisedNames
	StringU5BU5D_t1642385972* ___distinguisedNames_3;
	// System.Boolean Mono.Security.Protocol.Tls.TlsServerSettings::serverKeyExchange
	bool ___serverKeyExchange_4;
	// System.Boolean Mono.Security.Protocol.Tls.TlsServerSettings::certificateRequest
	bool ___certificateRequest_5;
	// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[] Mono.Security.Protocol.Tls.TlsServerSettings::certificateTypes
	ClientCertificateTypeU5BU5D_t2397899623* ___certificateTypes_6;

public:
	inline static int32_t get_offset_of_certificates_0() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___certificates_0)); }
	inline X509CertificateCollection_t3592472866 * get_certificates_0() const { return ___certificates_0; }
	inline X509CertificateCollection_t3592472866 ** get_address_of_certificates_0() { return &___certificates_0; }
	inline void set_certificates_0(X509CertificateCollection_t3592472866 * value)
	{
		___certificates_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_0), value);
	}

	inline static int32_t get_offset_of_certificateRSA_1() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___certificateRSA_1)); }
	inline RSA_t3719518354 * get_certificateRSA_1() const { return ___certificateRSA_1; }
	inline RSA_t3719518354 ** get_address_of_certificateRSA_1() { return &___certificateRSA_1; }
	inline void set_certificateRSA_1(RSA_t3719518354 * value)
	{
		___certificateRSA_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificateRSA_1), value);
	}

	inline static int32_t get_offset_of_rsaParameters_2() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___rsaParameters_2)); }
	inline RSAParameters_t1462703416  get_rsaParameters_2() const { return ___rsaParameters_2; }
	inline RSAParameters_t1462703416 * get_address_of_rsaParameters_2() { return &___rsaParameters_2; }
	inline void set_rsaParameters_2(RSAParameters_t1462703416  value)
	{
		___rsaParameters_2 = value;
	}

	inline static int32_t get_offset_of_distinguisedNames_3() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___distinguisedNames_3)); }
	inline StringU5BU5D_t1642385972* get_distinguisedNames_3() const { return ___distinguisedNames_3; }
	inline StringU5BU5D_t1642385972** get_address_of_distinguisedNames_3() { return &___distinguisedNames_3; }
	inline void set_distinguisedNames_3(StringU5BU5D_t1642385972* value)
	{
		___distinguisedNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___distinguisedNames_3), value);
	}

	inline static int32_t get_offset_of_serverKeyExchange_4() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___serverKeyExchange_4)); }
	inline bool get_serverKeyExchange_4() const { return ___serverKeyExchange_4; }
	inline bool* get_address_of_serverKeyExchange_4() { return &___serverKeyExchange_4; }
	inline void set_serverKeyExchange_4(bool value)
	{
		___serverKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_certificateRequest_5() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___certificateRequest_5)); }
	inline bool get_certificateRequest_5() const { return ___certificateRequest_5; }
	inline bool* get_address_of_certificateRequest_5() { return &___certificateRequest_5; }
	inline void set_certificateRequest_5(bool value)
	{
		___certificateRequest_5 = value;
	}

	inline static int32_t get_offset_of_certificateTypes_6() { return static_cast<int32_t>(offsetof(TlsServerSettings_t403340211, ___certificateTypes_6)); }
	inline ClientCertificateTypeU5BU5D_t2397899623* get_certificateTypes_6() const { return ___certificateTypes_6; }
	inline ClientCertificateTypeU5BU5D_t2397899623** get_address_of_certificateTypes_6() { return &___certificateTypes_6; }
	inline void set_certificateTypes_6(ClientCertificateTypeU5BU5D_t2397899623* value)
	{
		___certificateTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___certificateTypes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERSETTINGS_T403340211_H
#ifndef HANDSHAKESTATE_T1820731088_H
#define HANDSHAKESTATE_T1820731088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HandshakeState
struct  HandshakeState_t1820731088 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.HandshakeState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandshakeState_t1820731088, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKESTATE_T1820731088_H
#ifndef EXCHANGEALGORITHMTYPE_T954949548_H
#define EXCHANGEALGORITHMTYPE_T954949548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
struct  ExchangeAlgorithmType_t954949548 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.ExchangeAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExchangeAlgorithmType_t954949548, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCHANGEALGORITHMTYPE_T954949548_H
#ifndef SECURITYCOMPRESSIONTYPE_T3722381418_H
#define SECURITYCOMPRESSIONTYPE_T3722381418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityCompressionType
struct  SecurityCompressionType_t3722381418 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SecurityCompressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityCompressionType_t3722381418, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYCOMPRESSIONTYPE_T3722381418_H
#ifndef HASHALGORITHMTYPE_T1654661965_H
#define HASHALGORITHMTYPE_T1654661965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HashAlgorithmType
struct  HashAlgorithmType_t1654661965 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.HashAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HashAlgorithmType_t1654661965, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHMTYPE_T1654661965_H
#ifndef ALERTLEVEL_T1706602846_H
#define ALERTLEVEL_T1706602846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.AlertLevel
struct  AlertLevel_t1706602846 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.AlertLevel::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlertLevel_t1706602846, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTLEVEL_T1706602846_H
#ifndef CLIENTSESSIONINFO_T3468069089_H
#define CLIENTSESSIONINFO_T3468069089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ClientSessionInfo
struct  ClientSessionInfo_t3468069089  : public RuntimeObject
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.ClientSessionInfo::disposed
	bool ___disposed_1;
	// System.DateTime Mono.Security.Protocol.Tls.ClientSessionInfo::validuntil
	DateTime_t693205669  ___validuntil_2;
	// System.String Mono.Security.Protocol.Tls.ClientSessionInfo::host
	String_t* ___host_3;
	// System.Byte[] Mono.Security.Protocol.Tls.ClientSessionInfo::sid
	ByteU5BU5D_t3397334013* ___sid_4;
	// System.Byte[] Mono.Security.Protocol.Tls.ClientSessionInfo::masterSecret
	ByteU5BU5D_t3397334013* ___masterSecret_5;

public:
	inline static int32_t get_offset_of_disposed_1() { return static_cast<int32_t>(offsetof(ClientSessionInfo_t3468069089, ___disposed_1)); }
	inline bool get_disposed_1() const { return ___disposed_1; }
	inline bool* get_address_of_disposed_1() { return &___disposed_1; }
	inline void set_disposed_1(bool value)
	{
		___disposed_1 = value;
	}

	inline static int32_t get_offset_of_validuntil_2() { return static_cast<int32_t>(offsetof(ClientSessionInfo_t3468069089, ___validuntil_2)); }
	inline DateTime_t693205669  get_validuntil_2() const { return ___validuntil_2; }
	inline DateTime_t693205669 * get_address_of_validuntil_2() { return &___validuntil_2; }
	inline void set_validuntil_2(DateTime_t693205669  value)
	{
		___validuntil_2 = value;
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(ClientSessionInfo_t3468069089, ___host_3)); }
	inline String_t* get_host_3() const { return ___host_3; }
	inline String_t** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(String_t* value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier((&___host_3), value);
	}

	inline static int32_t get_offset_of_sid_4() { return static_cast<int32_t>(offsetof(ClientSessionInfo_t3468069089, ___sid_4)); }
	inline ByteU5BU5D_t3397334013* get_sid_4() const { return ___sid_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_sid_4() { return &___sid_4; }
	inline void set_sid_4(ByteU5BU5D_t3397334013* value)
	{
		___sid_4 = value;
		Il2CppCodeGenWriteBarrier((&___sid_4), value);
	}

	inline static int32_t get_offset_of_masterSecret_5() { return static_cast<int32_t>(offsetof(ClientSessionInfo_t3468069089, ___masterSecret_5)); }
	inline ByteU5BU5D_t3397334013* get_masterSecret_5() const { return ___masterSecret_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_masterSecret_5() { return &___masterSecret_5; }
	inline void set_masterSecret_5(ByteU5BU5D_t3397334013* value)
	{
		___masterSecret_5 = value;
		Il2CppCodeGenWriteBarrier((&___masterSecret_5), value);
	}
};

struct ClientSessionInfo_t3468069089_StaticFields
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.ClientSessionInfo::ValidityInterval
	int32_t ___ValidityInterval_0;

public:
	inline static int32_t get_offset_of_ValidityInterval_0() { return static_cast<int32_t>(offsetof(ClientSessionInfo_t3468069089_StaticFields, ___ValidityInterval_0)); }
	inline int32_t get_ValidityInterval_0() const { return ___ValidityInterval_0; }
	inline int32_t* get_address_of_ValidityInterval_0() { return &___ValidityInterval_0; }
	inline void set_ValidityInterval_0(int32_t value)
	{
		___ValidityInterval_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSESSIONINFO_T3468069089_H
#ifndef CIPHERALGORITHMTYPE_T4212518094_H
#define CIPHERALGORITHMTYPE_T4212518094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherAlgorithmType
struct  CipherAlgorithmType_t4212518094 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.CipherAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherAlgorithmType_t4212518094, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERALGORITHMTYPE_T4212518094_H
#ifndef ALERTDESCRIPTION_T844791462_H
#define ALERTDESCRIPTION_T844791462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.AlertDescription
struct  AlertDescription_t844791462 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.AlertDescription::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlertDescription_t844791462, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDESCRIPTION_T844791462_H
#ifndef CONTENTTYPE_T859870085_H
#define CONTENTTYPE_T859870085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ContentType
struct  ContentType_t859870085 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.ContentType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t859870085, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T859870085_H
#ifndef SECURITYPROTOCOLTYPE_T155967584_H
#define SECURITYPROTOCOLTYPE_T155967584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SecurityProtocolType
struct  SecurityProtocolType_t155967584 
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.SecurityProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t155967584, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T155967584_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305138_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305138  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::0701435C4E2C38EFE43C51BD22C114AB8B80124D
	__StaticArrayInitTypeSizeU3D12_t1381760541  ___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=68 <PrivateImplementationDetails>::0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1
	__StaticArrayInitTypeSizeU3D68_t3304074847  ___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::161F91CE1721D8F16622810CBB39887D21C47031
	__StaticArrayInitTypeSizeU3D12_t1381760541  ___161F91CE1721D8F16622810CBB39887D21C47031_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=44 <PrivateImplementationDetails>::221CE291CD044114B4369175B9B91177F5932876
	__StaticArrayInitTypeSizeU3D44_t2141275422  ___221CE291CD044114B4369175B9B91177F5932876_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::360487BE4278986419B568EFD887F6145383168A
	__StaticArrayInitTypeSizeU3D40_t2141275426  ___360487BE4278986419B568EFD887F6145383168A_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1
	__StaticArrayInitTypeSizeU3D20_t978476012  ___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::485F43E332C2F7530815B17C08DAC169A8F697E0
	__StaticArrayInitTypeSizeU3D32_t2544559955  ___485F43E332C2F7530815B17C08DAC169A8F697E0_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112 <PrivateImplementationDetails>::49C5BA13401986EC93E4677F52CBE2248184DFBD
	__StaticArrayInitTypeSizeU3D112_t2631791551  ___49C5BA13401986EC93E4677F52CBE2248184DFBD_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::51E4CA1C2B009A2876C6E57D8E69E3502BCA3440
	__StaticArrayInitTypeSizeU3D24_t978476008  ___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=56 <PrivateImplementationDetails>::574B9D4E4C39F6E8004181E5765B627B75EB1AD1
	__StaticArrayInitTypeSizeU3D56_t3707359365  ___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD
	__StaticArrayInitTypeSizeU3D12_t1381760541  ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98
	__StaticArrayInitTypeSizeU3D6_t2497549768  ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=416 <PrivateImplementationDetails>::6A0D50D692745A6663128CD315B71079584F3E59
	__StaticArrayInitTypeSizeU3D416_t1045504634  ___6A0D50D692745A6663128CD315B71079584F3E59_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::702F6A3276CBE481D247A77C20B5459FB94D07D2
	__StaticArrayInitTypeSizeU3D24_t978476008  ___702F6A3276CBE481D247A77C20B5459FB94D07D2_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::7A32E1A19C182315E4263A65A72066492550D8CD
	__StaticArrayInitTypeSizeU3D32_t2544559955  ___7A32E1A19C182315E4263A65A72066492550D8CD_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::8B4E5E81A88D29642679AFCE41DCA380F9000462
	__StaticArrayInitTypeSizeU3D16_t1381760537  ___8B4E5E81A88D29642679AFCE41DCA380F9000462_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::99F0664C2AC8464B51252D92FC24F3834C6FB90C
	__StaticArrayInitTypeSizeU3D12_t1381760541  ___99F0664C2AC8464B51252D92FC24F3834C6FB90C_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t978476019  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144 <PrivateImplementationDetails>::9E374D7263B2452E25DE3D6E617F6A728D98A439
	__StaticArrayInitTypeSizeU3D144_t3794590962  ___9E374D7263B2452E25DE3D6E617F6A728D98A439_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C
	__StaticArrayInitTypeSizeU3D24_t978476008  ___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=416 <PrivateImplementationDetails>::B368804F0C6DAB083B253A6B106D0783D5C32E90
	__StaticArrayInitTypeSizeU3D416_t1045504634  ___B368804F0C6DAB083B253A6B106D0783D5C32E90_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::B9F0004E3873FDDCABFDA6174EA18F0859B637B4
	__StaticArrayInitTypeSizeU3D40_t2141275426  ___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::BAD037B714E1CD1052149B51238A3D4351DD10B5
	__StaticArrayInitTypeSizeU3D20_t978476012  ___BAD037B714E1CD1052149B51238A3D4351DD10B5_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C
	__StaticArrayInitTypeSizeU3D16_t1381760537  ___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::DCF398750721AA7A27A6BA56E99350329B06E8B1
	__StaticArrayInitTypeSizeU3D16_t1381760537  ___DCF398750721AA7A27A6BA56E99350329B06E8B1_24;
	// System.Int64 <PrivateImplementationDetails>::EBC658B067B5C785A3F0BB67D73755F6FEE7F70C
	int64_t ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=36 <PrivateImplementationDetails>::ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4
	__StaticArrayInitTypeSizeU3D36_t2544559951  ___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=64 <PrivateImplementationDetails>::EE3413A2C088FF9432054D6E60A7CB6A498D25F0
	__StaticArrayInitTypeSizeU3D64_t3304074836  ___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::FFE3F15642234E7FAD6951D432F1134D5AD15922
	__StaticArrayInitTypeSizeU3D20_t978476012  ___FFE3F15642234E7FAD6951D432F1134D5AD15922_28;

public:
	inline static int32_t get_offset_of_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541  get_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0() const { return ___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0; }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541 * get_address_of_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0() { return &___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0; }
	inline void set_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0(__StaticArrayInitTypeSizeU3D12_t1381760541  value)
	{
		___0701435C4E2C38EFE43C51BD22C114AB8B80124D_0 = value;
	}

	inline static int32_t get_offset_of_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1)); }
	inline __StaticArrayInitTypeSizeU3D68_t3304074847  get_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1() const { return ___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1; }
	inline __StaticArrayInitTypeSizeU3D68_t3304074847 * get_address_of_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1() { return &___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1; }
	inline void set_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1(__StaticArrayInitTypeSizeU3D68_t3304074847  value)
	{
		___0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1 = value;
	}

	inline static int32_t get_offset_of_U3161F91CE1721D8F16622810CBB39887D21C47031_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___161F91CE1721D8F16622810CBB39887D21C47031_2)); }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541  get_U3161F91CE1721D8F16622810CBB39887D21C47031_2() const { return ___161F91CE1721D8F16622810CBB39887D21C47031_2; }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541 * get_address_of_U3161F91CE1721D8F16622810CBB39887D21C47031_2() { return &___161F91CE1721D8F16622810CBB39887D21C47031_2; }
	inline void set_U3161F91CE1721D8F16622810CBB39887D21C47031_2(__StaticArrayInitTypeSizeU3D12_t1381760541  value)
	{
		___161F91CE1721D8F16622810CBB39887D21C47031_2 = value;
	}

	inline static int32_t get_offset_of_U3221CE291CD044114B4369175B9B91177F5932876_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___221CE291CD044114B4369175B9B91177F5932876_3)); }
	inline __StaticArrayInitTypeSizeU3D44_t2141275422  get_U3221CE291CD044114B4369175B9B91177F5932876_3() const { return ___221CE291CD044114B4369175B9B91177F5932876_3; }
	inline __StaticArrayInitTypeSizeU3D44_t2141275422 * get_address_of_U3221CE291CD044114B4369175B9B91177F5932876_3() { return &___221CE291CD044114B4369175B9B91177F5932876_3; }
	inline void set_U3221CE291CD044114B4369175B9B91177F5932876_3(__StaticArrayInitTypeSizeU3D44_t2141275422  value)
	{
		___221CE291CD044114B4369175B9B91177F5932876_3 = value;
	}

	inline static int32_t get_offset_of_U3360487BE4278986419B568EFD887F6145383168A_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___360487BE4278986419B568EFD887F6145383168A_4)); }
	inline __StaticArrayInitTypeSizeU3D40_t2141275426  get_U3360487BE4278986419B568EFD887F6145383168A_4() const { return ___360487BE4278986419B568EFD887F6145383168A_4; }
	inline __StaticArrayInitTypeSizeU3D40_t2141275426 * get_address_of_U3360487BE4278986419B568EFD887F6145383168A_4() { return &___360487BE4278986419B568EFD887F6145383168A_4; }
	inline void set_U3360487BE4278986419B568EFD887F6145383168A_4(__StaticArrayInitTypeSizeU3D40_t2141275426  value)
	{
		___360487BE4278986419B568EFD887F6145383168A_4 = value;
	}

	inline static int32_t get_offset_of_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5)); }
	inline __StaticArrayInitTypeSizeU3D20_t978476012  get_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5() const { return ___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5; }
	inline __StaticArrayInitTypeSizeU3D20_t978476012 * get_address_of_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5() { return &___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5; }
	inline void set_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5(__StaticArrayInitTypeSizeU3D20_t978476012  value)
	{
		___42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5 = value;
	}

	inline static int32_t get_offset_of_U3485F43E332C2F7530815B17C08DAC169A8F697E0_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___485F43E332C2F7530815B17C08DAC169A8F697E0_6)); }
	inline __StaticArrayInitTypeSizeU3D32_t2544559955  get_U3485F43E332C2F7530815B17C08DAC169A8F697E0_6() const { return ___485F43E332C2F7530815B17C08DAC169A8F697E0_6; }
	inline __StaticArrayInitTypeSizeU3D32_t2544559955 * get_address_of_U3485F43E332C2F7530815B17C08DAC169A8F697E0_6() { return &___485F43E332C2F7530815B17C08DAC169A8F697E0_6; }
	inline void set_U3485F43E332C2F7530815B17C08DAC169A8F697E0_6(__StaticArrayInitTypeSizeU3D32_t2544559955  value)
	{
		___485F43E332C2F7530815B17C08DAC169A8F697E0_6 = value;
	}

	inline static int32_t get_offset_of_U349C5BA13401986EC93E4677F52CBE2248184DFBD_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___49C5BA13401986EC93E4677F52CBE2248184DFBD_7)); }
	inline __StaticArrayInitTypeSizeU3D112_t2631791551  get_U349C5BA13401986EC93E4677F52CBE2248184DFBD_7() const { return ___49C5BA13401986EC93E4677F52CBE2248184DFBD_7; }
	inline __StaticArrayInitTypeSizeU3D112_t2631791551 * get_address_of_U349C5BA13401986EC93E4677F52CBE2248184DFBD_7() { return &___49C5BA13401986EC93E4677F52CBE2248184DFBD_7; }
	inline void set_U349C5BA13401986EC93E4677F52CBE2248184DFBD_7(__StaticArrayInitTypeSizeU3D112_t2631791551  value)
	{
		___49C5BA13401986EC93E4677F52CBE2248184DFBD_7 = value;
	}

	inline static int32_t get_offset_of_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8)); }
	inline __StaticArrayInitTypeSizeU3D24_t978476008  get_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8() const { return ___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8; }
	inline __StaticArrayInitTypeSizeU3D24_t978476008 * get_address_of_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8() { return &___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8; }
	inline void set_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8(__StaticArrayInitTypeSizeU3D24_t978476008  value)
	{
		___51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8 = value;
	}

	inline static int32_t get_offset_of_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9)); }
	inline __StaticArrayInitTypeSizeU3D56_t3707359365  get_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9() const { return ___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9; }
	inline __StaticArrayInitTypeSizeU3D56_t3707359365 * get_address_of_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9() { return &___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9; }
	inline void set_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9(__StaticArrayInitTypeSizeU3D56_t3707359365  value)
	{
		___574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9 = value;
	}

	inline static int32_t get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10)); }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541  get_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10() const { return ___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10; }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541 * get_address_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10() { return &___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10; }
	inline void set_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10(__StaticArrayInitTypeSizeU3D12_t1381760541  value)
	{
		___5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10 = value;
	}

	inline static int32_t get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11)); }
	inline __StaticArrayInitTypeSizeU3D6_t2497549768  get_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11() const { return ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11; }
	inline __StaticArrayInitTypeSizeU3D6_t2497549768 * get_address_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11() { return &___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11; }
	inline void set_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11(__StaticArrayInitTypeSizeU3D6_t2497549768  value)
	{
		___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11 = value;
	}

	inline static int32_t get_offset_of_U36A0D50D692745A6663128CD315B71079584F3E59_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___6A0D50D692745A6663128CD315B71079584F3E59_12)); }
	inline __StaticArrayInitTypeSizeU3D416_t1045504634  get_U36A0D50D692745A6663128CD315B71079584F3E59_12() const { return ___6A0D50D692745A6663128CD315B71079584F3E59_12; }
	inline __StaticArrayInitTypeSizeU3D416_t1045504634 * get_address_of_U36A0D50D692745A6663128CD315B71079584F3E59_12() { return &___6A0D50D692745A6663128CD315B71079584F3E59_12; }
	inline void set_U36A0D50D692745A6663128CD315B71079584F3E59_12(__StaticArrayInitTypeSizeU3D416_t1045504634  value)
	{
		___6A0D50D692745A6663128CD315B71079584F3E59_12 = value;
	}

	inline static int32_t get_offset_of_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___702F6A3276CBE481D247A77C20B5459FB94D07D2_13)); }
	inline __StaticArrayInitTypeSizeU3D24_t978476008  get_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_13() const { return ___702F6A3276CBE481D247A77C20B5459FB94D07D2_13; }
	inline __StaticArrayInitTypeSizeU3D24_t978476008 * get_address_of_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_13() { return &___702F6A3276CBE481D247A77C20B5459FB94D07D2_13; }
	inline void set_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_13(__StaticArrayInitTypeSizeU3D24_t978476008  value)
	{
		___702F6A3276CBE481D247A77C20B5459FB94D07D2_13 = value;
	}

	inline static int32_t get_offset_of_U37A32E1A19C182315E4263A65A72066492550D8CD_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___7A32E1A19C182315E4263A65A72066492550D8CD_14)); }
	inline __StaticArrayInitTypeSizeU3D32_t2544559955  get_U37A32E1A19C182315E4263A65A72066492550D8CD_14() const { return ___7A32E1A19C182315E4263A65A72066492550D8CD_14; }
	inline __StaticArrayInitTypeSizeU3D32_t2544559955 * get_address_of_U37A32E1A19C182315E4263A65A72066492550D8CD_14() { return &___7A32E1A19C182315E4263A65A72066492550D8CD_14; }
	inline void set_U37A32E1A19C182315E4263A65A72066492550D8CD_14(__StaticArrayInitTypeSizeU3D32_t2544559955  value)
	{
		___7A32E1A19C182315E4263A65A72066492550D8CD_14 = value;
	}

	inline static int32_t get_offset_of_U38B4E5E81A88D29642679AFCE41DCA380F9000462_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___8B4E5E81A88D29642679AFCE41DCA380F9000462_15)); }
	inline __StaticArrayInitTypeSizeU3D16_t1381760537  get_U38B4E5E81A88D29642679AFCE41DCA380F9000462_15() const { return ___8B4E5E81A88D29642679AFCE41DCA380F9000462_15; }
	inline __StaticArrayInitTypeSizeU3D16_t1381760537 * get_address_of_U38B4E5E81A88D29642679AFCE41DCA380F9000462_15() { return &___8B4E5E81A88D29642679AFCE41DCA380F9000462_15; }
	inline void set_U38B4E5E81A88D29642679AFCE41DCA380F9000462_15(__StaticArrayInitTypeSizeU3D16_t1381760537  value)
	{
		___8B4E5E81A88D29642679AFCE41DCA380F9000462_15 = value;
	}

	inline static int32_t get_offset_of_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___99F0664C2AC8464B51252D92FC24F3834C6FB90C_16)); }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541  get_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_16() const { return ___99F0664C2AC8464B51252D92FC24F3834C6FB90C_16; }
	inline __StaticArrayInitTypeSizeU3D12_t1381760541 * get_address_of_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_16() { return &___99F0664C2AC8464B51252D92FC24F3834C6FB90C_16; }
	inline void set_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_16(__StaticArrayInitTypeSizeU3D12_t1381760541  value)
	{
		___99F0664C2AC8464B51252D92FC24F3834C6FB90C_16 = value;
	}

	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_17)); }
	inline __StaticArrayInitTypeSizeU3D28_t978476019  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_17() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_17; }
	inline __StaticArrayInitTypeSizeU3D28_t978476019 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_17() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_17; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_17(__StaticArrayInitTypeSizeU3D28_t978476019  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_17 = value;
	}

	inline static int32_t get_offset_of_U39E374D7263B2452E25DE3D6E617F6A728D98A439_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___9E374D7263B2452E25DE3D6E617F6A728D98A439_18)); }
	inline __StaticArrayInitTypeSizeU3D144_t3794590962  get_U39E374D7263B2452E25DE3D6E617F6A728D98A439_18() const { return ___9E374D7263B2452E25DE3D6E617F6A728D98A439_18; }
	inline __StaticArrayInitTypeSizeU3D144_t3794590962 * get_address_of_U39E374D7263B2452E25DE3D6E617F6A728D98A439_18() { return &___9E374D7263B2452E25DE3D6E617F6A728D98A439_18; }
	inline void set_U39E374D7263B2452E25DE3D6E617F6A728D98A439_18(__StaticArrayInitTypeSizeU3D144_t3794590962  value)
	{
		___9E374D7263B2452E25DE3D6E617F6A728D98A439_18 = value;
	}

	inline static int32_t get_offset_of_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19)); }
	inline __StaticArrayInitTypeSizeU3D24_t978476008  get_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19() const { return ___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19; }
	inline __StaticArrayInitTypeSizeU3D24_t978476008 * get_address_of_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19() { return &___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19; }
	inline void set_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19(__StaticArrayInitTypeSizeU3D24_t978476008  value)
	{
		___AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19 = value;
	}

	inline static int32_t get_offset_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___B368804F0C6DAB083B253A6B106D0783D5C32E90_20)); }
	inline __StaticArrayInitTypeSizeU3D416_t1045504634  get_B368804F0C6DAB083B253A6B106D0783D5C32E90_20() const { return ___B368804F0C6DAB083B253A6B106D0783D5C32E90_20; }
	inline __StaticArrayInitTypeSizeU3D416_t1045504634 * get_address_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_20() { return &___B368804F0C6DAB083B253A6B106D0783D5C32E90_20; }
	inline void set_B368804F0C6DAB083B253A6B106D0783D5C32E90_20(__StaticArrayInitTypeSizeU3D416_t1045504634  value)
	{
		___B368804F0C6DAB083B253A6B106D0783D5C32E90_20 = value;
	}

	inline static int32_t get_offset_of_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21)); }
	inline __StaticArrayInitTypeSizeU3D40_t2141275426  get_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21() const { return ___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21; }
	inline __StaticArrayInitTypeSizeU3D40_t2141275426 * get_address_of_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21() { return &___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21; }
	inline void set_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21(__StaticArrayInitTypeSizeU3D40_t2141275426  value)
	{
		___B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21 = value;
	}

	inline static int32_t get_offset_of_BAD037B714E1CD1052149B51238A3D4351DD10B5_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___BAD037B714E1CD1052149B51238A3D4351DD10B5_22)); }
	inline __StaticArrayInitTypeSizeU3D20_t978476012  get_BAD037B714E1CD1052149B51238A3D4351DD10B5_22() const { return ___BAD037B714E1CD1052149B51238A3D4351DD10B5_22; }
	inline __StaticArrayInitTypeSizeU3D20_t978476012 * get_address_of_BAD037B714E1CD1052149B51238A3D4351DD10B5_22() { return &___BAD037B714E1CD1052149B51238A3D4351DD10B5_22; }
	inline void set_BAD037B714E1CD1052149B51238A3D4351DD10B5_22(__StaticArrayInitTypeSizeU3D20_t978476012  value)
	{
		___BAD037B714E1CD1052149B51238A3D4351DD10B5_22 = value;
	}

	inline static int32_t get_offset_of_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23)); }
	inline __StaticArrayInitTypeSizeU3D16_t1381760537  get_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23() const { return ___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23; }
	inline __StaticArrayInitTypeSizeU3D16_t1381760537 * get_address_of_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23() { return &___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23; }
	inline void set_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23(__StaticArrayInitTypeSizeU3D16_t1381760537  value)
	{
		___C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23 = value;
	}

	inline static int32_t get_offset_of_DCF398750721AA7A27A6BA56E99350329B06E8B1_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___DCF398750721AA7A27A6BA56E99350329B06E8B1_24)); }
	inline __StaticArrayInitTypeSizeU3D16_t1381760537  get_DCF398750721AA7A27A6BA56E99350329B06E8B1_24() const { return ___DCF398750721AA7A27A6BA56E99350329B06E8B1_24; }
	inline __StaticArrayInitTypeSizeU3D16_t1381760537 * get_address_of_DCF398750721AA7A27A6BA56E99350329B06E8B1_24() { return &___DCF398750721AA7A27A6BA56E99350329B06E8B1_24; }
	inline void set_DCF398750721AA7A27A6BA56E99350329B06E8B1_24(__StaticArrayInitTypeSizeU3D16_t1381760537  value)
	{
		___DCF398750721AA7A27A6BA56E99350329B06E8B1_24 = value;
	}

	inline static int32_t get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25)); }
	inline int64_t get_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25() const { return ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25; }
	inline int64_t* get_address_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25() { return &___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25; }
	inline void set_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25(int64_t value)
	{
		___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25 = value;
	}

	inline static int32_t get_offset_of_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26)); }
	inline __StaticArrayInitTypeSizeU3D36_t2544559951  get_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26() const { return ___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26; }
	inline __StaticArrayInitTypeSizeU3D36_t2544559951 * get_address_of_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26() { return &___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26; }
	inline void set_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26(__StaticArrayInitTypeSizeU3D36_t2544559951  value)
	{
		___ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26 = value;
	}

	inline static int32_t get_offset_of_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27)); }
	inline __StaticArrayInitTypeSizeU3D64_t3304074836  get_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27() const { return ___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27; }
	inline __StaticArrayInitTypeSizeU3D64_t3304074836 * get_address_of_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27() { return &___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27; }
	inline void set_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27(__StaticArrayInitTypeSizeU3D64_t3304074836  value)
	{
		___EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27 = value;
	}

	inline static int32_t get_offset_of_FFE3F15642234E7FAD6951D432F1134D5AD15922_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields, ___FFE3F15642234E7FAD6951D432F1134D5AD15922_28)); }
	inline __StaticArrayInitTypeSizeU3D20_t978476012  get_FFE3F15642234E7FAD6951D432F1134D5AD15922_28() const { return ___FFE3F15642234E7FAD6951D432F1134D5AD15922_28; }
	inline __StaticArrayInitTypeSizeU3D20_t978476012 * get_address_of_FFE3F15642234E7FAD6951D432F1134D5AD15922_28() { return &___FFE3F15642234E7FAD6951D432F1134D5AD15922_28; }
	inline void set_FFE3F15642234E7FAD6951D432F1134D5AD15922_28(__StaticArrayInitTypeSizeU3D20_t978476012  value)
	{
		___FFE3F15642234E7FAD6951D432F1134D5AD15922_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305138_H
#ifndef PARTS_T3593783327_H
#define PARTS_T3593783327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration/Parts
struct  Parts_t3593783327 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration/Parts::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Parts_t3593783327, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTS_T3593783327_H
#ifndef DURATIONTYPE_T1846793513_H
#define DURATIONTYPE_T1846793513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration/DurationType
struct  DurationType_t1846793513 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration/DurationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DurationType_t1846793513, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONTYPE_T1846793513_H
#ifndef XMLREADERSECTION_T3603186601_H
#define XMLREADERSECTION_T3603186601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConfiguration.XmlReaderSection
struct  XmlReaderSection_t3603186601  : public ConfigurationSection_t2600766927
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSECTION_T3603186601_H
#ifndef MONOSSLPOLICYERRORS_T621534536_H
#define MONOSSLPOLICYERRORS_T621534536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoSslPolicyErrors
struct  MonoSslPolicyErrors_t621534536 
{
public:
	// System.Int32 Mono.Security.Interface.MonoSslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonoSslPolicyErrors_t621534536, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLPOLICYERRORS_T621534536_H
#ifndef CIPHERSUITECODE_T4229451518_H
#define CIPHERSUITECODE_T4229451518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.CipherSuiteCode
struct  CipherSuiteCode_t4229451518 
{
public:
	// System.UInt16 Mono.Security.Interface.CipherSuiteCode::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherSuiteCode_t4229451518, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITECODE_T4229451518_H
#ifndef TLSPROTOCOLS_T1951446164_H
#define TLSPROTOCOLS_T1951446164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.TlsProtocols
struct  TlsProtocols_t1951446164 
{
public:
	// System.Int32 Mono.Security.Interface.TlsProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TlsProtocols_t1951446164, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOLS_T1951446164_H
#ifndef NULLABLE_1_T214512479_H
#define NULLABLE_1_T214512479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mono.Security.Interface.TlsProtocols>
struct  Nullable_1_t214512479 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t214512479, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t214512479, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T214512479_H
#ifndef ALERT_T3405955216_H
#define ALERT_T3405955216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Alert
struct  Alert_t3405955216  : public RuntimeObject
{
public:
	// Mono.Security.Protocol.Tls.AlertLevel Mono.Security.Protocol.Tls.Alert::level
	uint8_t ___level_0;
	// Mono.Security.Protocol.Tls.AlertDescription Mono.Security.Protocol.Tls.Alert::description
	uint8_t ___description_1;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(Alert_t3405955216, ___level_0)); }
	inline uint8_t get_level_0() const { return ___level_0; }
	inline uint8_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(uint8_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(Alert_t3405955216, ___description_1)); }
	inline uint8_t get_description_1() const { return ___description_1; }
	inline uint8_t* get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(uint8_t value)
	{
		___description_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERT_T3405955216_H
#ifndef NULLABLE_1_T3179568147_H
#define NULLABLE_1_T3179568147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>
struct  Nullable_1_t3179568147 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3179568147, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3179568147, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3179568147_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef XSDVALIDATOR_T535151321_H
#define XSDVALIDATOR_T535151321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdValidator
struct  XsdValidator_t535151321  : public BaseValidator_t3557140249
{
public:
	// System.Int32 System.Xml.Schema.XsdValidator::startIDConstraint
	int32_t ___startIDConstraint_15;
	// System.Xml.HWStack System.Xml.Schema.XsdValidator::validationStack
	HWStack_t738999989 * ___validationStack_16;
	// System.Collections.Hashtable System.Xml.Schema.XsdValidator::attPresence
	Hashtable_t909839986 * ___attPresence_17;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdValidator::nsManager
	XmlNamespaceManager_t486731501 * ___nsManager_18;
	// System.Boolean System.Xml.Schema.XsdValidator::bManageNamespaces
	bool ___bManageNamespaces_19;
	// System.Collections.Hashtable System.Xml.Schema.XsdValidator::IDs
	Hashtable_t909839986 * ___IDs_20;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.XsdValidator::idRefListHead
	IdRefNode_t224554150 * ___idRefListHead_21;
	// System.Xml.Schema.Parser System.Xml.Schema.XsdValidator::inlineSchemaParser
	Parser_t1940171737 * ___inlineSchemaParser_22;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XsdValidator::processContents
	int32_t ___processContents_23;
	// System.String System.Xml.Schema.XsdValidator::NsXmlNs
	String_t* ___NsXmlNs_27;
	// System.String System.Xml.Schema.XsdValidator::NsXs
	String_t* ___NsXs_28;
	// System.String System.Xml.Schema.XsdValidator::NsXsi
	String_t* ___NsXsi_29;
	// System.String System.Xml.Schema.XsdValidator::XsiType
	String_t* ___XsiType_30;
	// System.String System.Xml.Schema.XsdValidator::XsiNil
	String_t* ___XsiNil_31;
	// System.String System.Xml.Schema.XsdValidator::XsiSchemaLocation
	String_t* ___XsiSchemaLocation_32;
	// System.String System.Xml.Schema.XsdValidator::XsiNoNamespaceSchemaLocation
	String_t* ___XsiNoNamespaceSchemaLocation_33;
	// System.String System.Xml.Schema.XsdValidator::XsdSchema
	String_t* ___XsdSchema_34;

public:
	inline static int32_t get_offset_of_startIDConstraint_15() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___startIDConstraint_15)); }
	inline int32_t get_startIDConstraint_15() const { return ___startIDConstraint_15; }
	inline int32_t* get_address_of_startIDConstraint_15() { return &___startIDConstraint_15; }
	inline void set_startIDConstraint_15(int32_t value)
	{
		___startIDConstraint_15 = value;
	}

	inline static int32_t get_offset_of_validationStack_16() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___validationStack_16)); }
	inline HWStack_t738999989 * get_validationStack_16() const { return ___validationStack_16; }
	inline HWStack_t738999989 ** get_address_of_validationStack_16() { return &___validationStack_16; }
	inline void set_validationStack_16(HWStack_t738999989 * value)
	{
		___validationStack_16 = value;
		Il2CppCodeGenWriteBarrier((&___validationStack_16), value);
	}

	inline static int32_t get_offset_of_attPresence_17() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___attPresence_17)); }
	inline Hashtable_t909839986 * get_attPresence_17() const { return ___attPresence_17; }
	inline Hashtable_t909839986 ** get_address_of_attPresence_17() { return &___attPresence_17; }
	inline void set_attPresence_17(Hashtable_t909839986 * value)
	{
		___attPresence_17 = value;
		Il2CppCodeGenWriteBarrier((&___attPresence_17), value);
	}

	inline static int32_t get_offset_of_nsManager_18() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___nsManager_18)); }
	inline XmlNamespaceManager_t486731501 * get_nsManager_18() const { return ___nsManager_18; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsManager_18() { return &___nsManager_18; }
	inline void set_nsManager_18(XmlNamespaceManager_t486731501 * value)
	{
		___nsManager_18 = value;
		Il2CppCodeGenWriteBarrier((&___nsManager_18), value);
	}

	inline static int32_t get_offset_of_bManageNamespaces_19() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___bManageNamespaces_19)); }
	inline bool get_bManageNamespaces_19() const { return ___bManageNamespaces_19; }
	inline bool* get_address_of_bManageNamespaces_19() { return &___bManageNamespaces_19; }
	inline void set_bManageNamespaces_19(bool value)
	{
		___bManageNamespaces_19 = value;
	}

	inline static int32_t get_offset_of_IDs_20() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___IDs_20)); }
	inline Hashtable_t909839986 * get_IDs_20() const { return ___IDs_20; }
	inline Hashtable_t909839986 ** get_address_of_IDs_20() { return &___IDs_20; }
	inline void set_IDs_20(Hashtable_t909839986 * value)
	{
		___IDs_20 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_20), value);
	}

	inline static int32_t get_offset_of_idRefListHead_21() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___idRefListHead_21)); }
	inline IdRefNode_t224554150 * get_idRefListHead_21() const { return ___idRefListHead_21; }
	inline IdRefNode_t224554150 ** get_address_of_idRefListHead_21() { return &___idRefListHead_21; }
	inline void set_idRefListHead_21(IdRefNode_t224554150 * value)
	{
		___idRefListHead_21 = value;
		Il2CppCodeGenWriteBarrier((&___idRefListHead_21), value);
	}

	inline static int32_t get_offset_of_inlineSchemaParser_22() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___inlineSchemaParser_22)); }
	inline Parser_t1940171737 * get_inlineSchemaParser_22() const { return ___inlineSchemaParser_22; }
	inline Parser_t1940171737 ** get_address_of_inlineSchemaParser_22() { return &___inlineSchemaParser_22; }
	inline void set_inlineSchemaParser_22(Parser_t1940171737 * value)
	{
		___inlineSchemaParser_22 = value;
		Il2CppCodeGenWriteBarrier((&___inlineSchemaParser_22), value);
	}

	inline static int32_t get_offset_of_processContents_23() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___processContents_23)); }
	inline int32_t get_processContents_23() const { return ___processContents_23; }
	inline int32_t* get_address_of_processContents_23() { return &___processContents_23; }
	inline void set_processContents_23(int32_t value)
	{
		___processContents_23 = value;
	}

	inline static int32_t get_offset_of_NsXmlNs_27() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___NsXmlNs_27)); }
	inline String_t* get_NsXmlNs_27() const { return ___NsXmlNs_27; }
	inline String_t** get_address_of_NsXmlNs_27() { return &___NsXmlNs_27; }
	inline void set_NsXmlNs_27(String_t* value)
	{
		___NsXmlNs_27 = value;
		Il2CppCodeGenWriteBarrier((&___NsXmlNs_27), value);
	}

	inline static int32_t get_offset_of_NsXs_28() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___NsXs_28)); }
	inline String_t* get_NsXs_28() const { return ___NsXs_28; }
	inline String_t** get_address_of_NsXs_28() { return &___NsXs_28; }
	inline void set_NsXs_28(String_t* value)
	{
		___NsXs_28 = value;
		Il2CppCodeGenWriteBarrier((&___NsXs_28), value);
	}

	inline static int32_t get_offset_of_NsXsi_29() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___NsXsi_29)); }
	inline String_t* get_NsXsi_29() const { return ___NsXsi_29; }
	inline String_t** get_address_of_NsXsi_29() { return &___NsXsi_29; }
	inline void set_NsXsi_29(String_t* value)
	{
		___NsXsi_29 = value;
		Il2CppCodeGenWriteBarrier((&___NsXsi_29), value);
	}

	inline static int32_t get_offset_of_XsiType_30() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiType_30)); }
	inline String_t* get_XsiType_30() const { return ___XsiType_30; }
	inline String_t** get_address_of_XsiType_30() { return &___XsiType_30; }
	inline void set_XsiType_30(String_t* value)
	{
		___XsiType_30 = value;
		Il2CppCodeGenWriteBarrier((&___XsiType_30), value);
	}

	inline static int32_t get_offset_of_XsiNil_31() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiNil_31)); }
	inline String_t* get_XsiNil_31() const { return ___XsiNil_31; }
	inline String_t** get_address_of_XsiNil_31() { return &___XsiNil_31; }
	inline void set_XsiNil_31(String_t* value)
	{
		___XsiNil_31 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNil_31), value);
	}

	inline static int32_t get_offset_of_XsiSchemaLocation_32() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiSchemaLocation_32)); }
	inline String_t* get_XsiSchemaLocation_32() const { return ___XsiSchemaLocation_32; }
	inline String_t** get_address_of_XsiSchemaLocation_32() { return &___XsiSchemaLocation_32; }
	inline void set_XsiSchemaLocation_32(String_t* value)
	{
		___XsiSchemaLocation_32 = value;
		Il2CppCodeGenWriteBarrier((&___XsiSchemaLocation_32), value);
	}

	inline static int32_t get_offset_of_XsiNoNamespaceSchemaLocation_33() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiNoNamespaceSchemaLocation_33)); }
	inline String_t* get_XsiNoNamespaceSchemaLocation_33() const { return ___XsiNoNamespaceSchemaLocation_33; }
	inline String_t** get_address_of_XsiNoNamespaceSchemaLocation_33() { return &___XsiNoNamespaceSchemaLocation_33; }
	inline void set_XsiNoNamespaceSchemaLocation_33(String_t* value)
	{
		___XsiNoNamespaceSchemaLocation_33 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNoNamespaceSchemaLocation_33), value);
	}

	inline static int32_t get_offset_of_XsdSchema_34() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsdSchema_34)); }
	inline String_t* get_XsdSchema_34() const { return ___XsdSchema_34; }
	inline String_t** get_address_of_XsdSchema_34() { return &___XsdSchema_34; }
	inline void set_XsdSchema_34(String_t* value)
	{
		___XsdSchema_34 = value;
		Il2CppCodeGenWriteBarrier((&___XsdSchema_34), value);
	}
};

struct XsdValidator_t535151321_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtCDATA
	XmlSchemaDatatype_t1195946242 * ___dtCDATA_24;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtQName
	XmlSchemaDatatype_t1195946242 * ___dtQName_25;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtStringArray
	XmlSchemaDatatype_t1195946242 * ___dtStringArray_26;

public:
	inline static int32_t get_offset_of_dtCDATA_24() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321_StaticFields, ___dtCDATA_24)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtCDATA_24() const { return ___dtCDATA_24; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtCDATA_24() { return &___dtCDATA_24; }
	inline void set_dtCDATA_24(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtCDATA_24 = value;
		Il2CppCodeGenWriteBarrier((&___dtCDATA_24), value);
	}

	inline static int32_t get_offset_of_dtQName_25() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321_StaticFields, ___dtQName_25)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtQName_25() const { return ___dtQName_25; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtQName_25() { return &___dtQName_25; }
	inline void set_dtQName_25(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtQName_25 = value;
		Il2CppCodeGenWriteBarrier((&___dtQName_25), value);
	}

	inline static int32_t get_offset_of_dtStringArray_26() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321_StaticFields, ___dtStringArray_26)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtStringArray_26() const { return ___dtStringArray_26; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtStringArray_26() { return &___dtStringArray_26; }
	inline void set_dtStringArray_26(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtStringArray_26 = value;
		Il2CppCodeGenWriteBarrier((&___dtStringArray_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATOR_T535151321_H
#ifndef CIPHERSUITE_T491456551_H
#define CIPHERSUITE_T491456551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherSuite
struct  CipherSuite_t491456551  : public RuntimeObject
{
public:
	// System.Int16 Mono.Security.Protocol.Tls.CipherSuite::code
	int16_t ___code_1;
	// System.String Mono.Security.Protocol.Tls.CipherSuite::name
	String_t* ___name_2;
	// Mono.Security.Protocol.Tls.CipherAlgorithmType Mono.Security.Protocol.Tls.CipherSuite::cipherAlgorithmType
	int32_t ___cipherAlgorithmType_3;
	// Mono.Security.Protocol.Tls.HashAlgorithmType Mono.Security.Protocol.Tls.CipherSuite::hashAlgorithmType
	int32_t ___hashAlgorithmType_4;
	// Mono.Security.Protocol.Tls.ExchangeAlgorithmType Mono.Security.Protocol.Tls.CipherSuite::exchangeAlgorithmType
	int32_t ___exchangeAlgorithmType_5;
	// System.Boolean Mono.Security.Protocol.Tls.CipherSuite::isExportable
	bool ___isExportable_6;
	// System.Security.Cryptography.CipherMode Mono.Security.Protocol.Tls.CipherSuite::cipherMode
	int32_t ___cipherMode_7;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::keyMaterialSize
	uint8_t ___keyMaterialSize_8;
	// System.Int32 Mono.Security.Protocol.Tls.CipherSuite::keyBlockSize
	int32_t ___keyBlockSize_9;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::expandedKeyMaterialSize
	uint8_t ___expandedKeyMaterialSize_10;
	// System.Int16 Mono.Security.Protocol.Tls.CipherSuite::effectiveKeyBits
	int16_t ___effectiveKeyBits_11;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::ivSize
	uint8_t ___ivSize_12;
	// System.Byte Mono.Security.Protocol.Tls.CipherSuite::blockSize
	uint8_t ___blockSize_13;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.CipherSuite::context
	Context_t4285182719 * ___context_14;
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Protocol.Tls.CipherSuite::encryptionAlgorithm
	SymmetricAlgorithm_t1108166522 * ___encryptionAlgorithm_15;
	// System.Security.Cryptography.ICryptoTransform Mono.Security.Protocol.Tls.CipherSuite::encryptionCipher
	RuntimeObject* ___encryptionCipher_16;
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Protocol.Tls.CipherSuite::decryptionAlgorithm
	SymmetricAlgorithm_t1108166522 * ___decryptionAlgorithm_17;
	// System.Security.Cryptography.ICryptoTransform Mono.Security.Protocol.Tls.CipherSuite::decryptionCipher
	RuntimeObject* ___decryptionCipher_18;
	// System.Security.Cryptography.KeyedHashAlgorithm Mono.Security.Protocol.Tls.CipherSuite::clientHMAC
	KeyedHashAlgorithm_t1374150027 * ___clientHMAC_19;
	// System.Security.Cryptography.KeyedHashAlgorithm Mono.Security.Protocol.Tls.CipherSuite::serverHMAC
	KeyedHashAlgorithm_t1374150027 * ___serverHMAC_20;

public:
	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___code_1)); }
	inline int16_t get_code_1() const { return ___code_1; }
	inline int16_t* get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(int16_t value)
	{
		___code_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_cipherAlgorithmType_3() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___cipherAlgorithmType_3)); }
	inline int32_t get_cipherAlgorithmType_3() const { return ___cipherAlgorithmType_3; }
	inline int32_t* get_address_of_cipherAlgorithmType_3() { return &___cipherAlgorithmType_3; }
	inline void set_cipherAlgorithmType_3(int32_t value)
	{
		___cipherAlgorithmType_3 = value;
	}

	inline static int32_t get_offset_of_hashAlgorithmType_4() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___hashAlgorithmType_4)); }
	inline int32_t get_hashAlgorithmType_4() const { return ___hashAlgorithmType_4; }
	inline int32_t* get_address_of_hashAlgorithmType_4() { return &___hashAlgorithmType_4; }
	inline void set_hashAlgorithmType_4(int32_t value)
	{
		___hashAlgorithmType_4 = value;
	}

	inline static int32_t get_offset_of_exchangeAlgorithmType_5() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___exchangeAlgorithmType_5)); }
	inline int32_t get_exchangeAlgorithmType_5() const { return ___exchangeAlgorithmType_5; }
	inline int32_t* get_address_of_exchangeAlgorithmType_5() { return &___exchangeAlgorithmType_5; }
	inline void set_exchangeAlgorithmType_5(int32_t value)
	{
		___exchangeAlgorithmType_5 = value;
	}

	inline static int32_t get_offset_of_isExportable_6() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___isExportable_6)); }
	inline bool get_isExportable_6() const { return ___isExportable_6; }
	inline bool* get_address_of_isExportable_6() { return &___isExportable_6; }
	inline void set_isExportable_6(bool value)
	{
		___isExportable_6 = value;
	}

	inline static int32_t get_offset_of_cipherMode_7() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___cipherMode_7)); }
	inline int32_t get_cipherMode_7() const { return ___cipherMode_7; }
	inline int32_t* get_address_of_cipherMode_7() { return &___cipherMode_7; }
	inline void set_cipherMode_7(int32_t value)
	{
		___cipherMode_7 = value;
	}

	inline static int32_t get_offset_of_keyMaterialSize_8() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___keyMaterialSize_8)); }
	inline uint8_t get_keyMaterialSize_8() const { return ___keyMaterialSize_8; }
	inline uint8_t* get_address_of_keyMaterialSize_8() { return &___keyMaterialSize_8; }
	inline void set_keyMaterialSize_8(uint8_t value)
	{
		___keyMaterialSize_8 = value;
	}

	inline static int32_t get_offset_of_keyBlockSize_9() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___keyBlockSize_9)); }
	inline int32_t get_keyBlockSize_9() const { return ___keyBlockSize_9; }
	inline int32_t* get_address_of_keyBlockSize_9() { return &___keyBlockSize_9; }
	inline void set_keyBlockSize_9(int32_t value)
	{
		___keyBlockSize_9 = value;
	}

	inline static int32_t get_offset_of_expandedKeyMaterialSize_10() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___expandedKeyMaterialSize_10)); }
	inline uint8_t get_expandedKeyMaterialSize_10() const { return ___expandedKeyMaterialSize_10; }
	inline uint8_t* get_address_of_expandedKeyMaterialSize_10() { return &___expandedKeyMaterialSize_10; }
	inline void set_expandedKeyMaterialSize_10(uint8_t value)
	{
		___expandedKeyMaterialSize_10 = value;
	}

	inline static int32_t get_offset_of_effectiveKeyBits_11() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___effectiveKeyBits_11)); }
	inline int16_t get_effectiveKeyBits_11() const { return ___effectiveKeyBits_11; }
	inline int16_t* get_address_of_effectiveKeyBits_11() { return &___effectiveKeyBits_11; }
	inline void set_effectiveKeyBits_11(int16_t value)
	{
		___effectiveKeyBits_11 = value;
	}

	inline static int32_t get_offset_of_ivSize_12() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___ivSize_12)); }
	inline uint8_t get_ivSize_12() const { return ___ivSize_12; }
	inline uint8_t* get_address_of_ivSize_12() { return &___ivSize_12; }
	inline void set_ivSize_12(uint8_t value)
	{
		___ivSize_12 = value;
	}

	inline static int32_t get_offset_of_blockSize_13() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___blockSize_13)); }
	inline uint8_t get_blockSize_13() const { return ___blockSize_13; }
	inline uint8_t* get_address_of_blockSize_13() { return &___blockSize_13; }
	inline void set_blockSize_13(uint8_t value)
	{
		___blockSize_13 = value;
	}

	inline static int32_t get_offset_of_context_14() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___context_14)); }
	inline Context_t4285182719 * get_context_14() const { return ___context_14; }
	inline Context_t4285182719 ** get_address_of_context_14() { return &___context_14; }
	inline void set_context_14(Context_t4285182719 * value)
	{
		___context_14 = value;
		Il2CppCodeGenWriteBarrier((&___context_14), value);
	}

	inline static int32_t get_offset_of_encryptionAlgorithm_15() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___encryptionAlgorithm_15)); }
	inline SymmetricAlgorithm_t1108166522 * get_encryptionAlgorithm_15() const { return ___encryptionAlgorithm_15; }
	inline SymmetricAlgorithm_t1108166522 ** get_address_of_encryptionAlgorithm_15() { return &___encryptionAlgorithm_15; }
	inline void set_encryptionAlgorithm_15(SymmetricAlgorithm_t1108166522 * value)
	{
		___encryptionAlgorithm_15 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionAlgorithm_15), value);
	}

	inline static int32_t get_offset_of_encryptionCipher_16() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___encryptionCipher_16)); }
	inline RuntimeObject* get_encryptionCipher_16() const { return ___encryptionCipher_16; }
	inline RuntimeObject** get_address_of_encryptionCipher_16() { return &___encryptionCipher_16; }
	inline void set_encryptionCipher_16(RuntimeObject* value)
	{
		___encryptionCipher_16 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionCipher_16), value);
	}

	inline static int32_t get_offset_of_decryptionAlgorithm_17() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___decryptionAlgorithm_17)); }
	inline SymmetricAlgorithm_t1108166522 * get_decryptionAlgorithm_17() const { return ___decryptionAlgorithm_17; }
	inline SymmetricAlgorithm_t1108166522 ** get_address_of_decryptionAlgorithm_17() { return &___decryptionAlgorithm_17; }
	inline void set_decryptionAlgorithm_17(SymmetricAlgorithm_t1108166522 * value)
	{
		___decryptionAlgorithm_17 = value;
		Il2CppCodeGenWriteBarrier((&___decryptionAlgorithm_17), value);
	}

	inline static int32_t get_offset_of_decryptionCipher_18() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___decryptionCipher_18)); }
	inline RuntimeObject* get_decryptionCipher_18() const { return ___decryptionCipher_18; }
	inline RuntimeObject** get_address_of_decryptionCipher_18() { return &___decryptionCipher_18; }
	inline void set_decryptionCipher_18(RuntimeObject* value)
	{
		___decryptionCipher_18 = value;
		Il2CppCodeGenWriteBarrier((&___decryptionCipher_18), value);
	}

	inline static int32_t get_offset_of_clientHMAC_19() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___clientHMAC_19)); }
	inline KeyedHashAlgorithm_t1374150027 * get_clientHMAC_19() const { return ___clientHMAC_19; }
	inline KeyedHashAlgorithm_t1374150027 ** get_address_of_clientHMAC_19() { return &___clientHMAC_19; }
	inline void set_clientHMAC_19(KeyedHashAlgorithm_t1374150027 * value)
	{
		___clientHMAC_19 = value;
		Il2CppCodeGenWriteBarrier((&___clientHMAC_19), value);
	}

	inline static int32_t get_offset_of_serverHMAC_20() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551, ___serverHMAC_20)); }
	inline KeyedHashAlgorithm_t1374150027 * get_serverHMAC_20() const { return ___serverHMAC_20; }
	inline KeyedHashAlgorithm_t1374150027 ** get_address_of_serverHMAC_20() { return &___serverHMAC_20; }
	inline void set_serverHMAC_20(KeyedHashAlgorithm_t1374150027 * value)
	{
		___serverHMAC_20 = value;
		Il2CppCodeGenWriteBarrier((&___serverHMAC_20), value);
	}
};

struct CipherSuite_t491456551_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.CipherSuite::EmptyArray
	ByteU5BU5D_t3397334013* ___EmptyArray_0;

public:
	inline static int32_t get_offset_of_EmptyArray_0() { return static_cast<int32_t>(offsetof(CipherSuite_t491456551_StaticFields, ___EmptyArray_0)); }
	inline ByteU5BU5D_t3397334013* get_EmptyArray_0() const { return ___EmptyArray_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_EmptyArray_0() { return &___EmptyArray_0; }
	inline void set_EmptyArray_0(ByteU5BU5D_t3397334013* value)
	{
		___EmptyArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITE_T491456551_H
#ifndef NEGOTIATEASYNCRESULT_T3602869449_H
#define NEGOTIATEASYNCRESULT_T3602869449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult
struct  NegotiateAsyncResult_t3602869449  : public RuntimeObject
{
public:
	// System.Object Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::locker
	RuntimeObject * ___locker_0;
	// System.AsyncCallback Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::_userCallback
	AsyncCallback_t163412349 * ____userCallback_1;
	// System.Object Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::_userState
	RuntimeObject * ____userState_2;
	// System.Exception Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::_asyncException
	Exception_t1927440687 * ____asyncException_3;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::handle
	ManualResetEvent_t926074657 * ___handle_4;
	// Mono.Security.Protocol.Tls.SslClientStream/NegotiateState Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::_state
	int32_t ____state_5;
	// System.Boolean Mono.Security.Protocol.Tls.SslClientStream/NegotiateAsyncResult::completed
	bool ___completed_6;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of__userCallback_1() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ____userCallback_1)); }
	inline AsyncCallback_t163412349 * get__userCallback_1() const { return ____userCallback_1; }
	inline AsyncCallback_t163412349 ** get_address_of__userCallback_1() { return &____userCallback_1; }
	inline void set__userCallback_1(AsyncCallback_t163412349 * value)
	{
		____userCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____userCallback_1), value);
	}

	inline static int32_t get_offset_of__userState_2() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ____userState_2)); }
	inline RuntimeObject * get__userState_2() const { return ____userState_2; }
	inline RuntimeObject ** get_address_of__userState_2() { return &____userState_2; }
	inline void set__userState_2(RuntimeObject * value)
	{
		____userState_2 = value;
		Il2CppCodeGenWriteBarrier((&____userState_2), value);
	}

	inline static int32_t get_offset_of__asyncException_3() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ____asyncException_3)); }
	inline Exception_t1927440687 * get__asyncException_3() const { return ____asyncException_3; }
	inline Exception_t1927440687 ** get_address_of__asyncException_3() { return &____asyncException_3; }
	inline void set__asyncException_3(Exception_t1927440687 * value)
	{
		____asyncException_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncException_3), value);
	}

	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ___handle_4)); }
	inline ManualResetEvent_t926074657 * get_handle_4() const { return ___handle_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(ManualResetEvent_t926074657 * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of__state_5() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ____state_5)); }
	inline int32_t get__state_5() const { return ____state_5; }
	inline int32_t* get_address_of__state_5() { return &____state_5; }
	inline void set__state_5(int32_t value)
	{
		____state_5 = value;
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(NegotiateAsyncResult_t3602869449, ___completed_6)); }
	inline bool get_completed_6() const { return ___completed_6; }
	inline bool* get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(bool value)
	{
		___completed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGOTIATEASYNCRESULT_T3602869449_H
#ifndef CIPHERSUITECOLLECTION_T2431504453_H
#define CIPHERSUITECOLLECTION_T2431504453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherSuiteCollection
struct  CipherSuiteCollection_t2431504453  : public List_1_t4155544979
{
public:
	// Mono.Security.Protocol.Tls.SecurityProtocolType Mono.Security.Protocol.Tls.CipherSuiteCollection::protocol
	int32_t ___protocol_6;

public:
	inline static int32_t get_offset_of_protocol_6() { return static_cast<int32_t>(offsetof(CipherSuiteCollection_t2431504453, ___protocol_6)); }
	inline int32_t get_protocol_6() const { return ___protocol_6; }
	inline int32_t* get_address_of_protocol_6() { return &___protocol_6; }
	inline void set_protocol_6(int32_t value)
	{
		___protocol_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITECOLLECTION_T2431504453_H
#ifndef SSLCLIENTSTREAM_T3918817353_H
#define SSLCLIENTSTREAM_T3918817353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream
struct  SslClientStream_t3918817353  : public SslStreamBase_t934199321
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback Mono.Security.Protocol.Tls.SslClientStream::ServerCertValidation
	CertificateValidationCallback_t989458295 * ___ServerCertValidation_18;
	// Mono.Security.Protocol.Tls.CertificateSelectionCallback Mono.Security.Protocol.Tls.SslClientStream::ClientCertSelection
	CertificateSelectionCallback_t3721235490 * ___ClientCertSelection_19;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.SslClientStream::PrivateKeySelection
	PrivateKeySelectionCallback_t1663566523 * ___PrivateKeySelection_20;
	// Mono.Security.Protocol.Tls.CertificateValidationCallback2 Mono.Security.Protocol.Tls.SslClientStream::ServerCertValidation2
	CertificateValidationCallback2_t3318447433 * ___ServerCertValidation2_21;

public:
	inline static int32_t get_offset_of_ServerCertValidation_18() { return static_cast<int32_t>(offsetof(SslClientStream_t3918817353, ___ServerCertValidation_18)); }
	inline CertificateValidationCallback_t989458295 * get_ServerCertValidation_18() const { return ___ServerCertValidation_18; }
	inline CertificateValidationCallback_t989458295 ** get_address_of_ServerCertValidation_18() { return &___ServerCertValidation_18; }
	inline void set_ServerCertValidation_18(CertificateValidationCallback_t989458295 * value)
	{
		___ServerCertValidation_18 = value;
		Il2CppCodeGenWriteBarrier((&___ServerCertValidation_18), value);
	}

	inline static int32_t get_offset_of_ClientCertSelection_19() { return static_cast<int32_t>(offsetof(SslClientStream_t3918817353, ___ClientCertSelection_19)); }
	inline CertificateSelectionCallback_t3721235490 * get_ClientCertSelection_19() const { return ___ClientCertSelection_19; }
	inline CertificateSelectionCallback_t3721235490 ** get_address_of_ClientCertSelection_19() { return &___ClientCertSelection_19; }
	inline void set_ClientCertSelection_19(CertificateSelectionCallback_t3721235490 * value)
	{
		___ClientCertSelection_19 = value;
		Il2CppCodeGenWriteBarrier((&___ClientCertSelection_19), value);
	}

	inline static int32_t get_offset_of_PrivateKeySelection_20() { return static_cast<int32_t>(offsetof(SslClientStream_t3918817353, ___PrivateKeySelection_20)); }
	inline PrivateKeySelectionCallback_t1663566523 * get_PrivateKeySelection_20() const { return ___PrivateKeySelection_20; }
	inline PrivateKeySelectionCallback_t1663566523 ** get_address_of_PrivateKeySelection_20() { return &___PrivateKeySelection_20; }
	inline void set_PrivateKeySelection_20(PrivateKeySelectionCallback_t1663566523 * value)
	{
		___PrivateKeySelection_20 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateKeySelection_20), value);
	}

	inline static int32_t get_offset_of_ServerCertValidation2_21() { return static_cast<int32_t>(offsetof(SslClientStream_t3918817353, ___ServerCertValidation2_21)); }
	inline CertificateValidationCallback2_t3318447433 * get_ServerCertValidation2_21() const { return ___ServerCertValidation2_21; }
	inline CertificateValidationCallback2_t3318447433 ** get_address_of_ServerCertValidation2_21() { return &___ServerCertValidation2_21; }
	inline void set_ServerCertValidation2_21(CertificateValidationCallback2_t3318447433 * value)
	{
		___ServerCertValidation2_21 = value;
		Il2CppCodeGenWriteBarrier((&___ServerCertValidation2_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCLIENTSTREAM_T3918817353_H
#ifndef SSLSERVERSTREAM_T330206493_H
#define SSLSERVERSTREAM_T330206493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslServerStream
struct  SslServerStream_t330206493  : public SslStreamBase_t934199321
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback Mono.Security.Protocol.Tls.SslServerStream::ClientCertValidation
	CertificateValidationCallback_t989458295 * ___ClientCertValidation_18;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.SslServerStream::PrivateKeySelection
	PrivateKeySelectionCallback_t1663566523 * ___PrivateKeySelection_19;
	// Mono.Security.Protocol.Tls.CertificateValidationCallback2 Mono.Security.Protocol.Tls.SslServerStream::ClientCertValidation2
	CertificateValidationCallback2_t3318447433 * ___ClientCertValidation2_20;

public:
	inline static int32_t get_offset_of_ClientCertValidation_18() { return static_cast<int32_t>(offsetof(SslServerStream_t330206493, ___ClientCertValidation_18)); }
	inline CertificateValidationCallback_t989458295 * get_ClientCertValidation_18() const { return ___ClientCertValidation_18; }
	inline CertificateValidationCallback_t989458295 ** get_address_of_ClientCertValidation_18() { return &___ClientCertValidation_18; }
	inline void set_ClientCertValidation_18(CertificateValidationCallback_t989458295 * value)
	{
		___ClientCertValidation_18 = value;
		Il2CppCodeGenWriteBarrier((&___ClientCertValidation_18), value);
	}

	inline static int32_t get_offset_of_PrivateKeySelection_19() { return static_cast<int32_t>(offsetof(SslServerStream_t330206493, ___PrivateKeySelection_19)); }
	inline PrivateKeySelectionCallback_t1663566523 * get_PrivateKeySelection_19() const { return ___PrivateKeySelection_19; }
	inline PrivateKeySelectionCallback_t1663566523 ** get_address_of_PrivateKeySelection_19() { return &___PrivateKeySelection_19; }
	inline void set_PrivateKeySelection_19(PrivateKeySelectionCallback_t1663566523 * value)
	{
		___PrivateKeySelection_19 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateKeySelection_19), value);
	}

	inline static int32_t get_offset_of_ClientCertValidation2_20() { return static_cast<int32_t>(offsetof(SslServerStream_t330206493, ___ClientCertValidation2_20)); }
	inline CertificateValidationCallback2_t3318447433 * get_ClientCertValidation2_20() const { return ___ClientCertValidation2_20; }
	inline CertificateValidationCallback2_t3318447433 ** get_address_of_ClientCertValidation2_20() { return &___ClientCertValidation2_20; }
	inline void set_ClientCertValidation2_20(CertificateValidationCallback2_t3318447433 * value)
	{
		___ClientCertValidation2_20 = value;
		Il2CppCodeGenWriteBarrier((&___ClientCertValidation2_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSERVERSTREAM_T330206493_H
#ifndef PARSER_T2170101039_H
#define PARSER_T2170101039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/Parser
struct  Parser_t2170101039 
{
public:
	// System.Xml.Schema.XsdDateTime/DateTimeTypeCode System.Xml.Schema.XsdDateTime/Parser::typeCode
	int32_t ___typeCode_0;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::year
	int32_t ___year_1;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::month
	int32_t ___month_2;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::day
	int32_t ___day_3;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::hour
	int32_t ___hour_4;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::minute
	int32_t ___minute_5;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::second
	int32_t ___second_6;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::fraction
	int32_t ___fraction_7;
	// System.Xml.Schema.XsdDateTime/XsdDateTimeKind System.Xml.Schema.XsdDateTime/Parser::kind
	int32_t ___kind_8;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::zoneHour
	int32_t ___zoneHour_9;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::zoneMinute
	int32_t ___zoneMinute_10;
	// System.String System.Xml.Schema.XsdDateTime/Parser::text
	String_t* ___text_11;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::length
	int32_t ___length_12;

public:
	inline static int32_t get_offset_of_typeCode_0() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___typeCode_0)); }
	inline int32_t get_typeCode_0() const { return ___typeCode_0; }
	inline int32_t* get_address_of_typeCode_0() { return &___typeCode_0; }
	inline void set_typeCode_0(int32_t value)
	{
		___typeCode_0 = value;
	}

	inline static int32_t get_offset_of_year_1() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___year_1)); }
	inline int32_t get_year_1() const { return ___year_1; }
	inline int32_t* get_address_of_year_1() { return &___year_1; }
	inline void set_year_1(int32_t value)
	{
		___year_1 = value;
	}

	inline static int32_t get_offset_of_month_2() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___month_2)); }
	inline int32_t get_month_2() const { return ___month_2; }
	inline int32_t* get_address_of_month_2() { return &___month_2; }
	inline void set_month_2(int32_t value)
	{
		___month_2 = value;
	}

	inline static int32_t get_offset_of_day_3() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___day_3)); }
	inline int32_t get_day_3() const { return ___day_3; }
	inline int32_t* get_address_of_day_3() { return &___day_3; }
	inline void set_day_3(int32_t value)
	{
		___day_3 = value;
	}

	inline static int32_t get_offset_of_hour_4() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___hour_4)); }
	inline int32_t get_hour_4() const { return ___hour_4; }
	inline int32_t* get_address_of_hour_4() { return &___hour_4; }
	inline void set_hour_4(int32_t value)
	{
		___hour_4 = value;
	}

	inline static int32_t get_offset_of_minute_5() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___minute_5)); }
	inline int32_t get_minute_5() const { return ___minute_5; }
	inline int32_t* get_address_of_minute_5() { return &___minute_5; }
	inline void set_minute_5(int32_t value)
	{
		___minute_5 = value;
	}

	inline static int32_t get_offset_of_second_6() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___second_6)); }
	inline int32_t get_second_6() const { return ___second_6; }
	inline int32_t* get_address_of_second_6() { return &___second_6; }
	inline void set_second_6(int32_t value)
	{
		___second_6 = value;
	}

	inline static int32_t get_offset_of_fraction_7() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___fraction_7)); }
	inline int32_t get_fraction_7() const { return ___fraction_7; }
	inline int32_t* get_address_of_fraction_7() { return &___fraction_7; }
	inline void set_fraction_7(int32_t value)
	{
		___fraction_7 = value;
	}

	inline static int32_t get_offset_of_kind_8() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___kind_8)); }
	inline int32_t get_kind_8() const { return ___kind_8; }
	inline int32_t* get_address_of_kind_8() { return &___kind_8; }
	inline void set_kind_8(int32_t value)
	{
		___kind_8 = value;
	}

	inline static int32_t get_offset_of_zoneHour_9() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___zoneHour_9)); }
	inline int32_t get_zoneHour_9() const { return ___zoneHour_9; }
	inline int32_t* get_address_of_zoneHour_9() { return &___zoneHour_9; }
	inline void set_zoneHour_9(int32_t value)
	{
		___zoneHour_9 = value;
	}

	inline static int32_t get_offset_of_zoneMinute_10() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___zoneMinute_10)); }
	inline int32_t get_zoneMinute_10() const { return ___zoneMinute_10; }
	inline int32_t* get_address_of_zoneMinute_10() { return &___zoneMinute_10; }
	inline void set_zoneMinute_10(int32_t value)
	{
		___zoneMinute_10 = value;
	}

	inline static int32_t get_offset_of_text_11() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___text_11)); }
	inline String_t* get_text_11() const { return ___text_11; }
	inline String_t** get_address_of_text_11() { return &___text_11; }
	inline void set_text_11(String_t* value)
	{
		___text_11 = value;
		Il2CppCodeGenWriteBarrier((&___text_11), value);
	}

	inline static int32_t get_offset_of_length_12() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___length_12)); }
	inline int32_t get_length_12() const { return ___length_12; }
	inline int32_t* get_address_of_length_12() { return &___length_12; }
	inline void set_length_12(int32_t value)
	{
		___length_12 = value;
	}
};

struct Parser_t2170101039_StaticFields
{
public:
	// System.Int32[] System.Xml.Schema.XsdDateTime/Parser::Power10
	Int32U5BU5D_t3030399641* ___Power10_13;

public:
	inline static int32_t get_offset_of_Power10_13() { return static_cast<int32_t>(offsetof(Parser_t2170101039_StaticFields, ___Power10_13)); }
	inline Int32U5BU5D_t3030399641* get_Power10_13() const { return ___Power10_13; }
	inline Int32U5BU5D_t3030399641** get_address_of_Power10_13() { return &___Power10_13; }
	inline void set_Power10_13(Int32U5BU5D_t3030399641* value)
	{
		___Power10_13 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XsdDateTime/Parser
struct Parser_t2170101039_marshaled_pinvoke
{
	int32_t ___typeCode_0;
	int32_t ___year_1;
	int32_t ___month_2;
	int32_t ___day_3;
	int32_t ___hour_4;
	int32_t ___minute_5;
	int32_t ___second_6;
	int32_t ___fraction_7;
	int32_t ___kind_8;
	int32_t ___zoneHour_9;
	int32_t ___zoneMinute_10;
	char* ___text_11;
	int32_t ___length_12;
};
// Native definition for COM marshalling of System.Xml.Schema.XsdDateTime/Parser
struct Parser_t2170101039_marshaled_com
{
	int32_t ___typeCode_0;
	int32_t ___year_1;
	int32_t ___month_2;
	int32_t ___day_3;
	int32_t ___hour_4;
	int32_t ___minute_5;
	int32_t ___second_6;
	int32_t ___fraction_7;
	int32_t ___kind_8;
	int32_t ___zoneHour_9;
	int32_t ___zoneMinute_10;
	Il2CppChar* ___text_11;
	int32_t ___length_12;
};
#endif // PARSER_T2170101039_H
#ifndef HANDSHAKEMESSAGE_T3938752374_H
#define HANDSHAKEMESSAGE_T3938752374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct  HandshakeMessage_t3938752374  : public TlsStream_t4089752859
{
public:
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::context
	Context_t4285182719 * ___context_8;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::handshakeType
	uint8_t ___handshakeType_9;
	// Mono.Security.Protocol.Tls.ContentType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::contentType
	uint8_t ___contentType_10;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::cache
	ByteU5BU5D_t3397334013* ___cache_11;

public:
	inline static int32_t get_offset_of_context_8() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___context_8)); }
	inline Context_t4285182719 * get_context_8() const { return ___context_8; }
	inline Context_t4285182719 ** get_address_of_context_8() { return &___context_8; }
	inline void set_context_8(Context_t4285182719 * value)
	{
		___context_8 = value;
		Il2CppCodeGenWriteBarrier((&___context_8), value);
	}

	inline static int32_t get_offset_of_handshakeType_9() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___handshakeType_9)); }
	inline uint8_t get_handshakeType_9() const { return ___handshakeType_9; }
	inline uint8_t* get_address_of_handshakeType_9() { return &___handshakeType_9; }
	inline void set_handshakeType_9(uint8_t value)
	{
		___handshakeType_9 = value;
	}

	inline static int32_t get_offset_of_contentType_10() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___contentType_10)); }
	inline uint8_t get_contentType_10() const { return ___contentType_10; }
	inline uint8_t* get_address_of_contentType_10() { return &___contentType_10; }
	inline void set_contentType_10(uint8_t value)
	{
		___contentType_10 = value;
	}

	inline static int32_t get_offset_of_cache_11() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___cache_11)); }
	inline ByteU5BU5D_t3397334013* get_cache_11() const { return ___cache_11; }
	inline ByteU5BU5D_t3397334013** get_address_of_cache_11() { return &___cache_11; }
	inline void set_cache_11(ByteU5BU5D_t3397334013* value)
	{
		___cache_11 = value;
		Il2CppCodeGenWriteBarrier((&___cache_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEMESSAGE_T3938752374_H
#ifndef CONTEXT_T4285182719_H
#define CONTEXT_T4285182719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Context
struct  Context_t4285182719  : public RuntimeObject
{
public:
	// Mono.Security.Protocol.Tls.SecurityProtocolType Mono.Security.Protocol.Tls.Context::securityProtocol
	int32_t ___securityProtocol_0;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::sessionId
	ByteU5BU5D_t3397334013* ___sessionId_1;
	// Mono.Security.Protocol.Tls.SecurityCompressionType Mono.Security.Protocol.Tls.Context::compressionMethod
	int32_t ___compressionMethod_2;
	// Mono.Security.Protocol.Tls.TlsServerSettings Mono.Security.Protocol.Tls.Context::serverSettings
	TlsServerSettings_t403340211 * ___serverSettings_3;
	// Mono.Security.Protocol.Tls.TlsClientSettings Mono.Security.Protocol.Tls.Context::clientSettings
	TlsClientSettings_t2311449551 * ___clientSettings_4;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::current
	SecurityParameters_t2290372928 * ___current_5;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::negotiating
	SecurityParameters_t2290372928 * ___negotiating_6;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::read
	SecurityParameters_t2290372928 * ___read_7;
	// Mono.Security.Protocol.Tls.SecurityParameters Mono.Security.Protocol.Tls.Context::write
	SecurityParameters_t2290372928 * ___write_8;
	// Mono.Security.Protocol.Tls.CipherSuiteCollection Mono.Security.Protocol.Tls.Context::supportedCiphers
	CipherSuiteCollection_t2431504453 * ___supportedCiphers_9;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeType Mono.Security.Protocol.Tls.Context::lastHandshakeMsg
	uint8_t ___lastHandshakeMsg_10;
	// Mono.Security.Protocol.Tls.HandshakeState Mono.Security.Protocol.Tls.Context::handshakeState
	int32_t ___handshakeState_11;
	// System.Boolean Mono.Security.Protocol.Tls.Context::abbreviatedHandshake
	bool ___abbreviatedHandshake_12;
	// System.Boolean Mono.Security.Protocol.Tls.Context::receivedConnectionEnd
	bool ___receivedConnectionEnd_13;
	// System.Boolean Mono.Security.Protocol.Tls.Context::sentConnectionEnd
	bool ___sentConnectionEnd_14;
	// System.Boolean Mono.Security.Protocol.Tls.Context::protocolNegotiated
	bool ___protocolNegotiated_15;
	// System.UInt64 Mono.Security.Protocol.Tls.Context::writeSequenceNumber
	uint64_t ___writeSequenceNumber_16;
	// System.UInt64 Mono.Security.Protocol.Tls.Context::readSequenceNumber
	uint64_t ___readSequenceNumber_17;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::clientRandom
	ByteU5BU5D_t3397334013* ___clientRandom_18;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::serverRandom
	ByteU5BU5D_t3397334013* ___serverRandom_19;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::randomCS
	ByteU5BU5D_t3397334013* ___randomCS_20;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::randomSC
	ByteU5BU5D_t3397334013* ___randomSC_21;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::masterSecret
	ByteU5BU5D_t3397334013* ___masterSecret_22;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::clientWriteKey
	ByteU5BU5D_t3397334013* ___clientWriteKey_23;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::serverWriteKey
	ByteU5BU5D_t3397334013* ___serverWriteKey_24;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::clientWriteIV
	ByteU5BU5D_t3397334013* ___clientWriteIV_25;
	// System.Byte[] Mono.Security.Protocol.Tls.Context::serverWriteIV
	ByteU5BU5D_t3397334013* ___serverWriteIV_26;
	// Mono.Security.Protocol.Tls.TlsStream Mono.Security.Protocol.Tls.Context::handshakeMessages
	TlsStream_t4089752859 * ___handshakeMessages_27;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Protocol.Tls.Context::random
	RandomNumberGenerator_t2510243513 * ___random_28;
	// Mono.Security.Protocol.Tls.RecordProtocol Mono.Security.Protocol.Tls.Context::recordProtocol
	RecordProtocol_t3166895267 * ___recordProtocol_29;
	// System.Boolean Mono.Security.Protocol.Tls.Context::<ChangeCipherSpecDone>k__BackingField
	bool ___U3CChangeCipherSpecDoneU3Ek__BackingField_30;

public:
	inline static int32_t get_offset_of_securityProtocol_0() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___securityProtocol_0)); }
	inline int32_t get_securityProtocol_0() const { return ___securityProtocol_0; }
	inline int32_t* get_address_of_securityProtocol_0() { return &___securityProtocol_0; }
	inline void set_securityProtocol_0(int32_t value)
	{
		___securityProtocol_0 = value;
	}

	inline static int32_t get_offset_of_sessionId_1() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___sessionId_1)); }
	inline ByteU5BU5D_t3397334013* get_sessionId_1() const { return ___sessionId_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_sessionId_1() { return &___sessionId_1; }
	inline void set_sessionId_1(ByteU5BU5D_t3397334013* value)
	{
		___sessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___sessionId_1), value);
	}

	inline static int32_t get_offset_of_compressionMethod_2() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___compressionMethod_2)); }
	inline int32_t get_compressionMethod_2() const { return ___compressionMethod_2; }
	inline int32_t* get_address_of_compressionMethod_2() { return &___compressionMethod_2; }
	inline void set_compressionMethod_2(int32_t value)
	{
		___compressionMethod_2 = value;
	}

	inline static int32_t get_offset_of_serverSettings_3() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___serverSettings_3)); }
	inline TlsServerSettings_t403340211 * get_serverSettings_3() const { return ___serverSettings_3; }
	inline TlsServerSettings_t403340211 ** get_address_of_serverSettings_3() { return &___serverSettings_3; }
	inline void set_serverSettings_3(TlsServerSettings_t403340211 * value)
	{
		___serverSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___serverSettings_3), value);
	}

	inline static int32_t get_offset_of_clientSettings_4() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___clientSettings_4)); }
	inline TlsClientSettings_t2311449551 * get_clientSettings_4() const { return ___clientSettings_4; }
	inline TlsClientSettings_t2311449551 ** get_address_of_clientSettings_4() { return &___clientSettings_4; }
	inline void set_clientSettings_4(TlsClientSettings_t2311449551 * value)
	{
		___clientSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&___clientSettings_4), value);
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___current_5)); }
	inline SecurityParameters_t2290372928 * get_current_5() const { return ___current_5; }
	inline SecurityParameters_t2290372928 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(SecurityParameters_t2290372928 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_negotiating_6() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___negotiating_6)); }
	inline SecurityParameters_t2290372928 * get_negotiating_6() const { return ___negotiating_6; }
	inline SecurityParameters_t2290372928 ** get_address_of_negotiating_6() { return &___negotiating_6; }
	inline void set_negotiating_6(SecurityParameters_t2290372928 * value)
	{
		___negotiating_6 = value;
		Il2CppCodeGenWriteBarrier((&___negotiating_6), value);
	}

	inline static int32_t get_offset_of_read_7() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___read_7)); }
	inline SecurityParameters_t2290372928 * get_read_7() const { return ___read_7; }
	inline SecurityParameters_t2290372928 ** get_address_of_read_7() { return &___read_7; }
	inline void set_read_7(SecurityParameters_t2290372928 * value)
	{
		___read_7 = value;
		Il2CppCodeGenWriteBarrier((&___read_7), value);
	}

	inline static int32_t get_offset_of_write_8() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___write_8)); }
	inline SecurityParameters_t2290372928 * get_write_8() const { return ___write_8; }
	inline SecurityParameters_t2290372928 ** get_address_of_write_8() { return &___write_8; }
	inline void set_write_8(SecurityParameters_t2290372928 * value)
	{
		___write_8 = value;
		Il2CppCodeGenWriteBarrier((&___write_8), value);
	}

	inline static int32_t get_offset_of_supportedCiphers_9() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___supportedCiphers_9)); }
	inline CipherSuiteCollection_t2431504453 * get_supportedCiphers_9() const { return ___supportedCiphers_9; }
	inline CipherSuiteCollection_t2431504453 ** get_address_of_supportedCiphers_9() { return &___supportedCiphers_9; }
	inline void set_supportedCiphers_9(CipherSuiteCollection_t2431504453 * value)
	{
		___supportedCiphers_9 = value;
		Il2CppCodeGenWriteBarrier((&___supportedCiphers_9), value);
	}

	inline static int32_t get_offset_of_lastHandshakeMsg_10() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___lastHandshakeMsg_10)); }
	inline uint8_t get_lastHandshakeMsg_10() const { return ___lastHandshakeMsg_10; }
	inline uint8_t* get_address_of_lastHandshakeMsg_10() { return &___lastHandshakeMsg_10; }
	inline void set_lastHandshakeMsg_10(uint8_t value)
	{
		___lastHandshakeMsg_10 = value;
	}

	inline static int32_t get_offset_of_handshakeState_11() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___handshakeState_11)); }
	inline int32_t get_handshakeState_11() const { return ___handshakeState_11; }
	inline int32_t* get_address_of_handshakeState_11() { return &___handshakeState_11; }
	inline void set_handshakeState_11(int32_t value)
	{
		___handshakeState_11 = value;
	}

	inline static int32_t get_offset_of_abbreviatedHandshake_12() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___abbreviatedHandshake_12)); }
	inline bool get_abbreviatedHandshake_12() const { return ___abbreviatedHandshake_12; }
	inline bool* get_address_of_abbreviatedHandshake_12() { return &___abbreviatedHandshake_12; }
	inline void set_abbreviatedHandshake_12(bool value)
	{
		___abbreviatedHandshake_12 = value;
	}

	inline static int32_t get_offset_of_receivedConnectionEnd_13() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___receivedConnectionEnd_13)); }
	inline bool get_receivedConnectionEnd_13() const { return ___receivedConnectionEnd_13; }
	inline bool* get_address_of_receivedConnectionEnd_13() { return &___receivedConnectionEnd_13; }
	inline void set_receivedConnectionEnd_13(bool value)
	{
		___receivedConnectionEnd_13 = value;
	}

	inline static int32_t get_offset_of_sentConnectionEnd_14() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___sentConnectionEnd_14)); }
	inline bool get_sentConnectionEnd_14() const { return ___sentConnectionEnd_14; }
	inline bool* get_address_of_sentConnectionEnd_14() { return &___sentConnectionEnd_14; }
	inline void set_sentConnectionEnd_14(bool value)
	{
		___sentConnectionEnd_14 = value;
	}

	inline static int32_t get_offset_of_protocolNegotiated_15() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___protocolNegotiated_15)); }
	inline bool get_protocolNegotiated_15() const { return ___protocolNegotiated_15; }
	inline bool* get_address_of_protocolNegotiated_15() { return &___protocolNegotiated_15; }
	inline void set_protocolNegotiated_15(bool value)
	{
		___protocolNegotiated_15 = value;
	}

	inline static int32_t get_offset_of_writeSequenceNumber_16() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___writeSequenceNumber_16)); }
	inline uint64_t get_writeSequenceNumber_16() const { return ___writeSequenceNumber_16; }
	inline uint64_t* get_address_of_writeSequenceNumber_16() { return &___writeSequenceNumber_16; }
	inline void set_writeSequenceNumber_16(uint64_t value)
	{
		___writeSequenceNumber_16 = value;
	}

	inline static int32_t get_offset_of_readSequenceNumber_17() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___readSequenceNumber_17)); }
	inline uint64_t get_readSequenceNumber_17() const { return ___readSequenceNumber_17; }
	inline uint64_t* get_address_of_readSequenceNumber_17() { return &___readSequenceNumber_17; }
	inline void set_readSequenceNumber_17(uint64_t value)
	{
		___readSequenceNumber_17 = value;
	}

	inline static int32_t get_offset_of_clientRandom_18() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___clientRandom_18)); }
	inline ByteU5BU5D_t3397334013* get_clientRandom_18() const { return ___clientRandom_18; }
	inline ByteU5BU5D_t3397334013** get_address_of_clientRandom_18() { return &___clientRandom_18; }
	inline void set_clientRandom_18(ByteU5BU5D_t3397334013* value)
	{
		___clientRandom_18 = value;
		Il2CppCodeGenWriteBarrier((&___clientRandom_18), value);
	}

	inline static int32_t get_offset_of_serverRandom_19() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___serverRandom_19)); }
	inline ByteU5BU5D_t3397334013* get_serverRandom_19() const { return ___serverRandom_19; }
	inline ByteU5BU5D_t3397334013** get_address_of_serverRandom_19() { return &___serverRandom_19; }
	inline void set_serverRandom_19(ByteU5BU5D_t3397334013* value)
	{
		___serverRandom_19 = value;
		Il2CppCodeGenWriteBarrier((&___serverRandom_19), value);
	}

	inline static int32_t get_offset_of_randomCS_20() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___randomCS_20)); }
	inline ByteU5BU5D_t3397334013* get_randomCS_20() const { return ___randomCS_20; }
	inline ByteU5BU5D_t3397334013** get_address_of_randomCS_20() { return &___randomCS_20; }
	inline void set_randomCS_20(ByteU5BU5D_t3397334013* value)
	{
		___randomCS_20 = value;
		Il2CppCodeGenWriteBarrier((&___randomCS_20), value);
	}

	inline static int32_t get_offset_of_randomSC_21() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___randomSC_21)); }
	inline ByteU5BU5D_t3397334013* get_randomSC_21() const { return ___randomSC_21; }
	inline ByteU5BU5D_t3397334013** get_address_of_randomSC_21() { return &___randomSC_21; }
	inline void set_randomSC_21(ByteU5BU5D_t3397334013* value)
	{
		___randomSC_21 = value;
		Il2CppCodeGenWriteBarrier((&___randomSC_21), value);
	}

	inline static int32_t get_offset_of_masterSecret_22() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___masterSecret_22)); }
	inline ByteU5BU5D_t3397334013* get_masterSecret_22() const { return ___masterSecret_22; }
	inline ByteU5BU5D_t3397334013** get_address_of_masterSecret_22() { return &___masterSecret_22; }
	inline void set_masterSecret_22(ByteU5BU5D_t3397334013* value)
	{
		___masterSecret_22 = value;
		Il2CppCodeGenWriteBarrier((&___masterSecret_22), value);
	}

	inline static int32_t get_offset_of_clientWriteKey_23() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___clientWriteKey_23)); }
	inline ByteU5BU5D_t3397334013* get_clientWriteKey_23() const { return ___clientWriteKey_23; }
	inline ByteU5BU5D_t3397334013** get_address_of_clientWriteKey_23() { return &___clientWriteKey_23; }
	inline void set_clientWriteKey_23(ByteU5BU5D_t3397334013* value)
	{
		___clientWriteKey_23 = value;
		Il2CppCodeGenWriteBarrier((&___clientWriteKey_23), value);
	}

	inline static int32_t get_offset_of_serverWriteKey_24() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___serverWriteKey_24)); }
	inline ByteU5BU5D_t3397334013* get_serverWriteKey_24() const { return ___serverWriteKey_24; }
	inline ByteU5BU5D_t3397334013** get_address_of_serverWriteKey_24() { return &___serverWriteKey_24; }
	inline void set_serverWriteKey_24(ByteU5BU5D_t3397334013* value)
	{
		___serverWriteKey_24 = value;
		Il2CppCodeGenWriteBarrier((&___serverWriteKey_24), value);
	}

	inline static int32_t get_offset_of_clientWriteIV_25() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___clientWriteIV_25)); }
	inline ByteU5BU5D_t3397334013* get_clientWriteIV_25() const { return ___clientWriteIV_25; }
	inline ByteU5BU5D_t3397334013** get_address_of_clientWriteIV_25() { return &___clientWriteIV_25; }
	inline void set_clientWriteIV_25(ByteU5BU5D_t3397334013* value)
	{
		___clientWriteIV_25 = value;
		Il2CppCodeGenWriteBarrier((&___clientWriteIV_25), value);
	}

	inline static int32_t get_offset_of_serverWriteIV_26() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___serverWriteIV_26)); }
	inline ByteU5BU5D_t3397334013* get_serverWriteIV_26() const { return ___serverWriteIV_26; }
	inline ByteU5BU5D_t3397334013** get_address_of_serverWriteIV_26() { return &___serverWriteIV_26; }
	inline void set_serverWriteIV_26(ByteU5BU5D_t3397334013* value)
	{
		___serverWriteIV_26 = value;
		Il2CppCodeGenWriteBarrier((&___serverWriteIV_26), value);
	}

	inline static int32_t get_offset_of_handshakeMessages_27() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___handshakeMessages_27)); }
	inline TlsStream_t4089752859 * get_handshakeMessages_27() const { return ___handshakeMessages_27; }
	inline TlsStream_t4089752859 ** get_address_of_handshakeMessages_27() { return &___handshakeMessages_27; }
	inline void set_handshakeMessages_27(TlsStream_t4089752859 * value)
	{
		___handshakeMessages_27 = value;
		Il2CppCodeGenWriteBarrier((&___handshakeMessages_27), value);
	}

	inline static int32_t get_offset_of_random_28() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___random_28)); }
	inline RandomNumberGenerator_t2510243513 * get_random_28() const { return ___random_28; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of_random_28() { return &___random_28; }
	inline void set_random_28(RandomNumberGenerator_t2510243513 * value)
	{
		___random_28 = value;
		Il2CppCodeGenWriteBarrier((&___random_28), value);
	}

	inline static int32_t get_offset_of_recordProtocol_29() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___recordProtocol_29)); }
	inline RecordProtocol_t3166895267 * get_recordProtocol_29() const { return ___recordProtocol_29; }
	inline RecordProtocol_t3166895267 ** get_address_of_recordProtocol_29() { return &___recordProtocol_29; }
	inline void set_recordProtocol_29(RecordProtocol_t3166895267 * value)
	{
		___recordProtocol_29 = value;
		Il2CppCodeGenWriteBarrier((&___recordProtocol_29), value);
	}

	inline static int32_t get_offset_of_U3CChangeCipherSpecDoneU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Context_t4285182719, ___U3CChangeCipherSpecDoneU3Ek__BackingField_30)); }
	inline bool get_U3CChangeCipherSpecDoneU3Ek__BackingField_30() const { return ___U3CChangeCipherSpecDoneU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CChangeCipherSpecDoneU3Ek__BackingField_30() { return &___U3CChangeCipherSpecDoneU3Ek__BackingField_30; }
	inline void set_U3CChangeCipherSpecDoneU3Ek__BackingField_30(bool value)
	{
		___U3CChangeCipherSpecDoneU3Ek__BackingField_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T4285182719_H
#ifndef CERTIFICATESELECTIONCALLBACK_T3721235490_H
#define CERTIFICATESELECTIONCALLBACK_T3721235490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct  CertificateSelectionCallback_t3721235490  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESELECTIONCALLBACK_T3721235490_H
#ifndef VALIDATIONRESULT_T2452252148_H
#define VALIDATIONRESULT_T2452252148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.ValidationResult
struct  ValidationResult_t2452252148  : public RuntimeObject
{
public:
	// System.Boolean Mono.Security.Interface.ValidationResult::trusted
	bool ___trusted_0;
	// System.Boolean Mono.Security.Interface.ValidationResult::user_denied
	bool ___user_denied_1;
	// System.Int32 Mono.Security.Interface.ValidationResult::error_code
	int32_t ___error_code_2;
	// System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors> Mono.Security.Interface.ValidationResult::policy_errors
	Nullable_1_t3179568147  ___policy_errors_3;

public:
	inline static int32_t get_offset_of_trusted_0() { return static_cast<int32_t>(offsetof(ValidationResult_t2452252148, ___trusted_0)); }
	inline bool get_trusted_0() const { return ___trusted_0; }
	inline bool* get_address_of_trusted_0() { return &___trusted_0; }
	inline void set_trusted_0(bool value)
	{
		___trusted_0 = value;
	}

	inline static int32_t get_offset_of_user_denied_1() { return static_cast<int32_t>(offsetof(ValidationResult_t2452252148, ___user_denied_1)); }
	inline bool get_user_denied_1() const { return ___user_denied_1; }
	inline bool* get_address_of_user_denied_1() { return &___user_denied_1; }
	inline void set_user_denied_1(bool value)
	{
		___user_denied_1 = value;
	}

	inline static int32_t get_offset_of_error_code_2() { return static_cast<int32_t>(offsetof(ValidationResult_t2452252148, ___error_code_2)); }
	inline int32_t get_error_code_2() const { return ___error_code_2; }
	inline int32_t* get_address_of_error_code_2() { return &___error_code_2; }
	inline void set_error_code_2(int32_t value)
	{
		___error_code_2 = value;
	}

	inline static int32_t get_offset_of_policy_errors_3() { return static_cast<int32_t>(offsetof(ValidationResult_t2452252148, ___policy_errors_3)); }
	inline Nullable_1_t3179568147  get_policy_errors_3() const { return ___policy_errors_3; }
	inline Nullable_1_t3179568147 * get_address_of_policy_errors_3() { return &___policy_errors_3; }
	inline void set_policy_errors_3(Nullable_1_t3179568147  value)
	{
		___policy_errors_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONRESULT_T2452252148_H
#ifndef CERTIFICATEVALIDATIONCALLBACK_T989458295_H
#define CERTIFICATEVALIDATIONCALLBACK_T989458295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct  CertificateValidationCallback_t989458295  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONCALLBACK_T989458295_H
#ifndef CERTIFICATEVALIDATIONCALLBACK2_T3318447433_H
#define CERTIFICATEVALIDATIONCALLBACK2_T3318447433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t3318447433  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONCALLBACK2_T3318447433_H
#ifndef PRIVATEKEYSELECTIONCALLBACK_T1663566523_H
#define PRIVATEKEYSELECTIONCALLBACK_T1663566523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct  PrivateKeySelectionCallback_t1663566523  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYSELECTIONCALLBACK_T1663566523_H
#ifndef TLSCIPHERSUITE_T396038680_H
#define TLSCIPHERSUITE_T396038680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsCipherSuite
struct  TlsCipherSuite_t396038680  : public CipherSuite_t491456551
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.TlsCipherSuite::header
	ByteU5BU5D_t3397334013* ___header_21;
	// System.Object Mono.Security.Protocol.Tls.TlsCipherSuite::headerLock
	RuntimeObject * ___headerLock_22;

public:
	inline static int32_t get_offset_of_header_21() { return static_cast<int32_t>(offsetof(TlsCipherSuite_t396038680, ___header_21)); }
	inline ByteU5BU5D_t3397334013* get_header_21() const { return ___header_21; }
	inline ByteU5BU5D_t3397334013** get_address_of_header_21() { return &___header_21; }
	inline void set_header_21(ByteU5BU5D_t3397334013* value)
	{
		___header_21 = value;
		Il2CppCodeGenWriteBarrier((&___header_21), value);
	}

	inline static int32_t get_offset_of_headerLock_22() { return static_cast<int32_t>(offsetof(TlsCipherSuite_t396038680, ___headerLock_22)); }
	inline RuntimeObject * get_headerLock_22() const { return ___headerLock_22; }
	inline RuntimeObject ** get_address_of_headerLock_22() { return &___headerLock_22; }
	inline void set_headerLock_22(RuntimeObject * value)
	{
		___headerLock_22 = value;
		Il2CppCodeGenWriteBarrier((&___headerLock_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCIPHERSUITE_T396038680_H
#ifndef SERVERCONTEXT_T3823737132_H
#define SERVERCONTEXT_T3823737132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ServerContext
struct  ServerContext_t3823737132  : public Context_t4285182719
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::request_client_certificate
	bool ___request_client_certificate_31;
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::clientCertificateRequired
	bool ___clientCertificateRequired_32;

public:
	inline static int32_t get_offset_of_request_client_certificate_31() { return static_cast<int32_t>(offsetof(ServerContext_t3823737132, ___request_client_certificate_31)); }
	inline bool get_request_client_certificate_31() const { return ___request_client_certificate_31; }
	inline bool* get_address_of_request_client_certificate_31() { return &___request_client_certificate_31; }
	inline void set_request_client_certificate_31(bool value)
	{
		___request_client_certificate_31 = value;
	}

	inline static int32_t get_offset_of_clientCertificateRequired_32() { return static_cast<int32_t>(offsetof(ServerContext_t3823737132, ___clientCertificateRequired_32)); }
	inline bool get_clientCertificateRequired_32() const { return ___clientCertificateRequired_32; }
	inline bool* get_address_of_clientCertificateRequired_32() { return &___clientCertificateRequired_32; }
	inline void set_clientCertificateRequired_32(bool value)
	{
		___clientCertificateRequired_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCONTEXT_T3823737132_H
#ifndef CLIENTCONTEXT_T3002158488_H
#define CLIENTCONTEXT_T3002158488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ClientContext
struct  ClientContext_t3002158488  : public Context_t4285182719
{
public:
	// Mono.Security.Protocol.Tls.SslClientStream Mono.Security.Protocol.Tls.ClientContext::sslStream
	SslClientStream_t3918817353 * ___sslStream_31;
	// System.Int16 Mono.Security.Protocol.Tls.ClientContext::clientHelloProtocol
	int16_t ___clientHelloProtocol_32;

public:
	inline static int32_t get_offset_of_sslStream_31() { return static_cast<int32_t>(offsetof(ClientContext_t3002158488, ___sslStream_31)); }
	inline SslClientStream_t3918817353 * get_sslStream_31() const { return ___sslStream_31; }
	inline SslClientStream_t3918817353 ** get_address_of_sslStream_31() { return &___sslStream_31; }
	inline void set_sslStream_31(SslClientStream_t3918817353 * value)
	{
		___sslStream_31 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_31), value);
	}

	inline static int32_t get_offset_of_clientHelloProtocol_32() { return static_cast<int32_t>(offsetof(ClientContext_t3002158488, ___clientHelloProtocol_32)); }
	inline int16_t get_clientHelloProtocol_32() const { return ___clientHelloProtocol_32; }
	inline int16_t* get_address_of_clientHelloProtocol_32() { return &___clientHelloProtocol_32; }
	inline void set_clientHelloProtocol_32(int16_t value)
	{
		___clientHelloProtocol_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONTEXT_T3002158488_H
#ifndef HTTPSCLIENTSTREAM_T3823629320_H
#define HTTPSCLIENTSTREAM_T3823629320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.HttpsClientStream
struct  HttpsClientStream_t3823629320  : public SslClientStream_t3918817353
{
public:
	// System.Net.HttpWebRequest Mono.Security.Protocol.Tls.HttpsClientStream::_request
	HttpWebRequest_t1951404513 * ____request_22;
	// System.Int32 Mono.Security.Protocol.Tls.HttpsClientStream::_status
	int32_t ____status_23;

public:
	inline static int32_t get_offset_of__request_22() { return static_cast<int32_t>(offsetof(HttpsClientStream_t3823629320, ____request_22)); }
	inline HttpWebRequest_t1951404513 * get__request_22() const { return ____request_22; }
	inline HttpWebRequest_t1951404513 ** get_address_of__request_22() { return &____request_22; }
	inline void set__request_22(HttpWebRequest_t1951404513 * value)
	{
		____request_22 = value;
		Il2CppCodeGenWriteBarrier((&____request_22), value);
	}

	inline static int32_t get_offset_of__status_23() { return static_cast<int32_t>(offsetof(HttpsClientStream_t3823629320, ____status_23)); }
	inline int32_t get__status_23() const { return ____status_23; }
	inline int32_t* get_address_of__status_23() { return &____status_23; }
	inline void set__status_23(int32_t value)
	{
		____status_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSCLIENTSTREAM_T3823629320_H
#ifndef MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T1929047274_H
#define MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T1929047274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct  MonoRemoteCertificateValidationCallback_t1929047274  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T1929047274_H
#ifndef SSLCIPHERSUITE_T1404755603_H
#define SSLCIPHERSUITE_T1404755603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslCipherSuite
struct  SslCipherSuite_t1404755603  : public CipherSuite_t491456551
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.SslCipherSuite::pad1
	ByteU5BU5D_t3397334013* ___pad1_21;
	// System.Byte[] Mono.Security.Protocol.Tls.SslCipherSuite::pad2
	ByteU5BU5D_t3397334013* ___pad2_22;
	// System.Byte[] Mono.Security.Protocol.Tls.SslCipherSuite::header
	ByteU5BU5D_t3397334013* ___header_23;

public:
	inline static int32_t get_offset_of_pad1_21() { return static_cast<int32_t>(offsetof(SslCipherSuite_t1404755603, ___pad1_21)); }
	inline ByteU5BU5D_t3397334013* get_pad1_21() const { return ___pad1_21; }
	inline ByteU5BU5D_t3397334013** get_address_of_pad1_21() { return &___pad1_21; }
	inline void set_pad1_21(ByteU5BU5D_t3397334013* value)
	{
		___pad1_21 = value;
		Il2CppCodeGenWriteBarrier((&___pad1_21), value);
	}

	inline static int32_t get_offset_of_pad2_22() { return static_cast<int32_t>(offsetof(SslCipherSuite_t1404755603, ___pad2_22)); }
	inline ByteU5BU5D_t3397334013* get_pad2_22() const { return ___pad2_22; }
	inline ByteU5BU5D_t3397334013** get_address_of_pad2_22() { return &___pad2_22; }
	inline void set_pad2_22(ByteU5BU5D_t3397334013* value)
	{
		___pad2_22 = value;
		Il2CppCodeGenWriteBarrier((&___pad2_22), value);
	}

	inline static int32_t get_offset_of_header_23() { return static_cast<int32_t>(offsetof(SslCipherSuite_t1404755603, ___header_23)); }
	inline ByteU5BU5D_t3397334013* get_header_23() const { return ___header_23; }
	inline ByteU5BU5D_t3397334013** get_address_of_header_23() { return &___header_23; }
	inline void set_header_23(ByteU5BU5D_t3397334013* value)
	{
		___header_23 = value;
		Il2CppCodeGenWriteBarrier((&___header_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCIPHERSUITE_T1404755603_H
#ifndef MONOTLSSETTINGS_T302829305_H
#define MONOTLSSETTINGS_T302829305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsSettings
struct  MonoTlsSettings_t302829305  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Security.Interface.MonoTlsSettings::<RemoteCertificateValidationCallback>k__BackingField
	MonoRemoteCertificateValidationCallback_t1929047274 * ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0;
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Security.Interface.MonoTlsSettings::<ClientCertificateSelectionCallback>k__BackingField
	MonoLocalCertificateSelectionCallback_t4080334132 * ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Security.Interface.MonoTlsSettings::<TrustAnchors>k__BackingField
	X509CertificateCollection_t1197680765 * ___U3CTrustAnchorsU3Ek__BackingField_2;
	// System.Object Mono.Security.Interface.MonoTlsSettings::<UserSettings>k__BackingField
	RuntimeObject * ___U3CUserSettingsU3Ek__BackingField_3;
	// System.Nullable`1<Mono.Security.Interface.TlsProtocols> Mono.Security.Interface.MonoTlsSettings::<EnabledProtocols>k__BackingField
	Nullable_1_t214512479  ___U3CEnabledProtocolsU3Ek__BackingField_4;
	// Mono.Security.Interface.CipherSuiteCode[] Mono.Security.Interface.MonoTlsSettings::<EnabledCiphers>k__BackingField
	CipherSuiteCodeU5BU5D_t4141109067* ___U3CEnabledCiphersU3Ek__BackingField_5;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::cloned
	bool ___cloned_6;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertName
	bool ___checkCertName_7;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_8;
	// System.Nullable`1<System.Boolean> Mono.Security.Interface.MonoTlsSettings::useServicePointManagerCallback
	Nullable_1_t2088641033  ___useServicePointManagerCallback_9;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::skipSystemValidators
	bool ___skipSystemValidators_10;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::callbackNeedsChain
	bool ___callbackNeedsChain_11;
	// Mono.Security.Interface.ICertificateValidator Mono.Security.Interface.MonoTlsSettings::certificateValidator
	RuntimeObject* ___certificateValidator_12;

public:
	inline static int32_t get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0)); }
	inline MonoRemoteCertificateValidationCallback_t1929047274 * get_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() const { return ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline MonoRemoteCertificateValidationCallback_t1929047274 ** get_address_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return &___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline void set_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(MonoRemoteCertificateValidationCallback_t1929047274 * value)
	{
		___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1)); }
	inline MonoLocalCertificateSelectionCallback_t4080334132 * get_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() const { return ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline MonoLocalCertificateSelectionCallback_t4080334132 ** get_address_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return &___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline void set_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(MonoLocalCertificateSelectionCallback_t4080334132 * value)
	{
		___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTrustAnchorsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CTrustAnchorsU3Ek__BackingField_2)); }
	inline X509CertificateCollection_t1197680765 * get_U3CTrustAnchorsU3Ek__BackingField_2() const { return ___U3CTrustAnchorsU3Ek__BackingField_2; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_U3CTrustAnchorsU3Ek__BackingField_2() { return &___U3CTrustAnchorsU3Ek__BackingField_2; }
	inline void set_U3CTrustAnchorsU3Ek__BackingField_2(X509CertificateCollection_t1197680765 * value)
	{
		___U3CTrustAnchorsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrustAnchorsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CUserSettingsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CUserSettingsU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CUserSettingsU3Ek__BackingField_3() const { return ___U3CUserSettingsU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CUserSettingsU3Ek__BackingField_3() { return &___U3CUserSettingsU3Ek__BackingField_3; }
	inline void set_U3CUserSettingsU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CUserSettingsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserSettingsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CEnabledProtocolsU3Ek__BackingField_4)); }
	inline Nullable_1_t214512479  get_U3CEnabledProtocolsU3Ek__BackingField_4() const { return ___U3CEnabledProtocolsU3Ek__BackingField_4; }
	inline Nullable_1_t214512479 * get_address_of_U3CEnabledProtocolsU3Ek__BackingField_4() { return &___U3CEnabledProtocolsU3Ek__BackingField_4; }
	inline void set_U3CEnabledProtocolsU3Ek__BackingField_4(Nullable_1_t214512479  value)
	{
		___U3CEnabledProtocolsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CEnabledCiphersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CEnabledCiphersU3Ek__BackingField_5)); }
	inline CipherSuiteCodeU5BU5D_t4141109067* get_U3CEnabledCiphersU3Ek__BackingField_5() const { return ___U3CEnabledCiphersU3Ek__BackingField_5; }
	inline CipherSuiteCodeU5BU5D_t4141109067** get_address_of_U3CEnabledCiphersU3Ek__BackingField_5() { return &___U3CEnabledCiphersU3Ek__BackingField_5; }
	inline void set_U3CEnabledCiphersU3Ek__BackingField_5(CipherSuiteCodeU5BU5D_t4141109067* value)
	{
		___U3CEnabledCiphersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnabledCiphersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_cloned_6() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___cloned_6)); }
	inline bool get_cloned_6() const { return ___cloned_6; }
	inline bool* get_address_of_cloned_6() { return &___cloned_6; }
	inline void set_cloned_6(bool value)
	{
		___cloned_6 = value;
	}

	inline static int32_t get_offset_of_checkCertName_7() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___checkCertName_7)); }
	inline bool get_checkCertName_7() const { return ___checkCertName_7; }
	inline bool* get_address_of_checkCertName_7() { return &___checkCertName_7; }
	inline void set_checkCertName_7(bool value)
	{
		___checkCertName_7 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_8() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___checkCertRevocationStatus_8)); }
	inline bool get_checkCertRevocationStatus_8() const { return ___checkCertRevocationStatus_8; }
	inline bool* get_address_of_checkCertRevocationStatus_8() { return &___checkCertRevocationStatus_8; }
	inline void set_checkCertRevocationStatus_8(bool value)
	{
		___checkCertRevocationStatus_8 = value;
	}

	inline static int32_t get_offset_of_useServicePointManagerCallback_9() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___useServicePointManagerCallback_9)); }
	inline Nullable_1_t2088641033  get_useServicePointManagerCallback_9() const { return ___useServicePointManagerCallback_9; }
	inline Nullable_1_t2088641033 * get_address_of_useServicePointManagerCallback_9() { return &___useServicePointManagerCallback_9; }
	inline void set_useServicePointManagerCallback_9(Nullable_1_t2088641033  value)
	{
		___useServicePointManagerCallback_9 = value;
	}

	inline static int32_t get_offset_of_skipSystemValidators_10() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___skipSystemValidators_10)); }
	inline bool get_skipSystemValidators_10() const { return ___skipSystemValidators_10; }
	inline bool* get_address_of_skipSystemValidators_10() { return &___skipSystemValidators_10; }
	inline void set_skipSystemValidators_10(bool value)
	{
		___skipSystemValidators_10 = value;
	}

	inline static int32_t get_offset_of_callbackNeedsChain_11() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___callbackNeedsChain_11)); }
	inline bool get_callbackNeedsChain_11() const { return ___callbackNeedsChain_11; }
	inline bool* get_address_of_callbackNeedsChain_11() { return &___callbackNeedsChain_11; }
	inline void set_callbackNeedsChain_11(bool value)
	{
		___callbackNeedsChain_11 = value;
	}

	inline static int32_t get_offset_of_certificateValidator_12() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___certificateValidator_12)); }
	inline RuntimeObject* get_certificateValidator_12() const { return ___certificateValidator_12; }
	inline RuntimeObject** get_address_of_certificateValidator_12() { return &___certificateValidator_12; }
	inline void set_certificateValidator_12(RuntimeObject* value)
	{
		___certificateValidator_12 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValidator_12), value);
	}
};

struct MonoTlsSettings_t302829305_StaticFields
{
public:
	// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::defaultSettings
	MonoTlsSettings_t302829305 * ___defaultSettings_13;

public:
	inline static int32_t get_offset_of_defaultSettings_13() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305_StaticFields, ___defaultSettings_13)); }
	inline MonoTlsSettings_t302829305 * get_defaultSettings_13() const { return ___defaultSettings_13; }
	inline MonoTlsSettings_t302829305 ** get_address_of_defaultSettings_13() { return &___defaultSettings_13; }
	inline void set_defaultSettings_13(MonoTlsSettings_t302829305 * value)
	{
		___defaultSettings_13 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSettings_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSSETTINGS_T302829305_H
#ifndef MONOLOCALCERTIFICATESELECTIONCALLBACK_T4080334132_H
#define MONOLOCALCERTIFICATESELECTIONCALLBACK_T4080334132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct  MonoLocalCertificateSelectionCallback_t4080334132  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOLOCALCERTIFICATESELECTIONCALLBACK_T4080334132_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (XsdDateTimeKind_t2077819616)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[5] = 
{
	XsdDateTimeKind_t2077819616::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (Parser_t2170101039)+ sizeof (RuntimeObject), sizeof(Parser_t2170101039_marshaled_pinvoke), sizeof(Parser_t2170101039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2101[14] = 
{
	Parser_t2170101039::get_offset_of_typeCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_year_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_month_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_day_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_hour_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_minute_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_second_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_fraction_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_kind_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_zoneHour_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_zoneMinute_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_text_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039::get_offset_of_length_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Parser_t2170101039_StaticFields::get_offset_of_Power10_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (XsdDuration_t2624036407)+ sizeof (RuntimeObject), sizeof(XsdDuration_t2624036407 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[7] = 
{
	XsdDuration_t2624036407::get_offset_of_years_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t2624036407::get_offset_of_months_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t2624036407::get_offset_of_days_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t2624036407::get_offset_of_hours_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t2624036407::get_offset_of_minutes_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t2624036407::get_offset_of_seconds_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XsdDuration_t2624036407::get_offset_of_nanoseconds_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (Parts_t3593783327)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2103[8] = 
{
	Parts_t3593783327::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (DurationType_t1846793513)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	DurationType_t1846793513::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (XsdValidator_t535151321), -1, sizeof(XsdValidator_t535151321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2105[20] = 
{
	XsdValidator_t535151321::get_offset_of_startIDConstraint_15(),
	XsdValidator_t535151321::get_offset_of_validationStack_16(),
	XsdValidator_t535151321::get_offset_of_attPresence_17(),
	XsdValidator_t535151321::get_offset_of_nsManager_18(),
	XsdValidator_t535151321::get_offset_of_bManageNamespaces_19(),
	XsdValidator_t535151321::get_offset_of_IDs_20(),
	XsdValidator_t535151321::get_offset_of_idRefListHead_21(),
	XsdValidator_t535151321::get_offset_of_inlineSchemaParser_22(),
	XsdValidator_t535151321::get_offset_of_processContents_23(),
	XsdValidator_t535151321_StaticFields::get_offset_of_dtCDATA_24(),
	XsdValidator_t535151321_StaticFields::get_offset_of_dtQName_25(),
	XsdValidator_t535151321_StaticFields::get_offset_of_dtStringArray_26(),
	XsdValidator_t535151321::get_offset_of_NsXmlNs_27(),
	XsdValidator_t535151321::get_offset_of_NsXs_28(),
	XsdValidator_t535151321::get_offset_of_NsXsi_29(),
	XsdValidator_t535151321::get_offset_of_XsiType_30(),
	XsdValidator_t535151321::get_offset_of_XsiNil_31(),
	XsdValidator_t535151321::get_offset_of_XsiSchemaLocation_32(),
	XsdValidator_t535151321::get_offset_of_XsiNoNamespaceSchemaLocation_33(),
	XsdValidator_t535151321::get_offset_of_XsdSchema_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (XmlConfigurationString_t3722747970), -1, sizeof(XmlConfigurationString_t3722747970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2106[2] = 
{
	XmlConfigurationString_t3722747970_StaticFields::get_offset_of_XmlReaderSectionPath_0(),
	XmlConfigurationString_t3722747970_StaticFields::get_offset_of_XsltSectionPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (XmlReaderSection_t3603186601), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305138), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2108[29] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U30701435C4E2C38EFE43C51BD22C114AB8B80124D_0(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U30F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U3161F91CE1721D8F16622810CBB39887D21C47031_2(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U3221CE291CD044114B4369175B9B91177F5932876_3(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U3360487BE4278986419B568EFD887F6145383168A_4(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U342DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_5(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U3485F43E332C2F7530815B17C08DAC169A8F697E0_6(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U349C5BA13401986EC93E4677F52CBE2248184DFBD_7(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U351E4CA1C2B009A2876C6E57D8E69E3502BCA3440_8(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U3574B9D4E4C39F6E8004181E5765B627B75EB1AD1_9(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U35BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_10(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_11(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U36A0D50D692745A6663128CD315B71079584F3E59_12(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U3702F6A3276CBE481D247A77C20B5459FB94D07D2_13(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U37A32E1A19C182315E4263A65A72066492550D8CD_14(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U38B4E5E81A88D29642679AFCE41DCA380F9000462_15(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U399F0664C2AC8464B51252D92FC24F3834C6FB90C_16(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_17(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U39E374D7263B2452E25DE3D6E617F6A728D98A439_18(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_19(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_20(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_B9F0004E3873FDDCABFDA6174EA18F0859B637B4_21(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_BAD037B714E1CD1052149B51238A3D4351DD10B5_22(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_23(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_DCF398750721AA7A27A6BA56E99350329B06E8B1_24(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_25(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_26(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_EE3413A2C088FF9432054D6E60A7CB6A498D25F0_27(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_FFE3F15642234E7FAD6951D432F1134D5AD15922_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (__StaticArrayInitTypeSizeU3D6_t2497549768)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t2497549768 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (__StaticArrayInitTypeSizeU3D12_t1381760541)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t1381760541 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (__StaticArrayInitTypeSizeU3D16_t1381760537)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_t1381760537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (__StaticArrayInitTypeSizeU3D20_t978476012)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t978476012 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (__StaticArrayInitTypeSizeU3D24_t978476008)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t978476008 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (__StaticArrayInitTypeSizeU3D28_t978476019)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t978476019 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2544559955)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2544559955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (__StaticArrayInitTypeSizeU3D36_t2544559951)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_t2544559951 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (__StaticArrayInitTypeSizeU3D40_t2141275426)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_t2141275426 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (__StaticArrayInitTypeSizeU3D44_t2141275422)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_t2141275422 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (__StaticArrayInitTypeSizeU3D56_t3707359365)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D56_t3707359365 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (__StaticArrayInitTypeSizeU3D64_t3304074836)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_t3304074836 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (__StaticArrayInitTypeSizeU3D68_t3304074847)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D68_t3304074847 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (__StaticArrayInitTypeSizeU3D112_t2631791551)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D112_t2631791551 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (__StaticArrayInitTypeSizeU3D144_t3794590962)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D144_t3794590962 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (__StaticArrayInitTypeSizeU3D416_t1045504634)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D416_t1045504634 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (U3CModuleU3E_t3783534216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (Locale_t4255929015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (MiniParser_t185565106), -1, sizeof(MiniParser_t185565106_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2127[7] = 
{
	MiniParser_t185565106_StaticFields::get_offset_of_INPUT_RANGE_0(),
	MiniParser_t185565106_StaticFields::get_offset_of_tbl_1(),
	MiniParser_t185565106_StaticFields::get_offset_of_errors_2(),
	MiniParser_t185565106::get_offset_of_line_3(),
	MiniParser_t185565106::get_offset_of_col_4(),
	MiniParser_t185565106::get_offset_of_twoCharBuff_5(),
	MiniParser_t185565106::get_offset_of_splitCData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (HandlerAdapter_t3228504856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (AttrListImpl_t383345538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[2] = 
{
	AttrListImpl_t383345538::get_offset_of_names_0(),
	AttrListImpl_t383345538::get_offset_of_values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (XMLError_t1680455686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[3] = 
{
	XMLError_t1680455686::get_offset_of_descr_16(),
	XMLError_t1680455686::get_offset_of_line_17(),
	XMLError_t1680455686::get_offset_of_column_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (SecurityParser_t30730986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[5] = 
{
	SecurityParser_t30730986::get_offset_of_root_7(),
	SecurityParser_t30730986::get_offset_of_xmldoc_8(),
	SecurityParser_t30730986::get_offset_of_pos_9(),
	SecurityParser_t30730986::get_offset_of_current_10(),
	SecurityParser_t30730986::get_offset_of_stack_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (ASN1_t924533536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[3] = 
{
	ASN1_t924533536::get_offset_of_m_nTag_0(),
	ASN1_t924533536::get_offset_of_m_aValue_1(),
	ASN1_t924533536::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (ASN1Convert_t3301846397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (BitConverterLE_t2825370261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (PKCS7_t3223261923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (ContentInfo_t1443605388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	ContentInfo_t1443605388::get_offset_of_contentType_0(),
	ContentInfo_t1443605388::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (EncryptedData_t2656813773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[4] = 
{
	EncryptedData_t2656813773::get_offset_of__version_0(),
	EncryptedData_t2656813773::get_offset_of__content_1(),
	EncryptedData_t2656813773::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2656813773::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (ValidationResult_t2452252148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[4] = 
{
	ValidationResult_t2452252148::get_offset_of_trusted_0(),
	ValidationResult_t2452252148::get_offset_of_user_denied_1(),
	ValidationResult_t2452252148::get_offset_of_error_code_2(),
	ValidationResult_t2452252148::get_offset_of_policy_errors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (CertificateValidationHelper_t1569212914), -1, sizeof(CertificateValidationHelper_t1569212914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[2] = 
{
	CertificateValidationHelper_t1569212914_StaticFields::get_offset_of_noX509Chain_0(),
	CertificateValidationHelper_t1569212914_StaticFields::get_offset_of_supportsTrustAnchors_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (CipherSuiteCode_t4229451518)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2146[267] = 
{
	CipherSuiteCode_t4229451518::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (MonoSslPolicyErrors_t621534536)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2148[5] = 
{
	MonoSslPolicyErrors_t621534536::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (MonoRemoteCertificateValidationCallback_t1929047274), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (MonoLocalCertificateSelectionCallback_t4080334132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (MonoTlsProvider_t823784021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (MonoTlsSettings_t302829305), -1, sizeof(MonoTlsSettings_t302829305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2152[14] = 
{
	MonoTlsSettings_t302829305::get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(),
	MonoTlsSettings_t302829305::get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(),
	MonoTlsSettings_t302829305::get_offset_of_U3CTrustAnchorsU3Ek__BackingField_2(),
	MonoTlsSettings_t302829305::get_offset_of_U3CUserSettingsU3Ek__BackingField_3(),
	MonoTlsSettings_t302829305::get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_4(),
	MonoTlsSettings_t302829305::get_offset_of_U3CEnabledCiphersU3Ek__BackingField_5(),
	MonoTlsSettings_t302829305::get_offset_of_cloned_6(),
	MonoTlsSettings_t302829305::get_offset_of_checkCertName_7(),
	MonoTlsSettings_t302829305::get_offset_of_checkCertRevocationStatus_8(),
	MonoTlsSettings_t302829305::get_offset_of_useServicePointManagerCallback_9(),
	MonoTlsSettings_t302829305::get_offset_of_skipSystemValidators_10(),
	MonoTlsSettings_t302829305::get_offset_of_callbackNeedsChain_11(),
	MonoTlsSettings_t302829305::get_offset_of_certificateValidator_12(),
	MonoTlsSettings_t302829305_StaticFields::get_offset_of_defaultSettings_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (TlsProtocols_t1951446164)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2153[13] = 
{
	TlsProtocols_t1951446164::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (AlertLevel_t1706602846)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2154[3] = 
{
	AlertLevel_t1706602846::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (AlertDescription_t844791462)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[25] = 
{
	AlertDescription_t844791462::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (Alert_t3405955216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[2] = 
{
	Alert_t3405955216::get_offset_of_level_0(),
	Alert_t3405955216::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (CipherAlgorithmType_t4212518094)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[8] = 
{
	CipherAlgorithmType_t4212518094::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (CipherSuite_t491456551), -1, sizeof(CipherSuite_t491456551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2158[21] = 
{
	CipherSuite_t491456551_StaticFields::get_offset_of_EmptyArray_0(),
	CipherSuite_t491456551::get_offset_of_code_1(),
	CipherSuite_t491456551::get_offset_of_name_2(),
	CipherSuite_t491456551::get_offset_of_cipherAlgorithmType_3(),
	CipherSuite_t491456551::get_offset_of_hashAlgorithmType_4(),
	CipherSuite_t491456551::get_offset_of_exchangeAlgorithmType_5(),
	CipherSuite_t491456551::get_offset_of_isExportable_6(),
	CipherSuite_t491456551::get_offset_of_cipherMode_7(),
	CipherSuite_t491456551::get_offset_of_keyMaterialSize_8(),
	CipherSuite_t491456551::get_offset_of_keyBlockSize_9(),
	CipherSuite_t491456551::get_offset_of_expandedKeyMaterialSize_10(),
	CipherSuite_t491456551::get_offset_of_effectiveKeyBits_11(),
	CipherSuite_t491456551::get_offset_of_ivSize_12(),
	CipherSuite_t491456551::get_offset_of_blockSize_13(),
	CipherSuite_t491456551::get_offset_of_context_14(),
	CipherSuite_t491456551::get_offset_of_encryptionAlgorithm_15(),
	CipherSuite_t491456551::get_offset_of_encryptionCipher_16(),
	CipherSuite_t491456551::get_offset_of_decryptionAlgorithm_17(),
	CipherSuite_t491456551::get_offset_of_decryptionCipher_18(),
	CipherSuite_t491456551::get_offset_of_clientHMAC_19(),
	CipherSuite_t491456551::get_offset_of_serverHMAC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (CipherSuiteCollection_t2431504453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[1] = 
{
	CipherSuiteCollection_t2431504453::get_offset_of_protocol_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (CipherSuiteFactory_t3273693255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (ClientContext_t3002158488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[2] = 
{
	ClientContext_t3002158488::get_offset_of_sslStream_31(),
	ClientContext_t3002158488::get_offset_of_clientHelloProtocol_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (ClientRecordProtocol_t2694504884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (ClientSessionInfo_t3468069089), -1, sizeof(ClientSessionInfo_t3468069089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2163[6] = 
{
	ClientSessionInfo_t3468069089_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t3468069089::get_offset_of_disposed_1(),
	ClientSessionInfo_t3468069089::get_offset_of_validuntil_2(),
	ClientSessionInfo_t3468069089::get_offset_of_host_3(),
	ClientSessionInfo_t3468069089::get_offset_of_sid_4(),
	ClientSessionInfo_t3468069089::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (ClientSessionCache_t3595945587), -1, sizeof(ClientSessionCache_t3595945587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[2] = 
{
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (ContentType_t859870085)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2165[5] = 
{
	ContentType_t859870085::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (Context_t4285182719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[31] = 
{
	Context_t4285182719::get_offset_of_securityProtocol_0(),
	Context_t4285182719::get_offset_of_sessionId_1(),
	Context_t4285182719::get_offset_of_compressionMethod_2(),
	Context_t4285182719::get_offset_of_serverSettings_3(),
	Context_t4285182719::get_offset_of_clientSettings_4(),
	Context_t4285182719::get_offset_of_current_5(),
	Context_t4285182719::get_offset_of_negotiating_6(),
	Context_t4285182719::get_offset_of_read_7(),
	Context_t4285182719::get_offset_of_write_8(),
	Context_t4285182719::get_offset_of_supportedCiphers_9(),
	Context_t4285182719::get_offset_of_lastHandshakeMsg_10(),
	Context_t4285182719::get_offset_of_handshakeState_11(),
	Context_t4285182719::get_offset_of_abbreviatedHandshake_12(),
	Context_t4285182719::get_offset_of_receivedConnectionEnd_13(),
	Context_t4285182719::get_offset_of_sentConnectionEnd_14(),
	Context_t4285182719::get_offset_of_protocolNegotiated_15(),
	Context_t4285182719::get_offset_of_writeSequenceNumber_16(),
	Context_t4285182719::get_offset_of_readSequenceNumber_17(),
	Context_t4285182719::get_offset_of_clientRandom_18(),
	Context_t4285182719::get_offset_of_serverRandom_19(),
	Context_t4285182719::get_offset_of_randomCS_20(),
	Context_t4285182719::get_offset_of_randomSC_21(),
	Context_t4285182719::get_offset_of_masterSecret_22(),
	Context_t4285182719::get_offset_of_clientWriteKey_23(),
	Context_t4285182719::get_offset_of_serverWriteKey_24(),
	Context_t4285182719::get_offset_of_clientWriteIV_25(),
	Context_t4285182719::get_offset_of_serverWriteIV_26(),
	Context_t4285182719::get_offset_of_handshakeMessages_27(),
	Context_t4285182719::get_offset_of_random_28(),
	Context_t4285182719::get_offset_of_recordProtocol_29(),
	Context_t4285182719::get_offset_of_U3CChangeCipherSpecDoneU3Ek__BackingField_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (ExchangeAlgorithmType_t954949548)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2167[6] = 
{
	ExchangeAlgorithmType_t954949548::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (HandshakeState_t1820731088)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2168[4] = 
{
	HandshakeState_t1820731088::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (HashAlgorithmType_t1654661965)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2169[4] = 
{
	HashAlgorithmType_t1654661965::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (HttpsClientStream_t3823629320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[2] = 
{
	HttpsClientStream_t3823629320::get_offset_of__request_22(),
	HttpsClientStream_t3823629320::get_offset_of__status_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (U3CU3Ec_t1881663454), -1, sizeof(U3CU3Ec_t1881663454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[3] = 
{
	U3CU3Ec_t1881663454_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1881663454_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_t1881663454_StaticFields::get_offset_of_U3CU3E9__2_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (RecordProtocol_t3166895267), -1, sizeof(RecordProtocol_t3166895267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2172[3] = 
{
	RecordProtocol_t3166895267_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3166895267::get_offset_of_innerStream_1(),
	RecordProtocol_t3166895267::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ReceiveRecordAsyncResult_t1946181211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[9] = 
{
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (SendRecordAsyncResult_t173216930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[7] = 
{
	SendRecordAsyncResult_t173216930::get_offset_of_locker_0(),
	SendRecordAsyncResult_t173216930::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t173216930::get_offset_of__userState_2(),
	SendRecordAsyncResult_t173216930::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t173216930::get_offset_of_handle_4(),
	SendRecordAsyncResult_t173216930::get_offset_of__message_5(),
	SendRecordAsyncResult_t173216930::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (RSASslSignatureFormatter_t1282301050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[2] = 
{
	RSASslSignatureFormatter_t1282301050::get_offset_of_key_0(),
	RSASslSignatureFormatter_t1282301050::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (SecurityCompressionType_t3722381418)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2176[3] = 
{
	SecurityCompressionType_t3722381418::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (SecurityParameters_t2290372928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[3] = 
{
	SecurityParameters_t2290372928::get_offset_of_cipher_0(),
	SecurityParameters_t2290372928::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2290372928::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (SecurityProtocolType_t155967584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2178[7] = 
{
	SecurityProtocolType_t155967584::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (ServerContext_t3823737132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	ServerContext_t3823737132::get_offset_of_request_client_certificate_31(),
	ServerContext_t3823737132::get_offset_of_clientCertificateRequired_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (CertificateValidationCallback_t989458295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (CertificateValidationCallback2_t3318447433), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (CertificateSelectionCallback_t3721235490), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (PrivateKeySelectionCallback_t1663566523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (SslClientStream_t3918817353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[4] = 
{
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation_18(),
	SslClientStream_t3918817353::get_offset_of_ClientCertSelection_19(),
	SslClientStream_t3918817353::get_offset_of_PrivateKeySelection_20(),
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation2_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (NegotiateState_t62894417)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2185[8] = 
{
	NegotiateState_t62894417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (NegotiateAsyncResult_t3602869449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[7] = 
{
	NegotiateAsyncResult_t3602869449::get_offset_of_locker_0(),
	NegotiateAsyncResult_t3602869449::get_offset_of__userCallback_1(),
	NegotiateAsyncResult_t3602869449::get_offset_of__userState_2(),
	NegotiateAsyncResult_t3602869449::get_offset_of__asyncException_3(),
	NegotiateAsyncResult_t3602869449::get_offset_of_handle_4(),
	NegotiateAsyncResult_t3602869449::get_offset_of__state_5(),
	NegotiateAsyncResult_t3602869449::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (SslCipherSuite_t1404755603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[3] = 
{
	SslCipherSuite_t1404755603::get_offset_of_pad1_21(),
	SslCipherSuite_t1404755603::get_offset_of_pad2_22(),
	SslCipherSuite_t1404755603::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (SslHandshakeHash_t3044322977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[8] = 
{
	SslHandshakeHash_t3044322977::get_offset_of_md5_4(),
	SslHandshakeHash_t3044322977::get_offset_of_sha_5(),
	SslHandshakeHash_t3044322977::get_offset_of_hashing_6(),
	SslHandshakeHash_t3044322977::get_offset_of_secret_7(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (SslServerStream_t330206493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	SslServerStream_t330206493::get_offset_of_ClientCertValidation_18(),
	SslServerStream_t330206493::get_offset_of_PrivateKeySelection_19(),
	SslServerStream_t330206493::get_offset_of_ClientCertValidation2_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (SslStreamBase_t934199321), -1, sizeof(SslStreamBase_t934199321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2190[14] = 
{
	SslStreamBase_t934199321_StaticFields::get_offset_of_record_processing_4(),
	SslStreamBase_t934199321::get_offset_of_innerStream_5(),
	SslStreamBase_t934199321::get_offset_of_inputBuffer_6(),
	SslStreamBase_t934199321::get_offset_of_context_7(),
	SslStreamBase_t934199321::get_offset_of_protocol_8(),
	SslStreamBase_t934199321::get_offset_of_ownsStream_9(),
	SslStreamBase_t934199321::get_offset_of_disposed_10(),
	SslStreamBase_t934199321::get_offset_of_checkCertRevocationStatus_11(),
	SslStreamBase_t934199321::get_offset_of_negotiate_12(),
	SslStreamBase_t934199321::get_offset_of_read_13(),
	SslStreamBase_t934199321::get_offset_of_write_14(),
	SslStreamBase_t934199321::get_offset_of_negotiationComplete_15(),
	SslStreamBase_t934199321::get_offset_of_recbuf_16(),
	SslStreamBase_t934199321::get_offset_of_recordStream_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (InternalAsyncResult_t1610391122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[12] = 
{
	InternalAsyncResult_t1610391122::get_offset_of_locker_0(),
	InternalAsyncResult_t1610391122::get_offset_of__userCallback_1(),
	InternalAsyncResult_t1610391122::get_offset_of__userState_2(),
	InternalAsyncResult_t1610391122::get_offset_of__asyncException_3(),
	InternalAsyncResult_t1610391122::get_offset_of_handle_4(),
	InternalAsyncResult_t1610391122::get_offset_of_completed_5(),
	InternalAsyncResult_t1610391122::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t1610391122::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t1610391122::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t1610391122::get_offset_of__buffer_9(),
	InternalAsyncResult_t1610391122::get_offset_of__offset_10(),
	InternalAsyncResult_t1610391122::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (TlsCipherSuite_t396038680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	TlsCipherSuite_t396038680::get_offset_of_header_21(),
	TlsCipherSuite_t396038680::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (TlsClientSettings_t2311449551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[4] = 
{
	TlsClientSettings_t2311449551::get_offset_of_targetHost_0(),
	TlsClientSettings_t2311449551::get_offset_of_certificates_1(),
	TlsClientSettings_t2311449551::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2311449551::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (TlsException_t583514812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[1] = 
{
	TlsException_t583514812::get_offset_of_alert_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (TlsServerSettings_t403340211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[7] = 
{
	TlsServerSettings_t403340211::get_offset_of_certificates_0(),
	TlsServerSettings_t403340211::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t403340211::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t403340211::get_offset_of_distinguisedNames_3(),
	TlsServerSettings_t403340211::get_offset_of_serverKeyExchange_4(),
	TlsServerSettings_t403340211::get_offset_of_certificateRequest_5(),
	TlsServerSettings_t403340211::get_offset_of_certificateTypes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (TlsStream_t4089752859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[4] = 
{
	TlsStream_t4089752859::get_offset_of_canRead_4(),
	TlsStream_t4089752859::get_offset_of_canWrite_5(),
	TlsStream_t4089752859::get_offset_of_buffer_6(),
	TlsStream_t4089752859::get_offset_of_temp_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (ClientCertificateType_t4001384466)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	ClientCertificateType_t4001384466::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (HandshakeMessage_t3938752374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[4] = 
{
	HandshakeMessage_t3938752374::get_offset_of_context_8(),
	HandshakeMessage_t3938752374::get_offset_of_handshakeType_9(),
	HandshakeMessage_t3938752374::get_offset_of_contentType_10(),
	HandshakeMessage_t3938752374::get_offset_of_cache_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (HandshakeType_t2540099417)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[12] = 
{
	HandshakeType_t2540099417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
