﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t3718216671;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Text.RegularExpressions.RegexCharClass
struct RegexCharClass_t2441867401;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Text.RegularExpressions.RegexFC[]
struct RegexFCU5BU5D_t2364340679;
// System.Net.WebProxy
struct WebProxy_t1169192840;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t939014605;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t2272681992;
// System.Net.CredentialCache
struct CredentialCache_t1992799279;
// System.Net.ICredentials[]
struct ICredentialsU5BU5D_t196403748;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Version
struct Version_t1755874712;
// System.Uri
struct Uri_t19570940;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Security.SecureString
struct SecureString_t412202620;
// System.Net.IWebRequestCreate
struct IWebRequestCreate_t3933815702;
// System.Type
struct Type_t;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Net.HeaderParser
struct HeaderParser_t3706119548;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1656058977;
// System.Text.RegularExpressions.RegexRunner
struct RegexRunner_t3983612747;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t2469392150;
// System.Text.RegularExpressions.RegexRunnerFactory
struct RegexRunnerFactory_t3902733837;
// System.Text.RegularExpressions.ExclusiveReference
struct ExclusiveReference_t708182869;
// System.Text.RegularExpressions.SharedReference
struct SharedReference_t2137668360;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t1975884510;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t2217612696;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct NameObjectEntry_t4229094479;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t633582367;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.StringComparer
struct StringComparer_t1574862926;
// System.Security.Cryptography.Oid
struct Oid_t3221867120;
// System.Net.HeaderInfo
struct HeaderInfo_t3044570949;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t1013837165;
// System.Text.RegularExpressions.RegexBoyerMoore
struct RegexBoyerMoore_t2204811018;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.WeakReference
struct WeakReference_t1077405567;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange>
struct List_1_t3163364420;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String[0...,0...]
struct StringU5B0___U2C0___U5D_t1642385973;
// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping[]
struct LowerCaseMappingU5BU5D_t882374823;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Void
struct Void_t1841601450;
// System.Text.RegularExpressions.NoParamDelegate
struct NoParamDelegate_t2890182105;
// System.Text.RegularExpressions.FindFirstCharDelegate
struct FindFirstCharDelegate_t4203859986;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t3307743052;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.UInt16[]
struct UInt16U5BU5D_t2527266722;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;
// System.Text.RegularExpressions.RegexNode
struct RegexNode_t2469392321;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions>
struct List_1_t1787380859;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t1108969367;
// Mono.Security.X509.X509Store
struct X509Store_t4028973564;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t99948092;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t2116499186;
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t196522893;
// System.Text.RegularExpressions.RegexCompiler/BacktrackNote[]
struct BacktrackNoteU5BU5D_t2230749527;
// System.Net.TimerThread/Queue
struct Queue_t1332364135;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;
// System.Net.Cache.RequestCacheProtocol
struct RequestCacheProtocol_t2110185277;
// System.Net.Cache.RequestCacheBinding
struct RequestCacheBinding_t114276176;
// System.Net.WebRequest/DesignerWebRequestCreate
struct DesignerWebRequestCreate_t3086960480;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry>
struct LinkedList_1_t3858529280;
// System.Net.WebResponse
struct WebResponse_t1895226051;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.Net.HeaderInfoTable
struct HeaderInfoTable_t2462863175;
// System.SByte[]
struct SByteU5BU5D_t3472287392;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Net.WebHeaderCollection/RfcChar[]
struct RfcCharU5BU5D_t2287831188;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexNode>
struct List_1_t1838513453;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;

struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MATCHENUMERATOR_T3726517973_H
#define MATCHENUMERATOR_T3726517973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchEnumerator
struct  MatchEnumerator_t3726517973  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchEnumerator::_matchcoll
	MatchCollection_t3718216671 * ____matchcoll_0;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchEnumerator::_match
	Match_t3164245899 * ____match_1;
	// System.Int32 System.Text.RegularExpressions.MatchEnumerator::_curindex
	int32_t ____curindex_2;
	// System.Boolean System.Text.RegularExpressions.MatchEnumerator::_done
	bool ____done_3;

public:
	inline static int32_t get_offset_of__matchcoll_0() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____matchcoll_0)); }
	inline MatchCollection_t3718216671 * get__matchcoll_0() const { return ____matchcoll_0; }
	inline MatchCollection_t3718216671 ** get_address_of__matchcoll_0() { return &____matchcoll_0; }
	inline void set__matchcoll_0(MatchCollection_t3718216671 * value)
	{
		____matchcoll_0 = value;
		Il2CppCodeGenWriteBarrier((&____matchcoll_0), value);
	}

	inline static int32_t get_offset_of__match_1() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____match_1)); }
	inline Match_t3164245899 * get__match_1() const { return ____match_1; }
	inline Match_t3164245899 ** get_address_of__match_1() { return &____match_1; }
	inline void set__match_1(Match_t3164245899 * value)
	{
		____match_1 = value;
		Il2CppCodeGenWriteBarrier((&____match_1), value);
	}

	inline static int32_t get_offset_of__curindex_2() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____curindex_2)); }
	inline int32_t get__curindex_2() const { return ____curindex_2; }
	inline int32_t* get_address_of__curindex_2() { return &____curindex_2; }
	inline void set__curindex_2(int32_t value)
	{
		____curindex_2 = value;
	}

	inline static int32_t get_offset_of__done_3() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____done_3)); }
	inline bool get__done_3() const { return ____done_3; }
	inline bool* get_address_of__done_3() { return &____done_3; }
	inline void set__done_3(bool value)
	{
		____done_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHENUMERATOR_T3726517973_H
#ifndef MATCHCOLLECTION_T3718216671_H
#define MATCHCOLLECTION_T3718216671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t3718216671  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.MatchCollection::_regex
	Regex_t1803876613 * ____regex_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::_matches
	ArrayList_t4252133567 * ____matches_1;
	// System.Boolean System.Text.RegularExpressions.MatchCollection::_done
	bool ____done_2;
	// System.String System.Text.RegularExpressions.MatchCollection::_input
	String_t* ____input_3;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_beginning
	int32_t ____beginning_4;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_length
	int32_t ____length_5;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_startat
	int32_t ____startat_6;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_prevlen
	int32_t ____prevlen_7;

public:
	inline static int32_t get_offset_of__regex_0() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____regex_0)); }
	inline Regex_t1803876613 * get__regex_0() const { return ____regex_0; }
	inline Regex_t1803876613 ** get_address_of__regex_0() { return &____regex_0; }
	inline void set__regex_0(Regex_t1803876613 * value)
	{
		____regex_0 = value;
		Il2CppCodeGenWriteBarrier((&____regex_0), value);
	}

	inline static int32_t get_offset_of__matches_1() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____matches_1)); }
	inline ArrayList_t4252133567 * get__matches_1() const { return ____matches_1; }
	inline ArrayList_t4252133567 ** get_address_of__matches_1() { return &____matches_1; }
	inline void set__matches_1(ArrayList_t4252133567 * value)
	{
		____matches_1 = value;
		Il2CppCodeGenWriteBarrier((&____matches_1), value);
	}

	inline static int32_t get_offset_of__done_2() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____done_2)); }
	inline bool get__done_2() const { return ____done_2; }
	inline bool* get_address_of__done_2() { return &____done_2; }
	inline void set__done_2(bool value)
	{
		____done_2 = value;
	}

	inline static int32_t get_offset_of__input_3() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____input_3)); }
	inline String_t* get__input_3() const { return ____input_3; }
	inline String_t** get_address_of__input_3() { return &____input_3; }
	inline void set__input_3(String_t* value)
	{
		____input_3 = value;
		Il2CppCodeGenWriteBarrier((&____input_3), value);
	}

	inline static int32_t get_offset_of__beginning_4() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____beginning_4)); }
	inline int32_t get__beginning_4() const { return ____beginning_4; }
	inline int32_t* get_address_of__beginning_4() { return &____beginning_4; }
	inline void set__beginning_4(int32_t value)
	{
		____beginning_4 = value;
	}

	inline static int32_t get_offset_of__length_5() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____length_5)); }
	inline int32_t get__length_5() const { return ____length_5; }
	inline int32_t* get_address_of__length_5() { return &____length_5; }
	inline void set__length_5(int32_t value)
	{
		____length_5 = value;
	}

	inline static int32_t get_offset_of__startat_6() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____startat_6)); }
	inline int32_t get__startat_6() const { return ____startat_6; }
	inline int32_t* get_address_of__startat_6() { return &____startat_6; }
	inline void set__startat_6(int32_t value)
	{
		____startat_6 = value;
	}

	inline static int32_t get_offset_of__prevlen_7() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671, ____prevlen_7)); }
	inline int32_t get__prevlen_7() const { return ____prevlen_7; }
	inline int32_t* get_address_of__prevlen_7() { return &____prevlen_7; }
	inline void set__prevlen_7(int32_t value)
	{
		____prevlen_7 = value;
	}
};

struct MatchCollection_t3718216671_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.MatchCollection::infinite
	int32_t ___infinite_8;

public:
	inline static int32_t get_offset_of_infinite_8() { return static_cast<int32_t>(offsetof(MatchCollection_t3718216671_StaticFields, ___infinite_8)); }
	inline int32_t get_infinite_8() const { return ___infinite_8; }
	inline int32_t* get_address_of_infinite_8() { return &___infinite_8; }
	inline void set_infinite_8(int32_t value)
	{
		___infinite_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHCOLLECTION_T3718216671_H
#ifndef CASEINSENSITIVEASCII_T4138584646_H
#define CASEINSENSITIVEASCII_T4138584646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CaseInsensitiveAscii
struct  CaseInsensitiveAscii_t4138584646  : public RuntimeObject
{
public:

public:
};

struct CaseInsensitiveAscii_t4138584646_StaticFields
{
public:
	// System.Net.CaseInsensitiveAscii System.Net.CaseInsensitiveAscii::StaticInstance
	CaseInsensitiveAscii_t4138584646 * ___StaticInstance_0;
	// System.Byte[] System.Net.CaseInsensitiveAscii::AsciiToLower
	ByteU5BU5D_t3397334013* ___AsciiToLower_1;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_t4138584646_StaticFields, ___StaticInstance_0)); }
	inline CaseInsensitiveAscii_t4138584646 * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline CaseInsensitiveAscii_t4138584646 ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(CaseInsensitiveAscii_t4138584646 * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___StaticInstance_0), value);
	}

	inline static int32_t get_offset_of_AsciiToLower_1() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_t4138584646_StaticFields, ___AsciiToLower_1)); }
	inline ByteU5BU5D_t3397334013* get_AsciiToLower_1() const { return ___AsciiToLower_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_AsciiToLower_1() { return &___AsciiToLower_1; }
	inline void set_AsciiToLower_1(ByteU5BU5D_t3397334013* value)
	{
		___AsciiToLower_1 = value;
		Il2CppCodeGenWriteBarrier((&___AsciiToLower_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASEINSENSITIVEASCII_T4138584646_H
#ifndef SERVERCERTVALIDATIONCALLBACK_T2774612835_H
#define SERVERCERTVALIDATIONCALLBACK_T2774612835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback
struct  ServerCertValidationCallback_t2774612835  : public RuntimeObject
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServerCertValidationCallback::m_ValidationCallback
	RemoteCertificateValidationCallback_t2756269959 * ___m_ValidationCallback_0;
	// System.Threading.ExecutionContext System.Net.ServerCertValidationCallback::m_Context
	ExecutionContext_t1392266323 * ___m_Context_1;

public:
	inline static int32_t get_offset_of_m_ValidationCallback_0() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t2774612835, ___m_ValidationCallback_0)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_m_ValidationCallback_0() const { return ___m_ValidationCallback_0; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_m_ValidationCallback_0() { return &___m_ValidationCallback_0; }
	inline void set_m_ValidationCallback_0(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___m_ValidationCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidationCallback_0), value);
	}

	inline static int32_t get_offset_of_m_Context_1() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t2774612835, ___m_Context_1)); }
	inline ExecutionContext_t1392266323 * get_m_Context_1() const { return ___m_Context_1; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_Context_1() { return &___m_Context_1; }
	inline void set_m_Context_1(ExecutionContext_t1392266323 * value)
	{
		___m_Context_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCERTVALIDATIONCALLBACK_T2774612835_H
#ifndef SOCKETADDRESS_T838303055_H
#define SOCKETADDRESS_T838303055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_t838303055  : public RuntimeObject
{
public:
	// System.Int32 System.Net.SocketAddress::m_Size
	int32_t ___m_Size_0;
	// System.Byte[] System.Net.SocketAddress::m_Buffer
	ByteU5BU5D_t3397334013* ___m_Buffer_1;
	// System.Boolean System.Net.SocketAddress::m_changed
	bool ___m_changed_2;
	// System.Int32 System.Net.SocketAddress::m_hash
	int32_t ___m_hash_3;

public:
	inline static int32_t get_offset_of_m_Size_0() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_Size_0)); }
	inline int32_t get_m_Size_0() const { return ___m_Size_0; }
	inline int32_t* get_address_of_m_Size_0() { return &___m_Size_0; }
	inline void set_m_Size_0(int32_t value)
	{
		___m_Size_0 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_Buffer_1)); }
	inline ByteU5BU5D_t3397334013* get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(ByteU5BU5D_t3397334013* value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_1), value);
	}

	inline static int32_t get_offset_of_m_changed_2() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_changed_2)); }
	inline bool get_m_changed_2() const { return ___m_changed_2; }
	inline bool* get_address_of_m_changed_2() { return &___m_changed_2; }
	inline void set_m_changed_2(bool value)
	{
		___m_changed_2 = value;
	}

	inline static int32_t get_offset_of_m_hash_3() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_hash_3)); }
	inline int32_t get_m_hash_3() const { return ___m_hash_3; }
	inline int32_t* get_address_of_m_hash_3() { return &___m_hash_3; }
	inline void set_m_hash_3(int32_t value)
	{
		___m_hash_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETADDRESS_T838303055_H
#ifndef WEBEXCEPTIONMAPPING_T159714763_H
#define WEBEXCEPTIONMAPPING_T159714763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionMapping
struct  WebExceptionMapping_t159714763  : public RuntimeObject
{
public:

public:
};

struct WebExceptionMapping_t159714763_StaticFields
{
public:
	// System.String[] System.Net.WebExceptionMapping::s_Mapping
	StringU5BU5D_t1642385972* ___s_Mapping_0;

public:
	inline static int32_t get_offset_of_s_Mapping_0() { return static_cast<int32_t>(offsetof(WebExceptionMapping_t159714763_StaticFields, ___s_Mapping_0)); }
	inline StringU5BU5D_t1642385972* get_s_Mapping_0() const { return ___s_Mapping_0; }
	inline StringU5BU5D_t1642385972** get_address_of_s_Mapping_0() { return &___s_Mapping_0; }
	inline void set_s_Mapping_0(StringU5BU5D_t1642385972* value)
	{
		___s_Mapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mapping_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONMAPPING_T159714763_H
#ifndef DESIGNERWEBREQUESTCREATE_T3086960480_H
#define DESIGNERWEBREQUESTCREATE_T3086960480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/DesignerWebRequestCreate
struct  DesignerWebRequestCreate_t3086960480  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERWEBREQUESTCREATE_T3086960480_H
#ifndef REGEXPREFIX_T1013837165_H
#define REGEXPREFIX_T1013837165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexPrefix
struct  RegexPrefix_t1013837165  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.RegexPrefix::_prefix
	String_t* ____prefix_0;
	// System.Boolean System.Text.RegularExpressions.RegexPrefix::_caseInsensitive
	bool ____caseInsensitive_1;

public:
	inline static int32_t get_offset_of__prefix_0() { return static_cast<int32_t>(offsetof(RegexPrefix_t1013837165, ____prefix_0)); }
	inline String_t* get__prefix_0() const { return ____prefix_0; }
	inline String_t** get_address_of__prefix_0() { return &____prefix_0; }
	inline void set__prefix_0(String_t* value)
	{
		____prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefix_0), value);
	}

	inline static int32_t get_offset_of__caseInsensitive_1() { return static_cast<int32_t>(offsetof(RegexPrefix_t1013837165, ____caseInsensitive_1)); }
	inline bool get__caseInsensitive_1() const { return ____caseInsensitive_1; }
	inline bool* get_address_of__caseInsensitive_1() { return &____caseInsensitive_1; }
	inline void set__caseInsensitive_1(bool value)
	{
		____caseInsensitive_1 = value;
	}
};

struct RegexPrefix_t1013837165_StaticFields
{
public:
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexPrefix::_empty
	RegexPrefix_t1013837165 * ____empty_2;

public:
	inline static int32_t get_offset_of__empty_2() { return static_cast<int32_t>(offsetof(RegexPrefix_t1013837165_StaticFields, ____empty_2)); }
	inline RegexPrefix_t1013837165 * get__empty_2() const { return ____empty_2; }
	inline RegexPrefix_t1013837165 ** get_address_of__empty_2() { return &____empty_2; }
	inline void set__empty_2(RegexPrefix_t1013837165 * value)
	{
		____empty_2 = value;
		Il2CppCodeGenWriteBarrier((&____empty_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPREFIX_T1013837165_H
#ifndef REGEXFC_T2009854258_H
#define REGEXFC_T2009854258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexFC
struct  RegexFC_t2009854258  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexFC::_cc
	RegexCharClass_t2441867401 * ____cc_0;
	// System.Boolean System.Text.RegularExpressions.RegexFC::_nullable
	bool ____nullable_1;
	// System.Boolean System.Text.RegularExpressions.RegexFC::_caseInsensitive
	bool ____caseInsensitive_2;

public:
	inline static int32_t get_offset_of__cc_0() { return static_cast<int32_t>(offsetof(RegexFC_t2009854258, ____cc_0)); }
	inline RegexCharClass_t2441867401 * get__cc_0() const { return ____cc_0; }
	inline RegexCharClass_t2441867401 ** get_address_of__cc_0() { return &____cc_0; }
	inline void set__cc_0(RegexCharClass_t2441867401 * value)
	{
		____cc_0 = value;
		Il2CppCodeGenWriteBarrier((&____cc_0), value);
	}

	inline static int32_t get_offset_of__nullable_1() { return static_cast<int32_t>(offsetof(RegexFC_t2009854258, ____nullable_1)); }
	inline bool get__nullable_1() const { return ____nullable_1; }
	inline bool* get_address_of__nullable_1() { return &____nullable_1; }
	inline void set__nullable_1(bool value)
	{
		____nullable_1 = value;
	}

	inline static int32_t get_offset_of__caseInsensitive_2() { return static_cast<int32_t>(offsetof(RegexFC_t2009854258, ____caseInsensitive_2)); }
	inline bool get__caseInsensitive_2() const { return ____caseInsensitive_2; }
	inline bool* get_address_of__caseInsensitive_2() { return &____caseInsensitive_2; }
	inline void set__caseInsensitive_2(bool value)
	{
		____caseInsensitive_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXFC_T2009854258_H
#ifndef REGEXFCD_T150416822_H
#define REGEXFCD_T150416822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexFCD
struct  RegexFCD_t150416822  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexFCD::_intStack
	Int32U5BU5D_t3030399641* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexFCD::_intDepth
	int32_t ____intDepth_1;
	// System.Text.RegularExpressions.RegexFC[] System.Text.RegularExpressions.RegexFCD::_fcStack
	RegexFCU5BU5D_t2364340679* ____fcStack_2;
	// System.Int32 System.Text.RegularExpressions.RegexFCD::_fcDepth
	int32_t ____fcDepth_3;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_skipAllChildren
	bool ____skipAllChildren_4;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_skipchild
	bool ____skipchild_5;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_failed
	bool ____failed_6;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____intStack_0)); }
	inline Int32U5BU5D_t3030399641* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t3030399641* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier((&____intStack_0), value);
	}

	inline static int32_t get_offset_of__intDepth_1() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____intDepth_1)); }
	inline int32_t get__intDepth_1() const { return ____intDepth_1; }
	inline int32_t* get_address_of__intDepth_1() { return &____intDepth_1; }
	inline void set__intDepth_1(int32_t value)
	{
		____intDepth_1 = value;
	}

	inline static int32_t get_offset_of__fcStack_2() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____fcStack_2)); }
	inline RegexFCU5BU5D_t2364340679* get__fcStack_2() const { return ____fcStack_2; }
	inline RegexFCU5BU5D_t2364340679** get_address_of__fcStack_2() { return &____fcStack_2; }
	inline void set__fcStack_2(RegexFCU5BU5D_t2364340679* value)
	{
		____fcStack_2 = value;
		Il2CppCodeGenWriteBarrier((&____fcStack_2), value);
	}

	inline static int32_t get_offset_of__fcDepth_3() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____fcDepth_3)); }
	inline int32_t get__fcDepth_3() const { return ____fcDepth_3; }
	inline int32_t* get_address_of__fcDepth_3() { return &____fcDepth_3; }
	inline void set__fcDepth_3(int32_t value)
	{
		____fcDepth_3 = value;
	}

	inline static int32_t get_offset_of__skipAllChildren_4() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____skipAllChildren_4)); }
	inline bool get__skipAllChildren_4() const { return ____skipAllChildren_4; }
	inline bool* get_address_of__skipAllChildren_4() { return &____skipAllChildren_4; }
	inline void set__skipAllChildren_4(bool value)
	{
		____skipAllChildren_4 = value;
	}

	inline static int32_t get_offset_of__skipchild_5() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____skipchild_5)); }
	inline bool get__skipchild_5() const { return ____skipchild_5; }
	inline bool* get_address_of__skipchild_5() { return &____skipchild_5; }
	inline void set__skipchild_5(bool value)
	{
		____skipchild_5 = value;
	}

	inline static int32_t get_offset_of__failed_6() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____failed_6)); }
	inline bool get__failed_6() const { return ____failed_6; }
	inline bool* get_address_of__failed_6() { return &____failed_6; }
	inline void set__failed_6(bool value)
	{
		____failed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXFCD_T150416822_H
#ifndef WEBPROXYWRAPPEROPAQUE_T1012624580_H
#define WEBPROXYWRAPPEROPAQUE_T1012624580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/WebProxyWrapperOpaque
struct  WebProxyWrapperOpaque_t1012624580  : public RuntimeObject
{
public:
	// System.Net.WebProxy System.Net.WebRequest/WebProxyWrapperOpaque::webProxy
	WebProxy_t1169192840 * ___webProxy_0;

public:
	inline static int32_t get_offset_of_webProxy_0() { return static_cast<int32_t>(offsetof(WebProxyWrapperOpaque_t1012624580, ___webProxy_0)); }
	inline WebProxy_t1169192840 * get_webProxy_0() const { return ___webProxy_0; }
	inline WebProxy_t1169192840 ** get_address_of_webProxy_0() { return &___webProxy_0; }
	inline void set_webProxy_0(WebProxy_t1169192840 * value)
	{
		___webProxy_0 = value;
		Il2CppCodeGenWriteBarrier((&___webProxy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYWRAPPEROPAQUE_T1012624580_H
#ifndef GROUPENUMERATOR_T3864870211_H
#define GROUPENUMERATOR_T3864870211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupEnumerator
struct  GroupEnumerator_t3864870211  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.GroupEnumerator::_rgc
	GroupCollection_t939014605 * ____rgc_0;
	// System.Int32 System.Text.RegularExpressions.GroupEnumerator::_curindex
	int32_t ____curindex_1;

public:
	inline static int32_t get_offset_of__rgc_0() { return static_cast<int32_t>(offsetof(GroupEnumerator_t3864870211, ____rgc_0)); }
	inline GroupCollection_t939014605 * get__rgc_0() const { return ____rgc_0; }
	inline GroupCollection_t939014605 ** get_address_of__rgc_0() { return &____rgc_0; }
	inline void set__rgc_0(GroupCollection_t939014605 * value)
	{
		____rgc_0 = value;
		Il2CppCodeGenWriteBarrier((&____rgc_0), value);
	}

	inline static int32_t get_offset_of__curindex_1() { return static_cast<int32_t>(offsetof(GroupEnumerator_t3864870211, ____curindex_1)); }
	inline int32_t get__curindex_1() const { return ____curindex_1; }
	inline int32_t* get_address_of__curindex_1() { return &____curindex_1; }
	inline void set__curindex_1(int32_t value)
	{
		____curindex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPENUMERATOR_T3864870211_H
#ifndef GROUPCOLLECTION_T939014605_H
#define GROUPCOLLECTION_T939014605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_t939014605  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.GroupCollection::_match
	Match_t3164245899 * ____match_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.GroupCollection::_captureMap
	Hashtable_t909839986 * ____captureMap_1;
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::_groups
	GroupU5BU5D_t2272681992* ____groups_2;

public:
	inline static int32_t get_offset_of__match_0() { return static_cast<int32_t>(offsetof(GroupCollection_t939014605, ____match_0)); }
	inline Match_t3164245899 * get__match_0() const { return ____match_0; }
	inline Match_t3164245899 ** get_address_of__match_0() { return &____match_0; }
	inline void set__match_0(Match_t3164245899 * value)
	{
		____match_0 = value;
		Il2CppCodeGenWriteBarrier((&____match_0), value);
	}

	inline static int32_t get_offset_of__captureMap_1() { return static_cast<int32_t>(offsetof(GroupCollection_t939014605, ____captureMap_1)); }
	inline Hashtable_t909839986 * get__captureMap_1() const { return ____captureMap_1; }
	inline Hashtable_t909839986 ** get_address_of__captureMap_1() { return &____captureMap_1; }
	inline void set__captureMap_1(Hashtable_t909839986 * value)
	{
		____captureMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____captureMap_1), value);
	}

	inline static int32_t get_offset_of__groups_2() { return static_cast<int32_t>(offsetof(GroupCollection_t939014605, ____groups_2)); }
	inline GroupU5BU5D_t2272681992* get__groups_2() const { return ____groups_2; }
	inline GroupU5BU5D_t2272681992** get_address_of__groups_2() { return &____groups_2; }
	inline void set__groups_2(GroupU5BU5D_t2272681992* value)
	{
		____groups_2 = value;
		Il2CppCodeGenWriteBarrier((&____groups_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_T939014605_H
#ifndef REGEXRUNNER_T3983612747_H
#define REGEXRUNNER_T3983612747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunner
struct  RegexRunner_t3983612747  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextbeg
	int32_t ___runtextbeg_0;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextend
	int32_t ___runtextend_1;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextstart
	int32_t ___runtextstart_2;
	// System.String System.Text.RegularExpressions.RegexRunner::runtext
	String_t* ___runtext_3;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextpos
	int32_t ___runtextpos_4;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runtrack
	Int32U5BU5D_t3030399641* ___runtrack_5;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackpos
	int32_t ___runtrackpos_6;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runstack
	Int32U5BU5D_t3030399641* ___runstack_7;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runstackpos
	int32_t ___runstackpos_8;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runcrawl
	Int32U5BU5D_t3030399641* ___runcrawl_9;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runcrawlpos
	int32_t ___runcrawlpos_10;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackcount
	int32_t ___runtrackcount_11;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.RegexRunner::runmatch
	Match_t3164245899 * ___runmatch_12;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.RegexRunner::runregex
	Regex_t1803876613 * ___runregex_13;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeout
	int32_t ___timeout_14;
	// System.Boolean System.Text.RegularExpressions.RegexRunner::ignoreTimeout
	bool ___ignoreTimeout_15;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutOccursAt
	int32_t ___timeoutOccursAt_16;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutChecksToSkip
	int32_t ___timeoutChecksToSkip_17;

public:
	inline static int32_t get_offset_of_runtextbeg_0() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextbeg_0)); }
	inline int32_t get_runtextbeg_0() const { return ___runtextbeg_0; }
	inline int32_t* get_address_of_runtextbeg_0() { return &___runtextbeg_0; }
	inline void set_runtextbeg_0(int32_t value)
	{
		___runtextbeg_0 = value;
	}

	inline static int32_t get_offset_of_runtextend_1() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextend_1)); }
	inline int32_t get_runtextend_1() const { return ___runtextend_1; }
	inline int32_t* get_address_of_runtextend_1() { return &___runtextend_1; }
	inline void set_runtextend_1(int32_t value)
	{
		___runtextend_1 = value;
	}

	inline static int32_t get_offset_of_runtextstart_2() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextstart_2)); }
	inline int32_t get_runtextstart_2() const { return ___runtextstart_2; }
	inline int32_t* get_address_of_runtextstart_2() { return &___runtextstart_2; }
	inline void set_runtextstart_2(int32_t value)
	{
		___runtextstart_2 = value;
	}

	inline static int32_t get_offset_of_runtext_3() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtext_3)); }
	inline String_t* get_runtext_3() const { return ___runtext_3; }
	inline String_t** get_address_of_runtext_3() { return &___runtext_3; }
	inline void set_runtext_3(String_t* value)
	{
		___runtext_3 = value;
		Il2CppCodeGenWriteBarrier((&___runtext_3), value);
	}

	inline static int32_t get_offset_of_runtextpos_4() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextpos_4)); }
	inline int32_t get_runtextpos_4() const { return ___runtextpos_4; }
	inline int32_t* get_address_of_runtextpos_4() { return &___runtextpos_4; }
	inline void set_runtextpos_4(int32_t value)
	{
		___runtextpos_4 = value;
	}

	inline static int32_t get_offset_of_runtrack_5() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtrack_5)); }
	inline Int32U5BU5D_t3030399641* get_runtrack_5() const { return ___runtrack_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_runtrack_5() { return &___runtrack_5; }
	inline void set_runtrack_5(Int32U5BU5D_t3030399641* value)
	{
		___runtrack_5 = value;
		Il2CppCodeGenWriteBarrier((&___runtrack_5), value);
	}

	inline static int32_t get_offset_of_runtrackpos_6() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtrackpos_6)); }
	inline int32_t get_runtrackpos_6() const { return ___runtrackpos_6; }
	inline int32_t* get_address_of_runtrackpos_6() { return &___runtrackpos_6; }
	inline void set_runtrackpos_6(int32_t value)
	{
		___runtrackpos_6 = value;
	}

	inline static int32_t get_offset_of_runstack_7() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runstack_7)); }
	inline Int32U5BU5D_t3030399641* get_runstack_7() const { return ___runstack_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_runstack_7() { return &___runstack_7; }
	inline void set_runstack_7(Int32U5BU5D_t3030399641* value)
	{
		___runstack_7 = value;
		Il2CppCodeGenWriteBarrier((&___runstack_7), value);
	}

	inline static int32_t get_offset_of_runstackpos_8() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runstackpos_8)); }
	inline int32_t get_runstackpos_8() const { return ___runstackpos_8; }
	inline int32_t* get_address_of_runstackpos_8() { return &___runstackpos_8; }
	inline void set_runstackpos_8(int32_t value)
	{
		___runstackpos_8 = value;
	}

	inline static int32_t get_offset_of_runcrawl_9() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runcrawl_9)); }
	inline Int32U5BU5D_t3030399641* get_runcrawl_9() const { return ___runcrawl_9; }
	inline Int32U5BU5D_t3030399641** get_address_of_runcrawl_9() { return &___runcrawl_9; }
	inline void set_runcrawl_9(Int32U5BU5D_t3030399641* value)
	{
		___runcrawl_9 = value;
		Il2CppCodeGenWriteBarrier((&___runcrawl_9), value);
	}

	inline static int32_t get_offset_of_runcrawlpos_10() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runcrawlpos_10)); }
	inline int32_t get_runcrawlpos_10() const { return ___runcrawlpos_10; }
	inline int32_t* get_address_of_runcrawlpos_10() { return &___runcrawlpos_10; }
	inline void set_runcrawlpos_10(int32_t value)
	{
		___runcrawlpos_10 = value;
	}

	inline static int32_t get_offset_of_runtrackcount_11() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtrackcount_11)); }
	inline int32_t get_runtrackcount_11() const { return ___runtrackcount_11; }
	inline int32_t* get_address_of_runtrackcount_11() { return &___runtrackcount_11; }
	inline void set_runtrackcount_11(int32_t value)
	{
		___runtrackcount_11 = value;
	}

	inline static int32_t get_offset_of_runmatch_12() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runmatch_12)); }
	inline Match_t3164245899 * get_runmatch_12() const { return ___runmatch_12; }
	inline Match_t3164245899 ** get_address_of_runmatch_12() { return &___runmatch_12; }
	inline void set_runmatch_12(Match_t3164245899 * value)
	{
		___runmatch_12 = value;
		Il2CppCodeGenWriteBarrier((&___runmatch_12), value);
	}

	inline static int32_t get_offset_of_runregex_13() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runregex_13)); }
	inline Regex_t1803876613 * get_runregex_13() const { return ___runregex_13; }
	inline Regex_t1803876613 ** get_address_of_runregex_13() { return &___runregex_13; }
	inline void set_runregex_13(Regex_t1803876613 * value)
	{
		___runregex_13 = value;
		Il2CppCodeGenWriteBarrier((&___runregex_13), value);
	}

	inline static int32_t get_offset_of_timeout_14() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___timeout_14)); }
	inline int32_t get_timeout_14() const { return ___timeout_14; }
	inline int32_t* get_address_of_timeout_14() { return &___timeout_14; }
	inline void set_timeout_14(int32_t value)
	{
		___timeout_14 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeout_15() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___ignoreTimeout_15)); }
	inline bool get_ignoreTimeout_15() const { return ___ignoreTimeout_15; }
	inline bool* get_address_of_ignoreTimeout_15() { return &___ignoreTimeout_15; }
	inline void set_ignoreTimeout_15(bool value)
	{
		___ignoreTimeout_15 = value;
	}

	inline static int32_t get_offset_of_timeoutOccursAt_16() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___timeoutOccursAt_16)); }
	inline int32_t get_timeoutOccursAt_16() const { return ___timeoutOccursAt_16; }
	inline int32_t* get_address_of_timeoutOccursAt_16() { return &___timeoutOccursAt_16; }
	inline void set_timeoutOccursAt_16(int32_t value)
	{
		___timeoutOccursAt_16 = value;
	}

	inline static int32_t get_offset_of_timeoutChecksToSkip_17() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___timeoutChecksToSkip_17)); }
	inline int32_t get_timeoutChecksToSkip_17() const { return ___timeoutChecksToSkip_17; }
	inline int32_t* get_address_of_timeoutChecksToSkip_17() { return &___timeoutChecksToSkip_17; }
	inline void set_timeoutChecksToSkip_17(int32_t value)
	{
		___timeoutChecksToSkip_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNER_T3983612747_H
#ifndef CREDENTIALENUMERATOR_T3600195683_H
#define CREDENTIALENUMERATOR_T3600195683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialCache/CredentialEnumerator
struct  CredentialEnumerator_t3600195683  : public RuntimeObject
{
public:
	// System.Net.CredentialCache System.Net.CredentialCache/CredentialEnumerator::m_cache
	CredentialCache_t1992799279 * ___m_cache_0;
	// System.Net.ICredentials[] System.Net.CredentialCache/CredentialEnumerator::m_array
	ICredentialsU5BU5D_t196403748* ___m_array_1;
	// System.Int32 System.Net.CredentialCache/CredentialEnumerator::m_index
	int32_t ___m_index_2;
	// System.Int32 System.Net.CredentialCache/CredentialEnumerator::m_version
	int32_t ___m_version_3;

public:
	inline static int32_t get_offset_of_m_cache_0() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_cache_0)); }
	inline CredentialCache_t1992799279 * get_m_cache_0() const { return ___m_cache_0; }
	inline CredentialCache_t1992799279 ** get_address_of_m_cache_0() { return &___m_cache_0; }
	inline void set_m_cache_0(CredentialCache_t1992799279 * value)
	{
		___m_cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_cache_0), value);
	}

	inline static int32_t get_offset_of_m_array_1() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_array_1)); }
	inline ICredentialsU5BU5D_t196403748* get_m_array_1() const { return ___m_array_1; }
	inline ICredentialsU5BU5D_t196403748** get_address_of_m_array_1() { return &___m_array_1; }
	inline void set_m_array_1(ICredentialsU5BU5D_t196403748* value)
	{
		___m_array_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_array_1), value);
	}

	inline static int32_t get_offset_of_m_index_2() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_index_2)); }
	inline int32_t get_m_index_2() const { return ___m_index_2; }
	inline int32_t* get_address_of_m_index_2() { return &___m_index_2; }
	inline void set_m_index_2(int32_t value)
	{
		___m_index_2 = value;
	}

	inline static int32_t get_offset_of_m_version_3() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_version_3)); }
	inline int32_t get_m_version_3() const { return ___m_version_3; }
	inline int32_t* get_address_of_m_version_3() { return &___m_version_3; }
	inline void set_m_version_3(int32_t value)
	{
		___m_version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALENUMERATOR_T3600195683_H
#ifndef CREDENTIALCACHE_T1992799279_H
#define CREDENTIALCACHE_T1992799279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialCache
struct  CredentialCache_t1992799279  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Net.CredentialCache::cache
	Hashtable_t909839986 * ___cache_0;
	// System.Collections.Hashtable System.Net.CredentialCache::cacheForHosts
	Hashtable_t909839986 * ___cacheForHosts_1;
	// System.Int32 System.Net.CredentialCache::m_version
	int32_t ___m_version_2;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(CredentialCache_t1992799279, ___cache_0)); }
	inline Hashtable_t909839986 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t909839986 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t909839986 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}

	inline static int32_t get_offset_of_cacheForHosts_1() { return static_cast<int32_t>(offsetof(CredentialCache_t1992799279, ___cacheForHosts_1)); }
	inline Hashtable_t909839986 * get_cacheForHosts_1() const { return ___cacheForHosts_1; }
	inline Hashtable_t909839986 ** get_address_of_cacheForHosts_1() { return &___cacheForHosts_1; }
	inline void set_cacheForHosts_1(Hashtable_t909839986 * value)
	{
		___cacheForHosts_1 = value;
		Il2CppCodeGenWriteBarrier((&___cacheForHosts_1), value);
	}

	inline static int32_t get_offset_of_m_version_2() { return static_cast<int32_t>(offsetof(CredentialCache_t1992799279, ___m_version_2)); }
	inline int32_t get_m_version_2() const { return ___m_version_2; }
	inline int32_t* get_address_of_m_version_2() { return &___m_version_2; }
	inline void set_m_version_2(int32_t value)
	{
		___m_version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALCACHE_T1992799279_H
#ifndef AUTHORIZATION_T1602399_H
#define AUTHORIZATION_T1602399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Authorization
struct  Authorization_t1602399  : public RuntimeObject
{
public:
	// System.String System.Net.Authorization::m_Message
	String_t* ___m_Message_0;
	// System.Boolean System.Net.Authorization::m_Complete
	bool ___m_Complete_1;
	// System.String System.Net.Authorization::ModuleAuthenticationType
	String_t* ___ModuleAuthenticationType_2;

public:
	inline static int32_t get_offset_of_m_Message_0() { return static_cast<int32_t>(offsetof(Authorization_t1602399, ___m_Message_0)); }
	inline String_t* get_m_Message_0() const { return ___m_Message_0; }
	inline String_t** get_address_of_m_Message_0() { return &___m_Message_0; }
	inline void set_m_Message_0(String_t* value)
	{
		___m_Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Message_0), value);
	}

	inline static int32_t get_offset_of_m_Complete_1() { return static_cast<int32_t>(offsetof(Authorization_t1602399, ___m_Complete_1)); }
	inline bool get_m_Complete_1() const { return ___m_Complete_1; }
	inline bool* get_address_of_m_Complete_1() { return &___m_Complete_1; }
	inline void set_m_Complete_1(bool value)
	{
		___m_Complete_1 = value;
	}

	inline static int32_t get_offset_of_ModuleAuthenticationType_2() { return static_cast<int32_t>(offsetof(Authorization_t1602399, ___ModuleAuthenticationType_2)); }
	inline String_t* get_ModuleAuthenticationType_2() const { return ___ModuleAuthenticationType_2; }
	inline String_t** get_address_of_ModuleAuthenticationType_2() { return &___ModuleAuthenticationType_2; }
	inline void set_ModuleAuthenticationType_2(String_t* value)
	{
		___ModuleAuthenticationType_2 = value;
		Il2CppCodeGenWriteBarrier((&___ModuleAuthenticationType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORIZATION_T1602399_H
#ifndef IPHOSTENTRY_T994738509_H
#define IPHOSTENTRY_T994738509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPHostEntry
struct  IPHostEntry_t994738509  : public RuntimeObject
{
public:
	// System.String System.Net.IPHostEntry::hostName
	String_t* ___hostName_0;
	// System.String[] System.Net.IPHostEntry::aliases
	StringU5BU5D_t1642385972* ___aliases_1;
	// System.Net.IPAddress[] System.Net.IPHostEntry::addressList
	IPAddressU5BU5D_t4087230954* ___addressList_2;
	// System.Boolean System.Net.IPHostEntry::isTrustedHost
	bool ___isTrustedHost_3;

public:
	inline static int32_t get_offset_of_hostName_0() { return static_cast<int32_t>(offsetof(IPHostEntry_t994738509, ___hostName_0)); }
	inline String_t* get_hostName_0() const { return ___hostName_0; }
	inline String_t** get_address_of_hostName_0() { return &___hostName_0; }
	inline void set_hostName_0(String_t* value)
	{
		___hostName_0 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_0), value);
	}

	inline static int32_t get_offset_of_aliases_1() { return static_cast<int32_t>(offsetof(IPHostEntry_t994738509, ___aliases_1)); }
	inline StringU5BU5D_t1642385972* get_aliases_1() const { return ___aliases_1; }
	inline StringU5BU5D_t1642385972** get_address_of_aliases_1() { return &___aliases_1; }
	inline void set_aliases_1(StringU5BU5D_t1642385972* value)
	{
		___aliases_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliases_1), value);
	}

	inline static int32_t get_offset_of_addressList_2() { return static_cast<int32_t>(offsetof(IPHostEntry_t994738509, ___addressList_2)); }
	inline IPAddressU5BU5D_t4087230954* get_addressList_2() const { return ___addressList_2; }
	inline IPAddressU5BU5D_t4087230954** get_address_of_addressList_2() { return &___addressList_2; }
	inline void set_addressList_2(IPAddressU5BU5D_t4087230954* value)
	{
		___addressList_2 = value;
		Il2CppCodeGenWriteBarrier((&___addressList_2), value);
	}

	inline static int32_t get_offset_of_isTrustedHost_3() { return static_cast<int32_t>(offsetof(IPHostEntry_t994738509, ___isTrustedHost_3)); }
	inline bool get_isTrustedHost_3() const { return ___isTrustedHost_3; }
	inline bool* get_address_of_isTrustedHost_3() { return &___isTrustedHost_3; }
	inline void set_isTrustedHost_3(bool value)
	{
		___isTrustedHost_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPHOSTENTRY_T994738509_H
#ifndef HTTPVERSION_T1276659706_H
#define HTTPVERSION_T1276659706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpVersion
struct  HttpVersion_t1276659706  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t1276659706_StaticFields
{
public:
	// System.Version System.Net.HttpVersion::Version10
	Version_t1755874712 * ___Version10_0;
	// System.Version System.Net.HttpVersion::Version11
	Version_t1755874712 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t1276659706_StaticFields, ___Version10_0)); }
	inline Version_t1755874712 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t1755874712 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t1755874712 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t1276659706_StaticFields, ___Version11_1)); }
	inline Version_t1755874712 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t1755874712 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t1755874712 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T1276659706_H
#ifndef ENDPOINT_T4156119363_H
#define ENDPOINT_T4156119363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_t4156119363  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T4156119363_H
#ifndef CREDENTIALKEY_T4100643814_H
#define CREDENTIALKEY_T4100643814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialKey
struct  CredentialKey_t4100643814  : public RuntimeObject
{
public:
	// System.Uri System.Net.CredentialKey::UriPrefix
	Uri_t19570940 * ___UriPrefix_0;
	// System.Int32 System.Net.CredentialKey::UriPrefixLength
	int32_t ___UriPrefixLength_1;
	// System.String System.Net.CredentialKey::AuthenticationType
	String_t* ___AuthenticationType_2;
	// System.Int32 System.Net.CredentialKey::m_HashCode
	int32_t ___m_HashCode_3;
	// System.Boolean System.Net.CredentialKey::m_ComputedHashCode
	bool ___m_ComputedHashCode_4;

public:
	inline static int32_t get_offset_of_UriPrefix_0() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___UriPrefix_0)); }
	inline Uri_t19570940 * get_UriPrefix_0() const { return ___UriPrefix_0; }
	inline Uri_t19570940 ** get_address_of_UriPrefix_0() { return &___UriPrefix_0; }
	inline void set_UriPrefix_0(Uri_t19570940 * value)
	{
		___UriPrefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___UriPrefix_0), value);
	}

	inline static int32_t get_offset_of_UriPrefixLength_1() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___UriPrefixLength_1)); }
	inline int32_t get_UriPrefixLength_1() const { return ___UriPrefixLength_1; }
	inline int32_t* get_address_of_UriPrefixLength_1() { return &___UriPrefixLength_1; }
	inline void set_UriPrefixLength_1(int32_t value)
	{
		___UriPrefixLength_1 = value;
	}

	inline static int32_t get_offset_of_AuthenticationType_2() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___AuthenticationType_2)); }
	inline String_t* get_AuthenticationType_2() const { return ___AuthenticationType_2; }
	inline String_t** get_address_of_AuthenticationType_2() { return &___AuthenticationType_2; }
	inline void set_AuthenticationType_2(String_t* value)
	{
		___AuthenticationType_2 = value;
		Il2CppCodeGenWriteBarrier((&___AuthenticationType_2), value);
	}

	inline static int32_t get_offset_of_m_HashCode_3() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___m_HashCode_3)); }
	inline int32_t get_m_HashCode_3() const { return ___m_HashCode_3; }
	inline int32_t* get_address_of_m_HashCode_3() { return &___m_HashCode_3; }
	inline void set_m_HashCode_3(int32_t value)
	{
		___m_HashCode_3 = value;
	}

	inline static int32_t get_offset_of_m_ComputedHashCode_4() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___m_ComputedHashCode_4)); }
	inline bool get_m_ComputedHashCode_4() const { return ___m_ComputedHashCode_4; }
	inline bool* get_address_of_m_ComputedHashCode_4() { return &___m_ComputedHashCode_4; }
	inline void set_m_ComputedHashCode_4(bool value)
	{
		___m_ComputedHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALKEY_T4100643814_H
#ifndef REGEXWRITER_T536390172_H
#define REGEXWRITER_T536390172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexWriter
struct  RegexWriter_t536390172  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_intStack
	Int32U5BU5D_t3030399641* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_depth
	int32_t ____depth_1;
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_emitted
	Int32U5BU5D_t3030399641* ____emitted_2;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_curpos
	int32_t ____curpos_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Text.RegularExpressions.RegexWriter::_stringhash
	Dictionary_2_t3986656710 * ____stringhash_4;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexWriter::_stringtable
	List_1_t1398341365 * ____stringtable_5;
	// System.Boolean System.Text.RegularExpressions.RegexWriter::_counting
	bool ____counting_6;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_count
	int32_t ____count_7;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_trackcount
	int32_t ____trackcount_8;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexWriter::_caps
	Hashtable_t909839986 * ____caps_9;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____intStack_0)); }
	inline Int32U5BU5D_t3030399641* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t3030399641* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier((&____intStack_0), value);
	}

	inline static int32_t get_offset_of__depth_1() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____depth_1)); }
	inline int32_t get__depth_1() const { return ____depth_1; }
	inline int32_t* get_address_of__depth_1() { return &____depth_1; }
	inline void set__depth_1(int32_t value)
	{
		____depth_1 = value;
	}

	inline static int32_t get_offset_of__emitted_2() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____emitted_2)); }
	inline Int32U5BU5D_t3030399641* get__emitted_2() const { return ____emitted_2; }
	inline Int32U5BU5D_t3030399641** get_address_of__emitted_2() { return &____emitted_2; }
	inline void set__emitted_2(Int32U5BU5D_t3030399641* value)
	{
		____emitted_2 = value;
		Il2CppCodeGenWriteBarrier((&____emitted_2), value);
	}

	inline static int32_t get_offset_of__curpos_3() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____curpos_3)); }
	inline int32_t get__curpos_3() const { return ____curpos_3; }
	inline int32_t* get_address_of__curpos_3() { return &____curpos_3; }
	inline void set__curpos_3(int32_t value)
	{
		____curpos_3 = value;
	}

	inline static int32_t get_offset_of__stringhash_4() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____stringhash_4)); }
	inline Dictionary_2_t3986656710 * get__stringhash_4() const { return ____stringhash_4; }
	inline Dictionary_2_t3986656710 ** get_address_of__stringhash_4() { return &____stringhash_4; }
	inline void set__stringhash_4(Dictionary_2_t3986656710 * value)
	{
		____stringhash_4 = value;
		Il2CppCodeGenWriteBarrier((&____stringhash_4), value);
	}

	inline static int32_t get_offset_of__stringtable_5() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____stringtable_5)); }
	inline List_1_t1398341365 * get__stringtable_5() const { return ____stringtable_5; }
	inline List_1_t1398341365 ** get_address_of__stringtable_5() { return &____stringtable_5; }
	inline void set__stringtable_5(List_1_t1398341365 * value)
	{
		____stringtable_5 = value;
		Il2CppCodeGenWriteBarrier((&____stringtable_5), value);
	}

	inline static int32_t get_offset_of__counting_6() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____counting_6)); }
	inline bool get__counting_6() const { return ____counting_6; }
	inline bool* get_address_of__counting_6() { return &____counting_6; }
	inline void set__counting_6(bool value)
	{
		____counting_6 = value;
	}

	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__trackcount_8() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____trackcount_8)); }
	inline int32_t get__trackcount_8() const { return ____trackcount_8; }
	inline int32_t* get_address_of__trackcount_8() { return &____trackcount_8; }
	inline void set__trackcount_8(int32_t value)
	{
		____trackcount_8 = value;
	}

	inline static int32_t get_offset_of__caps_9() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____caps_9)); }
	inline Hashtable_t909839986 * get__caps_9() const { return ____caps_9; }
	inline Hashtable_t909839986 ** get_address_of__caps_9() { return &____caps_9; }
	inline void set__caps_9(Hashtable_t909839986 * value)
	{
		____caps_9 = value;
		Il2CppCodeGenWriteBarrier((&____caps_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXWRITER_T536390172_H
#ifndef NETWORKCREDENTIAL_T1714133953_H
#define NETWORKCREDENTIAL_T1714133953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkCredential
struct  NetworkCredential_t1714133953  : public RuntimeObject
{
public:
	// System.String System.Net.NetworkCredential::m_domain
	String_t* ___m_domain_0;
	// System.String System.Net.NetworkCredential::m_userName
	String_t* ___m_userName_1;
	// System.Security.SecureString System.Net.NetworkCredential::m_password
	SecureString_t412202620 * ___m_password_2;

public:
	inline static int32_t get_offset_of_m_domain_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t1714133953, ___m_domain_0)); }
	inline String_t* get_m_domain_0() const { return ___m_domain_0; }
	inline String_t** get_address_of_m_domain_0() { return &___m_domain_0; }
	inline void set_m_domain_0(String_t* value)
	{
		___m_domain_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_domain_0), value);
	}

	inline static int32_t get_offset_of_m_userName_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t1714133953, ___m_userName_1)); }
	inline String_t* get_m_userName_1() const { return ___m_userName_1; }
	inline String_t** get_address_of_m_userName_1() { return &___m_userName_1; }
	inline void set_m_userName_1(String_t* value)
	{
		___m_userName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_userName_1), value);
	}

	inline static int32_t get_offset_of_m_password_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t1714133953, ___m_password_2)); }
	inline SecureString_t412202620 * get_m_password_2() const { return ___m_password_2; }
	inline SecureString_t412202620 ** get_address_of_m_password_2() { return &___m_password_2; }
	inline void set_m_password_2(SecureString_t412202620 * value)
	{
		___m_password_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_password_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T1714133953_H
#ifndef REGEXRUNNERFACTORY_T3902733837_H
#define REGEXRUNNERFACTORY_T3902733837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunnerFactory
struct  RegexRunnerFactory_t3902733837  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNERFACTORY_T3902733837_H
#ifndef WEBREQUESTPREFIXELEMENT_T283218055_H
#define WEBREQUESTPREFIXELEMENT_T283218055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequestPrefixElement
struct  WebRequestPrefixElement_t283218055  : public RuntimeObject
{
public:
	// System.String System.Net.WebRequestPrefixElement::Prefix
	String_t* ___Prefix_0;
	// System.Net.IWebRequestCreate System.Net.WebRequestPrefixElement::creator
	RuntimeObject* ___creator_1;
	// System.Type System.Net.WebRequestPrefixElement::creatorType
	Type_t * ___creatorType_2;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(WebRequestPrefixElement_t283218055, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_creator_1() { return static_cast<int32_t>(offsetof(WebRequestPrefixElement_t283218055, ___creator_1)); }
	inline RuntimeObject* get_creator_1() const { return ___creator_1; }
	inline RuntimeObject** get_address_of_creator_1() { return &___creator_1; }
	inline void set_creator_1(RuntimeObject* value)
	{
		___creator_1 = value;
		Il2CppCodeGenWriteBarrier((&___creator_1), value);
	}

	inline static int32_t get_offset_of_creatorType_2() { return static_cast<int32_t>(offsetof(WebRequestPrefixElement_t283218055, ___creatorType_2)); }
	inline Type_t * get_creatorType_2() const { return ___creatorType_2; }
	inline Type_t ** get_address_of_creatorType_2() { return &___creatorType_2; }
	inline void set_creatorType_2(Type_t * value)
	{
		___creatorType_2 = value;
		Il2CppCodeGenWriteBarrier((&___creatorType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTPREFIXELEMENT_T283218055_H
#ifndef NCLUTILITIES_T1950342895_H
#define NCLUTILITIES_T1950342895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NclUtilities
struct  NclUtilities_t1950342895  : public RuntimeObject
{
public:

public:
};

struct NclUtilities_t1950342895_StaticFields
{
public:
	// System.Net.IPAddress[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.NclUtilities::_LocalAddresses
	IPAddressU5BU5D_t4087230954* ____LocalAddresses_0;
	// System.Object System.Net.NclUtilities::_LocalAddressesLock
	RuntimeObject * ____LocalAddressesLock_1;
	// System.String System.Net.NclUtilities::_LocalDomainName
	String_t* ____LocalDomainName_2;

public:
	inline static int32_t get_offset_of__LocalAddresses_0() { return static_cast<int32_t>(offsetof(NclUtilities_t1950342895_StaticFields, ____LocalAddresses_0)); }
	inline IPAddressU5BU5D_t4087230954* get__LocalAddresses_0() const { return ____LocalAddresses_0; }
	inline IPAddressU5BU5D_t4087230954** get_address_of__LocalAddresses_0() { return &____LocalAddresses_0; }
	inline void set__LocalAddresses_0(IPAddressU5BU5D_t4087230954* value)
	{
		____LocalAddresses_0 = value;
		Il2CppCodeGenWriteBarrier((&____LocalAddresses_0), value);
	}

	inline static int32_t get_offset_of__LocalAddressesLock_1() { return static_cast<int32_t>(offsetof(NclUtilities_t1950342895_StaticFields, ____LocalAddressesLock_1)); }
	inline RuntimeObject * get__LocalAddressesLock_1() const { return ____LocalAddressesLock_1; }
	inline RuntimeObject ** get_address_of__LocalAddressesLock_1() { return &____LocalAddressesLock_1; }
	inline void set__LocalAddressesLock_1(RuntimeObject * value)
	{
		____LocalAddressesLock_1 = value;
		Il2CppCodeGenWriteBarrier((&____LocalAddressesLock_1), value);
	}

	inline static int32_t get_offset_of__LocalDomainName_2() { return static_cast<int32_t>(offsetof(NclUtilities_t1950342895_StaticFields, ____LocalDomainName_2)); }
	inline String_t* get__LocalDomainName_2() const { return ____LocalDomainName_2; }
	inline String_t** get_address_of__LocalDomainName_2() { return &____LocalDomainName_2; }
	inline void set__LocalDomainName_2(String_t* value)
	{
		____LocalDomainName_2 = value;
		Il2CppCodeGenWriteBarrier((&____LocalDomainName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NCLUTILITIES_T1950342895_H
#ifndef VALIDATIONHELPER_T2126475125_H
#define VALIDATIONHELPER_T2126475125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ValidationHelper
struct  ValidationHelper_t2126475125  : public RuntimeObject
{
public:

public:
};

struct ValidationHelper_t2126475125_StaticFields
{
public:
	// System.String[] System.Net.ValidationHelper::EmptyArray
	StringU5BU5D_t1642385972* ___EmptyArray_0;
	// System.Char[] System.Net.ValidationHelper::InvalidMethodChars
	CharU5BU5D_t1328083999* ___InvalidMethodChars_1;
	// System.Char[] System.Net.ValidationHelper::InvalidParamChars
	CharU5BU5D_t1328083999* ___InvalidParamChars_2;

public:
	inline static int32_t get_offset_of_EmptyArray_0() { return static_cast<int32_t>(offsetof(ValidationHelper_t2126475125_StaticFields, ___EmptyArray_0)); }
	inline StringU5BU5D_t1642385972* get_EmptyArray_0() const { return ___EmptyArray_0; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyArray_0() { return &___EmptyArray_0; }
	inline void set_EmptyArray_0(StringU5BU5D_t1642385972* value)
	{
		___EmptyArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_0), value);
	}

	inline static int32_t get_offset_of_InvalidMethodChars_1() { return static_cast<int32_t>(offsetof(ValidationHelper_t2126475125_StaticFields, ___InvalidMethodChars_1)); }
	inline CharU5BU5D_t1328083999* get_InvalidMethodChars_1() const { return ___InvalidMethodChars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_InvalidMethodChars_1() { return &___InvalidMethodChars_1; }
	inline void set_InvalidMethodChars_1(CharU5BU5D_t1328083999* value)
	{
		___InvalidMethodChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidMethodChars_1), value);
	}

	inline static int32_t get_offset_of_InvalidParamChars_2() { return static_cast<int32_t>(offsetof(ValidationHelper_t2126475125_StaticFields, ___InvalidParamChars_2)); }
	inline CharU5BU5D_t1328083999* get_InvalidParamChars_2() const { return ___InvalidParamChars_2; }
	inline CharU5BU5D_t1328083999** get_address_of_InvalidParamChars_2() { return &___InvalidParamChars_2; }
	inline void set_InvalidParamChars_2(CharU5BU5D_t1328083999* value)
	{
		___InvalidParamChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidParamChars_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONHELPER_T2126475125_H
#ifndef EXCEPTIONHELPER_T2752294609_H
#define EXCEPTIONHELPER_T2752294609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ExceptionHelper
struct  ExceptionHelper_t2752294609  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHELPER_T2752294609_H
#ifndef HEADERINFO_T3044570949_H
#define HEADERINFO_T3044570949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfo
struct  HeaderInfo_t3044570949  : public RuntimeObject
{
public:
	// System.Boolean System.Net.HeaderInfo::IsRequestRestricted
	bool ___IsRequestRestricted_0;
	// System.Boolean System.Net.HeaderInfo::IsResponseRestricted
	bool ___IsResponseRestricted_1;
	// System.Net.HeaderParser System.Net.HeaderInfo::Parser
	HeaderParser_t3706119548 * ___Parser_2;
	// System.String System.Net.HeaderInfo::HeaderName
	String_t* ___HeaderName_3;
	// System.Boolean System.Net.HeaderInfo::AllowMultiValues
	bool ___AllowMultiValues_4;

public:
	inline static int32_t get_offset_of_IsRequestRestricted_0() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___IsRequestRestricted_0)); }
	inline bool get_IsRequestRestricted_0() const { return ___IsRequestRestricted_0; }
	inline bool* get_address_of_IsRequestRestricted_0() { return &___IsRequestRestricted_0; }
	inline void set_IsRequestRestricted_0(bool value)
	{
		___IsRequestRestricted_0 = value;
	}

	inline static int32_t get_offset_of_IsResponseRestricted_1() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___IsResponseRestricted_1)); }
	inline bool get_IsResponseRestricted_1() const { return ___IsResponseRestricted_1; }
	inline bool* get_address_of_IsResponseRestricted_1() { return &___IsResponseRestricted_1; }
	inline void set_IsResponseRestricted_1(bool value)
	{
		___IsResponseRestricted_1 = value;
	}

	inline static int32_t get_offset_of_Parser_2() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___Parser_2)); }
	inline HeaderParser_t3706119548 * get_Parser_2() const { return ___Parser_2; }
	inline HeaderParser_t3706119548 ** get_address_of_Parser_2() { return &___Parser_2; }
	inline void set_Parser_2(HeaderParser_t3706119548 * value)
	{
		___Parser_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parser_2), value);
	}

	inline static int32_t get_offset_of_HeaderName_3() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___HeaderName_3)); }
	inline String_t* get_HeaderName_3() const { return ___HeaderName_3; }
	inline String_t** get_address_of_HeaderName_3() { return &___HeaderName_3; }
	inline void set_HeaderName_3(String_t* value)
	{
		___HeaderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderName_3), value);
	}

	inline static int32_t get_offset_of_AllowMultiValues_4() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___AllowMultiValues_4)); }
	inline bool get_AllowMultiValues_4() const { return ___AllowMultiValues_4; }
	inline bool* get_address_of_AllowMultiValues_4() { return &___AllowMultiValues_4; }
	inline void set_AllowMultiValues_4(bool value)
	{
		___AllowMultiValues_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFO_T3044570949_H
#ifndef GLOBALPROXYSELECTION_T2251180943_H
#define GLOBALPROXYSELECTION_T2251180943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.GlobalProxySelection
struct  GlobalProxySelection_t2251180943  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALPROXYSELECTION_T2251180943_H
#ifndef MARSHALBYREFOBJECT_T1285298191_H
#define MARSHALBYREFOBJECT_T1285298191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1285298191  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1656058977 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1285298191, ____identity_0)); }
	inline ServerIdentity_t1656058977 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1656058977 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1656058977 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_pinvoke
{
	ServerIdentity_t1656058977 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_com
{
	ServerIdentity_t1656058977 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T1285298191_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef EXCLUSIVEREFERENCE_T708182869_H
#define EXCLUSIVEREFERENCE_T708182869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.ExclusiveReference
struct  ExclusiveReference_t708182869  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexRunner System.Text.RegularExpressions.ExclusiveReference::_ref
	RegexRunner_t3983612747 * ____ref_0;
	// System.Object System.Text.RegularExpressions.ExclusiveReference::_obj
	RuntimeObject * ____obj_1;
	// System.Int32 System.Text.RegularExpressions.ExclusiveReference::_locked
	int32_t ____locked_2;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____ref_0)); }
	inline RegexRunner_t3983612747 * get__ref_0() const { return ____ref_0; }
	inline RegexRunner_t3983612747 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(RegexRunner_t3983612747 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier((&____ref_0), value);
	}

	inline static int32_t get_offset_of__obj_1() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____obj_1)); }
	inline RuntimeObject * get__obj_1() const { return ____obj_1; }
	inline RuntimeObject ** get_address_of__obj_1() { return &____obj_1; }
	inline void set__obj_1(RuntimeObject * value)
	{
		____obj_1 = value;
		Il2CppCodeGenWriteBarrier((&____obj_1), value);
	}

	inline static int32_t get_offset_of__locked_2() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____locked_2)); }
	inline int32_t get__locked_2() const { return ____locked_2; }
	inline int32_t* get_address_of__locked_2() { return &____locked_2; }
	inline void set__locked_2(int32_t value)
	{
		____locked_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUSIVEREFERENCE_T708182869_H
#ifndef CACHEDCODEENTRY_T3553821051_H
#define CACHEDCODEENTRY_T3553821051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CachedCodeEntry
struct  CachedCodeEntry_t3553821051  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.CachedCodeEntry::_key
	String_t* ____key_0;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.CachedCodeEntry::_code
	RegexCode_t2469392150 * ____code_1;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_caps
	Hashtable_t909839986 * ____caps_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_capnames
	Hashtable_t909839986 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.CachedCodeEntry::_capslist
	StringU5BU5D_t1642385972* ____capslist_4;
	// System.Int32 System.Text.RegularExpressions.CachedCodeEntry::_capsize
	int32_t ____capsize_5;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.CachedCodeEntry::_factory
	RegexRunnerFactory_t3902733837 * ____factory_6;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.CachedCodeEntry::_runnerref
	ExclusiveReference_t708182869 * ____runnerref_7;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.CachedCodeEntry::_replref
	SharedReference_t2137668360 * ____replref_8;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____key_0)); }
	inline String_t* get__key_0() const { return ____key_0; }
	inline String_t** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(String_t* value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__code_1() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____code_1)); }
	inline RegexCode_t2469392150 * get__code_1() const { return ____code_1; }
	inline RegexCode_t2469392150 ** get_address_of__code_1() { return &____code_1; }
	inline void set__code_1(RegexCode_t2469392150 * value)
	{
		____code_1 = value;
		Il2CppCodeGenWriteBarrier((&____code_1), value);
	}

	inline static int32_t get_offset_of__caps_2() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____caps_2)); }
	inline Hashtable_t909839986 * get__caps_2() const { return ____caps_2; }
	inline Hashtable_t909839986 ** get_address_of__caps_2() { return &____caps_2; }
	inline void set__caps_2(Hashtable_t909839986 * value)
	{
		____caps_2 = value;
		Il2CppCodeGenWriteBarrier((&____caps_2), value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capnames_3)); }
	inline Hashtable_t909839986 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t909839986 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t909839986 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_3), value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capslist_4)); }
	inline StringU5BU5D_t1642385972* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1642385972** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1642385972* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier((&____capslist_4), value);
	}

	inline static int32_t get_offset_of__capsize_5() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capsize_5)); }
	inline int32_t get__capsize_5() const { return ____capsize_5; }
	inline int32_t* get_address_of__capsize_5() { return &____capsize_5; }
	inline void set__capsize_5(int32_t value)
	{
		____capsize_5 = value;
	}

	inline static int32_t get_offset_of__factory_6() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____factory_6)); }
	inline RegexRunnerFactory_t3902733837 * get__factory_6() const { return ____factory_6; }
	inline RegexRunnerFactory_t3902733837 ** get_address_of__factory_6() { return &____factory_6; }
	inline void set__factory_6(RegexRunnerFactory_t3902733837 * value)
	{
		____factory_6 = value;
		Il2CppCodeGenWriteBarrier((&____factory_6), value);
	}

	inline static int32_t get_offset_of__runnerref_7() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____runnerref_7)); }
	inline ExclusiveReference_t708182869 * get__runnerref_7() const { return ____runnerref_7; }
	inline ExclusiveReference_t708182869 ** get_address_of__runnerref_7() { return &____runnerref_7; }
	inline void set__runnerref_7(ExclusiveReference_t708182869 * value)
	{
		____runnerref_7 = value;
		Il2CppCodeGenWriteBarrier((&____runnerref_7), value);
	}

	inline static int32_t get_offset_of__replref_8() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____replref_8)); }
	inline SharedReference_t2137668360 * get__replref_8() const { return ____replref_8; }
	inline SharedReference_t2137668360 ** get_address_of__replref_8() { return &____replref_8; }
	inline void set__replref_8(SharedReference_t2137668360 * value)
	{
		____replref_8 = value;
		Il2CppCodeGenWriteBarrier((&____replref_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDCODEENTRY_T3553821051_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1927440687 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t169632028* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____innerException_4)); }
	inline Exception_t1927440687 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1927440687 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1927440687 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t1975884510 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t1975884510 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t1975884510 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t2217612696* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t2217612696** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t2217612696* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t169632028* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t169632028** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t169632028* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1927440687_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1927440687_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1927440687_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1927440687_H
#ifndef X509EXTENSIONENUMERATOR_T3763443773_H
#define X509EXTENSIONENUMERATOR_T3763443773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
struct  X509ExtensionEnumerator_t3763443773  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ExtensionEnumerator_t3763443773, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONENUMERATOR_T3763443773_H
#ifndef X509EXTENSIONCOLLECTION_T650873211_H
#define X509EXTENSIONCOLLECTION_T650873211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct  X509ExtensionCollection_t650873211  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ExtensionCollection::_list
	ArrayList_t4252133567 * ____list_1;

public:
	inline static int32_t get_offset_of__list_1() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t650873211, ____list_1)); }
	inline ArrayList_t4252133567 * get__list_1() const { return ____list_1; }
	inline ArrayList_t4252133567 ** get_address_of__list_1() { return &____list_1; }
	inline void set__list_1(ArrayList_t4252133567 * value)
	{
		____list_1 = value;
		Il2CppCodeGenWriteBarrier((&____list_1), value);
	}
};

struct X509ExtensionCollection_t650873211_StaticFields
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509ExtensionCollection::Empty
	ByteU5BU5D_t3397334013* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t650873211_StaticFields, ___Empty_0)); }
	inline ByteU5BU5D_t3397334013* get_Empty_0() const { return ___Empty_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ByteU5BU5D_t3397334013* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_T650873211_H
#ifndef X509HELPER2_T1000999714_H
#define X509HELPER2_T1000999714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Helper2
struct  X509Helper2_t1000999714  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509HELPER2_T1000999714_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#define NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2034248631  : public RuntimeObject
{
public:
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::_readOnly
	bool ____readOnly_0;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::_entriesArray
	ArrayList_t4252133567 * ____entriesArray_1;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::_keyComparer
	RuntimeObject* ____keyComparer_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_entriesTable
	Hashtable_t909839986 * ____entriesTable_3;
	// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_nullKeyEntry
	NameObjectEntry_t4229094479 * ____nullKeyEntry_4;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::_keys
	KeysCollection_t633582367 * ____keys_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::_serializationInfo
	SerializationInfo_t228987430 * ____serializationInfo_6;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::_version
	int32_t ____version_7;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase::_syncRoot
	RuntimeObject * ____syncRoot_8;

public:
	inline static int32_t get_offset_of__readOnly_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____readOnly_0)); }
	inline bool get__readOnly_0() const { return ____readOnly_0; }
	inline bool* get_address_of__readOnly_0() { return &____readOnly_0; }
	inline void set__readOnly_0(bool value)
	{
		____readOnly_0 = value;
	}

	inline static int32_t get_offset_of__entriesArray_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____entriesArray_1)); }
	inline ArrayList_t4252133567 * get__entriesArray_1() const { return ____entriesArray_1; }
	inline ArrayList_t4252133567 ** get_address_of__entriesArray_1() { return &____entriesArray_1; }
	inline void set__entriesArray_1(ArrayList_t4252133567 * value)
	{
		____entriesArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____entriesArray_1), value);
	}

	inline static int32_t get_offset_of__keyComparer_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____keyComparer_2)); }
	inline RuntimeObject* get__keyComparer_2() const { return ____keyComparer_2; }
	inline RuntimeObject** get_address_of__keyComparer_2() { return &____keyComparer_2; }
	inline void set__keyComparer_2(RuntimeObject* value)
	{
		____keyComparer_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyComparer_2), value);
	}

	inline static int32_t get_offset_of__entriesTable_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____entriesTable_3)); }
	inline Hashtable_t909839986 * get__entriesTable_3() const { return ____entriesTable_3; }
	inline Hashtable_t909839986 ** get_address_of__entriesTable_3() { return &____entriesTable_3; }
	inline void set__entriesTable_3(Hashtable_t909839986 * value)
	{
		____entriesTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____entriesTable_3), value);
	}

	inline static int32_t get_offset_of__nullKeyEntry_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____nullKeyEntry_4)); }
	inline NameObjectEntry_t4229094479 * get__nullKeyEntry_4() const { return ____nullKeyEntry_4; }
	inline NameObjectEntry_t4229094479 ** get_address_of__nullKeyEntry_4() { return &____nullKeyEntry_4; }
	inline void set__nullKeyEntry_4(NameObjectEntry_t4229094479 * value)
	{
		____nullKeyEntry_4 = value;
		Il2CppCodeGenWriteBarrier((&____nullKeyEntry_4), value);
	}

	inline static int32_t get_offset_of__keys_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____keys_5)); }
	inline KeysCollection_t633582367 * get__keys_5() const { return ____keys_5; }
	inline KeysCollection_t633582367 ** get_address_of__keys_5() { return &____keys_5; }
	inline void set__keys_5(KeysCollection_t633582367 * value)
	{
		____keys_5 = value;
		Il2CppCodeGenWriteBarrier((&____keys_5), value);
	}

	inline static int32_t get_offset_of__serializationInfo_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____serializationInfo_6)); }
	inline SerializationInfo_t228987430 * get__serializationInfo_6() const { return ____serializationInfo_6; }
	inline SerializationInfo_t228987430 ** get_address_of__serializationInfo_6() { return &____serializationInfo_6; }
	inline void set__serializationInfo_6(SerializationInfo_t228987430 * value)
	{
		____serializationInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____serializationInfo_6), value);
	}

	inline static int32_t get_offset_of__version_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____version_7)); }
	inline int32_t get__version_7() const { return ____version_7; }
	inline int32_t* get_address_of__version_7() { return &____version_7; }
	inline void set__version_7(int32_t value)
	{
		____version_7 = value;
	}

	inline static int32_t get_offset_of__syncRoot_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____syncRoot_8)); }
	inline RuntimeObject * get__syncRoot_8() const { return ____syncRoot_8; }
	inline RuntimeObject ** get_address_of__syncRoot_8() { return &____syncRoot_8; }
	inline void set__syncRoot_8(RuntimeObject * value)
	{
		____syncRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_8), value);
	}
};

struct NameObjectCollectionBase_t2034248631_StaticFields
{
public:
	// System.StringComparer System.Collections.Specialized.NameObjectCollectionBase::defaultComparer
	StringComparer_t1574862926 * ___defaultComparer_9;

public:
	inline static int32_t get_offset_of_defaultComparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631_StaticFields, ___defaultComparer_9)); }
	inline StringComparer_t1574862926 * get_defaultComparer_9() const { return ___defaultComparer_9; }
	inline StringComparer_t1574862926 ** get_address_of_defaultComparer_9() { return &___defaultComparer_9; }
	inline void set_defaultComparer_9(StringComparer_t1574862926 * value)
	{
		___defaultComparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultComparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#ifndef MYNATIVEHELPER_T2161630009_H
#define MYNATIVEHELPER_T2161630009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Helper2/MyNativeHelper
struct  MyNativeHelper_t2161630009  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYNATIVEHELPER_T2161630009_H
#ifndef ASNENCODEDDATA_T463456204_H
#define ASNENCODEDDATA_T463456204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t463456204  : public RuntimeObject
{
public:
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t3221867120 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t3397334013* ____raw_1;

public:
	inline static int32_t get_offset_of__oid_0() { return static_cast<int32_t>(offsetof(AsnEncodedData_t463456204, ____oid_0)); }
	inline Oid_t3221867120 * get__oid_0() const { return ____oid_0; }
	inline Oid_t3221867120 ** get_address_of__oid_0() { return &____oid_0; }
	inline void set__oid_0(Oid_t3221867120 * value)
	{
		____oid_0 = value;
		Il2CppCodeGenWriteBarrier((&____oid_0), value);
	}

	inline static int32_t get_offset_of__raw_1() { return static_cast<int32_t>(offsetof(AsnEncodedData_t463456204, ____raw_1)); }
	inline ByteU5BU5D_t3397334013* get__raw_1() const { return ____raw_1; }
	inline ByteU5BU5D_t3397334013** get_address_of__raw_1() { return &____raw_1; }
	inline void set__raw_1(ByteU5BU5D_t3397334013* value)
	{
		____raw_1 = value;
		Il2CppCodeGenWriteBarrier((&____raw_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNENCODEDDATA_T463456204_H
#ifndef HEADERINFOTABLE_T2462863175_H
#define HEADERINFOTABLE_T2462863175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfoTable
struct  HeaderInfoTable_t2462863175  : public RuntimeObject
{
public:

public:
};

struct HeaderInfoTable_t2462863175_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.HeaderInfoTable::HeaderHashTable
	Hashtable_t909839986 * ___HeaderHashTable_0;
	// System.Net.HeaderInfo System.Net.HeaderInfoTable::UnknownHeaderInfo
	HeaderInfo_t3044570949 * ___UnknownHeaderInfo_1;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::SingleParser
	HeaderParser_t3706119548 * ___SingleParser_2;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::MultiParser
	HeaderParser_t3706119548 * ___MultiParser_3;

public:
	inline static int32_t get_offset_of_HeaderHashTable_0() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___HeaderHashTable_0)); }
	inline Hashtable_t909839986 * get_HeaderHashTable_0() const { return ___HeaderHashTable_0; }
	inline Hashtable_t909839986 ** get_address_of_HeaderHashTable_0() { return &___HeaderHashTable_0; }
	inline void set_HeaderHashTable_0(Hashtable_t909839986 * value)
	{
		___HeaderHashTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderHashTable_0), value);
	}

	inline static int32_t get_offset_of_UnknownHeaderInfo_1() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___UnknownHeaderInfo_1)); }
	inline HeaderInfo_t3044570949 * get_UnknownHeaderInfo_1() const { return ___UnknownHeaderInfo_1; }
	inline HeaderInfo_t3044570949 ** get_address_of_UnknownHeaderInfo_1() { return &___UnknownHeaderInfo_1; }
	inline void set_UnknownHeaderInfo_1(HeaderInfo_t3044570949 * value)
	{
		___UnknownHeaderInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnknownHeaderInfo_1), value);
	}

	inline static int32_t get_offset_of_SingleParser_2() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___SingleParser_2)); }
	inline HeaderParser_t3706119548 * get_SingleParser_2() const { return ___SingleParser_2; }
	inline HeaderParser_t3706119548 ** get_address_of_SingleParser_2() { return &___SingleParser_2; }
	inline void set_SingleParser_2(HeaderParser_t3706119548 * value)
	{
		___SingleParser_2 = value;
		Il2CppCodeGenWriteBarrier((&___SingleParser_2), value);
	}

	inline static int32_t get_offset_of_MultiParser_3() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___MultiParser_3)); }
	inline HeaderParser_t3706119548 * get_MultiParser_3() const { return ___MultiParser_3; }
	inline HeaderParser_t3706119548 ** get_address_of_MultiParser_3() { return &___MultiParser_3; }
	inline void set_MultiParser_3(HeaderParser_t3706119548 * value)
	{
		___MultiParser_3 = value;
		Il2CppCodeGenWriteBarrier((&___MultiParser_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFOTABLE_T2462863175_H
#ifndef REGEXCODE_T2469392150_H
#define REGEXCODE_T2469392150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCode
struct  RegexCode_t2469392150  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexCode::_codes
	Int32U5BU5D_t3030399641* ____codes_0;
	// System.String[] System.Text.RegularExpressions.RegexCode::_strings
	StringU5BU5D_t1642385972* ____strings_1;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_trackcount
	int32_t ____trackcount_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexCode::_caps
	Hashtable_t909839986 * ____caps_3;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_capsize
	int32_t ____capsize_4;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexCode::_fcPrefix
	RegexPrefix_t1013837165 * ____fcPrefix_5;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexCode::_bmPrefix
	RegexBoyerMoore_t2204811018 * ____bmPrefix_6;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_anchors
	int32_t ____anchors_7;
	// System.Boolean System.Text.RegularExpressions.RegexCode::_rightToLeft
	bool ____rightToLeft_8;

public:
	inline static int32_t get_offset_of__codes_0() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____codes_0)); }
	inline Int32U5BU5D_t3030399641* get__codes_0() const { return ____codes_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__codes_0() { return &____codes_0; }
	inline void set__codes_0(Int32U5BU5D_t3030399641* value)
	{
		____codes_0 = value;
		Il2CppCodeGenWriteBarrier((&____codes_0), value);
	}

	inline static int32_t get_offset_of__strings_1() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____strings_1)); }
	inline StringU5BU5D_t1642385972* get__strings_1() const { return ____strings_1; }
	inline StringU5BU5D_t1642385972** get_address_of__strings_1() { return &____strings_1; }
	inline void set__strings_1(StringU5BU5D_t1642385972* value)
	{
		____strings_1 = value;
		Il2CppCodeGenWriteBarrier((&____strings_1), value);
	}

	inline static int32_t get_offset_of__trackcount_2() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____trackcount_2)); }
	inline int32_t get__trackcount_2() const { return ____trackcount_2; }
	inline int32_t* get_address_of__trackcount_2() { return &____trackcount_2; }
	inline void set__trackcount_2(int32_t value)
	{
		____trackcount_2 = value;
	}

	inline static int32_t get_offset_of__caps_3() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____caps_3)); }
	inline Hashtable_t909839986 * get__caps_3() const { return ____caps_3; }
	inline Hashtable_t909839986 ** get_address_of__caps_3() { return &____caps_3; }
	inline void set__caps_3(Hashtable_t909839986 * value)
	{
		____caps_3 = value;
		Il2CppCodeGenWriteBarrier((&____caps_3), value);
	}

	inline static int32_t get_offset_of__capsize_4() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____capsize_4)); }
	inline int32_t get__capsize_4() const { return ____capsize_4; }
	inline int32_t* get_address_of__capsize_4() { return &____capsize_4; }
	inline void set__capsize_4(int32_t value)
	{
		____capsize_4 = value;
	}

	inline static int32_t get_offset_of__fcPrefix_5() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____fcPrefix_5)); }
	inline RegexPrefix_t1013837165 * get__fcPrefix_5() const { return ____fcPrefix_5; }
	inline RegexPrefix_t1013837165 ** get_address_of__fcPrefix_5() { return &____fcPrefix_5; }
	inline void set__fcPrefix_5(RegexPrefix_t1013837165 * value)
	{
		____fcPrefix_5 = value;
		Il2CppCodeGenWriteBarrier((&____fcPrefix_5), value);
	}

	inline static int32_t get_offset_of__bmPrefix_6() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____bmPrefix_6)); }
	inline RegexBoyerMoore_t2204811018 * get__bmPrefix_6() const { return ____bmPrefix_6; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of__bmPrefix_6() { return &____bmPrefix_6; }
	inline void set__bmPrefix_6(RegexBoyerMoore_t2204811018 * value)
	{
		____bmPrefix_6 = value;
		Il2CppCodeGenWriteBarrier((&____bmPrefix_6), value);
	}

	inline static int32_t get_offset_of__anchors_7() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____anchors_7)); }
	inline int32_t get__anchors_7() const { return ____anchors_7; }
	inline int32_t* get_address_of__anchors_7() { return &____anchors_7; }
	inline void set__anchors_7(int32_t value)
	{
		____anchors_7 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_8() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____rightToLeft_8)); }
	inline bool get__rightToLeft_8() const { return ____rightToLeft_8; }
	inline bool* get_address_of__rightToLeft_8() { return &____rightToLeft_8; }
	inline void set__rightToLeft_8(bool value)
	{
		____rightToLeft_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCODE_T2469392150_H
#ifndef SINGLERANGE_T3794243288_H
#define SINGLERANGE_T3794243288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/SingleRange
struct  SingleRange_t3794243288  : public RuntimeObject
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/SingleRange::_first
	Il2CppChar ____first_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/SingleRange::_last
	Il2CppChar ____last_1;

public:
	inline static int32_t get_offset_of__first_0() { return static_cast<int32_t>(offsetof(SingleRange_t3794243288, ____first_0)); }
	inline Il2CppChar get__first_0() const { return ____first_0; }
	inline Il2CppChar* get_address_of__first_0() { return &____first_0; }
	inline void set__first_0(Il2CppChar value)
	{
		____first_0 = value;
	}

	inline static int32_t get_offset_of__last_1() { return static_cast<int32_t>(offsetof(SingleRange_t3794243288, ____last_1)); }
	inline Il2CppChar get__last_1() const { return ____last_1; }
	inline Il2CppChar* get_address_of__last_1() { return &____last_1; }
	inline void set__last_1(Il2CppChar value)
	{
		____last_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERANGE_T3794243288_H
#ifndef SINGLERANGECOMPARER_T3640568023_H
#define SINGLERANGECOMPARER_T3640568023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/SingleRangeComparer
struct  SingleRangeComparer_t3640568023  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERANGECOMPARER_T3640568023_H
#ifndef CAPTURE_T4157900610_H
#define CAPTURE_T4157900610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t4157900610  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Capture::_text
	String_t* ____text_0;
	// System.Int32 System.Text.RegularExpressions.Capture::_index
	int32_t ____index_1;
	// System.Int32 System.Text.RegularExpressions.Capture::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__text_0() { return static_cast<int32_t>(offsetof(Capture_t4157900610, ____text_0)); }
	inline String_t* get__text_0() const { return ____text_0; }
	inline String_t** get_address_of__text_0() { return &____text_0; }
	inline void set__text_0(String_t* value)
	{
		____text_0 = value;
		Il2CppCodeGenWriteBarrier((&____text_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Capture_t4157900610, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Capture_t4157900610, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T4157900610_H
#ifndef REGEXBOYERMOORE_T2204811018_H
#define REGEXBOYERMOORE_T2204811018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexBoyerMoore
struct  RegexBoyerMoore_t2204811018  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexBoyerMoore::_positive
	Int32U5BU5D_t3030399641* ____positive_0;
	// System.Int32[] System.Text.RegularExpressions.RegexBoyerMoore::_negativeASCII
	Int32U5BU5D_t3030399641* ____negativeASCII_1;
	// System.Int32[][] System.Text.RegularExpressions.RegexBoyerMoore::_negativeUnicode
	Int32U5BU5DU5BU5D_t3750818532* ____negativeUnicode_2;
	// System.String System.Text.RegularExpressions.RegexBoyerMoore::_pattern
	String_t* ____pattern_3;
	// System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::_lowASCII
	int32_t ____lowASCII_4;
	// System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::_highASCII
	int32_t ____highASCII_5;
	// System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::_rightToLeft
	bool ____rightToLeft_6;
	// System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::_caseInsensitive
	bool ____caseInsensitive_7;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexBoyerMoore::_culture
	CultureInfo_t3500843524 * ____culture_8;

public:
	inline static int32_t get_offset_of__positive_0() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____positive_0)); }
	inline Int32U5BU5D_t3030399641* get__positive_0() const { return ____positive_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__positive_0() { return &____positive_0; }
	inline void set__positive_0(Int32U5BU5D_t3030399641* value)
	{
		____positive_0 = value;
		Il2CppCodeGenWriteBarrier((&____positive_0), value);
	}

	inline static int32_t get_offset_of__negativeASCII_1() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____negativeASCII_1)); }
	inline Int32U5BU5D_t3030399641* get__negativeASCII_1() const { return ____negativeASCII_1; }
	inline Int32U5BU5D_t3030399641** get_address_of__negativeASCII_1() { return &____negativeASCII_1; }
	inline void set__negativeASCII_1(Int32U5BU5D_t3030399641* value)
	{
		____negativeASCII_1 = value;
		Il2CppCodeGenWriteBarrier((&____negativeASCII_1), value);
	}

	inline static int32_t get_offset_of__negativeUnicode_2() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____negativeUnicode_2)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__negativeUnicode_2() const { return ____negativeUnicode_2; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__negativeUnicode_2() { return &____negativeUnicode_2; }
	inline void set__negativeUnicode_2(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____negativeUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&____negativeUnicode_2), value);
	}

	inline static int32_t get_offset_of__pattern_3() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____pattern_3)); }
	inline String_t* get__pattern_3() const { return ____pattern_3; }
	inline String_t** get_address_of__pattern_3() { return &____pattern_3; }
	inline void set__pattern_3(String_t* value)
	{
		____pattern_3 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_3), value);
	}

	inline static int32_t get_offset_of__lowASCII_4() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____lowASCII_4)); }
	inline int32_t get__lowASCII_4() const { return ____lowASCII_4; }
	inline int32_t* get_address_of__lowASCII_4() { return &____lowASCII_4; }
	inline void set__lowASCII_4(int32_t value)
	{
		____lowASCII_4 = value;
	}

	inline static int32_t get_offset_of__highASCII_5() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____highASCII_5)); }
	inline int32_t get__highASCII_5() const { return ____highASCII_5; }
	inline int32_t* get_address_of__highASCII_5() { return &____highASCII_5; }
	inline void set__highASCII_5(int32_t value)
	{
		____highASCII_5 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_6() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____rightToLeft_6)); }
	inline bool get__rightToLeft_6() const { return ____rightToLeft_6; }
	inline bool* get_address_of__rightToLeft_6() { return &____rightToLeft_6; }
	inline void set__rightToLeft_6(bool value)
	{
		____rightToLeft_6 = value;
	}

	inline static int32_t get_offset_of__caseInsensitive_7() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____caseInsensitive_7)); }
	inline bool get__caseInsensitive_7() const { return ____caseInsensitive_7; }
	inline bool* get_address_of__caseInsensitive_7() { return &____caseInsensitive_7; }
	inline void set__caseInsensitive_7(bool value)
	{
		____caseInsensitive_7 = value;
	}

	inline static int32_t get_offset_of__culture_8() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____culture_8)); }
	inline CultureInfo_t3500843524 * get__culture_8() const { return ____culture_8; }
	inline CultureInfo_t3500843524 ** get_address_of__culture_8() { return &____culture_8; }
	inline void set__culture_8(CultureInfo_t3500843524 * value)
	{
		____culture_8 = value;
		Il2CppCodeGenWriteBarrier((&____culture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXBOYERMOORE_T2204811018_H
#ifndef SHAREDREFERENCE_T2137668360_H
#define SHAREDREFERENCE_T2137668360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.SharedReference
struct  SharedReference_t2137668360  : public RuntimeObject
{
public:
	// System.WeakReference System.Text.RegularExpressions.SharedReference::_ref
	WeakReference_t1077405567 * ____ref_0;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(SharedReference_t2137668360, ____ref_0)); }
	inline WeakReference_t1077405567 * get__ref_0() const { return ____ref_0; }
	inline WeakReference_t1077405567 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(WeakReference_t1077405567 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier((&____ref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDREFERENCE_T2137668360_H
#ifndef REGEXCHARCLASS_T2441867401_H
#define REGEXCHARCLASS_T2441867401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass
struct  RegexCharClass_t2441867401  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange> System.Text.RegularExpressions.RegexCharClass::_rangelist
	List_1_t3163364420 * ____rangelist_0;
	// System.Text.StringBuilder System.Text.RegularExpressions.RegexCharClass::_categories
	StringBuilder_t1221177846 * ____categories_1;
	// System.Boolean System.Text.RegularExpressions.RegexCharClass::_canonical
	bool ____canonical_2;
	// System.Boolean System.Text.RegularExpressions.RegexCharClass::_negate
	bool ____negate_3;
	// System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexCharClass::_subtractor
	RegexCharClass_t2441867401 * ____subtractor_4;

public:
	inline static int32_t get_offset_of__rangelist_0() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____rangelist_0)); }
	inline List_1_t3163364420 * get__rangelist_0() const { return ____rangelist_0; }
	inline List_1_t3163364420 ** get_address_of__rangelist_0() { return &____rangelist_0; }
	inline void set__rangelist_0(List_1_t3163364420 * value)
	{
		____rangelist_0 = value;
		Il2CppCodeGenWriteBarrier((&____rangelist_0), value);
	}

	inline static int32_t get_offset_of__categories_1() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____categories_1)); }
	inline StringBuilder_t1221177846 * get__categories_1() const { return ____categories_1; }
	inline StringBuilder_t1221177846 ** get_address_of__categories_1() { return &____categories_1; }
	inline void set__categories_1(StringBuilder_t1221177846 * value)
	{
		____categories_1 = value;
		Il2CppCodeGenWriteBarrier((&____categories_1), value);
	}

	inline static int32_t get_offset_of__canonical_2() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____canonical_2)); }
	inline bool get__canonical_2() const { return ____canonical_2; }
	inline bool* get_address_of__canonical_2() { return &____canonical_2; }
	inline void set__canonical_2(bool value)
	{
		____canonical_2 = value;
	}

	inline static int32_t get_offset_of__negate_3() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____negate_3)); }
	inline bool get__negate_3() const { return ____negate_3; }
	inline bool* get_address_of__negate_3() { return &____negate_3; }
	inline void set__negate_3(bool value)
	{
		____negate_3 = value;
	}

	inline static int32_t get_offset_of__subtractor_4() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____subtractor_4)); }
	inline RegexCharClass_t2441867401 * get__subtractor_4() const { return ____subtractor_4; }
	inline RegexCharClass_t2441867401 ** get_address_of__subtractor_4() { return &____subtractor_4; }
	inline void set__subtractor_4(RegexCharClass_t2441867401 * value)
	{
		____subtractor_4 = value;
		Il2CppCodeGenWriteBarrier((&____subtractor_4), value);
	}
};

struct RegexCharClass_t2441867401_StaticFields
{
public:
	// System.String System.Text.RegularExpressions.RegexCharClass::InternalRegexIgnoreCase
	String_t* ___InternalRegexIgnoreCase_5;
	// System.String System.Text.RegularExpressions.RegexCharClass::Space
	String_t* ___Space_6;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotSpace
	String_t* ___NotSpace_7;
	// System.String System.Text.RegularExpressions.RegexCharClass::Word
	String_t* ___Word_8;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotWord
	String_t* ___NotWord_9;
	// System.String System.Text.RegularExpressions.RegexCharClass::SpaceClass
	String_t* ___SpaceClass_10;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotSpaceClass
	String_t* ___NotSpaceClass_11;
	// System.String System.Text.RegularExpressions.RegexCharClass::WordClass
	String_t* ___WordClass_12;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotWordClass
	String_t* ___NotWordClass_13;
	// System.String System.Text.RegularExpressions.RegexCharClass::DigitClass
	String_t* ___DigitClass_14;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotDigitClass
	String_t* ___NotDigitClass_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Text.RegularExpressions.RegexCharClass::_definedCategories
	Dictionary_2_t3943999495 * ____definedCategories_16;
	// System.String[0...,0...] System.Text.RegularExpressions.RegexCharClass::_propTable
	StringU5B0___U2C0___U5D_t1642385973* ____propTable_17;
	// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping[] System.Text.RegularExpressions.RegexCharClass::_lcTable
	LowerCaseMappingU5BU5D_t882374823* ____lcTable_18;

public:
	inline static int32_t get_offset_of_InternalRegexIgnoreCase_5() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___InternalRegexIgnoreCase_5)); }
	inline String_t* get_InternalRegexIgnoreCase_5() const { return ___InternalRegexIgnoreCase_5; }
	inline String_t** get_address_of_InternalRegexIgnoreCase_5() { return &___InternalRegexIgnoreCase_5; }
	inline void set_InternalRegexIgnoreCase_5(String_t* value)
	{
		___InternalRegexIgnoreCase_5 = value;
		Il2CppCodeGenWriteBarrier((&___InternalRegexIgnoreCase_5), value);
	}

	inline static int32_t get_offset_of_Space_6() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___Space_6)); }
	inline String_t* get_Space_6() const { return ___Space_6; }
	inline String_t** get_address_of_Space_6() { return &___Space_6; }
	inline void set_Space_6(String_t* value)
	{
		___Space_6 = value;
		Il2CppCodeGenWriteBarrier((&___Space_6), value);
	}

	inline static int32_t get_offset_of_NotSpace_7() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotSpace_7)); }
	inline String_t* get_NotSpace_7() const { return ___NotSpace_7; }
	inline String_t** get_address_of_NotSpace_7() { return &___NotSpace_7; }
	inline void set_NotSpace_7(String_t* value)
	{
		___NotSpace_7 = value;
		Il2CppCodeGenWriteBarrier((&___NotSpace_7), value);
	}

	inline static int32_t get_offset_of_Word_8() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___Word_8)); }
	inline String_t* get_Word_8() const { return ___Word_8; }
	inline String_t** get_address_of_Word_8() { return &___Word_8; }
	inline void set_Word_8(String_t* value)
	{
		___Word_8 = value;
		Il2CppCodeGenWriteBarrier((&___Word_8), value);
	}

	inline static int32_t get_offset_of_NotWord_9() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotWord_9)); }
	inline String_t* get_NotWord_9() const { return ___NotWord_9; }
	inline String_t** get_address_of_NotWord_9() { return &___NotWord_9; }
	inline void set_NotWord_9(String_t* value)
	{
		___NotWord_9 = value;
		Il2CppCodeGenWriteBarrier((&___NotWord_9), value);
	}

	inline static int32_t get_offset_of_SpaceClass_10() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___SpaceClass_10)); }
	inline String_t* get_SpaceClass_10() const { return ___SpaceClass_10; }
	inline String_t** get_address_of_SpaceClass_10() { return &___SpaceClass_10; }
	inline void set_SpaceClass_10(String_t* value)
	{
		___SpaceClass_10 = value;
		Il2CppCodeGenWriteBarrier((&___SpaceClass_10), value);
	}

	inline static int32_t get_offset_of_NotSpaceClass_11() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotSpaceClass_11)); }
	inline String_t* get_NotSpaceClass_11() const { return ___NotSpaceClass_11; }
	inline String_t** get_address_of_NotSpaceClass_11() { return &___NotSpaceClass_11; }
	inline void set_NotSpaceClass_11(String_t* value)
	{
		___NotSpaceClass_11 = value;
		Il2CppCodeGenWriteBarrier((&___NotSpaceClass_11), value);
	}

	inline static int32_t get_offset_of_WordClass_12() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___WordClass_12)); }
	inline String_t* get_WordClass_12() const { return ___WordClass_12; }
	inline String_t** get_address_of_WordClass_12() { return &___WordClass_12; }
	inline void set_WordClass_12(String_t* value)
	{
		___WordClass_12 = value;
		Il2CppCodeGenWriteBarrier((&___WordClass_12), value);
	}

	inline static int32_t get_offset_of_NotWordClass_13() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotWordClass_13)); }
	inline String_t* get_NotWordClass_13() const { return ___NotWordClass_13; }
	inline String_t** get_address_of_NotWordClass_13() { return &___NotWordClass_13; }
	inline void set_NotWordClass_13(String_t* value)
	{
		___NotWordClass_13 = value;
		Il2CppCodeGenWriteBarrier((&___NotWordClass_13), value);
	}

	inline static int32_t get_offset_of_DigitClass_14() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___DigitClass_14)); }
	inline String_t* get_DigitClass_14() const { return ___DigitClass_14; }
	inline String_t** get_address_of_DigitClass_14() { return &___DigitClass_14; }
	inline void set_DigitClass_14(String_t* value)
	{
		___DigitClass_14 = value;
		Il2CppCodeGenWriteBarrier((&___DigitClass_14), value);
	}

	inline static int32_t get_offset_of_NotDigitClass_15() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotDigitClass_15)); }
	inline String_t* get_NotDigitClass_15() const { return ___NotDigitClass_15; }
	inline String_t** get_address_of_NotDigitClass_15() { return &___NotDigitClass_15; }
	inline void set_NotDigitClass_15(String_t* value)
	{
		___NotDigitClass_15 = value;
		Il2CppCodeGenWriteBarrier((&___NotDigitClass_15), value);
	}

	inline static int32_t get_offset_of__definedCategories_16() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ____definedCategories_16)); }
	inline Dictionary_2_t3943999495 * get__definedCategories_16() const { return ____definedCategories_16; }
	inline Dictionary_2_t3943999495 ** get_address_of__definedCategories_16() { return &____definedCategories_16; }
	inline void set__definedCategories_16(Dictionary_2_t3943999495 * value)
	{
		____definedCategories_16 = value;
		Il2CppCodeGenWriteBarrier((&____definedCategories_16), value);
	}

	inline static int32_t get_offset_of__propTable_17() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ____propTable_17)); }
	inline StringU5B0___U2C0___U5D_t1642385973* get__propTable_17() const { return ____propTable_17; }
	inline StringU5B0___U2C0___U5D_t1642385973** get_address_of__propTable_17() { return &____propTable_17; }
	inline void set__propTable_17(StringU5B0___U2C0___U5D_t1642385973* value)
	{
		____propTable_17 = value;
		Il2CppCodeGenWriteBarrier((&____propTable_17), value);
	}

	inline static int32_t get_offset_of__lcTable_18() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ____lcTable_18)); }
	inline LowerCaseMappingU5BU5D_t882374823* get__lcTable_18() const { return ____lcTable_18; }
	inline LowerCaseMappingU5BU5D_t882374823** get_address_of__lcTable_18() { return &____lcTable_18; }
	inline void set__lcTable_18(LowerCaseMappingU5BU5D_t882374823* value)
	{
		____lcTable_18 = value;
		Il2CppCodeGenWriteBarrier((&____lcTable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCHARCLASS_T2441867401_H
#ifndef IPENDPOINT_T2615413766_H
#define IPENDPOINT_T2615413766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_t2615413766  : public EndPoint_t4156119363
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::m_Address
	IPAddress_t1399971723 * ___m_Address_0;
	// System.Int32 System.Net.IPEndPoint::m_Port
	int32_t ___m_Port_1;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766, ___m_Address_0)); }
	inline IPAddress_t1399971723 * get_m_Address_0() const { return ___m_Address_0; }
	inline IPAddress_t1399971723 ** get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(IPAddress_t1399971723 * value)
	{
		___m_Address_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Address_0), value);
	}

	inline static int32_t get_offset_of_m_Port_1() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766, ___m_Port_1)); }
	inline int32_t get_m_Port_1() const { return ___m_Port_1; }
	inline int32_t* get_address_of_m_Port_1() { return &___m_Port_1; }
	inline void set_m_Port_1(int32_t value)
	{
		___m_Port_1 = value;
	}
};

struct IPEndPoint_t2615413766_StaticFields
{
public:
	// System.Net.IPEndPoint System.Net.IPEndPoint::Any
	IPEndPoint_t2615413766 * ___Any_2;
	// System.Net.IPEndPoint System.Net.IPEndPoint::IPv6Any
	IPEndPoint_t2615413766 * ___IPv6Any_3;

public:
	inline static int32_t get_offset_of_Any_2() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766_StaticFields, ___Any_2)); }
	inline IPEndPoint_t2615413766 * get_Any_2() const { return ___Any_2; }
	inline IPEndPoint_t2615413766 ** get_address_of_Any_2() { return &___Any_2; }
	inline void set_Any_2(IPEndPoint_t2615413766 * value)
	{
		___Any_2 = value;
		Il2CppCodeGenWriteBarrier((&___Any_2), value);
	}

	inline static int32_t get_offset_of_IPv6Any_3() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766_StaticFields, ___IPv6Any_3)); }
	inline IPEndPoint_t2615413766 * get_IPv6Any_3() const { return ___IPv6Any_3; }
	inline IPEndPoint_t2615413766 ** get_address_of_IPv6Any_3() { return &___IPv6Any_3; }
	inline void set_IPv6Any_3(IPEndPoint_t2615413766 * value)
	{
		___IPv6Any_3 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPENDPOINT_T2615413766_H
#ifndef WEBRESPONSE_T1895226051_H
#define WEBRESPONSE_T1895226051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t1895226051  : public MarshalByRefObject_t1285298191
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T1895226051_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef LABEL_T4243202660_H
#define LABEL_T4243202660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.Label
struct  Label_t4243202660 
{
public:
	// System.Int32 System.Reflection.Emit.Label::label
	int32_t ___label_0;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(Label_t4243202660, ___label_0)); }
	inline int32_t get_label_0() const { return ___label_0; }
	inline int32_t* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(int32_t value)
	{
		___label_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABEL_T4243202660_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1841601450__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef WEBPROXYWRAPPER_T3016229293_H
#define WEBPROXYWRAPPER_T3016229293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/WebProxyWrapper
struct  WebProxyWrapper_t3016229293  : public WebProxyWrapperOpaque_t1012624580
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYWRAPPER_T3016229293_H
#ifndef NAMEVALUECOLLECTION_T3047564564_H
#define NAMEVALUECOLLECTION_T3047564564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t3047564564  : public NameObjectCollectionBase_t2034248631
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::_all
	StringU5BU5D_t1642385972* ____all_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::_allKeys
	StringU5BU5D_t1642385972* ____allKeys_11;

public:
	inline static int32_t get_offset_of__all_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t3047564564, ____all_10)); }
	inline StringU5BU5D_t1642385972* get__all_10() const { return ____all_10; }
	inline StringU5BU5D_t1642385972** get_address_of__all_10() { return &____all_10; }
	inline void set__all_10(StringU5BU5D_t1642385972* value)
	{
		____all_10 = value;
		Il2CppCodeGenWriteBarrier((&____all_10), value);
	}

	inline static int32_t get_offset_of__allKeys_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t3047564564, ____allKeys_11)); }
	inline StringU5BU5D_t1642385972* get__allKeys_11() const { return ____allKeys_11; }
	inline StringU5BU5D_t1642385972** get_address_of__allKeys_11() { return &____allKeys_11; }
	inline void set__allKeys_11(StringU5BU5D_t1642385972* value)
	{
		____allKeys_11 = value;
		Il2CppCodeGenWriteBarrier((&____allKeys_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T3047564564_H
#ifndef COMPILEDREGEXRUNNER_T592995170_H
#define COMPILEDREGEXRUNNER_T592995170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CompiledRegexRunner
struct  CompiledRegexRunner_t592995170  : public RegexRunner_t3983612747
{
public:
	// System.Text.RegularExpressions.NoParamDelegate System.Text.RegularExpressions.CompiledRegexRunner::goMethod
	NoParamDelegate_t2890182105 * ___goMethod_18;
	// System.Text.RegularExpressions.FindFirstCharDelegate System.Text.RegularExpressions.CompiledRegexRunner::findFirstCharMethod
	FindFirstCharDelegate_t4203859986 * ___findFirstCharMethod_19;
	// System.Text.RegularExpressions.NoParamDelegate System.Text.RegularExpressions.CompiledRegexRunner::initTrackCountMethod
	NoParamDelegate_t2890182105 * ___initTrackCountMethod_20;

public:
	inline static int32_t get_offset_of_goMethod_18() { return static_cast<int32_t>(offsetof(CompiledRegexRunner_t592995170, ___goMethod_18)); }
	inline NoParamDelegate_t2890182105 * get_goMethod_18() const { return ___goMethod_18; }
	inline NoParamDelegate_t2890182105 ** get_address_of_goMethod_18() { return &___goMethod_18; }
	inline void set_goMethod_18(NoParamDelegate_t2890182105 * value)
	{
		___goMethod_18 = value;
		Il2CppCodeGenWriteBarrier((&___goMethod_18), value);
	}

	inline static int32_t get_offset_of_findFirstCharMethod_19() { return static_cast<int32_t>(offsetof(CompiledRegexRunner_t592995170, ___findFirstCharMethod_19)); }
	inline FindFirstCharDelegate_t4203859986 * get_findFirstCharMethod_19() const { return ___findFirstCharMethod_19; }
	inline FindFirstCharDelegate_t4203859986 ** get_address_of_findFirstCharMethod_19() { return &___findFirstCharMethod_19; }
	inline void set_findFirstCharMethod_19(FindFirstCharDelegate_t4203859986 * value)
	{
		___findFirstCharMethod_19 = value;
		Il2CppCodeGenWriteBarrier((&___findFirstCharMethod_19), value);
	}

	inline static int32_t get_offset_of_initTrackCountMethod_20() { return static_cast<int32_t>(offsetof(CompiledRegexRunner_t592995170, ___initTrackCountMethod_20)); }
	inline NoParamDelegate_t2890182105 * get_initTrackCountMethod_20() const { return ___initTrackCountMethod_20; }
	inline NoParamDelegate_t2890182105 ** get_address_of_initTrackCountMethod_20() { return &___initTrackCountMethod_20; }
	inline void set_initTrackCountMethod_20(NoParamDelegate_t2890182105 * value)
	{
		___initTrackCountMethod_20 = value;
		Il2CppCodeGenWriteBarrier((&___initTrackCountMethod_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDREGEXRUNNER_T592995170_H
#ifndef COMPILEDREGEXRUNNERFACTORY_T3379954222_H
#define COMPILEDREGEXRUNNERFACTORY_T3379954222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CompiledRegexRunnerFactory
struct  CompiledRegexRunnerFactory_t3379954222  : public RegexRunnerFactory_t3902733837
{
public:
	// System.Reflection.Emit.DynamicMethod System.Text.RegularExpressions.CompiledRegexRunnerFactory::goMethod
	DynamicMethod_t3307743052 * ___goMethod_0;
	// System.Reflection.Emit.DynamicMethod System.Text.RegularExpressions.CompiledRegexRunnerFactory::findFirstCharMethod
	DynamicMethod_t3307743052 * ___findFirstCharMethod_1;
	// System.Reflection.Emit.DynamicMethod System.Text.RegularExpressions.CompiledRegexRunnerFactory::initTrackCountMethod
	DynamicMethod_t3307743052 * ___initTrackCountMethod_2;

public:
	inline static int32_t get_offset_of_goMethod_0() { return static_cast<int32_t>(offsetof(CompiledRegexRunnerFactory_t3379954222, ___goMethod_0)); }
	inline DynamicMethod_t3307743052 * get_goMethod_0() const { return ___goMethod_0; }
	inline DynamicMethod_t3307743052 ** get_address_of_goMethod_0() { return &___goMethod_0; }
	inline void set_goMethod_0(DynamicMethod_t3307743052 * value)
	{
		___goMethod_0 = value;
		Il2CppCodeGenWriteBarrier((&___goMethod_0), value);
	}

	inline static int32_t get_offset_of_findFirstCharMethod_1() { return static_cast<int32_t>(offsetof(CompiledRegexRunnerFactory_t3379954222, ___findFirstCharMethod_1)); }
	inline DynamicMethod_t3307743052 * get_findFirstCharMethod_1() const { return ___findFirstCharMethod_1; }
	inline DynamicMethod_t3307743052 ** get_address_of_findFirstCharMethod_1() { return &___findFirstCharMethod_1; }
	inline void set_findFirstCharMethod_1(DynamicMethod_t3307743052 * value)
	{
		___findFirstCharMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___findFirstCharMethod_1), value);
	}

	inline static int32_t get_offset_of_initTrackCountMethod_2() { return static_cast<int32_t>(offsetof(CompiledRegexRunnerFactory_t3379954222, ___initTrackCountMethod_2)); }
	inline DynamicMethod_t3307743052 * get_initTrackCountMethod_2() const { return ___initTrackCountMethod_2; }
	inline DynamicMethod_t3307743052 ** get_address_of_initTrackCountMethod_2() { return &___initTrackCountMethod_2; }
	inline void set_initTrackCountMethod_2(DynamicMethod_t3307743052 * value)
	{
		___initTrackCountMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&___initTrackCountMethod_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDREGEXRUNNERFACTORY_T3379954222_H
#ifndef LOWERCASEMAPPING_T2153935826_H
#define LOWERCASEMAPPING_T2153935826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct  LowerCaseMapping_t2153935826 
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMin
	Il2CppChar ____chMin_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMax
	Il2CppChar ____chMax_1;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_lcOp
	int32_t ____lcOp_2;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_data
	int32_t ____data_3;

public:
	inline static int32_t get_offset_of__chMin_0() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____chMin_0)); }
	inline Il2CppChar get__chMin_0() const { return ____chMin_0; }
	inline Il2CppChar* get_address_of__chMin_0() { return &____chMin_0; }
	inline void set__chMin_0(Il2CppChar value)
	{
		____chMin_0 = value;
	}

	inline static int32_t get_offset_of__chMax_1() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____chMax_1)); }
	inline Il2CppChar get__chMax_1() const { return ____chMax_1; }
	inline Il2CppChar* get_address_of__chMax_1() { return &____chMax_1; }
	inline void set__chMax_1(Il2CppChar value)
	{
		____chMax_1 = value;
	}

	inline static int32_t get_offset_of__lcOp_2() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____lcOp_2)); }
	inline int32_t get__lcOp_2() const { return ____lcOp_2; }
	inline int32_t* get_address_of__lcOp_2() { return &____lcOp_2; }
	inline void set__lcOp_2(int32_t value)
	{
		____lcOp_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____data_3)); }
	inline int32_t get__data_3() const { return ____data_3; }
	inline int32_t* get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(int32_t value)
	{
		____data_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2153935826_marshaled_pinvoke
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2153935826_marshaled_com
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
#endif // LOWERCASEMAPPING_T2153935826_H
#ifndef REGEXINTERPRETER_T884291495_H
#define REGEXINTERPRETER_T884291495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexInterpreter
struct  RegexInterpreter_t884291495  : public RegexRunner_t3983612747
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runoperator
	int32_t ___runoperator_18;
	// System.Int32[] System.Text.RegularExpressions.RegexInterpreter::runcodes
	Int32U5BU5D_t3030399641* ___runcodes_19;
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runcodepos
	int32_t ___runcodepos_20;
	// System.String[] System.Text.RegularExpressions.RegexInterpreter::runstrings
	StringU5BU5D_t1642385972* ___runstrings_21;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexInterpreter::runcode
	RegexCode_t2469392150 * ___runcode_22;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexInterpreter::runfcPrefix
	RegexPrefix_t1013837165 * ___runfcPrefix_23;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexInterpreter::runbmPrefix
	RegexBoyerMoore_t2204811018 * ___runbmPrefix_24;
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runanchors
	int32_t ___runanchors_25;
	// System.Boolean System.Text.RegularExpressions.RegexInterpreter::runrtl
	bool ___runrtl_26;
	// System.Boolean System.Text.RegularExpressions.RegexInterpreter::runci
	bool ___runci_27;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexInterpreter::runculture
	CultureInfo_t3500843524 * ___runculture_28;

public:
	inline static int32_t get_offset_of_runoperator_18() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runoperator_18)); }
	inline int32_t get_runoperator_18() const { return ___runoperator_18; }
	inline int32_t* get_address_of_runoperator_18() { return &___runoperator_18; }
	inline void set_runoperator_18(int32_t value)
	{
		___runoperator_18 = value;
	}

	inline static int32_t get_offset_of_runcodes_19() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runcodes_19)); }
	inline Int32U5BU5D_t3030399641* get_runcodes_19() const { return ___runcodes_19; }
	inline Int32U5BU5D_t3030399641** get_address_of_runcodes_19() { return &___runcodes_19; }
	inline void set_runcodes_19(Int32U5BU5D_t3030399641* value)
	{
		___runcodes_19 = value;
		Il2CppCodeGenWriteBarrier((&___runcodes_19), value);
	}

	inline static int32_t get_offset_of_runcodepos_20() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runcodepos_20)); }
	inline int32_t get_runcodepos_20() const { return ___runcodepos_20; }
	inline int32_t* get_address_of_runcodepos_20() { return &___runcodepos_20; }
	inline void set_runcodepos_20(int32_t value)
	{
		___runcodepos_20 = value;
	}

	inline static int32_t get_offset_of_runstrings_21() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runstrings_21)); }
	inline StringU5BU5D_t1642385972* get_runstrings_21() const { return ___runstrings_21; }
	inline StringU5BU5D_t1642385972** get_address_of_runstrings_21() { return &___runstrings_21; }
	inline void set_runstrings_21(StringU5BU5D_t1642385972* value)
	{
		___runstrings_21 = value;
		Il2CppCodeGenWriteBarrier((&___runstrings_21), value);
	}

	inline static int32_t get_offset_of_runcode_22() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runcode_22)); }
	inline RegexCode_t2469392150 * get_runcode_22() const { return ___runcode_22; }
	inline RegexCode_t2469392150 ** get_address_of_runcode_22() { return &___runcode_22; }
	inline void set_runcode_22(RegexCode_t2469392150 * value)
	{
		___runcode_22 = value;
		Il2CppCodeGenWriteBarrier((&___runcode_22), value);
	}

	inline static int32_t get_offset_of_runfcPrefix_23() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runfcPrefix_23)); }
	inline RegexPrefix_t1013837165 * get_runfcPrefix_23() const { return ___runfcPrefix_23; }
	inline RegexPrefix_t1013837165 ** get_address_of_runfcPrefix_23() { return &___runfcPrefix_23; }
	inline void set_runfcPrefix_23(RegexPrefix_t1013837165 * value)
	{
		___runfcPrefix_23 = value;
		Il2CppCodeGenWriteBarrier((&___runfcPrefix_23), value);
	}

	inline static int32_t get_offset_of_runbmPrefix_24() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runbmPrefix_24)); }
	inline RegexBoyerMoore_t2204811018 * get_runbmPrefix_24() const { return ___runbmPrefix_24; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of_runbmPrefix_24() { return &___runbmPrefix_24; }
	inline void set_runbmPrefix_24(RegexBoyerMoore_t2204811018 * value)
	{
		___runbmPrefix_24 = value;
		Il2CppCodeGenWriteBarrier((&___runbmPrefix_24), value);
	}

	inline static int32_t get_offset_of_runanchors_25() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runanchors_25)); }
	inline int32_t get_runanchors_25() const { return ___runanchors_25; }
	inline int32_t* get_address_of_runanchors_25() { return &___runanchors_25; }
	inline void set_runanchors_25(int32_t value)
	{
		___runanchors_25 = value;
	}

	inline static int32_t get_offset_of_runrtl_26() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runrtl_26)); }
	inline bool get_runrtl_26() const { return ___runrtl_26; }
	inline bool* get_address_of_runrtl_26() { return &___runrtl_26; }
	inline void set_runrtl_26(bool value)
	{
		___runrtl_26 = value;
	}

	inline static int32_t get_offset_of_runci_27() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runci_27)); }
	inline bool get_runci_27() const { return ___runci_27; }
	inline bool* get_address_of_runci_27() { return &___runci_27; }
	inline void set_runci_27(bool value)
	{
		___runci_27 = value;
	}

	inline static int32_t get_offset_of_runculture_28() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runculture_28)); }
	inline CultureInfo_t3500843524 * get_runculture_28() const { return ___runculture_28; }
	inline CultureInfo_t3500843524 ** get_address_of_runculture_28() { return &___runculture_28; }
	inline void set_runculture_28(CultureInfo_t3500843524 * value)
	{
		___runculture_28 = value;
		Il2CppCodeGenWriteBarrier((&___runculture_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXINTERPRETER_T884291495_H
#ifndef SYSTEMNETWORKCREDENTIAL_T677084018_H
#define SYSTEMNETWORKCREDENTIAL_T677084018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SystemNetworkCredential
struct  SystemNetworkCredential_t677084018  : public NetworkCredential_t1714133953
{
public:

public:
};

struct SystemNetworkCredential_t677084018_StaticFields
{
public:
	// System.Net.SystemNetworkCredential System.Net.SystemNetworkCredential::defaultCredential
	SystemNetworkCredential_t677084018 * ___defaultCredential_3;

public:
	inline static int32_t get_offset_of_defaultCredential_3() { return static_cast<int32_t>(offsetof(SystemNetworkCredential_t677084018_StaticFields, ___defaultCredential_3)); }
	inline SystemNetworkCredential_t677084018 * get_defaultCredential_3() const { return ___defaultCredential_3; }
	inline SystemNetworkCredential_t677084018 ** get_address_of_defaultCredential_3() { return &___defaultCredential_3; }
	inline void set_defaultCredential_3(SystemNetworkCredential_t677084018 * value)
	{
		___defaultCredential_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCredential_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMNETWORKCREDENTIAL_T677084018_H
#ifndef GROUP_T3761430853_H
#define GROUP_T3761430853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t3761430853  : public Capture_t4157900610
{
public:
	// System.Int32[] System.Text.RegularExpressions.Group::_caps
	Int32U5BU5D_t3030399641* ____caps_4;
	// System.Int32 System.Text.RegularExpressions.Group::_capcount
	int32_t ____capcount_5;

public:
	inline static int32_t get_offset_of__caps_4() { return static_cast<int32_t>(offsetof(Group_t3761430853, ____caps_4)); }
	inline Int32U5BU5D_t3030399641* get__caps_4() const { return ____caps_4; }
	inline Int32U5BU5D_t3030399641** get_address_of__caps_4() { return &____caps_4; }
	inline void set__caps_4(Int32U5BU5D_t3030399641* value)
	{
		____caps_4 = value;
		Il2CppCodeGenWriteBarrier((&____caps_4), value);
	}

	inline static int32_t get_offset_of__capcount_5() { return static_cast<int32_t>(offsetof(Group_t3761430853, ____capcount_5)); }
	inline int32_t get__capcount_5() const { return ____capcount_5; }
	inline int32_t* get_address_of__capcount_5() { return &____capcount_5; }
	inline void set__capcount_5(int32_t value)
	{
		____capcount_5 = value;
	}
};

struct Group_t3761430853_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::_emptygroup
	Group_t3761430853 * ____emptygroup_3;

public:
	inline static int32_t get_offset_of__emptygroup_3() { return static_cast<int32_t>(offsetof(Group_t3761430853_StaticFields, ____emptygroup_3)); }
	inline Group_t3761430853 * get__emptygroup_3() const { return ____emptygroup_3; }
	inline Group_t3761430853 ** get_address_of__emptygroup_3() { return &____emptygroup_3; }
	inline void set__emptygroup_3(Group_t3761430853 * value)
	{
		____emptygroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____emptygroup_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T3761430853_H
#ifndef X509EXTENSION_T1320896183_H
#define X509EXTENSION_T1320896183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Extension
struct  X509Extension_t1320896183  : public AsnEncodedData_t463456204
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Extension::_critical
	bool ____critical_2;

public:
	inline static int32_t get_offset_of__critical_2() { return static_cast<int32_t>(offsetof(X509Extension_t1320896183, ____critical_2)); }
	inline bool get__critical_2() const { return ____critical_2; }
	inline bool* get_address_of__critical_2() { return &____critical_2; }
	inline void set__critical_2(bool value)
	{
		____critical_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T1320896183_H
#ifndef OPENFLAGS_T2370524385_H
#define OPENFLAGS_T2370524385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OpenFlags
struct  OpenFlags_t2370524385 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OpenFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OpenFlags_t2370524385, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENFLAGS_T2370524385_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_0)); }
	inline TimeSpan_t3430258949  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_t3430258949  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_t3430258949  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_t3430258949  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_2)); }
	inline TimeSpan_t3430258949  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_t3430258949  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef SSLPROTOCOLS_T894678499_H
#define SSLPROTOCOLS_T894678499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t894678499 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslProtocols_t894678499, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T894678499_H
#ifndef X509VERIFICATIONFLAGS_T2169036324_H
#define X509VERIFICATIONFLAGS_T2169036324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509VerificationFlags
struct  X509VerificationFlags_t2169036324 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509VerificationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509VerificationFlags_t2169036324, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509VERIFICATIONFLAGS_T2169036324_H
#ifndef STORELOCATION_T1570828128_H
#define STORELOCATION_T1570828128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreLocation
struct  StoreLocation_t1570828128 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StoreLocation_t1570828128, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELOCATION_T1570828128_H
#ifndef HTTPSTATUSCODE_T1898409641_H
#define HTTPSTATUSCODE_T1898409641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t1898409641 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpStatusCode_t1898409641, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T1898409641_H
#ifndef INVALIDOPERATIONEXCEPTION_T721527559_H
#define INVALIDOPERATIONEXCEPTION_T721527559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t721527559  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T721527559_H
#ifndef BACKTRACKNOTE_T1775865058_H
#define BACKTRACKNOTE_T1775865058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCompiler/BacktrackNote
struct  BacktrackNote_t1775865058  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexCompiler/BacktrackNote::_codepos
	int32_t ____codepos_0;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler/BacktrackNote::_flags
	int32_t ____flags_1;
	// System.Reflection.Emit.Label System.Text.RegularExpressions.RegexCompiler/BacktrackNote::_label
	Label_t4243202660  ____label_2;

public:
	inline static int32_t get_offset_of__codepos_0() { return static_cast<int32_t>(offsetof(BacktrackNote_t1775865058, ____codepos_0)); }
	inline int32_t get__codepos_0() const { return ____codepos_0; }
	inline int32_t* get_address_of__codepos_0() { return &____codepos_0; }
	inline void set__codepos_0(int32_t value)
	{
		____codepos_0 = value;
	}

	inline static int32_t get_offset_of__flags_1() { return static_cast<int32_t>(offsetof(BacktrackNote_t1775865058, ____flags_1)); }
	inline int32_t get__flags_1() const { return ____flags_1; }
	inline int32_t* get_address_of__flags_1() { return &____flags_1; }
	inline void set__flags_1(int32_t value)
	{
		____flags_1 = value;
	}

	inline static int32_t get_offset_of__label_2() { return static_cast<int32_t>(offsetof(BacktrackNote_t1775865058, ____label_2)); }
	inline Label_t4243202660  get__label_2() const { return ____label_2; }
	inline Label_t4243202660 * get_address_of__label_2() { return &____label_2; }
	inline void set__label_2(Label_t4243202660  value)
	{
		____label_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKTRACKNOTE_T1775865058_H
#ifndef ASNDECODESTATUS_T1962003286_H
#define ASNDECODESTATUS_T1962003286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t1962003286 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t1962003286, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T1962003286_H
#ifndef CLOSEEXSTATE_T4185113936_H
#define CLOSEEXSTATE_T4185113936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CloseExState
struct  CloseExState_t4185113936 
{
public:
	// System.Int32 System.Net.CloseExState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloseExState_t4185113936, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEEXSTATE_T4185113936_H
#ifndef TIMEOUTEXCEPTION_T3246754798_H
#define TIMEOUTEXCEPTION_T3246754798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeoutException
struct  TimeoutException_t3246754798  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEOUTEXCEPTION_T3246754798_H
#ifndef TOKENIMPERSONATIONLEVEL_T2477301187_H
#define TOKENIMPERSONATIONLEVEL_T2477301187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.TokenImpersonationLevel
struct  TokenImpersonationLevel_t2477301187 
{
public:
	// System.Int32 System.Security.Principal.TokenImpersonationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TokenImpersonationLevel_t2477301187, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIMPERSONATIONLEVEL_T2477301187_H
#ifndef X509FINDTYPE_T3221716179_H
#define X509FINDTYPE_T3221716179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509FindType
struct  X509FindType_t3221716179 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509FindType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509FindType_t3221716179, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509FINDTYPE_T3221716179_H
#ifndef X509NAMETYPE_T2669466891_H
#define X509NAMETYPE_T2669466891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509NameType
struct  X509NameType_t2669466891 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509NameType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509NameType_t2669466891, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMETYPE_T2669466891_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef X509KEYUSAGEFLAGS_T2461349531_H
#define X509KEYUSAGEFLAGS_T2461349531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t2461349531 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t2461349531, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T2461349531_H
#ifndef AUTHENTICATIONLEVEL_T2424130044_H
#define AUTHENTICATIONLEVEL_T2424130044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_t2424130044 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationLevel_t2424130044, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_T2424130044_H
#ifndef ADDRESSFAMILY_T303362630_H
#define ADDRESSFAMILY_T303362630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t303362630 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AddressFamily_t303362630, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T303362630_H
#ifndef X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T110301003_H
#define X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T110301003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
struct  X509SubjectKeyIdentifierHashAlgorithm_t110301003 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierHashAlgorithm_t110301003, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T110301003_H
#ifndef SSLPOLICYERRORS_T1928581431_H
#define SSLPOLICYERRORS_T1928581431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.SslPolicyErrors
struct  SslPolicyErrors_t1928581431 
{
public:
	// System.Int32 System.Net.Security.SslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslPolicyErrors_t1928581431, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPOLICYERRORS_T1928581431_H
#ifndef X509REVOCATIONFLAG_T2166064554_H
#define X509REVOCATIONFLAG_T2166064554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationFlag
struct  X509RevocationFlag_t2166064554 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationFlag::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509RevocationFlag_t2166064554, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONFLAG_T2166064554_H
#ifndef X509REVOCATIONMODE_T2065307963_H
#define X509REVOCATIONMODE_T2065307963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t2065307963 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509RevocationMode_t2065307963, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T2065307963_H
#ifndef WEBHEADERCOLLECTIONTYPE_T1212469221_H
#define WEBHEADERCOLLECTIONTYPE_T1212469221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollectionType
struct  WebHeaderCollectionType_t1212469221 
{
public:
	// System.UInt16 System.Net.WebHeaderCollectionType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebHeaderCollectionType_t1212469221, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTIONTYPE_T1212469221_H
#ifndef RFCCHAR_T1416622761_H
#define RFCCHAR_T1416622761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection/RfcChar
struct  RfcChar_t1416622761 
{
public:
	// System.Byte System.Net.WebHeaderCollection/RfcChar::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RfcChar_t1416622761, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFCCHAR_T1416622761_H
#ifndef SECURITYPROTOCOLTYPE_T3099771628_H
#define SECURITYPROTOCOLTYPE_T3099771628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t3099771628 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t3099771628, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T3099771628_H
#ifndef REGEXOPTIONS_T2418259727_H
#define REGEXOPTIONS_T2418259727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t2418259727 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RegexOptions_t2418259727, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T2418259727_H
#ifndef WEBEXCEPTIONINTERNALSTATUS_T1357294310_H
#define WEBEXCEPTIONINTERNALSTATUS_T1357294310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionInternalStatus
struct  WebExceptionInternalStatus_t1357294310 
{
public:
	// System.Int32 System.Net.WebExceptionInternalStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionInternalStatus_t1357294310, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONINTERNALSTATUS_T1357294310_H
#ifndef WEBEXCEPTIONSTATUS_T1169373531_H
#define WEBEXCEPTIONSTATUS_T1169373531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t1169373531 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t1169373531, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T1169373531_H
#ifndef INTERNALEXCEPTION_T1919254860_H
#define INTERNALEXCEPTION_T1919254860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.InternalException
struct  InternalException_t1919254860  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALEXCEPTION_T1919254860_H
#ifndef FTPSTATUSCODE_T1448112771_H
#define FTPSTATUSCODE_T1448112771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpStatusCode
struct  FtpStatusCode_t1448112771 
{
public:
	// System.Int32 System.Net.FtpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FtpStatusCode_t1448112771, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPSTATUSCODE_T1448112771_H
#ifndef MATCH_T3164245899_H
#define MATCH_T3164245899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t3164245899  : public Group_t3761430853
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::_groupcoll
	GroupCollection_t939014605 * ____groupcoll_7;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::_regex
	Regex_t1803876613 * ____regex_8;
	// System.Int32 System.Text.RegularExpressions.Match::_textbeg
	int32_t ____textbeg_9;
	// System.Int32 System.Text.RegularExpressions.Match::_textpos
	int32_t ____textpos_10;
	// System.Int32 System.Text.RegularExpressions.Match::_textend
	int32_t ____textend_11;
	// System.Int32 System.Text.RegularExpressions.Match::_textstart
	int32_t ____textstart_12;
	// System.Int32[][] System.Text.RegularExpressions.Match::_matches
	Int32U5BU5DU5BU5D_t3750818532* ____matches_13;
	// System.Int32[] System.Text.RegularExpressions.Match::_matchcount
	Int32U5BU5D_t3030399641* ____matchcount_14;
	// System.Boolean System.Text.RegularExpressions.Match::_balancing
	bool ____balancing_15;

public:
	inline static int32_t get_offset_of__groupcoll_7() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____groupcoll_7)); }
	inline GroupCollection_t939014605 * get__groupcoll_7() const { return ____groupcoll_7; }
	inline GroupCollection_t939014605 ** get_address_of__groupcoll_7() { return &____groupcoll_7; }
	inline void set__groupcoll_7(GroupCollection_t939014605 * value)
	{
		____groupcoll_7 = value;
		Il2CppCodeGenWriteBarrier((&____groupcoll_7), value);
	}

	inline static int32_t get_offset_of__regex_8() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____regex_8)); }
	inline Regex_t1803876613 * get__regex_8() const { return ____regex_8; }
	inline Regex_t1803876613 ** get_address_of__regex_8() { return &____regex_8; }
	inline void set__regex_8(Regex_t1803876613 * value)
	{
		____regex_8 = value;
		Il2CppCodeGenWriteBarrier((&____regex_8), value);
	}

	inline static int32_t get_offset_of__textbeg_9() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textbeg_9)); }
	inline int32_t get__textbeg_9() const { return ____textbeg_9; }
	inline int32_t* get_address_of__textbeg_9() { return &____textbeg_9; }
	inline void set__textbeg_9(int32_t value)
	{
		____textbeg_9 = value;
	}

	inline static int32_t get_offset_of__textpos_10() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textpos_10)); }
	inline int32_t get__textpos_10() const { return ____textpos_10; }
	inline int32_t* get_address_of__textpos_10() { return &____textpos_10; }
	inline void set__textpos_10(int32_t value)
	{
		____textpos_10 = value;
	}

	inline static int32_t get_offset_of__textend_11() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textend_11)); }
	inline int32_t get__textend_11() const { return ____textend_11; }
	inline int32_t* get_address_of__textend_11() { return &____textend_11; }
	inline void set__textend_11(int32_t value)
	{
		____textend_11 = value;
	}

	inline static int32_t get_offset_of__textstart_12() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textstart_12)); }
	inline int32_t get__textstart_12() const { return ____textstart_12; }
	inline int32_t* get_address_of__textstart_12() { return &____textstart_12; }
	inline void set__textstart_12(int32_t value)
	{
		____textstart_12 = value;
	}

	inline static int32_t get_offset_of__matches_13() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____matches_13)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__matches_13() const { return ____matches_13; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__matches_13() { return &____matches_13; }
	inline void set__matches_13(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____matches_13 = value;
		Il2CppCodeGenWriteBarrier((&____matches_13), value);
	}

	inline static int32_t get_offset_of__matchcount_14() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____matchcount_14)); }
	inline Int32U5BU5D_t3030399641* get__matchcount_14() const { return ____matchcount_14; }
	inline Int32U5BU5D_t3030399641** get_address_of__matchcount_14() { return &____matchcount_14; }
	inline void set__matchcount_14(Int32U5BU5D_t3030399641* value)
	{
		____matchcount_14 = value;
		Il2CppCodeGenWriteBarrier((&____matchcount_14), value);
	}

	inline static int32_t get_offset_of__balancing_15() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____balancing_15)); }
	inline bool get__balancing_15() const { return ____balancing_15; }
	inline bool* get_address_of__balancing_15() { return &____balancing_15; }
	inline void set__balancing_15(bool value)
	{
		____balancing_15 = value;
	}
};

struct Match_t3164245899_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::_empty
	Match_t3164245899 * ____empty_6;

public:
	inline static int32_t get_offset_of__empty_6() { return static_cast<int32_t>(offsetof(Match_t3164245899_StaticFields, ____empty_6)); }
	inline Match_t3164245899 * get__empty_6() const { return ____empty_6; }
	inline Match_t3164245899 ** get_address_of__empty_6() { return &____empty_6; }
	inline void set__empty_6(Match_t3164245899 * value)
	{
		____empty_6 = value;
		Il2CppCodeGenWriteBarrier((&____empty_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T3164245899_H
#ifndef IPADDRESS_T1399971723_H
#define IPADDRESS_T1399971723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t1399971723  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_4;
	// System.String System.Net.IPAddress::m_ToString
	String_t* ___m_ToString_5;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_9;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t2527266722* ___m_Numbers_10;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_11;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_12;

public:
	inline static int32_t get_offset_of_m_Address_4() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Address_4)); }
	inline int64_t get_m_Address_4() const { return ___m_Address_4; }
	inline int64_t* get_address_of_m_Address_4() { return &___m_Address_4; }
	inline void set_m_Address_4(int64_t value)
	{
		___m_Address_4 = value;
	}

	inline static int32_t get_offset_of_m_ToString_5() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_ToString_5)); }
	inline String_t* get_m_ToString_5() const { return ___m_ToString_5; }
	inline String_t** get_address_of_m_ToString_5() { return &___m_ToString_5; }
	inline void set_m_ToString_5(String_t* value)
	{
		___m_ToString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToString_5), value);
	}

	inline static int32_t get_offset_of_m_Family_9() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Family_9)); }
	inline int32_t get_m_Family_9() const { return ___m_Family_9; }
	inline int32_t* get_address_of_m_Family_9() { return &___m_Family_9; }
	inline void set_m_Family_9(int32_t value)
	{
		___m_Family_9 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_10() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Numbers_10)); }
	inline UInt16U5BU5D_t2527266722* get_m_Numbers_10() const { return ___m_Numbers_10; }
	inline UInt16U5BU5D_t2527266722** get_address_of_m_Numbers_10() { return &___m_Numbers_10; }
	inline void set_m_Numbers_10(UInt16U5BU5D_t2527266722* value)
	{
		___m_Numbers_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_10), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_11() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_ScopeId_11)); }
	inline int64_t get_m_ScopeId_11() const { return ___m_ScopeId_11; }
	inline int64_t* get_address_of_m_ScopeId_11() { return &___m_ScopeId_11; }
	inline void set_m_ScopeId_11(int64_t value)
	{
		___m_ScopeId_11 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_12() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_HashCode_12)); }
	inline int32_t get_m_HashCode_12() const { return ___m_HashCode_12; }
	inline int32_t* get_address_of_m_HashCode_12() { return &___m_HashCode_12; }
	inline void set_m_HashCode_12(int32_t value)
	{
		___m_HashCode_12 = value;
	}
};

struct IPAddress_t1399971723_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t1399971723 * ___Any_0;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t1399971723 * ___Loopback_1;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t1399971723 * ___Broadcast_2;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t1399971723 * ___None_3;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t1399971723 * ___IPv6Any_6;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t1399971723 * ___IPv6Loopback_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t1399971723 * ___IPv6None_8;

public:
	inline static int32_t get_offset_of_Any_0() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Any_0)); }
	inline IPAddress_t1399971723 * get_Any_0() const { return ___Any_0; }
	inline IPAddress_t1399971723 ** get_address_of_Any_0() { return &___Any_0; }
	inline void set_Any_0(IPAddress_t1399971723 * value)
	{
		___Any_0 = value;
		Il2CppCodeGenWriteBarrier((&___Any_0), value);
	}

	inline static int32_t get_offset_of_Loopback_1() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Loopback_1)); }
	inline IPAddress_t1399971723 * get_Loopback_1() const { return ___Loopback_1; }
	inline IPAddress_t1399971723 ** get_address_of_Loopback_1() { return &___Loopback_1; }
	inline void set_Loopback_1(IPAddress_t1399971723 * value)
	{
		___Loopback_1 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_1), value);
	}

	inline static int32_t get_offset_of_Broadcast_2() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Broadcast_2)); }
	inline IPAddress_t1399971723 * get_Broadcast_2() const { return ___Broadcast_2; }
	inline IPAddress_t1399971723 ** get_address_of_Broadcast_2() { return &___Broadcast_2; }
	inline void set_Broadcast_2(IPAddress_t1399971723 * value)
	{
		___Broadcast_2 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_2), value);
	}

	inline static int32_t get_offset_of_None_3() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___None_3)); }
	inline IPAddress_t1399971723 * get_None_3() const { return ___None_3; }
	inline IPAddress_t1399971723 ** get_address_of_None_3() { return &___None_3; }
	inline void set_None_3(IPAddress_t1399971723 * value)
	{
		___None_3 = value;
		Il2CppCodeGenWriteBarrier((&___None_3), value);
	}

	inline static int32_t get_offset_of_IPv6Any_6() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6Any_6)); }
	inline IPAddress_t1399971723 * get_IPv6Any_6() const { return ___IPv6Any_6; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6Any_6() { return &___IPv6Any_6; }
	inline void set_IPv6Any_6(IPAddress_t1399971723 * value)
	{
		___IPv6Any_6 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_6), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_7() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6Loopback_7)); }
	inline IPAddress_t1399971723 * get_IPv6Loopback_7() const { return ___IPv6Loopback_7; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6Loopback_7() { return &___IPv6Loopback_7; }
	inline void set_IPv6Loopback_7(IPAddress_t1399971723 * value)
	{
		___IPv6Loopback_7 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_7), value);
	}

	inline static int32_t get_offset_of_IPv6None_8() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6None_8)); }
	inline IPAddress_t1399971723 * get_IPv6None_8() const { return ___IPv6None_8; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6None_8() { return &___IPv6None_8; }
	inline void set_IPv6None_8(IPAddress_t1399971723 * value)
	{
		___IPv6None_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T1399971723_H
#ifndef X509SUBJECTKEYIDENTIFIEREXTENSION_T2508879999_H
#define X509SUBJECTKEYIDENTIFIEREXTENSION_T2508879999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
struct  X509SubjectKeyIdentifierExtension_t2508879999  : public X509Extension_t1320896183
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_subjectKeyIdentifier
	ByteU5BU5D_t3397334013* ____subjectKeyIdentifier_5;
	// System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_ski
	String_t* ____ski_6;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_status
	int32_t ____status_7;

public:
	inline static int32_t get_offset_of__subjectKeyIdentifier_5() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2508879999, ____subjectKeyIdentifier_5)); }
	inline ByteU5BU5D_t3397334013* get__subjectKeyIdentifier_5() const { return ____subjectKeyIdentifier_5; }
	inline ByteU5BU5D_t3397334013** get_address_of__subjectKeyIdentifier_5() { return &____subjectKeyIdentifier_5; }
	inline void set__subjectKeyIdentifier_5(ByteU5BU5D_t3397334013* value)
	{
		____subjectKeyIdentifier_5 = value;
		Il2CppCodeGenWriteBarrier((&____subjectKeyIdentifier_5), value);
	}

	inline static int32_t get_offset_of__ski_6() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2508879999, ____ski_6)); }
	inline String_t* get__ski_6() const { return ____ski_6; }
	inline String_t** get_address_of__ski_6() { return &____ski_6; }
	inline void set__ski_6(String_t* value)
	{
		____ski_6 = value;
		Il2CppCodeGenWriteBarrier((&____ski_6), value);
	}

	inline static int32_t get_offset_of__status_7() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2508879999, ____status_7)); }
	inline int32_t get__status_7() const { return ____status_7; }
	inline int32_t* get_address_of__status_7() { return &____status_7; }
	inline void set__status_7(int32_t value)
	{
		____status_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIEREXTENSION_T2508879999_H
#ifndef PROTOCOLVIOLATIONEXCEPTION_T4263317570_H
#define PROTOCOLVIOLATIONEXCEPTION_T4263317570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_t4263317570  : public InvalidOperationException_t721527559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVIOLATIONEXCEPTION_T4263317570_H
#ifndef CALLBACKCONTEXT_T3155372874_H
#define CALLBACKCONTEXT_T3155372874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback/CallbackContext
struct  CallbackContext_t3155372874  : public RuntimeObject
{
public:
	// System.Object System.Net.ServerCertValidationCallback/CallbackContext::request
	RuntimeObject * ___request_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServerCertValidationCallback/CallbackContext::certificate
	X509Certificate_t283079845 * ___certificate_1;
	// System.Security.Cryptography.X509Certificates.X509Chain System.Net.ServerCertValidationCallback/CallbackContext::chain
	X509Chain_t777637347 * ___chain_2;
	// System.Net.Security.SslPolicyErrors System.Net.ServerCertValidationCallback/CallbackContext::sslPolicyErrors
	int32_t ___sslPolicyErrors_3;
	// System.Boolean System.Net.ServerCertValidationCallback/CallbackContext::result
	bool ___result_4;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___request_0)); }
	inline RuntimeObject * get_request_0() const { return ___request_0; }
	inline RuntimeObject ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RuntimeObject * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_certificate_1() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___certificate_1)); }
	inline X509Certificate_t283079845 * get_certificate_1() const { return ___certificate_1; }
	inline X509Certificate_t283079845 ** get_address_of_certificate_1() { return &___certificate_1; }
	inline void set_certificate_1(X509Certificate_t283079845 * value)
	{
		___certificate_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_1), value);
	}

	inline static int32_t get_offset_of_chain_2() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___chain_2)); }
	inline X509Chain_t777637347 * get_chain_2() const { return ___chain_2; }
	inline X509Chain_t777637347 ** get_address_of_chain_2() { return &___chain_2; }
	inline void set_chain_2(X509Chain_t777637347 * value)
	{
		___chain_2 = value;
		Il2CppCodeGenWriteBarrier((&___chain_2), value);
	}

	inline static int32_t get_offset_of_sslPolicyErrors_3() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___sslPolicyErrors_3)); }
	inline int32_t get_sslPolicyErrors_3() const { return ___sslPolicyErrors_3; }
	inline int32_t* get_address_of_sslPolicyErrors_3() { return &___sslPolicyErrors_3; }
	inline void set_sslPolicyErrors_3(int32_t value)
	{
		___sslPolicyErrors_3 = value;
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___result_4)); }
	inline bool get_result_4() const { return ___result_4; }
	inline bool* get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(bool value)
	{
		___result_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKCONTEXT_T3155372874_H
#ifndef REGEXPARSER_T5944516_H
#define REGEXPARSER_T5944516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexParser
struct  RegexParser_t5944516  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_stack
	RegexNode_t2469392321 * ____stack_0;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_group
	RegexNode_t2469392321 * ____group_1;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_alternation
	RegexNode_t2469392321 * ____alternation_2;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_concatenation
	RegexNode_t2469392321 * ____concatenation_3;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_unit
	RegexNode_t2469392321 * ____unit_4;
	// System.String System.Text.RegularExpressions.RegexParser::_pattern
	String_t* ____pattern_5;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_currentPos
	int32_t ____currentPos_6;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexParser::_culture
	CultureInfo_t3500843524 * ____culture_7;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_autocap
	int32_t ____autocap_8;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capcount
	int32_t ____capcount_9;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_captop
	int32_t ____captop_10;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capsize
	int32_t ____capsize_11;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_caps
	Hashtable_t909839986 * ____caps_12;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_capnames
	Hashtable_t909839986 * ____capnames_13;
	// System.Int32[] System.Text.RegularExpressions.RegexParser::_capnumlist
	Int32U5BU5D_t3030399641* ____capnumlist_14;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexParser::_capnamelist
	List_1_t1398341365 * ____capnamelist_15;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexParser::_options
	int32_t ____options_16;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions> System.Text.RegularExpressions.RegexParser::_optionsStack
	List_1_t1787380859 * ____optionsStack_17;
	// System.Boolean System.Text.RegularExpressions.RegexParser::_ignoreNextParen
	bool ____ignoreNextParen_18;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____stack_0)); }
	inline RegexNode_t2469392321 * get__stack_0() const { return ____stack_0; }
	inline RegexNode_t2469392321 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(RegexNode_t2469392321 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((&____stack_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____group_1)); }
	inline RegexNode_t2469392321 * get__group_1() const { return ____group_1; }
	inline RegexNode_t2469392321 ** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RegexNode_t2469392321 * value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}

	inline static int32_t get_offset_of__alternation_2() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____alternation_2)); }
	inline RegexNode_t2469392321 * get__alternation_2() const { return ____alternation_2; }
	inline RegexNode_t2469392321 ** get_address_of__alternation_2() { return &____alternation_2; }
	inline void set__alternation_2(RegexNode_t2469392321 * value)
	{
		____alternation_2 = value;
		Il2CppCodeGenWriteBarrier((&____alternation_2), value);
	}

	inline static int32_t get_offset_of__concatenation_3() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____concatenation_3)); }
	inline RegexNode_t2469392321 * get__concatenation_3() const { return ____concatenation_3; }
	inline RegexNode_t2469392321 ** get_address_of__concatenation_3() { return &____concatenation_3; }
	inline void set__concatenation_3(RegexNode_t2469392321 * value)
	{
		____concatenation_3 = value;
		Il2CppCodeGenWriteBarrier((&____concatenation_3), value);
	}

	inline static int32_t get_offset_of__unit_4() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____unit_4)); }
	inline RegexNode_t2469392321 * get__unit_4() const { return ____unit_4; }
	inline RegexNode_t2469392321 ** get_address_of__unit_4() { return &____unit_4; }
	inline void set__unit_4(RegexNode_t2469392321 * value)
	{
		____unit_4 = value;
		Il2CppCodeGenWriteBarrier((&____unit_4), value);
	}

	inline static int32_t get_offset_of__pattern_5() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____pattern_5)); }
	inline String_t* get__pattern_5() const { return ____pattern_5; }
	inline String_t** get_address_of__pattern_5() { return &____pattern_5; }
	inline void set__pattern_5(String_t* value)
	{
		____pattern_5 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_5), value);
	}

	inline static int32_t get_offset_of__currentPos_6() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____currentPos_6)); }
	inline int32_t get__currentPos_6() const { return ____currentPos_6; }
	inline int32_t* get_address_of__currentPos_6() { return &____currentPos_6; }
	inline void set__currentPos_6(int32_t value)
	{
		____currentPos_6 = value;
	}

	inline static int32_t get_offset_of__culture_7() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____culture_7)); }
	inline CultureInfo_t3500843524 * get__culture_7() const { return ____culture_7; }
	inline CultureInfo_t3500843524 ** get_address_of__culture_7() { return &____culture_7; }
	inline void set__culture_7(CultureInfo_t3500843524 * value)
	{
		____culture_7 = value;
		Il2CppCodeGenWriteBarrier((&____culture_7), value);
	}

	inline static int32_t get_offset_of__autocap_8() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____autocap_8)); }
	inline int32_t get__autocap_8() const { return ____autocap_8; }
	inline int32_t* get_address_of__autocap_8() { return &____autocap_8; }
	inline void set__autocap_8(int32_t value)
	{
		____autocap_8 = value;
	}

	inline static int32_t get_offset_of__capcount_9() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capcount_9)); }
	inline int32_t get__capcount_9() const { return ____capcount_9; }
	inline int32_t* get_address_of__capcount_9() { return &____capcount_9; }
	inline void set__capcount_9(int32_t value)
	{
		____capcount_9 = value;
	}

	inline static int32_t get_offset_of__captop_10() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____captop_10)); }
	inline int32_t get__captop_10() const { return ____captop_10; }
	inline int32_t* get_address_of__captop_10() { return &____captop_10; }
	inline void set__captop_10(int32_t value)
	{
		____captop_10 = value;
	}

	inline static int32_t get_offset_of__capsize_11() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capsize_11)); }
	inline int32_t get__capsize_11() const { return ____capsize_11; }
	inline int32_t* get_address_of__capsize_11() { return &____capsize_11; }
	inline void set__capsize_11(int32_t value)
	{
		____capsize_11 = value;
	}

	inline static int32_t get_offset_of__caps_12() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____caps_12)); }
	inline Hashtable_t909839986 * get__caps_12() const { return ____caps_12; }
	inline Hashtable_t909839986 ** get_address_of__caps_12() { return &____caps_12; }
	inline void set__caps_12(Hashtable_t909839986 * value)
	{
		____caps_12 = value;
		Il2CppCodeGenWriteBarrier((&____caps_12), value);
	}

	inline static int32_t get_offset_of__capnames_13() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capnames_13)); }
	inline Hashtable_t909839986 * get__capnames_13() const { return ____capnames_13; }
	inline Hashtable_t909839986 ** get_address_of__capnames_13() { return &____capnames_13; }
	inline void set__capnames_13(Hashtable_t909839986 * value)
	{
		____capnames_13 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_13), value);
	}

	inline static int32_t get_offset_of__capnumlist_14() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capnumlist_14)); }
	inline Int32U5BU5D_t3030399641* get__capnumlist_14() const { return ____capnumlist_14; }
	inline Int32U5BU5D_t3030399641** get_address_of__capnumlist_14() { return &____capnumlist_14; }
	inline void set__capnumlist_14(Int32U5BU5D_t3030399641* value)
	{
		____capnumlist_14 = value;
		Il2CppCodeGenWriteBarrier((&____capnumlist_14), value);
	}

	inline static int32_t get_offset_of__capnamelist_15() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capnamelist_15)); }
	inline List_1_t1398341365 * get__capnamelist_15() const { return ____capnamelist_15; }
	inline List_1_t1398341365 ** get_address_of__capnamelist_15() { return &____capnamelist_15; }
	inline void set__capnamelist_15(List_1_t1398341365 * value)
	{
		____capnamelist_15 = value;
		Il2CppCodeGenWriteBarrier((&____capnamelist_15), value);
	}

	inline static int32_t get_offset_of__options_16() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____options_16)); }
	inline int32_t get__options_16() const { return ____options_16; }
	inline int32_t* get_address_of__options_16() { return &____options_16; }
	inline void set__options_16(int32_t value)
	{
		____options_16 = value;
	}

	inline static int32_t get_offset_of__optionsStack_17() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____optionsStack_17)); }
	inline List_1_t1787380859 * get__optionsStack_17() const { return ____optionsStack_17; }
	inline List_1_t1787380859 ** get_address_of__optionsStack_17() { return &____optionsStack_17; }
	inline void set__optionsStack_17(List_1_t1787380859 * value)
	{
		____optionsStack_17 = value;
		Il2CppCodeGenWriteBarrier((&____optionsStack_17), value);
	}

	inline static int32_t get_offset_of__ignoreNextParen_18() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____ignoreNextParen_18)); }
	inline bool get__ignoreNextParen_18() const { return ____ignoreNextParen_18; }
	inline bool* get_address_of__ignoreNextParen_18() { return &____ignoreNextParen_18; }
	inline void set__ignoreNextParen_18(bool value)
	{
		____ignoreNextParen_18 = value;
	}
};

struct RegexParser_t5944516_StaticFields
{
public:
	// System.Byte[] System.Text.RegularExpressions.RegexParser::_category
	ByteU5BU5D_t3397334013* ____category_19;

public:
	inline static int32_t get_offset_of__category_19() { return static_cast<int32_t>(offsetof(RegexParser_t5944516_StaticFields, ____category_19)); }
	inline ByteU5BU5D_t3397334013* get__category_19() const { return ____category_19; }
	inline ByteU5BU5D_t3397334013** get_address_of__category_19() { return &____category_19; }
	inline void set__category_19(ByteU5BU5D_t3397334013* value)
	{
		____category_19 = value;
		Il2CppCodeGenWriteBarrier((&____category_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPARSER_T5944516_H
#ifndef REGEXTREE_T3175204897_H
#define REGEXTREE_T3175204897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexTree
struct  RegexTree_t3175204897  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexTree::_root
	RegexNode_t2469392321 * ____root_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_caps
	Hashtable_t909839986 * ____caps_1;
	// System.Int32[] System.Text.RegularExpressions.RegexTree::_capnumlist
	Int32U5BU5D_t3030399641* ____capnumlist_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_capnames
	Hashtable_t909839986 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.RegexTree::_capslist
	StringU5BU5D_t1642385972* ____capslist_4;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexTree::_options
	int32_t ____options_5;
	// System.Int32 System.Text.RegularExpressions.RegexTree::_captop
	int32_t ____captop_6;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____root_0)); }
	inline RegexNode_t2469392321 * get__root_0() const { return ____root_0; }
	inline RegexNode_t2469392321 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(RegexNode_t2469392321 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier((&____root_0), value);
	}

	inline static int32_t get_offset_of__caps_1() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____caps_1)); }
	inline Hashtable_t909839986 * get__caps_1() const { return ____caps_1; }
	inline Hashtable_t909839986 ** get_address_of__caps_1() { return &____caps_1; }
	inline void set__caps_1(Hashtable_t909839986 * value)
	{
		____caps_1 = value;
		Il2CppCodeGenWriteBarrier((&____caps_1), value);
	}

	inline static int32_t get_offset_of__capnumlist_2() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____capnumlist_2)); }
	inline Int32U5BU5D_t3030399641* get__capnumlist_2() const { return ____capnumlist_2; }
	inline Int32U5BU5D_t3030399641** get_address_of__capnumlist_2() { return &____capnumlist_2; }
	inline void set__capnumlist_2(Int32U5BU5D_t3030399641* value)
	{
		____capnumlist_2 = value;
		Il2CppCodeGenWriteBarrier((&____capnumlist_2), value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____capnames_3)); }
	inline Hashtable_t909839986 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t909839986 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t909839986 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_3), value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____capslist_4)); }
	inline StringU5BU5D_t1642385972* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1642385972** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1642385972* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier((&____capslist_4), value);
	}

	inline static int32_t get_offset_of__options_5() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____options_5)); }
	inline int32_t get__options_5() const { return ____options_5; }
	inline int32_t* get_address_of__options_5() { return &____options_5; }
	inline void set__options_5(int32_t value)
	{
		____options_5 = value;
	}

	inline static int32_t get_offset_of__captop_6() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____captop_6)); }
	inline int32_t get__captop_6() const { return ____captop_6; }
	inline int32_t* get_address_of__captop_6() { return &____captop_6; }
	inline void set__captop_6(int32_t value)
	{
		____captop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXTREE_T3175204897_H
#ifndef X509KEYUSAGEEXTENSION_T1038124237_H
#define X509KEYUSAGEEXTENSION_T1038124237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
struct  X509KeyUsageExtension_t1038124237  : public X509Extension_t1320896183
{
public:
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_keyUsages
	int32_t ____keyUsages_6;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_status
	int32_t ____status_7;

public:
	inline static int32_t get_offset_of__keyUsages_6() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t1038124237, ____keyUsages_6)); }
	inline int32_t get__keyUsages_6() const { return ____keyUsages_6; }
	inline int32_t* get_address_of__keyUsages_6() { return &____keyUsages_6; }
	inline void set__keyUsages_6(int32_t value)
	{
		____keyUsages_6 = value;
	}

	inline static int32_t get_offset_of__status_7() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t1038124237, ____status_7)); }
	inline int32_t get__status_7() const { return ____status_7; }
	inline int32_t* get_address_of__status_7() { return &____status_7; }
	inline void set__status_7(int32_t value)
	{
		____status_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEEXTENSION_T1038124237_H
#ifndef X509STORE_T1617430119_H
#define X509STORE_T1617430119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Store
struct  X509Store_t1617430119  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Store::_name
	String_t* ____name_0;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Store::_location
	int32_t ____location_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Store::list
	X509Certificate2Collection_t1108969367 * ___list_2;
	// System.Security.Cryptography.X509Certificates.OpenFlags System.Security.Cryptography.X509Certificates.X509Store::_flags
	int32_t ____flags_3;
	// Mono.Security.X509.X509Store System.Security.Cryptography.X509Certificates.X509Store::store
	X509Store_t4028973564 * ___store_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__location_1() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ____location_1)); }
	inline int32_t get__location_1() const { return ____location_1; }
	inline int32_t* get_address_of__location_1() { return &____location_1; }
	inline void set__location_1(int32_t value)
	{
		____location_1 = value;
	}

	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ___list_2)); }
	inline X509Certificate2Collection_t1108969367 * get_list_2() const { return ___list_2; }
	inline X509Certificate2Collection_t1108969367 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(X509Certificate2Collection_t1108969367 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ___store_4)); }
	inline X509Store_t4028973564 * get_store_4() const { return ___store_4; }
	inline X509Store_t4028973564 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(X509Store_t4028973564 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_T1617430119_H
#ifndef REGEXCOMPILER_T1714699756_H
#define REGEXCOMPILER_T1714699756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCompiler
struct  RegexCompiler_t1714699756  : public RuntimeObject
{
public:
	// System.Reflection.Emit.ILGenerator System.Text.RegularExpressions.RegexCompiler::_ilg
	ILGenerator_t99948092 * ____ilg_26;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textstartV
	LocalBuilder_t2116499186 * ____textstartV_27;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textbegV
	LocalBuilder_t2116499186 * ____textbegV_28;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textendV
	LocalBuilder_t2116499186 * ____textendV_29;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textposV
	LocalBuilder_t2116499186 * ____textposV_30;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textV
	LocalBuilder_t2116499186 * ____textV_31;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_trackposV
	LocalBuilder_t2116499186 * ____trackposV_32;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_trackV
	LocalBuilder_t2116499186 * ____trackV_33;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_stackposV
	LocalBuilder_t2116499186 * ____stackposV_34;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_stackV
	LocalBuilder_t2116499186 * ____stackV_35;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_tempV
	LocalBuilder_t2116499186 * ____tempV_36;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_temp2V
	LocalBuilder_t2116499186 * ____temp2V_37;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_temp3V
	LocalBuilder_t2116499186 * ____temp3V_38;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexCompiler::_code
	RegexCode_t2469392150 * ____code_39;
	// System.Int32[] System.Text.RegularExpressions.RegexCompiler::_codes
	Int32U5BU5D_t3030399641* ____codes_40;
	// System.String[] System.Text.RegularExpressions.RegexCompiler::_strings
	StringU5BU5D_t1642385972* ____strings_41;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexCompiler::_fcPrefix
	RegexPrefix_t1013837165 * ____fcPrefix_42;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexCompiler::_bmPrefix
	RegexBoyerMoore_t2204811018 * ____bmPrefix_43;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_anchors
	int32_t ____anchors_44;
	// System.Reflection.Emit.Label[] System.Text.RegularExpressions.RegexCompiler::_labels
	LabelU5BU5D_t196522893* ____labels_45;
	// System.Text.RegularExpressions.RegexCompiler/BacktrackNote[] System.Text.RegularExpressions.RegexCompiler::_notes
	BacktrackNoteU5BU5D_t2230749527* ____notes_46;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_notecount
	int32_t ____notecount_47;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_trackcount
	int32_t ____trackcount_48;
	// System.Reflection.Emit.Label System.Text.RegularExpressions.RegexCompiler::_backtrack
	Label_t4243202660  ____backtrack_49;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_regexopcode
	int32_t ____regexopcode_50;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_codepos
	int32_t ____codepos_51;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_backpos
	int32_t ____backpos_52;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexCompiler::_options
	int32_t ____options_53;
	// System.Int32[] System.Text.RegularExpressions.RegexCompiler::_uniquenote
	Int32U5BU5D_t3030399641* ____uniquenote_54;
	// System.Int32[] System.Text.RegularExpressions.RegexCompiler::_goto
	Int32U5BU5D_t3030399641* ____goto_55;

public:
	inline static int32_t get_offset_of__ilg_26() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____ilg_26)); }
	inline ILGenerator_t99948092 * get__ilg_26() const { return ____ilg_26; }
	inline ILGenerator_t99948092 ** get_address_of__ilg_26() { return &____ilg_26; }
	inline void set__ilg_26(ILGenerator_t99948092 * value)
	{
		____ilg_26 = value;
		Il2CppCodeGenWriteBarrier((&____ilg_26), value);
	}

	inline static int32_t get_offset_of__textstartV_27() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textstartV_27)); }
	inline LocalBuilder_t2116499186 * get__textstartV_27() const { return ____textstartV_27; }
	inline LocalBuilder_t2116499186 ** get_address_of__textstartV_27() { return &____textstartV_27; }
	inline void set__textstartV_27(LocalBuilder_t2116499186 * value)
	{
		____textstartV_27 = value;
		Il2CppCodeGenWriteBarrier((&____textstartV_27), value);
	}

	inline static int32_t get_offset_of__textbegV_28() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textbegV_28)); }
	inline LocalBuilder_t2116499186 * get__textbegV_28() const { return ____textbegV_28; }
	inline LocalBuilder_t2116499186 ** get_address_of__textbegV_28() { return &____textbegV_28; }
	inline void set__textbegV_28(LocalBuilder_t2116499186 * value)
	{
		____textbegV_28 = value;
		Il2CppCodeGenWriteBarrier((&____textbegV_28), value);
	}

	inline static int32_t get_offset_of__textendV_29() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textendV_29)); }
	inline LocalBuilder_t2116499186 * get__textendV_29() const { return ____textendV_29; }
	inline LocalBuilder_t2116499186 ** get_address_of__textendV_29() { return &____textendV_29; }
	inline void set__textendV_29(LocalBuilder_t2116499186 * value)
	{
		____textendV_29 = value;
		Il2CppCodeGenWriteBarrier((&____textendV_29), value);
	}

	inline static int32_t get_offset_of__textposV_30() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textposV_30)); }
	inline LocalBuilder_t2116499186 * get__textposV_30() const { return ____textposV_30; }
	inline LocalBuilder_t2116499186 ** get_address_of__textposV_30() { return &____textposV_30; }
	inline void set__textposV_30(LocalBuilder_t2116499186 * value)
	{
		____textposV_30 = value;
		Il2CppCodeGenWriteBarrier((&____textposV_30), value);
	}

	inline static int32_t get_offset_of__textV_31() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textV_31)); }
	inline LocalBuilder_t2116499186 * get__textV_31() const { return ____textV_31; }
	inline LocalBuilder_t2116499186 ** get_address_of__textV_31() { return &____textV_31; }
	inline void set__textV_31(LocalBuilder_t2116499186 * value)
	{
		____textV_31 = value;
		Il2CppCodeGenWriteBarrier((&____textV_31), value);
	}

	inline static int32_t get_offset_of__trackposV_32() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____trackposV_32)); }
	inline LocalBuilder_t2116499186 * get__trackposV_32() const { return ____trackposV_32; }
	inline LocalBuilder_t2116499186 ** get_address_of__trackposV_32() { return &____trackposV_32; }
	inline void set__trackposV_32(LocalBuilder_t2116499186 * value)
	{
		____trackposV_32 = value;
		Il2CppCodeGenWriteBarrier((&____trackposV_32), value);
	}

	inline static int32_t get_offset_of__trackV_33() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____trackV_33)); }
	inline LocalBuilder_t2116499186 * get__trackV_33() const { return ____trackV_33; }
	inline LocalBuilder_t2116499186 ** get_address_of__trackV_33() { return &____trackV_33; }
	inline void set__trackV_33(LocalBuilder_t2116499186 * value)
	{
		____trackV_33 = value;
		Il2CppCodeGenWriteBarrier((&____trackV_33), value);
	}

	inline static int32_t get_offset_of__stackposV_34() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____stackposV_34)); }
	inline LocalBuilder_t2116499186 * get__stackposV_34() const { return ____stackposV_34; }
	inline LocalBuilder_t2116499186 ** get_address_of__stackposV_34() { return &____stackposV_34; }
	inline void set__stackposV_34(LocalBuilder_t2116499186 * value)
	{
		____stackposV_34 = value;
		Il2CppCodeGenWriteBarrier((&____stackposV_34), value);
	}

	inline static int32_t get_offset_of__stackV_35() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____stackV_35)); }
	inline LocalBuilder_t2116499186 * get__stackV_35() const { return ____stackV_35; }
	inline LocalBuilder_t2116499186 ** get_address_of__stackV_35() { return &____stackV_35; }
	inline void set__stackV_35(LocalBuilder_t2116499186 * value)
	{
		____stackV_35 = value;
		Il2CppCodeGenWriteBarrier((&____stackV_35), value);
	}

	inline static int32_t get_offset_of__tempV_36() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____tempV_36)); }
	inline LocalBuilder_t2116499186 * get__tempV_36() const { return ____tempV_36; }
	inline LocalBuilder_t2116499186 ** get_address_of__tempV_36() { return &____tempV_36; }
	inline void set__tempV_36(LocalBuilder_t2116499186 * value)
	{
		____tempV_36 = value;
		Il2CppCodeGenWriteBarrier((&____tempV_36), value);
	}

	inline static int32_t get_offset_of__temp2V_37() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____temp2V_37)); }
	inline LocalBuilder_t2116499186 * get__temp2V_37() const { return ____temp2V_37; }
	inline LocalBuilder_t2116499186 ** get_address_of__temp2V_37() { return &____temp2V_37; }
	inline void set__temp2V_37(LocalBuilder_t2116499186 * value)
	{
		____temp2V_37 = value;
		Il2CppCodeGenWriteBarrier((&____temp2V_37), value);
	}

	inline static int32_t get_offset_of__temp3V_38() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____temp3V_38)); }
	inline LocalBuilder_t2116499186 * get__temp3V_38() const { return ____temp3V_38; }
	inline LocalBuilder_t2116499186 ** get_address_of__temp3V_38() { return &____temp3V_38; }
	inline void set__temp3V_38(LocalBuilder_t2116499186 * value)
	{
		____temp3V_38 = value;
		Il2CppCodeGenWriteBarrier((&____temp3V_38), value);
	}

	inline static int32_t get_offset_of__code_39() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____code_39)); }
	inline RegexCode_t2469392150 * get__code_39() const { return ____code_39; }
	inline RegexCode_t2469392150 ** get_address_of__code_39() { return &____code_39; }
	inline void set__code_39(RegexCode_t2469392150 * value)
	{
		____code_39 = value;
		Il2CppCodeGenWriteBarrier((&____code_39), value);
	}

	inline static int32_t get_offset_of__codes_40() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____codes_40)); }
	inline Int32U5BU5D_t3030399641* get__codes_40() const { return ____codes_40; }
	inline Int32U5BU5D_t3030399641** get_address_of__codes_40() { return &____codes_40; }
	inline void set__codes_40(Int32U5BU5D_t3030399641* value)
	{
		____codes_40 = value;
		Il2CppCodeGenWriteBarrier((&____codes_40), value);
	}

	inline static int32_t get_offset_of__strings_41() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____strings_41)); }
	inline StringU5BU5D_t1642385972* get__strings_41() const { return ____strings_41; }
	inline StringU5BU5D_t1642385972** get_address_of__strings_41() { return &____strings_41; }
	inline void set__strings_41(StringU5BU5D_t1642385972* value)
	{
		____strings_41 = value;
		Il2CppCodeGenWriteBarrier((&____strings_41), value);
	}

	inline static int32_t get_offset_of__fcPrefix_42() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____fcPrefix_42)); }
	inline RegexPrefix_t1013837165 * get__fcPrefix_42() const { return ____fcPrefix_42; }
	inline RegexPrefix_t1013837165 ** get_address_of__fcPrefix_42() { return &____fcPrefix_42; }
	inline void set__fcPrefix_42(RegexPrefix_t1013837165 * value)
	{
		____fcPrefix_42 = value;
		Il2CppCodeGenWriteBarrier((&____fcPrefix_42), value);
	}

	inline static int32_t get_offset_of__bmPrefix_43() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____bmPrefix_43)); }
	inline RegexBoyerMoore_t2204811018 * get__bmPrefix_43() const { return ____bmPrefix_43; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of__bmPrefix_43() { return &____bmPrefix_43; }
	inline void set__bmPrefix_43(RegexBoyerMoore_t2204811018 * value)
	{
		____bmPrefix_43 = value;
		Il2CppCodeGenWriteBarrier((&____bmPrefix_43), value);
	}

	inline static int32_t get_offset_of__anchors_44() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____anchors_44)); }
	inline int32_t get__anchors_44() const { return ____anchors_44; }
	inline int32_t* get_address_of__anchors_44() { return &____anchors_44; }
	inline void set__anchors_44(int32_t value)
	{
		____anchors_44 = value;
	}

	inline static int32_t get_offset_of__labels_45() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____labels_45)); }
	inline LabelU5BU5D_t196522893* get__labels_45() const { return ____labels_45; }
	inline LabelU5BU5D_t196522893** get_address_of__labels_45() { return &____labels_45; }
	inline void set__labels_45(LabelU5BU5D_t196522893* value)
	{
		____labels_45 = value;
		Il2CppCodeGenWriteBarrier((&____labels_45), value);
	}

	inline static int32_t get_offset_of__notes_46() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____notes_46)); }
	inline BacktrackNoteU5BU5D_t2230749527* get__notes_46() const { return ____notes_46; }
	inline BacktrackNoteU5BU5D_t2230749527** get_address_of__notes_46() { return &____notes_46; }
	inline void set__notes_46(BacktrackNoteU5BU5D_t2230749527* value)
	{
		____notes_46 = value;
		Il2CppCodeGenWriteBarrier((&____notes_46), value);
	}

	inline static int32_t get_offset_of__notecount_47() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____notecount_47)); }
	inline int32_t get__notecount_47() const { return ____notecount_47; }
	inline int32_t* get_address_of__notecount_47() { return &____notecount_47; }
	inline void set__notecount_47(int32_t value)
	{
		____notecount_47 = value;
	}

	inline static int32_t get_offset_of__trackcount_48() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____trackcount_48)); }
	inline int32_t get__trackcount_48() const { return ____trackcount_48; }
	inline int32_t* get_address_of__trackcount_48() { return &____trackcount_48; }
	inline void set__trackcount_48(int32_t value)
	{
		____trackcount_48 = value;
	}

	inline static int32_t get_offset_of__backtrack_49() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____backtrack_49)); }
	inline Label_t4243202660  get__backtrack_49() const { return ____backtrack_49; }
	inline Label_t4243202660 * get_address_of__backtrack_49() { return &____backtrack_49; }
	inline void set__backtrack_49(Label_t4243202660  value)
	{
		____backtrack_49 = value;
	}

	inline static int32_t get_offset_of__regexopcode_50() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____regexopcode_50)); }
	inline int32_t get__regexopcode_50() const { return ____regexopcode_50; }
	inline int32_t* get_address_of__regexopcode_50() { return &____regexopcode_50; }
	inline void set__regexopcode_50(int32_t value)
	{
		____regexopcode_50 = value;
	}

	inline static int32_t get_offset_of__codepos_51() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____codepos_51)); }
	inline int32_t get__codepos_51() const { return ____codepos_51; }
	inline int32_t* get_address_of__codepos_51() { return &____codepos_51; }
	inline void set__codepos_51(int32_t value)
	{
		____codepos_51 = value;
	}

	inline static int32_t get_offset_of__backpos_52() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____backpos_52)); }
	inline int32_t get__backpos_52() const { return ____backpos_52; }
	inline int32_t* get_address_of__backpos_52() { return &____backpos_52; }
	inline void set__backpos_52(int32_t value)
	{
		____backpos_52 = value;
	}

	inline static int32_t get_offset_of__options_53() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____options_53)); }
	inline int32_t get__options_53() const { return ____options_53; }
	inline int32_t* get_address_of__options_53() { return &____options_53; }
	inline void set__options_53(int32_t value)
	{
		____options_53 = value;
	}

	inline static int32_t get_offset_of__uniquenote_54() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____uniquenote_54)); }
	inline Int32U5BU5D_t3030399641* get__uniquenote_54() const { return ____uniquenote_54; }
	inline Int32U5BU5D_t3030399641** get_address_of__uniquenote_54() { return &____uniquenote_54; }
	inline void set__uniquenote_54(Int32U5BU5D_t3030399641* value)
	{
		____uniquenote_54 = value;
		Il2CppCodeGenWriteBarrier((&____uniquenote_54), value);
	}

	inline static int32_t get_offset_of__goto_55() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____goto_55)); }
	inline Int32U5BU5D_t3030399641* get__goto_55() const { return ____goto_55; }
	inline Int32U5BU5D_t3030399641** get_address_of__goto_55() { return &____goto_55; }
	inline void set__goto_55(Int32U5BU5D_t3030399641* value)
	{
		____goto_55 = value;
		Il2CppCodeGenWriteBarrier((&____goto_55), value);
	}
};

struct RegexCompiler_t1714699756_StaticFields
{
public:
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textbegF
	FieldInfo_t * ____textbegF_0;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textendF
	FieldInfo_t * ____textendF_1;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textstartF
	FieldInfo_t * ____textstartF_2;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textposF
	FieldInfo_t * ____textposF_3;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textF
	FieldInfo_t * ____textF_4;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_trackposF
	FieldInfo_t * ____trackposF_5;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_trackF
	FieldInfo_t * ____trackF_6;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_stackposF
	FieldInfo_t * ____stackposF_7;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_stackF
	FieldInfo_t * ____stackF_8;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_trackcountF
	FieldInfo_t * ____trackcountF_9;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_ensurestorageM
	MethodInfo_t * ____ensurestorageM_10;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_captureM
	MethodInfo_t * ____captureM_11;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_transferM
	MethodInfo_t * ____transferM_12;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_uncaptureM
	MethodInfo_t * ____uncaptureM_13;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_ismatchedM
	MethodInfo_t * ____ismatchedM_14;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_matchlengthM
	MethodInfo_t * ____matchlengthM_15;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_matchindexM
	MethodInfo_t * ____matchindexM_16;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_isboundaryM
	MethodInfo_t * ____isboundaryM_17;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_isECMABoundaryM
	MethodInfo_t * ____isECMABoundaryM_18;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_chartolowerM
	MethodInfo_t * ____chartolowerM_19;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_getcharM
	MethodInfo_t * ____getcharM_20;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_crawlposM
	MethodInfo_t * ____crawlposM_21;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_charInSetM
	MethodInfo_t * ____charInSetM_22;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_getCurrentCulture
	MethodInfo_t * ____getCurrentCulture_23;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_getInvariantCulture
	MethodInfo_t * ____getInvariantCulture_24;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_checkTimeoutM
	MethodInfo_t * ____checkTimeoutM_25;

public:
	inline static int32_t get_offset_of__textbegF_0() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textbegF_0)); }
	inline FieldInfo_t * get__textbegF_0() const { return ____textbegF_0; }
	inline FieldInfo_t ** get_address_of__textbegF_0() { return &____textbegF_0; }
	inline void set__textbegF_0(FieldInfo_t * value)
	{
		____textbegF_0 = value;
		Il2CppCodeGenWriteBarrier((&____textbegF_0), value);
	}

	inline static int32_t get_offset_of__textendF_1() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textendF_1)); }
	inline FieldInfo_t * get__textendF_1() const { return ____textendF_1; }
	inline FieldInfo_t ** get_address_of__textendF_1() { return &____textendF_1; }
	inline void set__textendF_1(FieldInfo_t * value)
	{
		____textendF_1 = value;
		Il2CppCodeGenWriteBarrier((&____textendF_1), value);
	}

	inline static int32_t get_offset_of__textstartF_2() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textstartF_2)); }
	inline FieldInfo_t * get__textstartF_2() const { return ____textstartF_2; }
	inline FieldInfo_t ** get_address_of__textstartF_2() { return &____textstartF_2; }
	inline void set__textstartF_2(FieldInfo_t * value)
	{
		____textstartF_2 = value;
		Il2CppCodeGenWriteBarrier((&____textstartF_2), value);
	}

	inline static int32_t get_offset_of__textposF_3() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textposF_3)); }
	inline FieldInfo_t * get__textposF_3() const { return ____textposF_3; }
	inline FieldInfo_t ** get_address_of__textposF_3() { return &____textposF_3; }
	inline void set__textposF_3(FieldInfo_t * value)
	{
		____textposF_3 = value;
		Il2CppCodeGenWriteBarrier((&____textposF_3), value);
	}

	inline static int32_t get_offset_of__textF_4() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textF_4)); }
	inline FieldInfo_t * get__textF_4() const { return ____textF_4; }
	inline FieldInfo_t ** get_address_of__textF_4() { return &____textF_4; }
	inline void set__textF_4(FieldInfo_t * value)
	{
		____textF_4 = value;
		Il2CppCodeGenWriteBarrier((&____textF_4), value);
	}

	inline static int32_t get_offset_of__trackposF_5() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____trackposF_5)); }
	inline FieldInfo_t * get__trackposF_5() const { return ____trackposF_5; }
	inline FieldInfo_t ** get_address_of__trackposF_5() { return &____trackposF_5; }
	inline void set__trackposF_5(FieldInfo_t * value)
	{
		____trackposF_5 = value;
		Il2CppCodeGenWriteBarrier((&____trackposF_5), value);
	}

	inline static int32_t get_offset_of__trackF_6() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____trackF_6)); }
	inline FieldInfo_t * get__trackF_6() const { return ____trackF_6; }
	inline FieldInfo_t ** get_address_of__trackF_6() { return &____trackF_6; }
	inline void set__trackF_6(FieldInfo_t * value)
	{
		____trackF_6 = value;
		Il2CppCodeGenWriteBarrier((&____trackF_6), value);
	}

	inline static int32_t get_offset_of__stackposF_7() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____stackposF_7)); }
	inline FieldInfo_t * get__stackposF_7() const { return ____stackposF_7; }
	inline FieldInfo_t ** get_address_of__stackposF_7() { return &____stackposF_7; }
	inline void set__stackposF_7(FieldInfo_t * value)
	{
		____stackposF_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackposF_7), value);
	}

	inline static int32_t get_offset_of__stackF_8() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____stackF_8)); }
	inline FieldInfo_t * get__stackF_8() const { return ____stackF_8; }
	inline FieldInfo_t ** get_address_of__stackF_8() { return &____stackF_8; }
	inline void set__stackF_8(FieldInfo_t * value)
	{
		____stackF_8 = value;
		Il2CppCodeGenWriteBarrier((&____stackF_8), value);
	}

	inline static int32_t get_offset_of__trackcountF_9() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____trackcountF_9)); }
	inline FieldInfo_t * get__trackcountF_9() const { return ____trackcountF_9; }
	inline FieldInfo_t ** get_address_of__trackcountF_9() { return &____trackcountF_9; }
	inline void set__trackcountF_9(FieldInfo_t * value)
	{
		____trackcountF_9 = value;
		Il2CppCodeGenWriteBarrier((&____trackcountF_9), value);
	}

	inline static int32_t get_offset_of__ensurestorageM_10() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____ensurestorageM_10)); }
	inline MethodInfo_t * get__ensurestorageM_10() const { return ____ensurestorageM_10; }
	inline MethodInfo_t ** get_address_of__ensurestorageM_10() { return &____ensurestorageM_10; }
	inline void set__ensurestorageM_10(MethodInfo_t * value)
	{
		____ensurestorageM_10 = value;
		Il2CppCodeGenWriteBarrier((&____ensurestorageM_10), value);
	}

	inline static int32_t get_offset_of__captureM_11() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____captureM_11)); }
	inline MethodInfo_t * get__captureM_11() const { return ____captureM_11; }
	inline MethodInfo_t ** get_address_of__captureM_11() { return &____captureM_11; }
	inline void set__captureM_11(MethodInfo_t * value)
	{
		____captureM_11 = value;
		Il2CppCodeGenWriteBarrier((&____captureM_11), value);
	}

	inline static int32_t get_offset_of__transferM_12() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____transferM_12)); }
	inline MethodInfo_t * get__transferM_12() const { return ____transferM_12; }
	inline MethodInfo_t ** get_address_of__transferM_12() { return &____transferM_12; }
	inline void set__transferM_12(MethodInfo_t * value)
	{
		____transferM_12 = value;
		Il2CppCodeGenWriteBarrier((&____transferM_12), value);
	}

	inline static int32_t get_offset_of__uncaptureM_13() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____uncaptureM_13)); }
	inline MethodInfo_t * get__uncaptureM_13() const { return ____uncaptureM_13; }
	inline MethodInfo_t ** get_address_of__uncaptureM_13() { return &____uncaptureM_13; }
	inline void set__uncaptureM_13(MethodInfo_t * value)
	{
		____uncaptureM_13 = value;
		Il2CppCodeGenWriteBarrier((&____uncaptureM_13), value);
	}

	inline static int32_t get_offset_of__ismatchedM_14() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____ismatchedM_14)); }
	inline MethodInfo_t * get__ismatchedM_14() const { return ____ismatchedM_14; }
	inline MethodInfo_t ** get_address_of__ismatchedM_14() { return &____ismatchedM_14; }
	inline void set__ismatchedM_14(MethodInfo_t * value)
	{
		____ismatchedM_14 = value;
		Il2CppCodeGenWriteBarrier((&____ismatchedM_14), value);
	}

	inline static int32_t get_offset_of__matchlengthM_15() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____matchlengthM_15)); }
	inline MethodInfo_t * get__matchlengthM_15() const { return ____matchlengthM_15; }
	inline MethodInfo_t ** get_address_of__matchlengthM_15() { return &____matchlengthM_15; }
	inline void set__matchlengthM_15(MethodInfo_t * value)
	{
		____matchlengthM_15 = value;
		Il2CppCodeGenWriteBarrier((&____matchlengthM_15), value);
	}

	inline static int32_t get_offset_of__matchindexM_16() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____matchindexM_16)); }
	inline MethodInfo_t * get__matchindexM_16() const { return ____matchindexM_16; }
	inline MethodInfo_t ** get_address_of__matchindexM_16() { return &____matchindexM_16; }
	inline void set__matchindexM_16(MethodInfo_t * value)
	{
		____matchindexM_16 = value;
		Il2CppCodeGenWriteBarrier((&____matchindexM_16), value);
	}

	inline static int32_t get_offset_of__isboundaryM_17() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____isboundaryM_17)); }
	inline MethodInfo_t * get__isboundaryM_17() const { return ____isboundaryM_17; }
	inline MethodInfo_t ** get_address_of__isboundaryM_17() { return &____isboundaryM_17; }
	inline void set__isboundaryM_17(MethodInfo_t * value)
	{
		____isboundaryM_17 = value;
		Il2CppCodeGenWriteBarrier((&____isboundaryM_17), value);
	}

	inline static int32_t get_offset_of__isECMABoundaryM_18() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____isECMABoundaryM_18)); }
	inline MethodInfo_t * get__isECMABoundaryM_18() const { return ____isECMABoundaryM_18; }
	inline MethodInfo_t ** get_address_of__isECMABoundaryM_18() { return &____isECMABoundaryM_18; }
	inline void set__isECMABoundaryM_18(MethodInfo_t * value)
	{
		____isECMABoundaryM_18 = value;
		Il2CppCodeGenWriteBarrier((&____isECMABoundaryM_18), value);
	}

	inline static int32_t get_offset_of__chartolowerM_19() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____chartolowerM_19)); }
	inline MethodInfo_t * get__chartolowerM_19() const { return ____chartolowerM_19; }
	inline MethodInfo_t ** get_address_of__chartolowerM_19() { return &____chartolowerM_19; }
	inline void set__chartolowerM_19(MethodInfo_t * value)
	{
		____chartolowerM_19 = value;
		Il2CppCodeGenWriteBarrier((&____chartolowerM_19), value);
	}

	inline static int32_t get_offset_of__getcharM_20() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____getcharM_20)); }
	inline MethodInfo_t * get__getcharM_20() const { return ____getcharM_20; }
	inline MethodInfo_t ** get_address_of__getcharM_20() { return &____getcharM_20; }
	inline void set__getcharM_20(MethodInfo_t * value)
	{
		____getcharM_20 = value;
		Il2CppCodeGenWriteBarrier((&____getcharM_20), value);
	}

	inline static int32_t get_offset_of__crawlposM_21() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____crawlposM_21)); }
	inline MethodInfo_t * get__crawlposM_21() const { return ____crawlposM_21; }
	inline MethodInfo_t ** get_address_of__crawlposM_21() { return &____crawlposM_21; }
	inline void set__crawlposM_21(MethodInfo_t * value)
	{
		____crawlposM_21 = value;
		Il2CppCodeGenWriteBarrier((&____crawlposM_21), value);
	}

	inline static int32_t get_offset_of__charInSetM_22() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____charInSetM_22)); }
	inline MethodInfo_t * get__charInSetM_22() const { return ____charInSetM_22; }
	inline MethodInfo_t ** get_address_of__charInSetM_22() { return &____charInSetM_22; }
	inline void set__charInSetM_22(MethodInfo_t * value)
	{
		____charInSetM_22 = value;
		Il2CppCodeGenWriteBarrier((&____charInSetM_22), value);
	}

	inline static int32_t get_offset_of__getCurrentCulture_23() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____getCurrentCulture_23)); }
	inline MethodInfo_t * get__getCurrentCulture_23() const { return ____getCurrentCulture_23; }
	inline MethodInfo_t ** get_address_of__getCurrentCulture_23() { return &____getCurrentCulture_23; }
	inline void set__getCurrentCulture_23(MethodInfo_t * value)
	{
		____getCurrentCulture_23 = value;
		Il2CppCodeGenWriteBarrier((&____getCurrentCulture_23), value);
	}

	inline static int32_t get_offset_of__getInvariantCulture_24() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____getInvariantCulture_24)); }
	inline MethodInfo_t * get__getInvariantCulture_24() const { return ____getInvariantCulture_24; }
	inline MethodInfo_t ** get_address_of__getInvariantCulture_24() { return &____getInvariantCulture_24; }
	inline void set__getInvariantCulture_24(MethodInfo_t * value)
	{
		____getInvariantCulture_24 = value;
		Il2CppCodeGenWriteBarrier((&____getInvariantCulture_24), value);
	}

	inline static int32_t get_offset_of__checkTimeoutM_25() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____checkTimeoutM_25)); }
	inline MethodInfo_t * get__checkTimeoutM_25() const { return ____checkTimeoutM_25; }
	inline MethodInfo_t ** get_address_of__checkTimeoutM_25() { return &____checkTimeoutM_25; }
	inline void set__checkTimeoutM_25(MethodInfo_t * value)
	{
		____checkTimeoutM_25 = value;
		Il2CppCodeGenWriteBarrier((&____checkTimeoutM_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCOMPILER_T1714699756_H
#ifndef REGEXMATCHTIMEOUTEXCEPTION_T197735700_H
#define REGEXMATCHTIMEOUTEXCEPTION_T197735700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexMatchTimeoutException
struct  RegexMatchTimeoutException_t197735700  : public TimeoutException_t3246754798
{
public:
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexInput
	String_t* ___regexInput_16;
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexPattern
	String_t* ___regexPattern_17;
	// System.TimeSpan System.Text.RegularExpressions.RegexMatchTimeoutException::matchTimeout
	TimeSpan_t3430258949  ___matchTimeout_18;

public:
	inline static int32_t get_offset_of_regexInput_16() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t197735700, ___regexInput_16)); }
	inline String_t* get_regexInput_16() const { return ___regexInput_16; }
	inline String_t** get_address_of_regexInput_16() { return &___regexInput_16; }
	inline void set_regexInput_16(String_t* value)
	{
		___regexInput_16 = value;
		Il2CppCodeGenWriteBarrier((&___regexInput_16), value);
	}

	inline static int32_t get_offset_of_regexPattern_17() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t197735700, ___regexPattern_17)); }
	inline String_t* get_regexPattern_17() const { return ___regexPattern_17; }
	inline String_t** get_address_of_regexPattern_17() { return &___regexPattern_17; }
	inline void set_regexPattern_17(String_t* value)
	{
		___regexPattern_17 = value;
		Il2CppCodeGenWriteBarrier((&___regexPattern_17), value);
	}

	inline static int32_t get_offset_of_matchTimeout_18() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t197735700, ___matchTimeout_18)); }
	inline TimeSpan_t3430258949  get_matchTimeout_18() const { return ___matchTimeout_18; }
	inline TimeSpan_t3430258949 * get_address_of_matchTimeout_18() { return &___matchTimeout_18; }
	inline void set_matchTimeout_18(TimeSpan_t3430258949  value)
	{
		___matchTimeout_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXMATCHTIMEOUTEXCEPTION_T197735700_H
#ifndef MATCHSPARSE_T4194398671_H
#define MATCHSPARSE_T4194398671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchSparse
struct  MatchSparse_t4194398671  : public Match_t3164245899
{
public:
	// System.Collections.Hashtable System.Text.RegularExpressions.MatchSparse::_caps
	Hashtable_t909839986 * ____caps_16;

public:
	inline static int32_t get_offset_of__caps_16() { return static_cast<int32_t>(offsetof(MatchSparse_t4194398671, ____caps_16)); }
	inline Hashtable_t909839986 * get__caps_16() const { return ____caps_16; }
	inline Hashtable_t909839986 ** get_address_of__caps_16() { return &____caps_16; }
	inline void set__caps_16(Hashtable_t909839986 * value)
	{
		____caps_16 = value;
		Il2CppCodeGenWriteBarrier((&____caps_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHSPARSE_T4194398671_H
#ifndef WEBREQUEST_T1365124353_H
#define WEBREQUEST_T1365124353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t1365124353  : public MarshalByRefObject_t1285298191
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::m_AuthenticationLevel
	int32_t ___m_AuthenticationLevel_4;
	// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::m_ImpersonationLevel
	int32_t ___m_ImpersonationLevel_5;
	// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::m_CachePolicy
	RequestCachePolicy_t2663429579 * ___m_CachePolicy_6;
	// System.Net.Cache.RequestCacheProtocol System.Net.WebRequest::m_CacheProtocol
	RequestCacheProtocol_t2110185277 * ___m_CacheProtocol_7;
	// System.Net.Cache.RequestCacheBinding System.Net.WebRequest::m_CacheBinding
	RequestCacheBinding_t114276176 * ___m_CacheBinding_8;

public:
	inline static int32_t get_offset_of_m_AuthenticationLevel_4() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_AuthenticationLevel_4)); }
	inline int32_t get_m_AuthenticationLevel_4() const { return ___m_AuthenticationLevel_4; }
	inline int32_t* get_address_of_m_AuthenticationLevel_4() { return &___m_AuthenticationLevel_4; }
	inline void set_m_AuthenticationLevel_4(int32_t value)
	{
		___m_AuthenticationLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_ImpersonationLevel_5() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_ImpersonationLevel_5)); }
	inline int32_t get_m_ImpersonationLevel_5() const { return ___m_ImpersonationLevel_5; }
	inline int32_t* get_address_of_m_ImpersonationLevel_5() { return &___m_ImpersonationLevel_5; }
	inline void set_m_ImpersonationLevel_5(int32_t value)
	{
		___m_ImpersonationLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_6() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_CachePolicy_6)); }
	inline RequestCachePolicy_t2663429579 * get_m_CachePolicy_6() const { return ___m_CachePolicy_6; }
	inline RequestCachePolicy_t2663429579 ** get_address_of_m_CachePolicy_6() { return &___m_CachePolicy_6; }
	inline void set_m_CachePolicy_6(RequestCachePolicy_t2663429579 * value)
	{
		___m_CachePolicy_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_6), value);
	}

	inline static int32_t get_offset_of_m_CacheProtocol_7() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_CacheProtocol_7)); }
	inline RequestCacheProtocol_t2110185277 * get_m_CacheProtocol_7() const { return ___m_CacheProtocol_7; }
	inline RequestCacheProtocol_t2110185277 ** get_address_of_m_CacheProtocol_7() { return &___m_CacheProtocol_7; }
	inline void set_m_CacheProtocol_7(RequestCacheProtocol_t2110185277 * value)
	{
		___m_CacheProtocol_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheProtocol_7), value);
	}

	inline static int32_t get_offset_of_m_CacheBinding_8() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_CacheBinding_8)); }
	inline RequestCacheBinding_t114276176 * get_m_CacheBinding_8() const { return ___m_CacheBinding_8; }
	inline RequestCacheBinding_t114276176 ** get_address_of_m_CacheBinding_8() { return &___m_CacheBinding_8; }
	inline void set_m_CacheBinding_8(RequestCacheBinding_t114276176 * value)
	{
		___m_CacheBinding_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheBinding_8), value);
	}
};

struct WebRequest_t1365124353_StaticFields
{
public:
	// System.Collections.ArrayList modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_PrefixList
	ArrayList_t4252133567 * ___s_PrefixList_1;
	// System.Object System.Net.WebRequest::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_2;
	// System.Net.TimerThread/Queue System.Net.WebRequest::s_DefaultTimerQueue
	Queue_t1332364135 * ___s_DefaultTimerQueue_3;
	// System.Net.WebRequest/DesignerWebRequestCreate System.Net.WebRequest::webRequestCreate
	DesignerWebRequestCreate_t3086960480 * ___webRequestCreate_9;
	// System.Net.IWebProxy modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxy
	RuntimeObject* ___s_DefaultWebProxy_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxyInitialized
	bool ___s_DefaultWebProxyInitialized_11;

public:
	inline static int32_t get_offset_of_s_PrefixList_1() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_PrefixList_1)); }
	inline ArrayList_t4252133567 * get_s_PrefixList_1() const { return ___s_PrefixList_1; }
	inline ArrayList_t4252133567 ** get_address_of_s_PrefixList_1() { return &___s_PrefixList_1; }
	inline void set_s_PrefixList_1(ArrayList_t4252133567 * value)
	{
		___s_PrefixList_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PrefixList_1), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_2() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_InternalSyncObject_2)); }
	inline RuntimeObject * get_s_InternalSyncObject_2() const { return ___s_InternalSyncObject_2; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_2() { return &___s_InternalSyncObject_2; }
	inline void set_s_InternalSyncObject_2(RuntimeObject * value)
	{
		___s_InternalSyncObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultTimerQueue_3() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_DefaultTimerQueue_3)); }
	inline Queue_t1332364135 * get_s_DefaultTimerQueue_3() const { return ___s_DefaultTimerQueue_3; }
	inline Queue_t1332364135 ** get_address_of_s_DefaultTimerQueue_3() { return &___s_DefaultTimerQueue_3; }
	inline void set_s_DefaultTimerQueue_3(Queue_t1332364135 * value)
	{
		___s_DefaultTimerQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultTimerQueue_3), value);
	}

	inline static int32_t get_offset_of_webRequestCreate_9() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___webRequestCreate_9)); }
	inline DesignerWebRequestCreate_t3086960480 * get_webRequestCreate_9() const { return ___webRequestCreate_9; }
	inline DesignerWebRequestCreate_t3086960480 ** get_address_of_webRequestCreate_9() { return &___webRequestCreate_9; }
	inline void set_webRequestCreate_9(DesignerWebRequestCreate_t3086960480 * value)
	{
		___webRequestCreate_9 = value;
		Il2CppCodeGenWriteBarrier((&___webRequestCreate_9), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxy_10() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_DefaultWebProxy_10)); }
	inline RuntimeObject* get_s_DefaultWebProxy_10() const { return ___s_DefaultWebProxy_10; }
	inline RuntimeObject** get_address_of_s_DefaultWebProxy_10() { return &___s_DefaultWebProxy_10; }
	inline void set_s_DefaultWebProxy_10(RuntimeObject* value)
	{
		___s_DefaultWebProxy_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultWebProxy_10), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxyInitialized_11() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_DefaultWebProxyInitialized_11)); }
	inline bool get_s_DefaultWebProxyInitialized_11() const { return ___s_DefaultWebProxyInitialized_11; }
	inline bool* get_address_of_s_DefaultWebProxyInitialized_11() { return &___s_DefaultWebProxyInitialized_11; }
	inline void set_s_DefaultWebProxyInitialized_11(bool value)
	{
		___s_DefaultWebProxyInitialized_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T1365124353_H
#ifndef REGEX_T1803876613_H
#define REGEX_T1803876613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t1803876613  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_0;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.Regex::factory
	RegexRunnerFactory_t3902733837 * ___factory_1;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_2;
	// System.TimeSpan System.Text.RegularExpressions.Regex::internalMatchTimeout
	TimeSpan_t3430258949  ___internalMatchTimeout_5;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::caps
	Hashtable_t909839986 * ___caps_8;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::capnames
	Hashtable_t909839986 * ___capnames_9;
	// System.String[] System.Text.RegularExpressions.Regex::capslist
	StringU5BU5D_t1642385972* ___capslist_10;
	// System.Int32 System.Text.RegularExpressions.Regex::capsize
	int32_t ___capsize_11;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.Regex::runnerref
	ExclusiveReference_t708182869 * ___runnerref_12;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.Regex::replref
	SharedReference_t2137668360 * ___replref_13;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.Regex::code
	RegexCode_t2469392150 * ___code_14;
	// System.Boolean System.Text.RegularExpressions.Regex::refsInitialized
	bool ___refsInitialized_15;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_factory_1() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___factory_1)); }
	inline RegexRunnerFactory_t3902733837 * get_factory_1() const { return ___factory_1; }
	inline RegexRunnerFactory_t3902733837 ** get_address_of_factory_1() { return &___factory_1; }
	inline void set_factory_1(RegexRunnerFactory_t3902733837 * value)
	{
		___factory_1 = value;
		Il2CppCodeGenWriteBarrier((&___factory_1), value);
	}

	inline static int32_t get_offset_of_roptions_2() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___roptions_2)); }
	inline int32_t get_roptions_2() const { return ___roptions_2; }
	inline int32_t* get_address_of_roptions_2() { return &___roptions_2; }
	inline void set_roptions_2(int32_t value)
	{
		___roptions_2 = value;
	}

	inline static int32_t get_offset_of_internalMatchTimeout_5() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___internalMatchTimeout_5)); }
	inline TimeSpan_t3430258949  get_internalMatchTimeout_5() const { return ___internalMatchTimeout_5; }
	inline TimeSpan_t3430258949 * get_address_of_internalMatchTimeout_5() { return &___internalMatchTimeout_5; }
	inline void set_internalMatchTimeout_5(TimeSpan_t3430258949  value)
	{
		___internalMatchTimeout_5 = value;
	}

	inline static int32_t get_offset_of_caps_8() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___caps_8)); }
	inline Hashtable_t909839986 * get_caps_8() const { return ___caps_8; }
	inline Hashtable_t909839986 ** get_address_of_caps_8() { return &___caps_8; }
	inline void set_caps_8(Hashtable_t909839986 * value)
	{
		___caps_8 = value;
		Il2CppCodeGenWriteBarrier((&___caps_8), value);
	}

	inline static int32_t get_offset_of_capnames_9() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___capnames_9)); }
	inline Hashtable_t909839986 * get_capnames_9() const { return ___capnames_9; }
	inline Hashtable_t909839986 ** get_address_of_capnames_9() { return &___capnames_9; }
	inline void set_capnames_9(Hashtable_t909839986 * value)
	{
		___capnames_9 = value;
		Il2CppCodeGenWriteBarrier((&___capnames_9), value);
	}

	inline static int32_t get_offset_of_capslist_10() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___capslist_10)); }
	inline StringU5BU5D_t1642385972* get_capslist_10() const { return ___capslist_10; }
	inline StringU5BU5D_t1642385972** get_address_of_capslist_10() { return &___capslist_10; }
	inline void set_capslist_10(StringU5BU5D_t1642385972* value)
	{
		___capslist_10 = value;
		Il2CppCodeGenWriteBarrier((&___capslist_10), value);
	}

	inline static int32_t get_offset_of_capsize_11() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___capsize_11)); }
	inline int32_t get_capsize_11() const { return ___capsize_11; }
	inline int32_t* get_address_of_capsize_11() { return &___capsize_11; }
	inline void set_capsize_11(int32_t value)
	{
		___capsize_11 = value;
	}

	inline static int32_t get_offset_of_runnerref_12() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___runnerref_12)); }
	inline ExclusiveReference_t708182869 * get_runnerref_12() const { return ___runnerref_12; }
	inline ExclusiveReference_t708182869 ** get_address_of_runnerref_12() { return &___runnerref_12; }
	inline void set_runnerref_12(ExclusiveReference_t708182869 * value)
	{
		___runnerref_12 = value;
		Il2CppCodeGenWriteBarrier((&___runnerref_12), value);
	}

	inline static int32_t get_offset_of_replref_13() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___replref_13)); }
	inline SharedReference_t2137668360 * get_replref_13() const { return ___replref_13; }
	inline SharedReference_t2137668360 ** get_address_of_replref_13() { return &___replref_13; }
	inline void set_replref_13(SharedReference_t2137668360 * value)
	{
		___replref_13 = value;
		Il2CppCodeGenWriteBarrier((&___replref_13), value);
	}

	inline static int32_t get_offset_of_code_14() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___code_14)); }
	inline RegexCode_t2469392150 * get_code_14() const { return ___code_14; }
	inline RegexCode_t2469392150 ** get_address_of_code_14() { return &___code_14; }
	inline void set_code_14(RegexCode_t2469392150 * value)
	{
		___code_14 = value;
		Il2CppCodeGenWriteBarrier((&___code_14), value);
	}

	inline static int32_t get_offset_of_refsInitialized_15() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___refsInitialized_15)); }
	inline bool get_refsInitialized_15() const { return ___refsInitialized_15; }
	inline bool* get_address_of_refsInitialized_15() { return &___refsInitialized_15; }
	inline void set_refsInitialized_15(bool value)
	{
		___refsInitialized_15 = value;
	}
};

struct Regex_t1803876613_StaticFields
{
public:
	// System.TimeSpan System.Text.RegularExpressions.Regex::MaximumMatchTimeout
	TimeSpan_t3430258949  ___MaximumMatchTimeout_3;
	// System.TimeSpan System.Text.RegularExpressions.Regex::InfiniteMatchTimeout
	TimeSpan_t3430258949  ___InfiniteMatchTimeout_4;
	// System.TimeSpan System.Text.RegularExpressions.Regex::FallbackDefaultMatchTimeout
	TimeSpan_t3430258949  ___FallbackDefaultMatchTimeout_6;
	// System.TimeSpan System.Text.RegularExpressions.Regex::DefaultMatchTimeout
	TimeSpan_t3430258949  ___DefaultMatchTimeout_7;
	// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry> System.Text.RegularExpressions.Regex::livecode
	LinkedList_1_t3858529280 * ___livecode_16;
	// System.Int32 System.Text.RegularExpressions.Regex::cacheSize
	int32_t ___cacheSize_17;

public:
	inline static int32_t get_offset_of_MaximumMatchTimeout_3() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___MaximumMatchTimeout_3)); }
	inline TimeSpan_t3430258949  get_MaximumMatchTimeout_3() const { return ___MaximumMatchTimeout_3; }
	inline TimeSpan_t3430258949 * get_address_of_MaximumMatchTimeout_3() { return &___MaximumMatchTimeout_3; }
	inline void set_MaximumMatchTimeout_3(TimeSpan_t3430258949  value)
	{
		___MaximumMatchTimeout_3 = value;
	}

	inline static int32_t get_offset_of_InfiniteMatchTimeout_4() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___InfiniteMatchTimeout_4)); }
	inline TimeSpan_t3430258949  get_InfiniteMatchTimeout_4() const { return ___InfiniteMatchTimeout_4; }
	inline TimeSpan_t3430258949 * get_address_of_InfiniteMatchTimeout_4() { return &___InfiniteMatchTimeout_4; }
	inline void set_InfiniteMatchTimeout_4(TimeSpan_t3430258949  value)
	{
		___InfiniteMatchTimeout_4 = value;
	}

	inline static int32_t get_offset_of_FallbackDefaultMatchTimeout_6() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___FallbackDefaultMatchTimeout_6)); }
	inline TimeSpan_t3430258949  get_FallbackDefaultMatchTimeout_6() const { return ___FallbackDefaultMatchTimeout_6; }
	inline TimeSpan_t3430258949 * get_address_of_FallbackDefaultMatchTimeout_6() { return &___FallbackDefaultMatchTimeout_6; }
	inline void set_FallbackDefaultMatchTimeout_6(TimeSpan_t3430258949  value)
	{
		___FallbackDefaultMatchTimeout_6 = value;
	}

	inline static int32_t get_offset_of_DefaultMatchTimeout_7() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___DefaultMatchTimeout_7)); }
	inline TimeSpan_t3430258949  get_DefaultMatchTimeout_7() const { return ___DefaultMatchTimeout_7; }
	inline TimeSpan_t3430258949 * get_address_of_DefaultMatchTimeout_7() { return &___DefaultMatchTimeout_7; }
	inline void set_DefaultMatchTimeout_7(TimeSpan_t3430258949  value)
	{
		___DefaultMatchTimeout_7 = value;
	}

	inline static int32_t get_offset_of_livecode_16() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___livecode_16)); }
	inline LinkedList_1_t3858529280 * get_livecode_16() const { return ___livecode_16; }
	inline LinkedList_1_t3858529280 ** get_address_of_livecode_16() { return &___livecode_16; }
	inline void set_livecode_16(LinkedList_1_t3858529280 * value)
	{
		___livecode_16 = value;
		Il2CppCodeGenWriteBarrier((&___livecode_16), value);
	}

	inline static int32_t get_offset_of_cacheSize_17() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___cacheSize_17)); }
	inline int32_t get_cacheSize_17() const { return ___cacheSize_17; }
	inline int32_t* get_address_of_cacheSize_17() { return &___cacheSize_17; }
	inline void set_cacheSize_17(int32_t value)
	{
		___cacheSize_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T1803876613_H
#ifndef WEBEXCEPTION_T3368933679_H
#define WEBEXCEPTION_T3368933679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_t3368933679  : public InvalidOperationException_t721527559
{
public:
	// System.Net.WebExceptionStatus System.Net.WebException::m_Status
	int32_t ___m_Status_16;
	// System.Net.WebResponse System.Net.WebException::m_Response
	WebResponse_t1895226051 * ___m_Response_17;
	// System.Net.WebExceptionInternalStatus System.Net.WebException::m_InternalStatus
	int32_t ___m_InternalStatus_18;

public:
	inline static int32_t get_offset_of_m_Status_16() { return static_cast<int32_t>(offsetof(WebException_t3368933679, ___m_Status_16)); }
	inline int32_t get_m_Status_16() const { return ___m_Status_16; }
	inline int32_t* get_address_of_m_Status_16() { return &___m_Status_16; }
	inline void set_m_Status_16(int32_t value)
	{
		___m_Status_16 = value;
	}

	inline static int32_t get_offset_of_m_Response_17() { return static_cast<int32_t>(offsetof(WebException_t3368933679, ___m_Response_17)); }
	inline WebResponse_t1895226051 * get_m_Response_17() const { return ___m_Response_17; }
	inline WebResponse_t1895226051 ** get_address_of_m_Response_17() { return &___m_Response_17; }
	inline void set_m_Response_17(WebResponse_t1895226051 * value)
	{
		___m_Response_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Response_17), value);
	}

	inline static int32_t get_offset_of_m_InternalStatus_18() { return static_cast<int32_t>(offsetof(WebException_t3368933679, ___m_InternalStatus_18)); }
	inline int32_t get_m_InternalStatus_18() const { return ___m_InternalStatus_18; }
	inline int32_t* get_address_of_m_InternalStatus_18() { return &___m_InternalStatus_18; }
	inline void set_m_InternalStatus_18(int32_t value)
	{
		___m_InternalStatus_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTION_T3368933679_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef WEBHEADERCOLLECTION_T3028142837_H
#define WEBHEADERCOLLECTION_T3028142837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_t3028142837  : public NameValueCollection_t3047564564
{
public:
	// System.String[] System.Net.WebHeaderCollection::m_CommonHeaders
	StringU5BU5D_t1642385972* ___m_CommonHeaders_13;
	// System.Int32 System.Net.WebHeaderCollection::m_NumCommonHeaders
	int32_t ___m_NumCommonHeaders_14;
	// System.Collections.Specialized.NameValueCollection System.Net.WebHeaderCollection::m_InnerCollection
	NameValueCollection_t3047564564 * ___m_InnerCollection_17;
	// System.Net.WebHeaderCollectionType System.Net.WebHeaderCollection::m_Type
	uint16_t ___m_Type_18;

public:
	inline static int32_t get_offset_of_m_CommonHeaders_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_CommonHeaders_13)); }
	inline StringU5BU5D_t1642385972* get_m_CommonHeaders_13() const { return ___m_CommonHeaders_13; }
	inline StringU5BU5D_t1642385972** get_address_of_m_CommonHeaders_13() { return &___m_CommonHeaders_13; }
	inline void set_m_CommonHeaders_13(StringU5BU5D_t1642385972* value)
	{
		___m_CommonHeaders_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommonHeaders_13), value);
	}

	inline static int32_t get_offset_of_m_NumCommonHeaders_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_NumCommonHeaders_14)); }
	inline int32_t get_m_NumCommonHeaders_14() const { return ___m_NumCommonHeaders_14; }
	inline int32_t* get_address_of_m_NumCommonHeaders_14() { return &___m_NumCommonHeaders_14; }
	inline void set_m_NumCommonHeaders_14(int32_t value)
	{
		___m_NumCommonHeaders_14 = value;
	}

	inline static int32_t get_offset_of_m_InnerCollection_17() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_InnerCollection_17)); }
	inline NameValueCollection_t3047564564 * get_m_InnerCollection_17() const { return ___m_InnerCollection_17; }
	inline NameValueCollection_t3047564564 ** get_address_of_m_InnerCollection_17() { return &___m_InnerCollection_17; }
	inline void set_m_InnerCollection_17(NameValueCollection_t3047564564 * value)
	{
		___m_InnerCollection_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_InnerCollection_17), value);
	}

	inline static int32_t get_offset_of_m_Type_18() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_Type_18)); }
	inline uint16_t get_m_Type_18() const { return ___m_Type_18; }
	inline uint16_t* get_address_of_m_Type_18() { return &___m_Type_18; }
	inline void set_m_Type_18(uint16_t value)
	{
		___m_Type_18 = value;
	}
};

struct WebHeaderCollection_t3028142837_StaticFields
{
public:
	// System.Net.HeaderInfoTable System.Net.WebHeaderCollection::HInfo
	HeaderInfoTable_t2462863175 * ___HInfo_12;
	// System.String[] System.Net.WebHeaderCollection::s_CommonHeaderNames
	StringU5BU5D_t1642385972* ___s_CommonHeaderNames_15;
	// System.SByte[] System.Net.WebHeaderCollection::s_CommonHeaderHints
	SByteU5BU5D_t3472287392* ___s_CommonHeaderHints_16;
	// System.Char[] System.Net.WebHeaderCollection::HttpTrimCharacters
	CharU5BU5D_t1328083999* ___HttpTrimCharacters_19;
	// System.Net.WebHeaderCollection/RfcChar[] System.Net.WebHeaderCollection::RfcCharMap
	RfcCharU5BU5D_t2287831188* ___RfcCharMap_20;

public:
	inline static int32_t get_offset_of_HInfo_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___HInfo_12)); }
	inline HeaderInfoTable_t2462863175 * get_HInfo_12() const { return ___HInfo_12; }
	inline HeaderInfoTable_t2462863175 ** get_address_of_HInfo_12() { return &___HInfo_12; }
	inline void set_HInfo_12(HeaderInfoTable_t2462863175 * value)
	{
		___HInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___HInfo_12), value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderNames_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___s_CommonHeaderNames_15)); }
	inline StringU5BU5D_t1642385972* get_s_CommonHeaderNames_15() const { return ___s_CommonHeaderNames_15; }
	inline StringU5BU5D_t1642385972** get_address_of_s_CommonHeaderNames_15() { return &___s_CommonHeaderNames_15; }
	inline void set_s_CommonHeaderNames_15(StringU5BU5D_t1642385972* value)
	{
		___s_CommonHeaderNames_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_CommonHeaderNames_15), value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderHints_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___s_CommonHeaderHints_16)); }
	inline SByteU5BU5D_t3472287392* get_s_CommonHeaderHints_16() const { return ___s_CommonHeaderHints_16; }
	inline SByteU5BU5D_t3472287392** get_address_of_s_CommonHeaderHints_16() { return &___s_CommonHeaderHints_16; }
	inline void set_s_CommonHeaderHints_16(SByteU5BU5D_t3472287392* value)
	{
		___s_CommonHeaderHints_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CommonHeaderHints_16), value);
	}

	inline static int32_t get_offset_of_HttpTrimCharacters_19() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___HttpTrimCharacters_19)); }
	inline CharU5BU5D_t1328083999* get_HttpTrimCharacters_19() const { return ___HttpTrimCharacters_19; }
	inline CharU5BU5D_t1328083999** get_address_of_HttpTrimCharacters_19() { return &___HttpTrimCharacters_19; }
	inline void set_HttpTrimCharacters_19(CharU5BU5D_t1328083999* value)
	{
		___HttpTrimCharacters_19 = value;
		Il2CppCodeGenWriteBarrier((&___HttpTrimCharacters_19), value);
	}

	inline static int32_t get_offset_of_RfcCharMap_20() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___RfcCharMap_20)); }
	inline RfcCharU5BU5D_t2287831188* get_RfcCharMap_20() const { return ___RfcCharMap_20; }
	inline RfcCharU5BU5D_t2287831188** get_address_of_RfcCharMap_20() { return &___RfcCharMap_20; }
	inline void set_RfcCharMap_20(RfcCharU5BU5D_t2287831188* value)
	{
		___RfcCharMap_20 = value;
		Il2CppCodeGenWriteBarrier((&___RfcCharMap_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T3028142837_H
#ifndef REGEXNODE_T2469392321_H
#define REGEXNODE_T2469392321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexNode
struct  RegexNode_t2469392321  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexNode::_type
	int32_t ____type_0;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexNode> System.Text.RegularExpressions.RegexNode::_children
	List_1_t1838513453 * ____children_1;
	// System.String System.Text.RegularExpressions.RegexNode::_str
	String_t* ____str_2;
	// System.Char System.Text.RegularExpressions.RegexNode::_ch
	Il2CppChar ____ch_3;
	// System.Int32 System.Text.RegularExpressions.RegexNode::_m
	int32_t ____m_4;
	// System.Int32 System.Text.RegularExpressions.RegexNode::_n
	int32_t ____n_5;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexNode::_options
	int32_t ____options_6;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::_next
	RegexNode_t2469392321 * ____next_7;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__children_1() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____children_1)); }
	inline List_1_t1838513453 * get__children_1() const { return ____children_1; }
	inline List_1_t1838513453 ** get_address_of__children_1() { return &____children_1; }
	inline void set__children_1(List_1_t1838513453 * value)
	{
		____children_1 = value;
		Il2CppCodeGenWriteBarrier((&____children_1), value);
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__ch_3() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____ch_3)); }
	inline Il2CppChar get__ch_3() const { return ____ch_3; }
	inline Il2CppChar* get_address_of__ch_3() { return &____ch_3; }
	inline void set__ch_3(Il2CppChar value)
	{
		____ch_3 = value;
	}

	inline static int32_t get_offset_of__m_4() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____m_4)); }
	inline int32_t get__m_4() const { return ____m_4; }
	inline int32_t* get_address_of__m_4() { return &____m_4; }
	inline void set__m_4(int32_t value)
	{
		____m_4 = value;
	}

	inline static int32_t get_offset_of__n_5() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____n_5)); }
	inline int32_t get__n_5() const { return ____n_5; }
	inline int32_t* get_address_of__n_5() { return &____n_5; }
	inline void set__n_5(int32_t value)
	{
		____n_5 = value;
	}

	inline static int32_t get_offset_of__options_6() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____options_6)); }
	inline int32_t get__options_6() const { return ____options_6; }
	inline int32_t* get_address_of__options_6() { return &____options_6; }
	inline void set__options_6(int32_t value)
	{
		____options_6 = value;
	}

	inline static int32_t get_offset_of__next_7() { return static_cast<int32_t>(offsetof(RegexNode_t2469392321, ____next_7)); }
	inline RegexNode_t2469392321 * get__next_7() const { return ____next_7; }
	inline RegexNode_t2469392321 ** get_address_of__next_7() { return &____next_7; }
	inline void set__next_7(RegexNode_t2469392321 * value)
	{
		____next_7 = value;
		Il2CppCodeGenWriteBarrier((&____next_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXNODE_T2469392321_H
#ifndef HEADERPARSER_T3706119548_H
#define HEADERPARSER_T3706119548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderParser
struct  HeaderParser_t3706119548  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERPARSER_T3706119548_H
#ifndef REGEXLWCGCOMPILER_T1552814047_H
#define REGEXLWCGCOMPILER_T1552814047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexLWCGCompiler
struct  RegexLWCGCompiler_t1552814047  : public RegexCompiler_t1714699756
{
public:

public:
};

struct RegexLWCGCompiler_t1552814047_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexLWCGCompiler::_regexCount
	int32_t ____regexCount_56;
	// System.Type[] System.Text.RegularExpressions.RegexLWCGCompiler::_paramTypes
	TypeU5BU5D_t1664964607* ____paramTypes_57;

public:
	inline static int32_t get_offset_of__regexCount_56() { return static_cast<int32_t>(offsetof(RegexLWCGCompiler_t1552814047_StaticFields, ____regexCount_56)); }
	inline int32_t get__regexCount_56() const { return ____regexCount_56; }
	inline int32_t* get_address_of__regexCount_56() { return &____regexCount_56; }
	inline void set__regexCount_56(int32_t value)
	{
		____regexCount_56 = value;
	}

	inline static int32_t get_offset_of__paramTypes_57() { return static_cast<int32_t>(offsetof(RegexLWCGCompiler_t1552814047_StaticFields, ____paramTypes_57)); }
	inline TypeU5BU5D_t1664964607* get__paramTypes_57() const { return ____paramTypes_57; }
	inline TypeU5BU5D_t1664964607** get_address_of__paramTypes_57() { return &____paramTypes_57; }
	inline void set__paramTypes_57(TypeU5BU5D_t1664964607* value)
	{
		____paramTypes_57 = value;
		Il2CppCodeGenWriteBarrier((&____paramTypes_57), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXLWCGCOMPILER_T1552814047_H
#ifndef HTTPCONTINUEDELEGATE_T2713047268_H
#define HTTPCONTINUEDELEGATE_T2713047268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpContinueDelegate
struct  HttpContinueDelegate_t2713047268  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONTINUEDELEGATE_T2713047268_H
#ifndef NOPARAMDELEGATE_T2890182105_H
#define NOPARAMDELEGATE_T2890182105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.NoParamDelegate
struct  NoParamDelegate_t2890182105  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOPARAMDELEGATE_T2890182105_H
#ifndef FINDFIRSTCHARDELEGATE_T4203859986_H
#define FINDFIRSTCHARDELEGATE_T4203859986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.FindFirstCharDelegate
struct  FindFirstCharDelegate_t4203859986  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDFIRSTCHARDELEGATE_T4203859986_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (X509Extension_t1320896183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[1] = 
{
	X509Extension_t1320896183::get_offset_of__critical_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (X509ExtensionCollection_t650873211), -1, sizeof(X509ExtensionCollection_t650873211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2401[2] = 
{
	X509ExtensionCollection_t650873211_StaticFields::get_offset_of_Empty_0(),
	X509ExtensionCollection_t650873211::get_offset_of__list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (X509ExtensionEnumerator_t3763443773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	X509ExtensionEnumerator_t3763443773::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (X509FindType_t3221716179)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2403[16] = 
{
	X509FindType_t3221716179::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (X509Helper2_t1000999714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (MyNativeHelper_t2161630009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (X509KeyUsageExtension_t1038124237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t1038124237::get_offset_of__keyUsages_6(),
	X509KeyUsageExtension_t1038124237::get_offset_of__status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (X509KeyUsageFlags_t2461349531)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2407[11] = 
{
	X509KeyUsageFlags_t2461349531::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (X509NameType_t2669466891)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[7] = 
{
	X509NameType_t2669466891::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (X509RevocationFlag_t2166064554)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2409[4] = 
{
	X509RevocationFlag_t2166064554::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (X509RevocationMode_t2065307963)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2410[4] = 
{
	X509RevocationMode_t2065307963::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (X509Store_t1617430119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[5] = 
{
	X509Store_t1617430119::get_offset_of__name_0(),
	X509Store_t1617430119::get_offset_of__location_1(),
	X509Store_t1617430119::get_offset_of_list_2(),
	X509Store_t1617430119::get_offset_of__flags_3(),
	X509Store_t1617430119::get_offset_of_store_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (X509SubjectKeyIdentifierExtension_t2508879999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__subjectKeyIdentifier_5(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__ski_6(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t110301003)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t110301003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (X509VerificationFlags_t2169036324)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[15] = 
{
	X509VerificationFlags_t2169036324::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (SslProtocols_t894678499)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2415[8] = 
{
	SslProtocols_t894678499::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (Regex_t1803876613), -1, sizeof(Regex_t1803876613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2416[18] = 
{
	Regex_t1803876613::get_offset_of_pattern_0(),
	Regex_t1803876613::get_offset_of_factory_1(),
	Regex_t1803876613::get_offset_of_roptions_2(),
	Regex_t1803876613_StaticFields::get_offset_of_MaximumMatchTimeout_3(),
	Regex_t1803876613_StaticFields::get_offset_of_InfiniteMatchTimeout_4(),
	Regex_t1803876613::get_offset_of_internalMatchTimeout_5(),
	Regex_t1803876613_StaticFields::get_offset_of_FallbackDefaultMatchTimeout_6(),
	Regex_t1803876613_StaticFields::get_offset_of_DefaultMatchTimeout_7(),
	Regex_t1803876613::get_offset_of_caps_8(),
	Regex_t1803876613::get_offset_of_capnames_9(),
	Regex_t1803876613::get_offset_of_capslist_10(),
	Regex_t1803876613::get_offset_of_capsize_11(),
	Regex_t1803876613::get_offset_of_runnerref_12(),
	Regex_t1803876613::get_offset_of_replref_13(),
	Regex_t1803876613::get_offset_of_code_14(),
	Regex_t1803876613::get_offset_of_refsInitialized_15(),
	Regex_t1803876613_StaticFields::get_offset_of_livecode_16(),
	Regex_t1803876613_StaticFields::get_offset_of_cacheSize_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (CachedCodeEntry_t3553821051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[9] = 
{
	CachedCodeEntry_t3553821051::get_offset_of__key_0(),
	CachedCodeEntry_t3553821051::get_offset_of__code_1(),
	CachedCodeEntry_t3553821051::get_offset_of__caps_2(),
	CachedCodeEntry_t3553821051::get_offset_of__capnames_3(),
	CachedCodeEntry_t3553821051::get_offset_of__capslist_4(),
	CachedCodeEntry_t3553821051::get_offset_of__capsize_5(),
	CachedCodeEntry_t3553821051::get_offset_of__factory_6(),
	CachedCodeEntry_t3553821051::get_offset_of__runnerref_7(),
	CachedCodeEntry_t3553821051::get_offset_of__replref_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (ExclusiveReference_t708182869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[3] = 
{
	ExclusiveReference_t708182869::get_offset_of__ref_0(),
	ExclusiveReference_t708182869::get_offset_of__obj_1(),
	ExclusiveReference_t708182869::get_offset_of__locked_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (SharedReference_t2137668360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[1] = 
{
	SharedReference_t2137668360::get_offset_of__ref_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (RegexBoyerMoore_t2204811018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[9] = 
{
	RegexBoyerMoore_t2204811018::get_offset_of__positive_0(),
	RegexBoyerMoore_t2204811018::get_offset_of__negativeASCII_1(),
	RegexBoyerMoore_t2204811018::get_offset_of__negativeUnicode_2(),
	RegexBoyerMoore_t2204811018::get_offset_of__pattern_3(),
	RegexBoyerMoore_t2204811018::get_offset_of__lowASCII_4(),
	RegexBoyerMoore_t2204811018::get_offset_of__highASCII_5(),
	RegexBoyerMoore_t2204811018::get_offset_of__rightToLeft_6(),
	RegexBoyerMoore_t2204811018::get_offset_of__caseInsensitive_7(),
	RegexBoyerMoore_t2204811018::get_offset_of__culture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	Capture_t4157900610::get_offset_of__text_0(),
	Capture_t4157900610::get_offset_of__index_1(),
	Capture_t4157900610::get_offset_of__length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (RegexCharClass_t2441867401), -1, sizeof(RegexCharClass_t2441867401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2422[19] = 
{
	RegexCharClass_t2441867401::get_offset_of__rangelist_0(),
	RegexCharClass_t2441867401::get_offset_of__categories_1(),
	RegexCharClass_t2441867401::get_offset_of__canonical_2(),
	RegexCharClass_t2441867401::get_offset_of__negate_3(),
	RegexCharClass_t2441867401::get_offset_of__subtractor_4(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_InternalRegexIgnoreCase_5(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_Space_6(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotSpace_7(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_Word_8(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotWord_9(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_SpaceClass_10(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotSpaceClass_11(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_WordClass_12(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotWordClass_13(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_DigitClass_14(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotDigitClass_15(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__definedCategories_16(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__propTable_17(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__lcTable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (LowerCaseMapping_t2153935826)+ sizeof (RuntimeObject), sizeof(LowerCaseMapping_t2153935826_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2423[4] = 
{
	LowerCaseMapping_t2153935826::get_offset_of__chMin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2153935826::get_offset_of__chMax_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2153935826::get_offset_of__lcOp_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2153935826::get_offset_of__data_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (SingleRangeComparer_t3640568023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (SingleRange_t3794243288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[2] = 
{
	SingleRange_t3794243288::get_offset_of__first_0(),
	SingleRange_t3794243288::get_offset_of__last_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (RegexCode_t2469392150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[9] = 
{
	RegexCode_t2469392150::get_offset_of__codes_0(),
	RegexCode_t2469392150::get_offset_of__strings_1(),
	RegexCode_t2469392150::get_offset_of__trackcount_2(),
	RegexCode_t2469392150::get_offset_of__caps_3(),
	RegexCode_t2469392150::get_offset_of__capsize_4(),
	RegexCode_t2469392150::get_offset_of__fcPrefix_5(),
	RegexCode_t2469392150::get_offset_of__bmPrefix_6(),
	RegexCode_t2469392150::get_offset_of__anchors_7(),
	RegexCode_t2469392150::get_offset_of__rightToLeft_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (RegexCompiler_t1714699756), -1, sizeof(RegexCompiler_t1714699756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2427[56] = 
{
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textbegF_0(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textendF_1(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textstartF_2(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textposF_3(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textF_4(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__trackposF_5(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__trackF_6(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__stackposF_7(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__stackF_8(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__trackcountF_9(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__ensurestorageM_10(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__captureM_11(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__transferM_12(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__uncaptureM_13(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__ismatchedM_14(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__matchlengthM_15(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__matchindexM_16(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__isboundaryM_17(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__isECMABoundaryM_18(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__chartolowerM_19(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__getcharM_20(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__crawlposM_21(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__charInSetM_22(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__getCurrentCulture_23(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__getInvariantCulture_24(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__checkTimeoutM_25(),
	RegexCompiler_t1714699756::get_offset_of__ilg_26(),
	RegexCompiler_t1714699756::get_offset_of__textstartV_27(),
	RegexCompiler_t1714699756::get_offset_of__textbegV_28(),
	RegexCompiler_t1714699756::get_offset_of__textendV_29(),
	RegexCompiler_t1714699756::get_offset_of__textposV_30(),
	RegexCompiler_t1714699756::get_offset_of__textV_31(),
	RegexCompiler_t1714699756::get_offset_of__trackposV_32(),
	RegexCompiler_t1714699756::get_offset_of__trackV_33(),
	RegexCompiler_t1714699756::get_offset_of__stackposV_34(),
	RegexCompiler_t1714699756::get_offset_of__stackV_35(),
	RegexCompiler_t1714699756::get_offset_of__tempV_36(),
	RegexCompiler_t1714699756::get_offset_of__temp2V_37(),
	RegexCompiler_t1714699756::get_offset_of__temp3V_38(),
	RegexCompiler_t1714699756::get_offset_of__code_39(),
	RegexCompiler_t1714699756::get_offset_of__codes_40(),
	RegexCompiler_t1714699756::get_offset_of__strings_41(),
	RegexCompiler_t1714699756::get_offset_of__fcPrefix_42(),
	RegexCompiler_t1714699756::get_offset_of__bmPrefix_43(),
	RegexCompiler_t1714699756::get_offset_of__anchors_44(),
	RegexCompiler_t1714699756::get_offset_of__labels_45(),
	RegexCompiler_t1714699756::get_offset_of__notes_46(),
	RegexCompiler_t1714699756::get_offset_of__notecount_47(),
	RegexCompiler_t1714699756::get_offset_of__trackcount_48(),
	RegexCompiler_t1714699756::get_offset_of__backtrack_49(),
	RegexCompiler_t1714699756::get_offset_of__regexopcode_50(),
	RegexCompiler_t1714699756::get_offset_of__codepos_51(),
	RegexCompiler_t1714699756::get_offset_of__backpos_52(),
	RegexCompiler_t1714699756::get_offset_of__options_53(),
	RegexCompiler_t1714699756::get_offset_of__uniquenote_54(),
	RegexCompiler_t1714699756::get_offset_of__goto_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (BacktrackNote_t1775865058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[3] = 
{
	BacktrackNote_t1775865058::get_offset_of__codepos_0(),
	BacktrackNote_t1775865058::get_offset_of__flags_1(),
	BacktrackNote_t1775865058::get_offset_of__label_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (RegexLWCGCompiler_t1552814047), -1, sizeof(RegexLWCGCompiler_t1552814047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[2] = 
{
	RegexLWCGCompiler_t1552814047_StaticFields::get_offset_of__regexCount_56(),
	RegexLWCGCompiler_t1552814047_StaticFields::get_offset_of__paramTypes_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (RegexFCD_t150416822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[7] = 
{
	RegexFCD_t150416822::get_offset_of__intStack_0(),
	RegexFCD_t150416822::get_offset_of__intDepth_1(),
	RegexFCD_t150416822::get_offset_of__fcStack_2(),
	RegexFCD_t150416822::get_offset_of__fcDepth_3(),
	RegexFCD_t150416822::get_offset_of__skipAllChildren_4(),
	RegexFCD_t150416822::get_offset_of__skipchild_5(),
	RegexFCD_t150416822::get_offset_of__failed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (RegexFC_t2009854258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[3] = 
{
	RegexFC_t2009854258::get_offset_of__cc_0(),
	RegexFC_t2009854258::get_offset_of__nullable_1(),
	RegexFC_t2009854258::get_offset_of__caseInsensitive_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (RegexPrefix_t1013837165), -1, sizeof(RegexPrefix_t1013837165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[3] = 
{
	RegexPrefix_t1013837165::get_offset_of__prefix_0(),
	RegexPrefix_t1013837165::get_offset_of__caseInsensitive_1(),
	RegexPrefix_t1013837165_StaticFields::get_offset_of__empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (Group_t3761430853), -1, sizeof(Group_t3761430853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2433[3] = 
{
	Group_t3761430853_StaticFields::get_offset_of__emptygroup_3(),
	Group_t3761430853::get_offset_of__caps_4(),
	Group_t3761430853::get_offset_of__capcount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (GroupCollection_t939014605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[3] = 
{
	GroupCollection_t939014605::get_offset_of__match_0(),
	GroupCollection_t939014605::get_offset_of__captureMap_1(),
	GroupCollection_t939014605::get_offset_of__groups_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (GroupEnumerator_t3864870211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	GroupEnumerator_t3864870211::get_offset_of__rgc_0(),
	GroupEnumerator_t3864870211::get_offset_of__curindex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (RegexInterpreter_t884291495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[11] = 
{
	RegexInterpreter_t884291495::get_offset_of_runoperator_18(),
	RegexInterpreter_t884291495::get_offset_of_runcodes_19(),
	RegexInterpreter_t884291495::get_offset_of_runcodepos_20(),
	RegexInterpreter_t884291495::get_offset_of_runstrings_21(),
	RegexInterpreter_t884291495::get_offset_of_runcode_22(),
	RegexInterpreter_t884291495::get_offset_of_runfcPrefix_23(),
	RegexInterpreter_t884291495::get_offset_of_runbmPrefix_24(),
	RegexInterpreter_t884291495::get_offset_of_runanchors_25(),
	RegexInterpreter_t884291495::get_offset_of_runrtl_26(),
	RegexInterpreter_t884291495::get_offset_of_runci_27(),
	RegexInterpreter_t884291495::get_offset_of_runculture_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (Match_t3164245899), -1, sizeof(Match_t3164245899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2437[10] = 
{
	Match_t3164245899_StaticFields::get_offset_of__empty_6(),
	Match_t3164245899::get_offset_of__groupcoll_7(),
	Match_t3164245899::get_offset_of__regex_8(),
	Match_t3164245899::get_offset_of__textbeg_9(),
	Match_t3164245899::get_offset_of__textpos_10(),
	Match_t3164245899::get_offset_of__textend_11(),
	Match_t3164245899::get_offset_of__textstart_12(),
	Match_t3164245899::get_offset_of__matches_13(),
	Match_t3164245899::get_offset_of__matchcount_14(),
	Match_t3164245899::get_offset_of__balancing_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (MatchSparse_t4194398671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	MatchSparse_t4194398671::get_offset_of__caps_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (MatchCollection_t3718216671), -1, sizeof(MatchCollection_t3718216671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2439[9] = 
{
	MatchCollection_t3718216671::get_offset_of__regex_0(),
	MatchCollection_t3718216671::get_offset_of__matches_1(),
	MatchCollection_t3718216671::get_offset_of__done_2(),
	MatchCollection_t3718216671::get_offset_of__input_3(),
	MatchCollection_t3718216671::get_offset_of__beginning_4(),
	MatchCollection_t3718216671::get_offset_of__length_5(),
	MatchCollection_t3718216671::get_offset_of__startat_6(),
	MatchCollection_t3718216671::get_offset_of__prevlen_7(),
	MatchCollection_t3718216671_StaticFields::get_offset_of_infinite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (MatchEnumerator_t3726517973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	MatchEnumerator_t3726517973::get_offset_of__matchcoll_0(),
	MatchEnumerator_t3726517973::get_offset_of__match_1(),
	MatchEnumerator_t3726517973::get_offset_of__curindex_2(),
	MatchEnumerator_t3726517973::get_offset_of__done_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (RegexMatchTimeoutException_t197735700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[3] = 
{
	RegexMatchTimeoutException_t197735700::get_offset_of_regexInput_16(),
	RegexMatchTimeoutException_t197735700::get_offset_of_regexPattern_17(),
	RegexMatchTimeoutException_t197735700::get_offset_of_matchTimeout_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (RegexNode_t2469392321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[8] = 
{
	RegexNode_t2469392321::get_offset_of__type_0(),
	RegexNode_t2469392321::get_offset_of__children_1(),
	RegexNode_t2469392321::get_offset_of__str_2(),
	RegexNode_t2469392321::get_offset_of__ch_3(),
	RegexNode_t2469392321::get_offset_of__m_4(),
	RegexNode_t2469392321::get_offset_of__n_5(),
	RegexNode_t2469392321::get_offset_of__options_6(),
	RegexNode_t2469392321::get_offset_of__next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (RegexOptions_t2418259727)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[11] = 
{
	RegexOptions_t2418259727::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (RegexParser_t5944516), -1, sizeof(RegexParser_t5944516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[20] = 
{
	RegexParser_t5944516::get_offset_of__stack_0(),
	RegexParser_t5944516::get_offset_of__group_1(),
	RegexParser_t5944516::get_offset_of__alternation_2(),
	RegexParser_t5944516::get_offset_of__concatenation_3(),
	RegexParser_t5944516::get_offset_of__unit_4(),
	RegexParser_t5944516::get_offset_of__pattern_5(),
	RegexParser_t5944516::get_offset_of__currentPos_6(),
	RegexParser_t5944516::get_offset_of__culture_7(),
	RegexParser_t5944516::get_offset_of__autocap_8(),
	RegexParser_t5944516::get_offset_of__capcount_9(),
	RegexParser_t5944516::get_offset_of__captop_10(),
	RegexParser_t5944516::get_offset_of__capsize_11(),
	RegexParser_t5944516::get_offset_of__caps_12(),
	RegexParser_t5944516::get_offset_of__capnames_13(),
	RegexParser_t5944516::get_offset_of__capnumlist_14(),
	RegexParser_t5944516::get_offset_of__capnamelist_15(),
	RegexParser_t5944516::get_offset_of__options_16(),
	RegexParser_t5944516::get_offset_of__optionsStack_17(),
	RegexParser_t5944516::get_offset_of__ignoreNextParen_18(),
	RegexParser_t5944516_StaticFields::get_offset_of__category_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (RegexRunner_t3983612747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[18] = 
{
	RegexRunner_t3983612747::get_offset_of_runtextbeg_0(),
	RegexRunner_t3983612747::get_offset_of_runtextend_1(),
	RegexRunner_t3983612747::get_offset_of_runtextstart_2(),
	RegexRunner_t3983612747::get_offset_of_runtext_3(),
	RegexRunner_t3983612747::get_offset_of_runtextpos_4(),
	RegexRunner_t3983612747::get_offset_of_runtrack_5(),
	RegexRunner_t3983612747::get_offset_of_runtrackpos_6(),
	RegexRunner_t3983612747::get_offset_of_runstack_7(),
	RegexRunner_t3983612747::get_offset_of_runstackpos_8(),
	RegexRunner_t3983612747::get_offset_of_runcrawl_9(),
	RegexRunner_t3983612747::get_offset_of_runcrawlpos_10(),
	RegexRunner_t3983612747::get_offset_of_runtrackcount_11(),
	RegexRunner_t3983612747::get_offset_of_runmatch_12(),
	RegexRunner_t3983612747::get_offset_of_runregex_13(),
	RegexRunner_t3983612747::get_offset_of_timeout_14(),
	RegexRunner_t3983612747::get_offset_of_ignoreTimeout_15(),
	RegexRunner_t3983612747::get_offset_of_timeoutOccursAt_16(),
	RegexRunner_t3983612747::get_offset_of_timeoutChecksToSkip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (RegexRunnerFactory_t3902733837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (RegexTree_t3175204897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[7] = 
{
	RegexTree_t3175204897::get_offset_of__root_0(),
	RegexTree_t3175204897::get_offset_of__caps_1(),
	RegexTree_t3175204897::get_offset_of__capnumlist_2(),
	RegexTree_t3175204897::get_offset_of__capnames_3(),
	RegexTree_t3175204897::get_offset_of__capslist_4(),
	RegexTree_t3175204897::get_offset_of__options_5(),
	RegexTree_t3175204897::get_offset_of__captop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (RegexWriter_t536390172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[10] = 
{
	RegexWriter_t536390172::get_offset_of__intStack_0(),
	RegexWriter_t536390172::get_offset_of__depth_1(),
	RegexWriter_t536390172::get_offset_of__emitted_2(),
	RegexWriter_t536390172::get_offset_of__curpos_3(),
	RegexWriter_t536390172::get_offset_of__stringhash_4(),
	RegexWriter_t536390172::get_offset_of__stringtable_5(),
	RegexWriter_t536390172::get_offset_of__counting_6(),
	RegexWriter_t536390172::get_offset_of__count_7(),
	RegexWriter_t536390172::get_offset_of__trackcount_8(),
	RegexWriter_t536390172::get_offset_of__caps_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (CompiledRegexRunner_t592995170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[3] = 
{
	CompiledRegexRunner_t592995170::get_offset_of_goMethod_18(),
	CompiledRegexRunner_t592995170::get_offset_of_findFirstCharMethod_19(),
	CompiledRegexRunner_t592995170::get_offset_of_initTrackCountMethod_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (NoParamDelegate_t2890182105), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (FindFirstCharDelegate_t4203859986), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (CompiledRegexRunnerFactory_t3379954222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[3] = 
{
	CompiledRegexRunnerFactory_t3379954222::get_offset_of_goMethod_0(),
	CompiledRegexRunnerFactory_t3379954222::get_offset_of_findFirstCharMethod_1(),
	CompiledRegexRunnerFactory_t3379954222::get_offset_of_initTrackCountMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (Authorization_t1602399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[3] = 
{
	Authorization_t1602399::get_offset_of_m_Message_0(),
	Authorization_t1602399::get_offset_of_m_Complete_1(),
	Authorization_t1602399::get_offset_of_ModuleAuthenticationType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (CredentialCache_t1992799279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[3] = 
{
	CredentialCache_t1992799279::get_offset_of_cache_0(),
	CredentialCache_t1992799279::get_offset_of_cacheForHosts_1(),
	CredentialCache_t1992799279::get_offset_of_m_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (CredentialEnumerator_t3600195683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[4] = 
{
	CredentialEnumerator_t3600195683::get_offset_of_m_cache_0(),
	CredentialEnumerator_t3600195683::get_offset_of_m_array_1(),
	CredentialEnumerator_t3600195683::get_offset_of_m_index_2(),
	CredentialEnumerator_t3600195683::get_offset_of_m_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (SystemNetworkCredential_t677084018), -1, sizeof(SystemNetworkCredential_t677084018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[1] = 
{
	SystemNetworkCredential_t677084018_StaticFields::get_offset_of_defaultCredential_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (CredentialKey_t4100643814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[5] = 
{
	CredentialKey_t4100643814::get_offset_of_UriPrefix_0(),
	CredentialKey_t4100643814::get_offset_of_UriPrefixLength_1(),
	CredentialKey_t4100643814::get_offset_of_AuthenticationType_2(),
	CredentialKey_t4100643814::get_offset_of_m_HashCode_3(),
	CredentialKey_t4100643814::get_offset_of_m_ComputedHashCode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (EndPoint_t4156119363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (FtpStatusCode_t1448112771)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2459[38] = 
{
	FtpStatusCode_t1448112771::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (GlobalProxySelection_t2251180943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (HttpStatusCode_t1898409641)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2461[48] = 
{
	HttpStatusCode_t1898409641::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (HttpVersion_t1276659706), -1, sizeof(HttpVersion_t1276659706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[2] = 
{
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2467[13] = 
{
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_0(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_1(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_2(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_3(),
	IPAddress_t1399971723::get_offset_of_m_Address_4(),
	IPAddress_t1399971723::get_offset_of_m_ToString_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_8(),
	IPAddress_t1399971723::get_offset_of_m_Family_9(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_10(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_11(),
	IPAddress_t1399971723::get_offset_of_m_HashCode_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (IPEndPoint_t2615413766), -1, sizeof(IPEndPoint_t2615413766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2468[4] = 
{
	IPEndPoint_t2615413766::get_offset_of_m_Address_0(),
	IPEndPoint_t2615413766::get_offset_of_m_Port_1(),
	IPEndPoint_t2615413766_StaticFields::get_offset_of_Any_2(),
	IPEndPoint_t2615413766_StaticFields::get_offset_of_IPv6Any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[4] = 
{
	IPHostEntry_t994738509::get_offset_of_hostName_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_addressList_2(),
	IPHostEntry_t994738509::get_offset_of_isTrustedHost_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (InternalException_t1919254860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (NclUtilities_t1950342895), -1, sizeof(NclUtilities_t1950342895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2472[3] = 
{
	NclUtilities_t1950342895_StaticFields::get_offset_of__LocalAddresses_0(),
	NclUtilities_t1950342895_StaticFields::get_offset_of__LocalAddressesLock_1(),
	NclUtilities_t1950342895_StaticFields::get_offset_of__LocalDomainName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (ValidationHelper_t2126475125), -1, sizeof(ValidationHelper_t2126475125_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2473[3] = 
{
	ValidationHelper_t2126475125_StaticFields::get_offset_of_EmptyArray_0(),
	ValidationHelper_t2126475125_StaticFields::get_offset_of_InvalidMethodChars_1(),
	ValidationHelper_t2126475125_StaticFields::get_offset_of_InvalidParamChars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (ExceptionHelper_t2752294609), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (WebRequestPrefixElement_t283218055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[3] = 
{
	WebRequestPrefixElement_t283218055::get_offset_of_Prefix_0(),
	WebRequestPrefixElement_t283218055::get_offset_of_creator_1(),
	WebRequestPrefixElement_t283218055::get_offset_of_creatorType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (HttpContinueDelegate_t2713047268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (NetworkCredential_t1714133953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[3] = 
{
	NetworkCredential_t1714133953::get_offset_of_m_domain_0(),
	NetworkCredential_t1714133953::get_offset_of_m_userName_1(),
	NetworkCredential_t1714133953::get_offset_of_m_password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (ProtocolViolationException_t4263317570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (SecurityProtocolType_t3099771628)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2479[5] = 
{
	SecurityProtocolType_t3099771628::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (ServerCertValidationCallback_t2774612835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[2] = 
{
	ServerCertValidationCallback_t2774612835::get_offset_of_m_ValidationCallback_0(),
	ServerCertValidationCallback_t2774612835::get_offset_of_m_Context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (CallbackContext_t3155372874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[5] = 
{
	CallbackContext_t3155372874::get_offset_of_request_0(),
	CallbackContext_t3155372874::get_offset_of_certificate_1(),
	CallbackContext_t3155372874::get_offset_of_chain_2(),
	CallbackContext_t3155372874::get_offset_of_sslPolicyErrors_3(),
	CallbackContext_t3155372874::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (SocketAddress_t838303055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[4] = 
{
	SocketAddress_t838303055::get_offset_of_m_Size_0(),
	SocketAddress_t838303055::get_offset_of_m_Buffer_1(),
	SocketAddress_t838303055::get_offset_of_m_changed_2(),
	SocketAddress_t838303055::get_offset_of_m_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (WebException_t3368933679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[3] = 
{
	WebException_t3368933679::get_offset_of_m_Status_16(),
	WebException_t3368933679::get_offset_of_m_Response_17(),
	WebException_t3368933679::get_offset_of_m_InternalStatus_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (WebExceptionInternalStatus_t1357294310)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2484[5] = 
{
	WebExceptionInternalStatus_t1357294310::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (WebExceptionStatus_t1169373531)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2485[22] = 
{
	WebExceptionStatus_t1169373531::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (WebExceptionMapping_t159714763), -1, sizeof(WebExceptionMapping_t159714763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2486[1] = 
{
	WebExceptionMapping_t159714763_StaticFields::get_offset_of_s_Mapping_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (WebHeaderCollectionType_t1212469221)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2487[12] = 
{
	WebHeaderCollectionType_t1212469221::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (WebHeaderCollection_t3028142837), -1, sizeof(WebHeaderCollection_t3028142837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2488[9] = 
{
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_HInfo_12(),
	WebHeaderCollection_t3028142837::get_offset_of_m_CommonHeaders_13(),
	WebHeaderCollection_t3028142837::get_offset_of_m_NumCommonHeaders_14(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_s_CommonHeaderNames_15(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_s_CommonHeaderHints_16(),
	WebHeaderCollection_t3028142837::get_offset_of_m_InnerCollection_17(),
	WebHeaderCollection_t3028142837::get_offset_of_m_Type_18(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_HttpTrimCharacters_19(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_RfcCharMap_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (RfcChar_t1416622761)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2489[9] = 
{
	RfcChar_t1416622761::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (CaseInsensitiveAscii_t4138584646), -1, sizeof(CaseInsensitiveAscii_t4138584646_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2490[2] = 
{
	CaseInsensitiveAscii_t4138584646_StaticFields::get_offset_of_StaticInstance_0(),
	CaseInsensitiveAscii_t4138584646_StaticFields::get_offset_of_AsciiToLower_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (WebRequest_t1365124353), -1, sizeof(WebRequest_t1365124353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2491[11] = 
{
	WebRequest_t1365124353_StaticFields::get_offset_of_s_PrefixList_1(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_InternalSyncObject_2(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_DefaultTimerQueue_3(),
	WebRequest_t1365124353::get_offset_of_m_AuthenticationLevel_4(),
	WebRequest_t1365124353::get_offset_of_m_ImpersonationLevel_5(),
	WebRequest_t1365124353::get_offset_of_m_CachePolicy_6(),
	WebRequest_t1365124353::get_offset_of_m_CacheProtocol_7(),
	WebRequest_t1365124353::get_offset_of_m_CacheBinding_8(),
	WebRequest_t1365124353_StaticFields::get_offset_of_webRequestCreate_9(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_DefaultWebProxy_10(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_DefaultWebProxyInitialized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (DesignerWebRequestCreate_t3086960480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (WebProxyWrapperOpaque_t1012624580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[1] = 
{
	WebProxyWrapperOpaque_t1012624580::get_offset_of_webProxy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (WebProxyWrapper_t3016229293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (WebResponse_t1895226051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (HeaderParser_t3706119548), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (HeaderInfo_t3044570949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[5] = 
{
	HeaderInfo_t3044570949::get_offset_of_IsRequestRestricted_0(),
	HeaderInfo_t3044570949::get_offset_of_IsResponseRestricted_1(),
	HeaderInfo_t3044570949::get_offset_of_Parser_2(),
	HeaderInfo_t3044570949::get_offset_of_HeaderName_3(),
	HeaderInfo_t3044570949::get_offset_of_AllowMultiValues_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (HeaderInfoTable_t2462863175), -1, sizeof(HeaderInfoTable_t2462863175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2498[4] = 
{
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_HeaderHashTable_0(),
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_UnknownHeaderInfo_1(),
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_SingleParser_2(),
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_MultiParser_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (CloseExState_t4185113936)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2499[4] = 
{
	CloseExState_t4185113936::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
